#define AnalysisBase_cxx
#include "AnalysisBase.h"
#include <TH1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "TLorentzVector.h"
#include <algorithm>    // std::sort

#if ROOT_VERSION_CODE >= ROOT_VERSION(6,14,0)
#include <ROOT/RDataFrame.hxx>
#else
#include <ROOT/TDataFrame.hxx>
#endif

void AnalysisBase::Loop()
{


  if (fChain == 0) return;

  if (DEBUG) cout << "In Loop()..." << endl;


  const double Zmass = 91.2; 
  const double GeV   = 1000.0;
  
  //create histograms:
  if (DEBUG) cout << "Creating histograms..." << endl;


  //theDSID=theDSID;//+"-DF";

  //histo.BookHFTree(theDSID); //so yeah we dont need Histograms.*
    histo = new HistFitterTree( theDSID, "/mnt/lustre/scratch/epp_test/ft81/outputs_mcx_JES30/"+systName );
  //  histo = new HistFitterTree( theDSID, "outputs_LowHT/"+systName );
  //  histo = new HistFitterTree( theDSID, "outputs_HighHT/"+systName );



  cout << "tree: " <<theDSID << endl;
  Long64_t nentries = fChain->GetEntriesFast();
  cout << "nentries = " << nentries << endl;
  Long64_t nbytes = 0, nb = 0;
  //vector<int> theRun;
  for (Long64_t jentry=0; jentry<nentries;jentry++) {
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    // if (Cut(ientry) < 0) continue;
    if(jentry%10000==0) cout << "jentry = " << jentry << endl;
    
    
    //cout << pass3lcut << endl;
    //---------------------------------------------------------------------------------------------------- event selection 
    //if(!pass2lcut) continue;
    if(!pass3lcut) continue;
    //      if(!(pass3lcut && nJets==0)) continue;
  //      if(!(pass3lcut && nJets > 0 && ht < 200)) continue;
    // if(!(pass3lcut && nJets > 0 && ht > 200)) continue;

    //cout << EventNumber << endl;

    //trigger requirement
    //if(!Pass2ElTrigger && !Pass2MuTrigger && !PassElMuTrigger) continue;
    
    //cout << "pass3lcut" << endl;
 
    met = eT_miss;
    metsig = met_Sig;
    htJ = ht;
   

    
    //-------------------------------------------------- WEIGHTS
    normWeight = 1.0;
    //setup the weight
    if(isMC) {
      //cout << "MC weight" << endl;
      //weight=elecSF*muonSF*btagSFCentral*EventWeight*pileupweight*sherpaNjetsWeight;
      //int isTrue = (LepTruth->at(0)==1 && LepTruth->at(1)==1 && LepTruth->at(2)==1);
      int isTrue = 1;
      weight=elecSF*muonSF*EventWeight*jvtSF*pileupweight*btagSFCentral;
      BtagWeight=btagSFCentral;
      PileUpWeight=pileupweight;
      JVTsf = jvtSF;
      Elec_SF = elecSF;
      Muon_SF = muonSF;
      GenWeight = EventWeight*isTrue;
      //cout << "weight: " << weight << endl;
      //cout << elecIDSF << ", " << muonIDSF << endl;
      //cout << btagSFCentral << endl;
      //cout << EventWeight << endl;
      //cout << pileupweight << " , " << sherpaNjetsWeight << endl;
      //cout << elecSF*muonSF*btagSFCentral*EventWeight << endl;
      //N=L x sigma
      //Sample lumi:
      //if(jentry==1) cout << "TotalEvent " << nEventsTotal << endl;
      //cout << XSecWeight << endl;
      //double lumiSample = nEventsTotal/XSecWeight;
      double lumiSample = 1./XSecWeight; //this is already taken as a weight
      // XSecWeight has scaled by targeted lumi
      //normWeight = luminosity/lumiSample;
      normWeight = 1./lumiSample;
      //cout << luminosity << " , " << lumiSample << endl;
      //cout << " normWeight -----------------------------> " << normWeight << endl;
    } else if(isSignal) {
      weight=elecSF*muonSF*EventWeight*jvtSF*pileupweight*btagSFCentral;//still missing pileup and sherpaNjets ---CHANGE THIS
      BtagWeight=btagSFCentral;
      PileUpWeight=pileupweight;
      JVTsf = jvtSF;
      Elec_SF = elecSF;
      Muon_SF = muonSF;
      GenWeight = EventWeight;
      //cout << elecSF << ", " << muonSF << endl;
      //cout << m_3lepweight << endl;
      //cout << btagSFCentral << endl;
      //cout << EventWeight << endl;
      //float lumisample = 1./m_3lepweight;
      float lumisample = 1./xsecWeight;
      //normWeight = luminosity/lumisample;
      normWeight = 1./lumisample;
      //cout << " normWeight -----------------------------> " << normWeight << endl;
    }
    else {
      weight=1.0;
      normWeight=1.0;
    }
    // cout << "Norm Weight: " << normWeight << " XSecWeight " << XSecWeight << " luminosity " << luminosity << endl;
    //cout << weight << endl;
    weight*=normWeight;
    //cout << weight << endl;
    
    //if(DEBUG) cout << "elecSF " << elecSF << " muonSF " << muonSF << " btagSFCentral "<< btagSFCentral << " EventWeight " << EventWeight << " normWeight " << normWeight << endl;
    


    //---------------------------------------------------------------------------------------------------- 3L selection
    //get jets sorted
  if(!isFake){  
    int Njets = (int)JetPt->size();
    vector<pair<TLorentzVector,int> > myJets;
    //cout << "nJets: " << Njets << endl; 
    
    //cout << "NO FAKE SAMPLE" << endl;
    
    for(unsigned int ijet=0; ijet<JetPt->size(); ijet++) {
      pair<TLorentzVector,int> temp;
      TLorentzVector tlv_temp;
      //cout << "in jet loop" << endl;
      tlv_temp.SetPtEtaPhiM(JetPt->at(ijet),JetEta->at(ijet),0.0,0.0);
      //tlv_temp.SetPtEtaPhiM(JetPt->at(ilep),JetEta->at(ilep),JetPhi->at(ilep),0.0);
      //cout << "jet vector done" << endl;
      temp.first = tlv_temp;
      myJets.push_back(temp);
    }
    sort(myJets.begin(),myJets.end(),SortTLV);

    //

    //end of jet crap
    //cout << "nJets" << endl;
    
    int nSignalJets   = 0;
    int nBtag         = 0;
    int nCentral      = 0;
    nSignalJets = nJets;
    /*
    //Categorizing jets
    for(int ijet=0; ijet<Njets; ijet++) {
      //     if(fabs(JetEta->at(ijet))<2.8 && JetPt->at(ijet)>20.00 ) {
        nSignalJets+=1;
	//    }
    }//end loop on jets
    */    
   // nSignalJets=Njets;

    // don't undrestand this
    //
    /*
    if (isWZ==true){
      if (hasjets==false && nCentral>0)  continue;
      if (hasjets==true  && nCentral<1)  continue;
    }
    //
    //cout <<nSignalJets << endl;
    */

    //--lepton part      
    vector<pair<TLorentzVector,int> > myLeptons;
    pair<TLorentzVector,int> temp;
    
    for(unsigned int ilep=0; ilep<LepPt->size(); ilep++) {
      
      pair<TLorentzVector,int> temp;
      TLorentzVector tlv_temp;
      
      //tlv_temp.SetPtEtaPhiM(LepPt->at(ilep)/GeV,LepEta->at(ilep),LepPhi->at(ilep),0.0);
      tlv_temp.SetPtEtaPhiM(LepPt->at(ilep),LepEta->at(ilep),LepPhi->at(ilep),0.0);
      temp.first = tlv_temp;
      htL += (LepPt->at(ilep));
      temp.second = LepCharge->at(ilep) * LepPdgId->at(ilep); 
      //cout << LepCharge->at(ilep) * LepPdgId->at(ilep) << endl;
      //cout << LepCharge->at(ilep) << "  "<< LepPdgId->at(ilep) << endl;
      myLeptons.push_back(temp);
    }
    sort(myLeptons.begin(),myLeptons.end(),SortTLV);
    //end lepton part
    
    //           testing --------------------------------------------------------------------
    
    // taking info directly from sussex ntuples
    foundSFOS = pass2lossf; //checked
    mll=mll3lZ; //checked
    M3l = mlll; //checked
    mt = mTWZ; //checked
    nBtag = num_bjets; //can it be checked?
    pDF = passDFOS;
    masshiggs = mHiggs;
    deltarmin = dRnear;
    Dphi12 = dPhi12;
    p3lcut = pass3lcut;
    nSignalJets = nJets;    
    m_cleaning = Cleaning;
    m_nCombLep = nCombLep;

    //--------------SELECTION ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    //cout << foundSFOS << endl;
    
    //if(!foundSFOS) continue; //kill the event there is no SF-OS pair checked \/
    //if(mll<40.00) continue; //checked \/
    //cout << "\t\t" << foundSFOS << endl;

      //---------------------------
    //    bool writeHistFitterTree=false;
    //    writeHistFitterTree=true;

    histo->SR=30000; 



    float mev=1000.;
    
    //cout <<  EventNumber << endl;
    //cout << nBtag << endl;
    //cout << "three lep"<< endl;
    histo->nSigLep=3;
    histo->eventNumber=EventNumber;
    histo->runNumber=RunNumber;     
    histo->eventweight=weight; 
    histo->NormWeight=normWeight;
    histo->bTagWeight=BtagWeight;
    histo->pileupWeight=PileUpWeight;
    histo->JVTSF=JVTsf;
    histo->ElecSF=Elec_SF;
    histo->MuonSF=Muon_SF;
    histo->EvtWeight=GenWeight;  
    histo->cleaning = m_cleaning;
    histo->n_comblep = m_nCombLep; 
 
    //cout << "lep1 "<< myLeptons[0].first.Pt()*mev<<  " lep2 "<< myLeptons[1].first.Pt()*mev<<  " lep3 "<< myLeptons[2].first.Pt()*mev<<  endl;
    histo->LepPt0=myLeptons[0].first.Pt()*mev;  
    histo->LepPt1=myLeptons[1].first.Pt()*mev;  
    histo->LepPt2=myLeptons[2].first.Pt()*mev;  
    histo->LepEta0=myLeptons[0].first.Eta();    
    histo->LepEta1=myLeptons[1].first.Eta();
    histo->LepEta2=myLeptons[2].first.Eta();
    histo->LepPhi0=myLeptons[0].first.Phi();
    histo->LepPhi1=myLeptons[1].first.Phi();
    histo->LepPhi2=myLeptons[2].first.Phi();
    histo->LepCharge0=myLeptons[0].second;
    histo->LepCharge1=myLeptons[1].second;
    histo->LepCharge2=myLeptons[2].second;
    
    histo->passSFOS=foundSFOS;
    histo->mSFOS=mll*mev; 
    histo->MET=met*mev;     
    histo->Mt=mt*mev; 
    histo->HT=htJ*mev;
    histo->HTLep=(LepPt->at(0)+LepPt->at(1)+LepPt->at(2))*mev;
    histo->METSig=metsig;
    histo->Mlll=M3l*mev;    
    
    histo->pass3l=p3lcut;
    histo->passDF=pDF;
    histo->mhiggs=masshiggs*mev;
    histo->deltaRmin=deltarmin;
    histo->dphi12=dPhi12;

    //cout << nSignalJets << endl;
    histo->nbjets=nBtag;
    histo->njets=nSignalJets;
    histo->nCentralLightJets=nCentral;
    histo->JetPt0=myJets[0].first.Pt()*mev;
    
    //histo->SR=mySR;
    histo->passtrigger=(Pass2El24Trigger || Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger || Pass1ElTrigger || Pass1MuTrigger);
    histo->matchtrigger=(PassDiLepTriggerMatch);

    histo->syst_FT_EFF_B_down=bEffDownWeight ; 
    histo->syst_FT_EFF_B_up=bEffUpWeight ; 
    histo->syst_FT_EFF_C_down=cEffDownWeight ;
    histo->syst_FT_EFF_C_up=cEffUpWeight ; 
    histo->syst_FT_EFF_Light_down=lEffDownWeight ;
    histo->syst_FT_EFF_Light_up=lEffUpWeight ; 
    

    //cout << "down" << bEffDownWeight << ",nom: "<< weight << " ,up: "<< bEffUpWeight << endl;

    //    histo->syst_FT_EFF_extrapolation_down=extrapolationDownWeight ;
    //   histo->syst_FT_EFF_extrapolation_up=extrapolationUpWeight ; 
    histo->syst_FT_EFF_extrapolationFromCharm_down=FT_EFF_extrapolationFromCharm_DOWN;
    histo->syst_FT_EFF_extrapolationFromCharm_up=FT_EFF_extrapolationFromCharm_UP;

    histo->syst_jvtSF_up=jvtSFup;
    histo->syst_jvtSF_down=jvtSFdown;

    
    histo->syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down=elecSF_EFF_ID_Down ; 
    histo->syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up=elecSF_EFF_ID_Up ; 
    histo->syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down=elecSF_EFF_Iso_Down ; 
    histo->syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up=elecSF_EFF_Iso_Up ; 
    histo->syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down=elecSF_EFF_Reco_Down ;
    histo->syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up=elecSF_EFF_Reco_Up ; 
    /* SUSYTools gives error message for trigger SFs sys
    histo->syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down=elecSF_EFF_Trigger_Down ; 
    histo->syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up=elecSF_EFF_Trigger_Up ; 
    histo->syst_EL_EFF_TriggerEff_down= elecSF_EFF_TriggerEff_Down ; 
    histo->syst_EL_EFF_TriggerEff_up= elecSF_EFF_TriggerEff_Up ; 
    histo->syst_MUON_EFF_TrigStatUncertainty_down= muonSF_EFF_TrigStatUnc_Down ;
    histo->syst_MUON_EFF_TrigStatUncertainty_up=muonSF_EFF_TrigStatUnc_Up ; 
    histo->syst_MUON_EFF_TrigSystUncertainty_down=muonSF_EFF_TrigSystUnc_Down ; 
    histo->syst_MUON_EFF_TrigSystUncertainty_up=muonSF_EFF_TrigSystUnc_Up ; 
    */
    
    histo->syst_MUON_EFF_STAT_down=muonSF_EFF_STAT_Down ;
    histo->syst_MUON_EFF_STAT_up=muonSF_EFF_STAT_Up ; 
    histo->syst_MUON_EFF_SYS_down=muonSF_EFF_SYS_Down ;
    histo->syst_MUON_EFF_SYS_up=muonSF_EFF_SYS_Up ; 

    histo->syst_MUON_EFF_STAT_LOWPT_down=  muonSF_EFF_STAT_LOWPT_Down ; 
    histo->syst_MUON_EFF_STAT_LOWPT_up=  muonSF_EFF_STAT_LOWPT_Up ; 
    histo->syst_MUON_EFF_SYS_LOWPT_down=  muonSF_EFF_SYS_LOWPT_Down ;
    histo->syst_MUON_EFF_SYS_LOWPT_up=  muonSF_EFF_SYS_LOWPT_Up ; 

    histo->syst_MUON_ISO_STAT_down= muonSF_ISO_STAT_Down;
    histo->syst_MUON_ISO_STAT_up= muonSF_ISO_STAT_Up;
    histo->syst_MUON_ISO_SYS_down= muonSF_ISO_SYS_Down ; 
    histo->syst_MUON_ISO_SYS_up= muonSF_ISO_SYS_Up ; 
    histo->syst_MUON_TTVA_STAT_down=  muonSF_TTVA_STAT_Down ;
    histo->syst_MUON_TTVA_STAT_up=  muonSF_TTVA_STAT_Up ; 
    histo->syst_MUON_TTVA_SYS_down=  muonSF_TTVA_SYS_Down ;
    histo->syst_MUON_TTVA_SYS_up=  muonSF_TTVA_SYS_Up ; 
    
    } //end of MC or DATA loop, next is the Fake 


  else {

    int nSignalJets   = 0;
    int nBtag         = 0;
    int nCentral      = 0;
    
    GenWeight = EventWeight*FFWeight;     
    BtagWeight=btagSFCentral;
    PileUpWeight=pileupweight;
    JVTsf = jvtSF;
    Elec_SF = elecSF;
    Muon_SF = muonSF; 
    normWeight = XSecWeight;
    foundSFOS = pass2lossf; //checked
    mll=mll3lZ; //checked
    M3l = mlll; //checked
    mt = mTWZ; //checked
    nBtag = num_bjets; //can it be checked?
    pDF = passDFOS;
    masshiggs = mHiggs;
    deltarmin = dRnear;
    Dphi12 = dPhi12;
    p3lcut = pass3lcut;
    nSignalJets = nJets;
    m_cleaning = Cleaning;

    Sum2fakesyst=pow(systWelCorr,2)+pow(systWmuCorr,2)+pow(systWelUnc,2)+pow(systWmuUnc,2);
    Totalfakesyst=sqrt(Sum2fakesyst);

    FakeSystUP=weight+Totalfakesyst;
    FakeSystDOWN=weight-Totalfakesyst;

    //    bool writeHistFitterTree=false;
    //    writeHistFitterTree=true;

    histo->SR=30000;
    float mev=1000.;

    if(DEBUG) cout << "elecSF " << elecSF << " muonSF " << muonSF << " btagSFCentral "<< btagSFCentral << " GenWeight " << GenWeight << " normWeight " << normWeight << endl;

    histo->nSigLep=3;
    histo->eventNumber=EventNumber;
    histo->EvtWeight=GenWeight;
    histo->NormWeight=normWeight;
    histo->bTagWeight=BtagWeight;
    histo->pileupWeight=PileUpWeight;
    histo->JVTSF=JVTsf;
    histo->ElecSF=Elec_SF;
    histo->MuonSF=Muon_SF;
    if(m_cleaning==1) {
    	histo->cleaning = 1;
	}
    else {
	histo->cleaning = 0;
	}
    histo->n_comblep = nCombLep;
 
    //Fake systematic uncertainties
    histo->Fake_syst_UP=FakeSystUP;
    histo->Fake_syst_DOWN=FakeSystDOWN;


    histo->LepPt0=LepPt->at(0)*mev;
    histo->LepPt1=LepPt->at(1)*mev;
    histo->LepPt2=LepPt->at(2)*mev;

    histo->JetPt0=JetPt->at(0)*mev; 

    histo->passSFOS=foundSFOS;
    histo->mSFOS=mll*mev;
    histo->MET=met*mev;
    histo->Mt=mt*mev;
    histo->Mlll=M3l*mev;
    histo->HT=htJ*mev;
    histo->HTLep=(LepPt->at(0)+LepPt->at(1)+LepPt->at(2))*mev;
    histo->METSig=metsig;


    histo->pass3l=p3lcut;
    histo->passDF=pDF;
    histo->deltaRmin=deltarmin;
    //histo->dphi12=dPhi12;

    histo->nbjets=nBtag;
    histo->njets=nSignalJets; 
   
    histo->passtrigger=(Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger || Pass1ElTrigger || Pass1MuTrigger);
    int trigger = PassDiLepTriggerMatch;
    if(PassDiLepTriggerMatch==1){
	histo->matchtrigger=true;
    	}
    else {
	histo->matchtrigger=false;
	}
    //cout << "Trigger: " << trigger << endl;
    //---
    
    //add weight sys
    //histo->WriteTree();
    
    /*
      cout << "weight "<< weight << endl;
      cout << "lep1 "<< myLeptons[0].first.Pt()*mev<< ", eta "<< myLeptons[0].first.Eta() << ", phi "<< myLeptons[0].first.Phi() << ", q "<< myLeptons[0].second<< endl;
      cout << "msfos "<< mll*mev << endl;
      cout << "met "<< met*mev << endl; 
      cout << "mt "<< mt*mev << endl;   
      cout << "mlll "<< M3l*mev << endl;
    */
    
    }
    
    histo->WriteTree();
    
    //}//end 3-lepton analysis
  }//end loop on events
  
  delete histo;
}

//====================================================================================================
//====================================================================================================
bool AnalysisBase::SortTLV(const pair<TLorentzVector,int> lv1, const pair<TLorentzVector,int> lv2)
{
  return lv1.first.Pt() > lv2.first.Pt();
}

