#define HistFitterTree_cxx
#include "HistFitterTree.h"
#include "TParameter.h"

using namespace std;



HistFitterTree::HistFitterTree(TString MCID, TString syst, TString fileName, TString treeName) 
{
    if(fileName==""){
        fileName = syst+"_"+MCID+".root";
    }
    if(treeName==""){
        treeName = "id_" + MCID;
    }
    // Setup a TTree in a output file
    file = TFile::Open(fileName, "RECREATE");
    file->cd();
    //tree = new TTree("id_"+MCID, "id_"+MCID);
    tree = new TTree(treeName, treeName);
    //tree->SetAutoSave(10000000);
    TTree::SetBranchStyle(1);
    tree->SetDirectory(file);

    mymcid = MCID;
    // Set Branches in output tree
    tree->Branch("LepPt0",&LepPt0);
    tree->Branch("LepPt1",&LepPt1);
    tree->Branch("LepPt2",&LepPt2);
    tree->Branch("LepEta0",&LepEta0);
    tree->Branch("LepEta1",&LepEta1);
    tree->Branch("LepEta2",&LepEta2);
    tree->Branch("LepPhi0",&LepPhi0);
    tree->Branch("LepPhi1",&LepPhi1);
    tree->Branch("LepPhi2",&LepPhi2);
    tree->Branch("LepCharge0",&LepCharge0);
    tree->Branch("LepCharge1",&LepCharge1);
    tree->Branch("LepCharge2",&LepCharge2);
    tree->Branch("LepPdgId0",&LepPdgId0);   //0=e 1=mu 2=tau
    tree->Branch("LepPdgId1",&LepPdgId1);   //0=e 1=mu 2=tau
    tree->Branch("LepPdgId2",&LepPdgId2);   //0=e 1=mu 2=tau

    tree->Branch("JetPt0",&JetPt0);
    tree->Branch("JetPt1",&JetPt1);
    tree->Branch("JetPt2",&JetPt2);

    tree->Branch("MET",&MET);
    tree->Branch("METSig",&METSig);
    tree->Branch("HT",&HT);
    tree->Branch("HTLep",&HTLep);


    tree->Branch("njets",&njets); //20GeV is default
    tree->Branch("nCentralLightJets",&nCentralLightJets);
    tree->Branch("nbjets",&nbjets);
    
    tree->Branch("cleaning",&cleaning);
    tree->Branch("n_comblep",&n_comblep);
    tree->Branch("nSigLep",&nSigLep);
    tree->Branch("eventweight",&eventweight);
    tree->Branch("NormWeight", &NormWeight);
    tree->Branch("bTagWeight", &bTagWeight);
    tree->Branch("pileupWeight", &pileupWeight);
    tree->Branch("JVTSF", &JVTSF);
    tree->Branch("ElecSF", &ElecSF);
    tree->Branch("MuonSF", &MuonSF);
    tree->Branch("EvtWeight", &EvtWeight);
    tree->Branch("SR",&SR);
    tree->Branch("runNumber",&runNumber);
    tree->Branch("eventNumber",&eventNumber);

    //trigger business
    tree->Branch("passtrigger",&passtrigger);
    tree->Branch("matchtrigger",&matchtrigger);

    //three-lepton
    tree->Branch("passSFOS",&passSFOS);
    tree->Branch("Mlll",&Mlll);
    tree->Branch("mSFOS",&mSFOS); //mll sfos pair closest to Z mass
    tree->Branch("Mt",&Mt);
    tree->Branch("passDF",&passDF);
    tree->Branch("mhiggs",&mhiggs);
    tree->Branch("deltaRmin",&deltaRmin);
    tree->Branch("dphi12",&dphi12);
    tree->Branch("pass3l",&pass3l);

    //Fake syst
    tree->Branch("Fake_syst_DOWN", &Fake_syst_DOWN);
    tree->Branch("Fake_syst_UP", &Fake_syst_UP);
     

    //weight-based systematics
    
    tree->Branch("syst_FT_EFF_B_down",&syst_FT_EFF_B_down);
    tree->Branch("syst_FT_EFF_B_up",&syst_FT_EFF_B_up);
    tree->Branch("syst_FT_EFF_C_down",&syst_FT_EFF_C_down);
    tree->Branch("syst_FT_EFF_C_up",&syst_FT_EFF_C_up);
    tree->Branch("syst_FT_EFF_Light_down",&syst_FT_EFF_Light_down);
    tree->Branch("syst_FT_EFF_Light_up",&syst_FT_EFF_Light_up);
    
    //   tree->Branch("syst_FT_EFF_extrapolation_down",&syst_FT_EFF_extrapolation_down);
    //  tree->Branch("syst_FT_EFF_extrapolation_up",&syst_FT_EFF_extrapolation_up);
    tree->Branch("syst_FT_EFF_extrapolationFromCharm_down",&syst_FT_EFF_extrapolationFromCharm_down);
    tree->Branch("syst_FT_EFF_extrapolationFromCharm_up",&syst_FT_EFF_extrapolationFromCharm_up);

    tree->Branch("syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down",&syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down);
    tree->Branch("syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up",&syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up);
    tree->Branch("syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down",&syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down);
    tree->Branch("syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up",&syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up);
    tree->Branch("syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down",&syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down);
    tree->Branch("syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up",&syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up);
    //   tree->Branch("syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down",&syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down);
    //  tree->Branch("syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up",&syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up);
    
    tree->Branch("syst_MUON_EFF_STAT_down",&syst_MUON_EFF_STAT_down);
    tree->Branch("syst_MUON_EFF_STAT_up",&syst_MUON_EFF_STAT_up);
    tree->Branch("syst_MUON_EFF_SYS_down",&syst_MUON_EFF_SYS_down);
    tree->Branch("syst_MUON_EFF_SYS_up",&syst_MUON_EFF_SYS_up);
    //    tree->Branch("syst_MUON_EFF_TrigStatUncertainty_down",&syst_MUON_EFF_TrigStatUncertainty_down);
    //    tree->Branch("syst_MUON_EFF_TrigStatUncertainty_up",&syst_MUON_EFF_TrigStatUncertainty_up);
    //    tree->Branch("syst_MUON_EFF_TrigSystUncertainty_down",&syst_MUON_EFF_TrigSystUncertainty_down);
    //    tree->Branch("syst_MUON_EFF_TrigSystUncertainty_up",&syst_MUON_EFF_TrigSystUncertainty_up);
    tree->Branch("syst_MUON_ISO_STAT_down",&syst_MUON_ISO_STAT_down);
    tree->Branch("syst_MUON_ISO_STAT_up",&syst_MUON_ISO_STAT_up);
    tree->Branch("syst_MUON_ISO_SYS_down",&syst_MUON_ISO_SYS_down);
    tree->Branch("syst_MUON_ISO_SYS_up",&syst_MUON_ISO_SYS_up);

    tree->Branch("syst_EL_EFF_TriggerEff_down",&syst_EL_EFF_TriggerEff_down);
    tree->Branch("syst_EL_EFF_TriggerEff_up",&syst_EL_EFF_TriggerEff_up);
    tree->Branch("syst_MUON_EFF_STAT_LOWPT_down",&syst_MUON_EFF_STAT_LOWPT_down);
    tree->Branch("syst_MUON_EFF_STAT_LOWPT_up",&syst_MUON_EFF_STAT_LOWPT_up);
    tree->Branch("syst_MUON_EFF_SYS_LOWPT_down",&syst_MUON_EFF_SYS_LOWPT_down);
    tree->Branch("syst_MUON_EFF_SYS_LOWPT_up",&syst_MUON_EFF_SYS_LOWPT_up);
    tree->Branch("syst_MUON_TTVA_STAT_down",&syst_MUON_TTVA_STAT_down);
    tree->Branch("syst_MUON_TTVA_STAT_up",&syst_MUON_TTVA_STAT_up);
    tree->Branch("syst_MUON_TTVA_SYS_down",&syst_MUON_TTVA_SYS_down);
    tree->Branch("syst_MUON_TTVA_SYS_up",&syst_MUON_TTVA_SYS_up);
    tree->Branch("syst_jvtSF_up",&syst_jvtSF_up);
    tree->Branch("syst_jvtSF_down",&syst_jvtSF_down);


    //end of weight-based systematics




    
    ClearOutputBranches();
}


void HistFitterTree::ClearOutputBranches(void) 
{
    LepPt0=0.;
    LepPt1=0.;
    LepPt2=0.;
    LepEta0=0.;
    LepEta1=0.;
    LepEta2=0.;
    LepPhi0=0.;
    LepPhi1=0.;
    LepPhi2=0.;
    LepCharge0=0.;
    LepCharge1=0.;
    LepCharge2=0.;
    LepPdgId0=-1;
    LepPdgId1=-1;
    LepPdgId2=-1;

    JetPt0=0.;
    JetPt1=0.;
    JetPt2=0.;

    MET=-1.;
    METSig=-1;
    HT=-1.;
    HTLep=-1.;
    passSFOS=0;

    njets=0;
    nCentralLightJets=0;
    nbjets=0;
    cleaning=0;
    n_comblep=0;
    nSigLep=0;
    eventweight=1.;
    NormWeight=1.;
    bTagWeight=1.;
    pileupWeight=1.;
    JVTSF=1.;
    ElecSF=1.;
    MuonSF=1.;
    EvtWeight=1.;
    SR=0;
    runNumber=-1;
    eventNumber=-1;
    
    passtrigger=false;
    matchtrigger=false;
    //pass3lcut=false;

    //three-lepton
    Mlll=-1.;	
    mSFOS=-1.;	
    Mt=-1.;	

    passDF=-1;
    mhiggs=-1;
    deltaRmin=-1;
    dphi12=-1;
    pass3l=-1;

    Fake_syst_UP=0.;
    Fake_syst_DOWN=0.;
    

    //-----systematics
    syst_FT_EFF_B_down=1.;
    syst_FT_EFF_B_up=1.;
    syst_FT_EFF_C_down=1.;
    syst_FT_EFF_C_up=1.;
    syst_FT_EFF_Light_down=1.;
    syst_FT_EFF_Light_up=1.;
    
    //    syst_FT_EFF_extrapolation_down=1.;
    //    syst_FT_EFF_extrapolation_up=1.;
    syst_FT_EFF_extrapolationFromCharm_down=1.;
    syst_FT_EFF_extrapolationFromCharm_up=1.;

    syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down=1.;
    syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up=1.;
    syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down=1.;
    syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up=1.;
    syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down=1.;
    syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up=1.;
    syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down=1.;
    syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up=1.;
    
    syst_MUON_EFF_STAT_down=1.;
    syst_MUON_EFF_STAT_up=1.;
    syst_MUON_EFF_SYS_down=1.;
    syst_MUON_EFF_SYS_up=1.;
    syst_MUON_EFF_TrigStatUncertainty_down=1.;
    syst_MUON_EFF_TrigStatUncertainty_up=1.;
    syst_MUON_EFF_TrigSystUncertainty_down=1.;
    syst_MUON_EFF_TrigSystUncertainty_up=1.;
    syst_MUON_ISO_STAT_down=1.;
    syst_MUON_ISO_STAT_up=1.;
    syst_MUON_ISO_SYS_down=1.;
    syst_MUON_ISO_SYS_up=1.;


    syst_EL_EFF_TriggerEff_down=1.;
    syst_EL_EFF_TriggerEff_up=1.;
    
    syst_MUON_EFF_STAT_LOWPT_down=1.;
    syst_MUON_EFF_STAT_LOWPT_up=1.;
    syst_MUON_EFF_SYS_LOWPT_down=1.;
    syst_MUON_EFF_SYS_LOWPT_up=1.;
    
    syst_MUON_TTVA_STAT_down=1.;
    syst_MUON_TTVA_STAT_up=1.;
    syst_MUON_TTVA_SYS_down=1.;
    syst_MUON_TTVA_SYS_up=1.;
    
    syst_jvtSF_up=1.;
    syst_jvtSF_down=1.;
    
    
    
    
}





//-----------------------------------------------------------------------------------------------------------
HistFitterTree::~HistFitterTree()
{
    // Write out the output tree and close the output file
  //file->cd();
  //tree->SetDirectory(file);
  //tree->Write();
  //file->Write();
    //tree->SetDirectory(0);
    //delete tree;
  file->Write();
  file->Close();
  delete file;
}


void HistFitterTree::WriteTree()
{
    tree->Fill();
    //tree->Write();
    //file->Write();
    //file->Close();
    //ClearOutputBranches();
}




void HistFitterTree::setSumOfMcWeights(double sumOfMcWeights) 
{
    // Define histogram
    TH1D *sumwhist = new TH1D("sumOfMcWeights_"+mymcid,"sumOfMcWeights_"+mymcid,1,0.,1.);

    // Fill histogram
    sumwhist -> Fill( 0. , sumOfMcWeights ) ;

    // Write intLumi to file
    file->cd();
    sumwhist->SetDirectory(file);
    sumwhist->Write();
    sumwhist->SetDirectory(0);

    delete sumwhist;
}



