#include "MakeHFT/Tools.h"
#include <string>
#include <fstream>
#include <sstream>
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"
#include "TVectorD.h"

using namespace std;

#if ROOT_VERSION_CODE >= ROOT_VERSION(6,14,0)
#include <ROOT/RDataFrame.hxx>
#else
#include <ROOT/TDataFrame.hxx>
#endif

Tools::Tools() {

  m_METrel = 0.;
  m_mt2=0.;
  m_TransverseSphericity = 0.;
}


Double_t Tools::GetMETrel(vector<TLorentzVector> lepton, vector<TLorentzVector> jet, TLorentzVector met){

  Double_t metrelx = 0.;
  Double_t metrely = 0.;

  Double_t dphi_lep,dphi_jet;
  Double_t dphimin;

  dphimin=999;
  
  /// Loop over leptons
  for (std::vector<TLorentzVector>::iterator Iter  = lepton.begin();  // baseline
       Iter != lepton.end(); Iter++ ) {
    
    // Delta phi
    dphi_lep = fabs(met.DeltaPhi(*Iter));
    if(dphi_lep < dphimin) {
      dphimin  = dphi_lep;
    }
  } // end loop leptons

  /// Loop over jets
  for (std::vector<TLorentzVector>::iterator jetIter  = jet.begin();  // baseline
       jetIter != jet.end(); jetIter++ ) {
    
    // Delta phi
    dphi_jet = fabs(met.DeltaPhi(*jetIter));
    if(dphi_jet < dphimin) {
      dphimin  = dphi_jet;
    }
  } // end loop jets

  if( dphimin > TMath::Pi()/2 ) {
    metrelx = met.Px();
    metrely = met.Py();
  } else {
    metrelx = met.Px() * TMath::Sin(dphimin);
    metrely = met.Py() * TMath::Sin(dphimin);
  } // at least one object is within Pi/2
	   
  m_METrel = sqrt( pow(metrelx,2) + pow(metrely,2) );
  

  return m_METrel;
}

Double_t Tools::GetMt2(vector<TLorentzVector> lepton, TLorentzVector met) {

  Double_t lep1_px = 0.;
  Double_t lep2_px = 0.;
  Double_t lep1_py = 0.;
  Double_t lep2_py = 0.;

  lep1_px = lepton[0].Px();
  lep2_px = lepton[1].Px();
  lep1_py = lepton[0].Py();
  lep2_py = lepton[1].Py();

  /////// mT2 Standard Calculation //////
  //pa, pb = {mass, px, py}
  Double_t patr[3] = {0.,lep1_px,lep1_py};
  Double_t pbtr[3] = {0.,lep2_px,lep2_py};
  //pmiss  = {NULL, pxmiss, pymiss}
  double pmisstr[3] = { 0, met.Px(),met.Py()};
  mt2calculator.set_momenta(patr,pbtr,pmisstr);
  mt2calculator.set_mn(0);
  m_mt2 = mt2calculator.get_mt2(); 

  return m_mt2;
  
}

bool Tools::SortingPredicate(const TLorentzVector &lv1, const TLorentzVector &lv2){

  return lv1.Pt() > lv2.Pt();
}

Double_t Tools::calc_mct(TLorentzVector v1, TLorentzVector v2) {

  Double_t mct = (v1.Mt() + v2.Mt())*(v1.Mt() + v2.Mt()) - (v1-v2).Perp2();
  mct = (mct >= 0.) ? sqrt(mct) : sqrt(-mct);
  return mct;
}


Double_t Tools::ComputeTransverseSphericity(vector<TLorentzVector> leptons, vector<TLorentzVector> jets) {
  Double_t sumpx2, sumpy2,sumpxpy;
  sumpx2  = 0;
  sumpy2  = 0;
  sumpxpy = 0;

  //jets
  for (UInt_t ijet = 0; ijet<jets.size(); ++ijet) {
  
    sumpx2  += jets[ijet].Px()*jets[ijet].Px();
    sumpy2  += jets[ijet].Py()*jets[ijet].Py();
    sumpxpy += jets[ijet].Px()*jets[ijet].Py();   
    
  } // ijet

  //leptons
  for (UInt_t ilepton = 0; ilepton<leptons.size();++ilepton) {
    
      sumpx2  += leptons[ilepton].Px()*leptons[ilepton].Px();
      sumpy2  += leptons[ilepton].Py()*leptons[ilepton].Py();
      sumpxpy += leptons[ilepton].Px()*leptons[ilepton].Py();
    
  } // end loop over leptons
   
  m_TransverseSphericity = TranverseSphericity(sumpx2,sumpy2,sumpxpy);

  return m_TransverseSphericity;
	
} // ComputeTransverseSphericity   
	
Double_t Tools::TranverseSphericity(Double_t sumpx2, Double_t sumpy2, Double_t sumpxpy) {
 
  Double_t marray[4] = {0.,0.,0.,0.};
  
  marray[0]=sumpx2;
  marray[1]=sumpxpy;
  marray[2]=sumpxpy;
  marray[3]=sumpy2;
	
  // use root matrix to find eigenvalues...
  TMatrixDSym matrix(2); 
  matrix.SetMatrixArray(marray);
	 
  TMatrixDSymEigen eigen(matrix); 
  TVectorD E = eigen.GetEigenValues();   
	
  // from the babar sphericity code...
  Double_t lambda1 = 0;
  Double_t lambda2 = 0;
	
  if(E[0] < E[1]) {
    lambda1 = E[0];
    lambda2 = E[1];
  } else {
    lambda1 = E[1];
    lambda2 = E[0];
  }
	
  Double_t ST = 0;
  ST = 2*lambda1/( lambda1 + lambda2);
 
  return ST;
} // TranverseSphericity



TLorentzVector Tools::FindZ(vector<TLorentzVector> leptons, vector<int> charges){

  TLorentzVector Zboson;
  Zboson.SetPtEtaPhiM(0,0,0,0);
  float Zmass = 91.1876;



  for(int ilep=0; ilep<leptons.size(); ilep++) {
    for(int jlep=ilep+1; jlep<leptons.size(); jlep++) {

      int i_pdgId = fabs(charges[ilep]);
      int j_pdgId = fabs(charges[jlep]);
      if(i_pdgId!=j_pdgId) continue;

      if(charges[ilep]*charges[jlep]<0) {
	
	float mass = (leptons[ilep]+leptons[jlep]).M();
	float eta = (leptons[ilep]+leptons[jlep]).Eta();
	float pt = (leptons[ilep]+leptons[jlep]).Pt();
	float phi = (leptons[ilep]+leptons[jlep]).Phi();
	
	float massdif = fabs(Zmass-mass);
	float minVal=10000000;
	if(massdif<minVal) {
	  minVal=massdif;
	  Zboson.SetPtEtaPhiM(pt,eta,phi,mass);
	  
	}

      }
      
    }
    
  }

  return Zboson;

}
