// Andreas Petridis
// Andreas.Petridis@cern.ch
// Code for analyzing mini ntuples 

// C++ headers
#include <iostream>
#include <fstream>
#include <sstream>

// ROOT headers
#include "TTree.h"
#include "TBranch.h"
#include <TROOT.h>
#include <TChain.h>
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TSystem.h"
#include "TLorentzVector.h"
#include "TRandom2.h"
#include "TDirectory.h"
#include "TString.h"
// Analysis headers
#include "AnalysisBase.h"
#include "HistFitterTree.h"
//#include "Histograms.h"
using namespace std;


#if ROOT_VERSION_CODE >= ROOT_VERSION(6,14,0)
#include <ROOT/RDataFrame.hxx>
#else
#include <ROOT/TDataFrame.hxx>
#endif

// ---------------------------------------------------------------------Set systematics here --------------------------------------------------------------------------------------------------------------------

bool DEBUG=false;  // run file


//void PrintMSG(string message);

int main(int argc, char *argv[]) {

  //if (DEBUG) PrintMSG("In main() ...");

  // if(argv[1]==NULL) PrintMSG("No input file has been specified ...\n Correct way to run:\n ./susy input.root output.root DATA");
  // if(argv[2]==NULL) PrintMSG("No output file has been specified ...\n Correct way to run:\n ./susy input.root output.root DATA");
  // if(argv[3]==NULL) PrintMSG("You didn't specify the sample you'll run (MCBKG or MCSIGNAL or DATA) ...\n Correct way to run:\n ./susy input.root output.root DATA");

  //main arguments input file, output file and sampleKind="MCBKG" or "MCSIGNAL" or "DATA"
  std::string inputFile         = argv[1];
  std::string theDSID           = argv[2];  
  std::string SampleKind        = argv[3];


  
  
  vector<TString> treeNames;
  treeNames.push_back("HFntupleNONE"); //Nominal
  //
    
  if(SampleKind!="DATA" && SampleKind!="DD") {
    treeNames.push_back("HFntupleEG_SCALE_ALL__1down");
    treeNames.push_back("HFntupleEG_SCALE_ALL__1up");
    //treeNames.push_back("HFntupleJET_GroupedNP_1__1up");
    //treeNames.push_back("HFntupleJET_GroupedNP_1__1down");
    //treeNames.push_back("HFntupleJET_GroupedNP_2__1up");
    //treeNames.push_back("HFntupleJET_GroupedNP_2__1down");
    //treeNames.push_back("HFntupleJET_GroupedNP_3__1up");
    //treeNames.push_back("HFntupleJET_GroupedNP_3__1down");
    treeNames.push_back("HFntupleJET_JER_DataVsMC_MC16__1up");
    treeNames.push_back("HFntupleJET_JER_DataVsMC_MC16__1down");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_1__1up");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_1__1down");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_2__1up");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_2__1down");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_3__1up");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_3__1down");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_4__1up");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_4__1down");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_5__1up");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_5__1down");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_6__1up");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_6__1down");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_7restTerm__1up");
    treeNames.push_back("HFntupleJET_JER_EffectiveNP_7restTerm__1down");
    treeNames.push_back("HFntupleMET_SoftTrk_ResoPara");
    treeNames.push_back("HFntupleMET_SoftTrk_ResoPerp");
    treeNames.push_back("HFntupleMET_SoftTrk_ScaleDown");
    treeNames.push_back("HFntupleMET_SoftTrk_ScaleUp");
    treeNames.push_back("HFntupleMUON_ID__1down");
    treeNames.push_back("HFntupleMUON_ID__1up");
    treeNames.push_back("HFntupleMUON_MS__1down");
    treeNames.push_back("HFntupleMUON_MS__1up");
    treeNames.push_back("HFntupleMUON_SAGITTA_RESBIAS__1down");
    treeNames.push_back("HFntupleMUON_SAGITTA_RESBIAS__1up");
    treeNames.push_back("HFntupleMUON_SAGITTA_RHO__1down");
    treeNames.push_back("HFntupleMUON_SAGITTA_RHO__1up");
    treeNames.push_back("HFntupleMUON_SCALE__1down");
    treeNames.push_back("HFntupleMUON_SCALE__1up");
    treeNames.push_back("HFntupleJET_BJES_Response__1up");
    treeNames.push_back("HFntupleJET_BJES_Response__1down"); 
    treeNames.push_back("HFntupleJET_EffectiveNP_Detector1__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Detector1__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Detector2__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Detector2__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Mixed1__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Mixed1__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Mixed2__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Mixed2__1down");  
    treeNames.push_back("HFntupleJET_EffectiveNP_Mixed3__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Mixed3__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Modelling1__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Modelling1__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Modelling2__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Modelling2__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Modelling3__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Modelling3__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Modelling4__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Modelling4__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical1__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical1__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical2__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical2__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical3__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical3__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical4__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical4__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical5__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical5__1down");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical6__1up");
    treeNames.push_back("HFntupleJET_EffectiveNP_Statistical6__1down");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_Modelling__1up");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_Modelling__1down");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_NonClosure_2018data__1up");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_NonClosure_2018data__1down"); 
    treeNames.push_back("HFntupleJET_EtaIntercalibration_NonClosure_highE__1up");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_NonClosure_highE__1down");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_NonClosure_negEta__1up");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_NonClosure_negEta__1down");   
    treeNames.push_back("HFntupleJET_EtaIntercalibration_NonClosure_posEta__1up");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_NonClosure_posEta__1down");   
    treeNames.push_back("HFntupleJET_EtaIntercalibration_TotalStat__1up");
    treeNames.push_back("HFntupleJET_EtaIntercalibration_TotalStat__1down");
    treeNames.push_back("HFntupleJET_Flavor_Composition__1up");
    treeNames.push_back("HFntupleJET_Flavor_Composition__1down");    
    treeNames.push_back("HFntupleJET_Flavor_Response__1up");
    treeNames.push_back("HFntupleJET_Flavor_Response__1down");
    treeNames.push_back("HFntupleJET_Pileup_OffsetMu__1up");
    treeNames.push_back("HFntupleJET_Pileup_OffsetMu__1down");  
    treeNames.push_back("HFntupleJET_Pileup_OffsetNPV__1up");  
    treeNames.push_back("HFntupleJET_Pileup_OffsetNPV__1down");
    treeNames.push_back("HFntupleJET_Pileup_PtTerm__1up");  
    treeNames.push_back("HFntupleJET_Pileup_PtTerm__1down");
    treeNames.push_back("HFntupleJET_Pileup_RhoTopology__1up");  
    treeNames.push_back("HFntupleJET_Pileup_RhoTopology__1down");
    treeNames.push_back("HFntupleJET_SingleParticle_HighPt__1up");  
    treeNames.push_back("HFntupleJET_SingleParticle_HighPt__1down");
    treeNames.push_back("HFntupleJET_RelativeNonClosure_AFII__1up");
    treeNames.push_back("HFntupleJET_RelativeNonClosure_AFII__1down");
    treeNames.push_back("HFntupleJET_PunchThrough_AFII__1up");
    treeNames.push_back("HFntupleJET_PunchThrough_AFII__1down");
    treeNames.push_back("HFntupleJET_JER_DataVsMC_AFII__1up");
    treeNames.push_back("HFntupleJET_JER_DataVsMC_AFII__1down"); 
}		
  //
  

  // treeNames.push_back("EG_RESOLUTION_ALL__1down");
  // treeNames.push_back("EG_RESOLUTION_ALL__1up");
  // treeNames.push_back("EG_SCALE_ALL__1down");
  // treeNames.push_back("EG_SCALE_ALL__1up");
  // treeNames.push_back("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down");
  // treeNames.push_back("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up");
  // treeNames.push_back("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down");
  // treeNames.push_back("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up");
  // treeNames.push_back("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down");
  // treeNames.push_back("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up");
  // treeNames.push_back("FT_EFF_B_systematics__1down");
  // treeNames.push_back("FT_EFF_B_systematics__1up");
  // treeNames.push_back("FT_EFF_C_systematics__1down");
  // treeNames.push_back("FT_EFF_C_systematics__1up");
  // treeNames.push_back("FT_EFF_extrapolation__1down");
  // treeNames.push_back("FT_EFF_extrapolation__1up");
  // treeNames.push_back("FT_EFF_extrapolation_from_charm__1down");
  // treeNames.push_back("FT_EFF_extrapolation_from_charm__1up");
  // treeNames.push_back("FT_EFF_Light_systematics__1down");
  // treeNames.push_back("FT_EFF_Light_systematics__1up");
  // treeNames.push_back("JET_EtaIntercalibration_NonClosure__1up");
  // treeNames.push_back("JET_EtaIntercalibration_NonClosure__1down");
  // treeNames.push_back("JET_GroupedNP_1__1up");
  // treeNames.push_back("JET_GroupedNP_1__1down");
  // treeNames.push_back("JET_GroupedNP_2__1up");
  // treeNames.push_back("JET_GroupedNP_2__1down");
  // treeNames.push_back("JET_GroupedNP_3__1up");
  // treeNames.push_back("JET_GroupedNP_3__1down");
  // treeNames.push_back("JET_JER_SINGLE_NP__1up");
  // treeNames.push_back("JvtEfficiencyDown");
  // treeNames.push_back("JvtEfficiencyUp");
  // treeNames.push_back("JET_RelativeNonClosure_AFII__1up");
  // treeNames.push_back("JET_RelativeNonClosure_AFII__1down");
  // treeNames.push_back("JET_Rtrk_Baseline_Kin__1up");
  // treeNames.push_back("JET_Rtrk_Baseline_Kin__1down");
  // treeNames.push_back("JET_Rtrk_Baseline_Sub__1up");
  // treeNames.push_back("JET_Rtrk_Baseline_Sub__1down");
  // treeNames.push_back("JET_Rtrk_Modelling_Kin__1up");
  // treeNames.push_back("JET_Rtrk_Modelling_Kin__1down");
  // treeNames.push_back("JET_Rtrk_Modelling_Sub__1up");
  // treeNames.push_back("JET_Rtrk_Modelling_Sub__1down");
  // treeNames.push_back("JET_Rtrk_TotalStat_Kin__1up");
  // treeNames.push_back("JET_Rtrk_TotalStat_Kin__1down");
  // treeNames.push_back("JET_Rtrk_TotalStat_Sub__1up");
  // treeNames.push_back("JET_Rtrk_TotalStat_Sub__1down");
  // treeNames.push_back("JET_Rtrk_Tracking_Kin__1up");
  // treeNames.push_back("JET_Rtrk_Tracking_Kin__1down");
  // treeNames.push_back("JET_Rtrk_Tracking_Sub__1up");  
  // treeNames.push_back("JET_Rtrk_Tracking_Sub__1down");  
  // treeNames.push_back("MET_SoftTrk_ResoPara");
  // treeNames.push_back("MET_SoftTrk_ResoPerp");
  // treeNames.push_back("MET_SoftTrk_ScaleDown");
  // treeNames.push_back("MET_SoftTrk_ScaleUp");
  // treeNames.push_back("MUONS_ID__1down");
  // treeNames.push_back("MUONS_ID__1up");
  // treeNames.push_back("MUONS_MS__1down");
  // treeNames.push_back("MUONS_MS__1up");
  // treeNames.push_back("MUONS_SCALE__1down");
  // treeNames.push_back("MUONS_SCALE__1up");
  // treeNames.push_back("MUON_EFF_SYS__1down");
  // treeNames.push_back("MUON_EFF_SYS__1up");
  // treeNames.push_back("MUON_ISO_SYS__1down");
  // treeNames.push_back("MUON_ISO_SYS__1up");
  // treeNames.push_back("MUON_EFF_STAT__1down");
  // treeNames.push_back("MUON_EFF_STAT__1up");
  // treeNames.push_back("MUON_EFF_STAT_LOWPT__1down");
  // treeNames.push_back("MUON_EFF_STAT_LOWPT__1up");
  // treeNames.push_back("MUON_EFF_SYS__1down");
  // treeNames.push_back("MUON_EFF_SYS__1up");
  // treeNames.push_back("MUON_EFF_SYS_LOWPT__1down");
  // treeNames.push_back("MUON_EFF_SYS_LOWPT__1up");
  // treeNames.push_back("MUON_ISO_STAT__1down");
  // treeNames.push_back("MUON_ISO_STAT__1up");
  // treeNames.push_back("MUON_ISO_SYS__1down");
  // treeNames.push_back("MUON_ISO_SYS__1up");
  //treeNames.push_back("PRW_DATASF__1down");
  //treeNames.push_back("PRW_DATASF__1up");


  int Nsyst = treeNames.size();

  for(int isyst=0; isyst<Nsyst; isyst++) {

    //if(isyst>0) break;
    
    cout << "-----------------------------------------------------------------------------------------" << treeNames[isyst] << endl;
    AnalysisBase susy(treeNames[isyst]);
    susy.chain->Add(inputFile.c_str());
    susy.fChain->GetEntry(0);
    
    //set debugging flag
    susy.DEBUG  = DEBUG;
    susy.systName = treeNames[isyst];

    const char *myDSID = theDSID.c_str();
    TString DSID = TString(myDSID);
    susy.theDSID = DSID;
    //cout << "DSID = " << susy.theDSID << endl;

    TFile *_infile = TFile::Open(inputFile.c_str(),"READ");
  
    if(SampleKind=="SIGNAL") {
      //susy.isSignal=true;
      susy.luminosity = 1000.0;//
      susy.isMC=true;
      susy.isFake=false;
    }   

    else if(SampleKind=="MCa") {
      susy.isMC=true;
      susy.isSignal=false;
      susy.luminosity = 36100.0;//
      susy.isFake=false;
    }

    else if(SampleKind=="MCd") {
      susy.isMC=true;
      susy.isSignal=false;
      susy.luminosity = 44100.0;//
      susy.isFake=false;
    }

    else if(SampleKind=="MC") {
      susy.isMC=true;
      susy.isSignal=false;
      susy.luminosity = 1.0;//
      susy.isFake=false;
    }

    else if(SampleKind=="0"){
      susy.isWZ=true;
      susy.hasjets=false;
      susy.isMC=true;
      susy.isSignal=false;
      susy.luminosity = 1000.0;//
    }
    else if(SampleKind=="1"){
      susy.isMC=true;
      susy.hasjets=true;
      susy.isWZ=true;
      susy.isSignal=false;
      susy.luminosity = 1000.0;//
    }
    else if(SampleKind=="DD"){
      susy.isMC=false;
      susy.isFake=true;
    } 
    else susy.isMC=false;  
    //----------
    // std::vector<string> samplefields;
    // stringstream ss(inputFile);
    // string token;

    // while( getline( ss, token, '/' ) ) {	        
    //   cout << " token " << token << endl;
    //   samplefields.push_back(token);
    // }
    //return 1;
    //cout << "after while loop " << samplefields[1] <<  endl;
    // const char *mychar = samplefields[13].c_str();
    // if(SampleKind=="MC")
    //   mychar = samplefields[10].c_str();
    // if(SampleKind=="SIGNAL")
    //   mychar = samplefields[13].c_str();
    // if(SampleKind=="DATA")
    //   mychar = samplefields[14].c_str();
    //TString mySampleName = TString(mychar);

    //
    //TString myoutFile = "outputs/" + treeNames[isyst]+"."+ mySampleName;
    //TString myoutFile = "outputs/" + treeNames[isyst]+"."+ susy.theDSID+".root";
    //TFile *myfile = new TFile(myoutFile,"RECREATE");

    //cout << "making: "<< myoutFile << endl;

    cout << "isMC check: " << susy.isMC << endl;
    cout << "luminosity: " << susy.luminosity << endl;

    cout << endl << endl;
    cout << "============================================================ " << endl;
    cout << " ****** Run Signal Region Tree *****" << endl;
    cout << "============================================================ " << endl;
    susy.Loop();
    

    delete _infile;

    //myfile->Write();
    //myfile->Close();    
    
  }

  return 0;
}


//void PrintMSG(string message){

//  cout << message << endl;
//  return;
//}
