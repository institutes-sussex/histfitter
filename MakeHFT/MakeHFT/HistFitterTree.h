//------------------------------------------
//   HistFitterTrees.h
//   definitions for core analysis
//
//
//   author: Lukas Marti
//-------------------------------------------
#ifndef HistFitterTree_h
#define HistFitterTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TH1F.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "TLorentzVector.h"
#include "TParameter.h"
//#include "HistFitterTree/MultiLepEvent.h"


class HistFitterTree  {

  public:

    // Constructor
    // - MCID => will be used to name the tree
    // - syst => will be used to name the file
    // That way all files are hadd'able
    // Optional arguments fileName and treeName can override default naming convention
    HistFitterTree(TString MCID, TString syst, TString fileName="", TString treeName="");
    ~HistFitterTree();

    TFile* file; 
    TTree* tree;
    TString mymcid;
    
    // Branches for output tree
    float LepPt0;  // now only light leptons
    float LepPt1;  // now only light leptons
    float LepPt2;  // now only light leptons
    float LepEta0; // now only light leptons
    float LepEta1; // now only light leptons
    float LepEta2; // now only light leptons
    float LepPhi0; // now only light leptons
    float LepPhi1; // now only light leptons
    float LepPhi2; // now only light leptons
    float LepCharge0; // now only light leptons
    float LepCharge1; // now only light leptons
    float LepCharge2; // now only light leptons
    int LepPdgId0;  // 0=e 1=mu
    int LepPdgId1;  // 0=e 1=mu
    int LepPdgId2;  // 0=e 1=mu

    float JetPt0;
    float JetPt1;
    float JetPt2;


    float MET;
    float METSig;
    float HT;
    float HTLep;

    int   njets;
    int   nbjets;
    int   nCentralLightJets;
    int   cleaning;
    int   n_comblep;
    int   nSigLep;
    double eventweight; // Has to be the same for all regions, must include the scale factor to 1fb^-1
    double NormWeight;
    double bTagWeight;
    double pileupWeight;
    double JVTSF;
    double ElecSF;
    double MuonSF;
    double EvtWeight;
    int   SR;
    int   runNumber;
    int   eventNumber;

    bool passtrigger;
    bool matchtrigger;
    
    //three-lepton
    int passSFOS;
    float Mlll;
    float mSFOS;
    float Mt;
    int pass3l; 
   
    int        passDF;
    double     deltaRmin;
    double     mhiggs;
    double     dphi12;//weight based systematics stored as ratio of (systematic event weight)/(nominal event weight)
    
    float syst_FT_EFF_B_down; //FT_EFF_B_systematics__1down in systematic registry
    float syst_FT_EFF_B_up; //FT_EFF_B_systematics__1up
    float syst_FT_EFF_C_down; //FT_EFF_C_systematics__1down
    float syst_FT_EFF_C_up; //FT_EFF_C_systematics__1up
    float syst_FT_EFF_Light_down; //FT_EFF_Light_systematics__1down
    float syst_FT_EFF_Light_up; //FT_EFF_Light_systematics__1up
    float syst_FT_EFF_extrapolation_down; //FT_EFF_extrapolation__1down
    float syst_FT_EFF_extrapolation_up; //FT_EFF_extrapolation__1up
    float syst_FT_EFF_extrapolationFromCharm_down; //FT_EFF_extrapolation_from_charm__1down
    float syst_FT_EFF_extrapolationFromCharm_up; //FT_EFF_extrapolation_from_charm__1up
    float syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down; //EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down
    float syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up; //EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up
    float syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down; //EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down
    float syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up; //EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up
    float syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down; //EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down
    float syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up; //EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__up
    float syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_down; //EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down
    float syst_EL_EFF_Trigger_TOTAL_UncorrUncertainty_up; //EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up

    float syst_EL_EFF_TriggerEff_down;
    float syst_EL_EFF_TriggerEff_up;


    float syst_MUON_EFF_STAT_down; //MUON_EFF_STAT__1down
    float syst_MUON_EFF_STAT_up; //MUON_EFF_STAT__1up
    float syst_MUON_EFF_SYS_down; //MUON_EFF_SYS__1down
    float syst_MUON_EFF_SYS_up; //MUON_EFF_SYS__1up
    float syst_MUON_EFF_TrigStatUncertainty_down; //MUON_EFF_TrigStatUncertainty__1down
    float syst_MUON_EFF_TrigStatUncertainty_up; //MUON_EFF_TrigStatUncertainty__1up
    float syst_MUON_EFF_TrigSystUncertainty_down; //MUON_EFF_TrigSystUncertainty__1down
    float syst_MUON_EFF_TrigSystUncertainty_up; //MUON_EFF_TrigSystUncertainty__1up

    float syst_MUON_EFF_STAT_LOWPT_down;
    float syst_MUON_EFF_STAT_LOWPT_up;
    float syst_MUON_EFF_SYS_LOWPT_down;
    float syst_MUON_EFF_SYS_LOWPT_up;

    float syst_MUON_ISO_STAT_down; //MUON_ISO_STAT__1down
    float syst_MUON_ISO_STAT_up; //MUON_ISO_STAT__1up
    float syst_MUON_ISO_SYS_down; //MUON_ISO_SYS__1down
    float syst_MUON_ISO_SYS_up; //MUON_ISO_SYS__1up

    float syst_MUON_TTVA_STAT_down;
    float syst_MUON_TTVA_STAT_up;
    float syst_MUON_TTVA_SYS_down;
    float syst_MUON_TTVA_SYS_up;

    float syst_jvtSF_up;
    float syst_jvtSF_down;
    
    float Fake_syst_DOWN;
    float Fake_syst_UP;

    //end of weight based systematics
    
    //virtual void InitializeOutput(TFile** file, TString filename,TTree** tree, TString treename );
    void ClearOutputBranches();

    void setSumOfMcWeights(double sumOfMcWeights);

    
    

    void WriteTree();

};
#endif // #ifdef HistFitterTrees_cxx
