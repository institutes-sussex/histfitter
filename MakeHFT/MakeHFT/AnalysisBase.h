//////////////////////////////////////////////////////////
// This class has been automatically generated on 
// Fri Jul  1 19:02:18 2016 by ROOT version 5.34/32
// adapted for Sussex ntuple format 
// found on file: mc15_13TeV.392513.C1C1_SlepSnu_x0p50_500p0.p2666.root
//////////////////////////////////////////////////////////

#ifndef AnalysisBase_h
#define AnalysisBase_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TLorentzVector.h>

#include <vector>
#include <iostream>
#include "Tools.h"

//#include "Histograms.h"
#include "HistFitterTree.h"

using namespace std;


// Fixed size dimensions of array or collections stored in the TTree if any.

class AnalysisBase {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   TChain         *chain;

   // Declaration of leaf types
   bool		   Cleaning;
   Int_t 	   nCombLep;
   ULong64_t       EventNumber;
   UInt_t          RunNumber;
   Double_t        XSecWeight;
   Double_t        m_3lepweight;
   Double_t        xsecWeight;
   vector<float>   *JetPt;
   vector<float>   *JetEta;
   vector<float>   *JetPhi;
   UInt_t    num_bjets;


   vector<float>   *LepEta;
   vector<float>   *LepPt;
   vector<float>   *LepM;
   vector<float>   *LepPhi;
   vector<Int_t> 	   *LepTruth;
   vector<float>     *LepCharge;
   UInt_t    pass3lcut;
   vector<int>     *LepPdgId;
   Double_t         eT_miss;
   Int_t	    nJets;
   Double_t 	    ht;
   Double_t        met_Sig;

   //Float_t eT_miss;

   Int_t      pass2lossf;
   Double_t   mlll;
   Double_t   mll3lZ;
   Double_t   mTWZ;

   //Float_t   mlll;
   //Float_t   mll3lZmin;
   //Float_t   mTWZmin;
  

   Int_t        passDFOS;
   Double_t     dRmin;
   //   Double_t     mHiggs;
   Double_t     dPhi12;
   Float_t         dRnear;
   Float_t         mHiggs;
   //Float_t     DeltaRmin;
   //Float_t     mHiggs;
   //Float_t     dPhi12;

   Double_t         elecSF;
   Double_t         elecIDSF;
   Double_t         muonSF;
   Double_t         muonIDSF;
   Double_t         btagSFCentral;
   Double_t         pileupweight;
   Double_t         sherpaNjetsWeight;
   Double_t         EventWeight;
   Double_t         jvtSF;
   Float_t 	   FFWeight;
   Float_t 	   systWelCorr;
   Float_t         systWmuCorr;
   Float_t         systWelUnc;
   Float_t         systWmuUnc;
   Int_t           Pass1ElTrigger;
   Int_t           Pass1MuTrigger;
   Int_t           Pass2ElTrigger;
   Int_t 	   Pass2El24Trigger;
   Int_t           Pass2MuTrigger;
   Int_t           PassElMuTrigger;
   int		   PassDiLepTriggerMatch;

   // systematics
   Double_t  elecSF_EFF_Iso_Down ; 
   Double_t  elecSF_EFF_Iso_Up ; 
   Double_t  elecSF_EFF_ID_Down ; 
   Double_t  elecSF_EFF_ID_Up ; 
   Double_t  elecSF_EFF_Reco_Down ;
   Double_t  elecSF_EFF_Reco_Up ; 
  
   Double_t  elecSF_EFF_Trigger_Down ; 
   Double_t  elecSF_EFF_Trigger_Up ; 
   Double_t  elecSF_EFF_TriggerEff_Down ; 
   Double_t  elecSF_EFF_TriggerEff_Up ; 
   Double_t  muonSF_EFF_STAT_Down ;
   Double_t  muonSF_EFF_STAT_Up ; 
   Double_t  muonSF_EFF_SYS_Down ;
   Double_t  muonSF_EFF_SYS_Up ; 
   Double_t  muonSF_EFF_STAT_LOWPT_Down ; 
   Double_t  muonSF_EFF_STAT_LOWPT_Up ; 
   Double_t  muonSF_EFF_SYS_LOWPT_Down ;
   Double_t  muonSF_EFF_SYS_LOWPT_Up ; 
   Double_t  muonSF_EFF_TrigStatUnc_Down ;
   Double_t  muonSF_EFF_TrigStatUnc_Up ; 
   Double_t  muonSF_EFF_TrigSystUnc_Down ; 
   Double_t  muonSF_EFF_TrigSystUnc_Up ; 
   Double_t  muonSF_ISO_STAT_Down ; 
   Double_t  muonSF_ISO_STAT_Up ; 
   Double_t  muonSF_ISO_SYS_Down ; 
   Double_t  muonSF_ISO_SYS_Up ; 
   Double_t  muonSF_TTVA_STAT_Down ;
   Double_t  muonSF_TTVA_STAT_Up ; 
   Double_t  muonSF_TTVA_SYS_Down ;
   Double_t  muonSF_TTVA_SYS_Up ; 
   Double_t  bEffUpWeight ; 
   Double_t  bEffDownWeight ; 
   Double_t  cEffUpWeight ; 
   Double_t  cEffDownWeight ;
   Double_t  lEffUpWeight ; 
   Double_t  lEffDownWeight ;
   Double_t  extrapolationUpWeight ; 
   Double_t  extrapolationDownWeight ;
   Double_t  jvtSFup   ;                                           
   Double_t  jvtSFdown ;                                           

   Double_t FT_EFF_extrapolationFromCharm_DOWN;
   Double_t FT_EFF_extrapolationFromCharm_UP;


   // List of branches
   TBranch        *b_Cleaning;
   TBranch	  *b_nCombLep;
   TBranch        *b_EventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_XSecWeight;   //!
   TBranch        *b_m_3lepweight;   //!
   TBranch	  *b_xsecWeight;
   TBranch        *b_JetPt;   //!
   TBranch        *b_JetPhi;   //!
   TBranch        *b_JetEta;   //!
   TBranch        *b_num_bjets;   //!
   TBranch        *b_btagSFCentral;   //!
   TBranch        *b_LepEta;   //!
   TBranch        *b_LepPt;   //!
   TBranch        *b_LepM;   //!
   TBranch        *b_LepPhi;   //!
   TBranch        *b_LepTruth;
   TBranch        *b_LepCharge;   //!
   TBranch        *b_pass3lcut;   //!
   TBranch        *b_LepPdgId;   //!
   /* TBranch        *b_eT_miss_Phi;   //! */
   TBranch        *b_eT_miss;   //!
   TBranch	  *b_nJets;
   TBranch        *b_ht;

   TBranch        *b_mlll;   //!
   TBranch        *b_mTWZ;   //!
   TBranch        *b_mll3lZ;   //!
   TBranch        *b_pass2lossf;   //!
   TBranch        *b_passDFOS;
   TBranch        *b_dRnear;
   TBranch        *b_mHiggs;
   TBranch        *b_dPhi12;
   TBranch        *b_met_Sig;   //!

   TBranch        *b_MET_px;   //!
   TBranch        *b_MET_py;   //!
   /* TBranch        *b_eT_missTST_Phi;   //! */
   /* TBranch        *b_eT_missTST;   //! */
   TBranch        *b_elecSF;   //!
   TBranch        *b_muonSF;   //!
   /* TBranch        *b_trigSF_weight;   //! */

   TBranch        *b_pileupweight;   //!
   /* TBranch        *b_PRWRndNumber;   //! */
   TBranch        *b_jvtSF;   //!
   TBranch        *b_EventWeight;   //!
   TBranch        *b_Pass1ElTrigger;   //!
   TBranch        *b_Pass1MuTrigger;   //!
   TBranch        *b_Pass2ElTrigger;   //!
   TBranch        *b_Pass2El24Trigger;
   TBranch        *b_Pass2MuTrigger;   //!
   TBranch        *b_PassElMuTrigger;   //!
   TBranch        *b_PassDiLepTriggerMatch;   //!
   /* TBranch        *b_HLT_e24_lhmedium_iloose_L1EM20VH;   //! */
   /* TBranch        *b_HLT_e24_lhmedium_iloose_L1EM18VH;   //! */
   /* TBranch        *b_HLT_e24_lhtight_iloose;   //! */
   /* TBranch        *b_HLT_e26_lhtight_iloose;   //! */
   /* TBranch        *b_HLT_e24_lhmedium_nod0_L1EM20VH;   //! */
   /* TBranch        *b_HLT_e24_lhtight_nod0_ivarloose;   //! */
   /* TBranch        *b_HLT_e60_lhmedium;   //! */
   /* TBranch        *b_HLT_e60_lhmedium_nod0;   //! */
   /* TBranch        *b_HLT_e140_lhloose_nod0;   //! */
   /* TBranch        *b_HLT_2e12_lhloose_L12EM10VH;   //! */
   /* TBranch        *b_HLT_2e15_lhvloose_nod0_L12EM13VH;   //! */
   /* TBranch        *b_HLT_2e17_lhloose_nod0;   //! */
   /* TBranch        *b_HLT_2e17_lhloose;   //! */
   /* TBranch        *b_HLT_mu26_imedium;   //! */
   /* TBranch        *b_HLT_mu24_imedium;   //! */
   /* TBranch        *b_HLT_mu24_iloose_L1MU15;   //! */
   /* TBranch        *b_HLT_mu20_iloose_L1MU15;   //! */
   /* TBranch        *b_HLT_mu50;   //! */
   /* TBranch        *b_HLT_mu18_mu8noL1;   //! */
   /* TBranch        *b_HLT_mu20_mu8noL1;   //! */
   /* TBranch        *b_HLT_mu22_mu8noL1;   //! */
   /* TBranch        *b_HLT_mu24_mu8noL1;   //! */
   /* TBranch        *b_HLT_2mu10;   //! */
   /* TBranch        *b_HLT_2mu14;   //! */
   /* TBranch        *b_HLT_e17_lhloose_mu14;   //! */
   /* TBranch        *b_HLT_e7_lhmedium_mu24;   //! */
   /* TBranch        *b_HLT_e17_lhloose_nod0_mu14;   //! */
   /* TBranch        *b_HLT_e24_lhmedium_L1EM20VHI_mu8noL1;   //! */

   // systematics
   TBranch        *b_elecSF_EFF_Iso_Down ; 
   TBranch        *b_elecSF_EFF_Iso_Up ; 
   TBranch        *b_elecSF_EFF_ID_Down ; 
   TBranch        *b_elecSF_EFF_ID_Up ; 
   TBranch        *b_elecSF_EFF_Reco_Down ;
   TBranch        *b_elecSF_EFF_Reco_Up ; 
   /*
   TBranch        *b_elecSF_EFF_Trigger_Down ; 
   TBranch        *b_elecSF_EFF_Trigger_Up ; 
   TBranch        *b_elecSF_EFF_TriggerEff_Down ; 
   TBranch        *b_elecSF_EFF_TriggerEff_Up ; 
   */
   TBranch        *b_muonSF_EFF_STAT_Down ;
   TBranch        *b_muonSF_EFF_STAT_Up ; 
   TBranch        *b_muonSF_EFF_SYS_Down ;
   TBranch        *b_muonSF_EFF_SYS_Up ; 
   TBranch        *b_muonSF_EFF_STAT_LOWPT_Down ; 
   TBranch        *b_muonSF_EFF_STAT_LOWPT_Up ; 
   TBranch        *b_muonSF_EFF_SYS_LOWPT_Down ;
   TBranch        *b_muonSF_EFF_SYS_LOWPT_Up ; 
   /*
   TBranch        *b_muonSF_EFF_TrigStatUnc_Down ;
   TBranch        *b_muonSF_EFF_TrigStatUnc_Up ; 
   TBranch        *b_muonSF_EFF_TrigSystUnc_Down ; 
   TBranch        *b_muonSF_EFF_TrigSystUnc_Up ;
   */ 
   TBranch        *b_muonSF_ISO_STAT_Down ; 
   TBranch        *b_muonSF_ISO_STAT_Up ; 
   TBranch        *b_muonSF_ISO_SYS_Down ; 
   TBranch        *b_muonSF_ISO_SYS_Up ; 
   TBranch        *b_muonSF_TTVA_STAT_Down ;
   TBranch        *b_muonSF_TTVA_STAT_Up ; 
   TBranch        *b_muonSF_TTVA_SYS_Down ;
   TBranch        *b_muonSF_TTVA_SYS_Up ; 
   TBranch        *b_bEffUpWeight ; 
   TBranch        *b_bEffDownWeight ; 
   TBranch        *b_cEffUpWeight ; 
   TBranch        *b_cEffDownWeight ;
   TBranch        *b_lEffUpWeight ; 
   TBranch        *b_lEffDownWeight ;
   //   TBranch        *b_extrapolationUpWeight ; 
   //   TBranch        *b_extrapolationDownWeight ;
   TBranch        *b_jvtSFup   ;                                           
   TBranch        *b_jvtSFdown ;                                      

   TBranch        *b_FT_EFF_extrapolationFromCharm_DOWN;
   TBranch        *b_FT_EFF_extrapolationFromCharm_UP;
     

   AnalysisBase(TString treeName);
   virtual ~AnalysisBase();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   //my own variables to be used in the mini event loop in---------------------------------------------------------------------------------------------------- For  analysisbase
   bool DEBUG;
   Tools tool; //needed for mt2 business
   TString systName;

   bool isFake;
   bool isMC;
   bool isWZ;
   bool hasjets;
   bool isSignal;
   TString theDSID;
   int m_cleaning;
   int m_nCombLep;

   bool is2L;
   bool is3L;

   double weight;
   double BtagWeight;
   double PileUpWeight;
   double JVTsf;
   double Elec_SF;
   double Muon_SF;
   double GenWeight;
   double nEventsTotal;
   double luminosity;
   double normWeight;

   int theChannel;

   bool isBtagJet;
   bool isForwardJet;
   bool isCentralLightJet;
   bool Zveto;
   double mt2;
   double  metsig ;
   double htJ ;
   double htL ;
   double M3l; //careful same string as HFT 

   int pDF;
   int p3lcut;
   double masshiggs;
   double deltarmin;
   double Dphi12;
   double mt;
   double pT3rd;
   double met;
   double mll;
   double massDiff;
   int TriggerMatch;
   bool foundSFOS;
   TLorentzVector lep1, lep2, lep3;
   
   float Totalfakesyst;
   float Sum2fakesyst;
   float FakeSystUP;
   float FakeSystDOWN;

   enum CHANNEL2L{EE,MM,EM,SF,ALL2L,NCHANNEL2L};
   enum CHANNEL3L{EEE,MMM,EEM,MME,ALL3L,NCHANNEL3L};


   // for the output treee!
   //Histograms histo; //change this to HF!?
   HistFitterTree* histo; //change this to HF!?


   static bool SortTLV(const pair<TLorentzVector,int> lv1, const pair<TLorentzVector,int> lv2);
   bool isSRWZ3LH(); 
   bool isSRWZ3LI();
   bool isVR3LFAKES(); 
   bool isVR3LWZ();
   bool isVR3LHIGHMET();
   bool isVR3LOFFZ(); 


};

#endif

//----------------------------------------------------------------------------------------------------initialise the variables of this own code
#ifdef AnalysisBase_cxx
AnalysisBase::AnalysisBase(TString treeName) : fChain(0) 
{

  chain = new TChain(treeName);
  Init(chain);
  

  isMC=false;
  isWZ=false;
  hasjets=false;
  isSignal=false;
  theDSID = "";     
  m_cleaning = 0;
  m_nCombLep = 0;

  weight     = -999.;
  BtagWeight = -999.;
  PileUpWeight = -999.;
  JVTsf = -999.;
  Elec_SF = -999.;
  Muon_SF = -999.;
  GenWeight = -999.;
  nEventsTotal = -999.0;
  luminosity = -9999.0;//
  normWeight=1.;

  isBtagJet = false;
  isForwardJet=false;
  isCentralLightJet=false;
  Zveto=false;
  mt2        = -999.;
  mll=-999.;


  foundSFOS=false;

  theChannel = -999;
  massDiff   = -999;
  M3l       = -999.;
  mt         = -999.;
  pT3rd      = -999.;
  met        = -999.;
  metsig     = -999.;
  htJ = -999.;
  htL = -999.;
  pDF    = -999.;
  masshiggs = -999.;
  deltarmin = -999.;
  Dphi12    = -999.;

}

AnalysisBase::~AnalysisBase()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t AnalysisBase::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t AnalysisBase::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void AnalysisBase::Init(TTree *tree) // ala Sussex stuff
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   JetPt = 0;
   JetPhi = 0;
   JetEta = 0;
   num_bjets = 0;
   btagSFCentral = 0;
   LepEta = 0;
   LepPt = 0;
   LepM = 0;
   LepPhi = 0;
   LepTruth = 0;
   LepCharge = 0;
   pass3lcut = 0;
   nJets = 0;
   /* lepton_isMedium = 0; */
   /* lepton_isTight = 0; */
   /* lepton_isLooseIso = 0; */
   /* lepton_isMediumIso = 0; */
   /* lepton_isTightIso = 0; */
   /* lepton_d0sig = 0; */
   /* lepton_z0 = 0; */
   /* lepton_origin = 0; */
   LepPdgId = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Cleaning",&Cleaning,&b_Cleaning);	
   fChain->SetBranchAddress("nCombLep",&nCombLep,&b_nCombLep);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("JetPt", &JetPt, &b_JetPt);
   fChain->SetBranchAddress("JetPhi", &JetPhi, &b_JetPhi);
   fChain->SetBranchAddress("JetEta", &JetEta, &b_JetEta);
   fChain->SetBranchAddress("num_bjets", &num_bjets, &b_num_bjets);
   fChain->SetBranchAddress("LepEta", &LepEta, &b_LepEta);
   fChain->SetBranchAddress("LepPt", &LepPt, &b_LepPt);
   fChain->SetBranchAddress("LepM", &LepM, &b_LepM);
   fChain->SetBranchAddress("LepPhi", &LepPhi, &b_LepPhi);
   fChain->SetBranchAddress("LepTruth", &LepTruth, &b_LepTruth);
   fChain->SetBranchAddress("LepCharge", &LepCharge, &b_LepCharge);
   fChain->SetBranchAddress("pass3lcut", &pass3lcut, &b_pass3lcut);
   fChain->SetBranchAddress("LepPdgId", &LepPdgId, &b_LepPdgId);
   fChain->SetBranchAddress("eT_miss", &eT_miss, &b_eT_miss);
   fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
   fChain->SetBranchAddress("met_Sig", &met_Sig, &b_met_Sig);
   fChain->SetBranchAddress("ht", &ht, &b_ht);

   // SFOS variables 
   fChain->SetBranchAddress("pass2lossf", &pass2lossf, &b_pass2lossf);
   fChain->SetBranchAddress("mlll", &mlll, &b_mlll);
   fChain->SetBranchAddress("mll3lZ", &mll3lZ, &b_mll3lZ);
   fChain->SetBranchAddress("mTWZ", &mTWZ, &b_mTWZ);

   // DFOS variables
   fChain->SetBranchAddress("passDFOS", &passDFOS, &b_passDFOS);
   fChain->SetBranchAddress("mHiggs", &mHiggs, &b_mHiggs);
   fChain->SetBranchAddress("dRnear", &dRnear, &b_dRnear);
   fChain->SetBranchAddress("dPhi12", &dPhi12, &b_dPhi12);

   // Weights
   fChain->SetBranchAddress("XSecWeight", &XSecWeight, &b_XSecWeight);
   fChain->SetBranchAddress("m_3lepweight", &m_3lepweight, &b_m_3lepweight);
   fChain->SetBranchAddress("elecSF", &elecSF, &b_elecSF);
   fChain->SetBranchAddress("muonSF", &muonSF, &b_muonSF);
   fChain->SetBranchAddress("btagSFCentral", &btagSFCentral, &b_btagSFCentral);
   fChain->SetBranchAddress("pileupweight", &pileupweight, &b_pileupweight);
   fChain->SetBranchAddress("EventWeight", &EventWeight, &b_EventWeight);
   fChain->SetBranchAddress("jvtSF", &jvtSF,&b_jvtSF);                                                 
   fChain->SetBranchAddress("FFWeight", &FFWeight);
   fChain->SetBranchAddress("systWelCorr", &systWelCorr);
   fChain->SetBranchAddress("systWmuCorr", &systWmuCorr);
   fChain->SetBranchAddress("systWelUnc", &systWelUnc);
   fChain->SetBranchAddress("systWmuUnc", &systWmuUnc);

   // systematics
   fChain->SetBranchAddress("elecSF_EFF_Iso_Down",                 &elecSF_EFF_Iso_Down,                    &b_elecSF_EFF_Iso_Down);                    
   fChain->SetBranchAddress("elecSF_EFF_Iso_Up",                   &elecSF_EFF_Iso_Up,                      &b_elecSF_EFF_Iso_Up);                      
   fChain->SetBranchAddress("elecSF_EFF_ID_Down",                  &elecSF_EFF_ID_Down,                     &b_elecSF_EFF_ID_Down);                     
   fChain->SetBranchAddress("elecSF_EFF_ID_Up",                    &elecSF_EFF_ID_Up,                       &b_elecSF_EFF_ID_Up);                       
   fChain->SetBranchAddress("elecSF_EFF_Reco_Down",                &elecSF_EFF_Reco_Down,                   &b_elecSF_EFF_Reco_Down);                   
   fChain->SetBranchAddress("elecSF_EFF_Reco_Up",                  &elecSF_EFF_Reco_Up,                     &b_elecSF_EFF_Reco_Up);  
   /*                   
   fChain->SetBranchAddress("elecSF_EFF_Trigger_Down",             &elecSF_EFF_Trigger_Down,                &b_elecSF_EFF_Trigger_Down);                
   fChain->SetBranchAddress("elecSF_EFF_Trigger_Up",               &elecSF_EFF_Trigger_Up,                  &b_elecSF_EFF_Trigger_Up);
                     
   fChain->SetBranchAddress("elecSF_EFF_TriggerEff_Down",          &elecSF_EFF_TriggerEff_Down,             &b_elecSF_EFF_TriggerEff_Down);             
   fChain->SetBranchAddress("elecSF_EFF_TriggerEff_Up",            &elecSF_EFF_TriggerEff_Up,               &b_elecSF_EFF_TriggerEff_Up);               
   */
   fChain->SetBranchAddress("muonSF_EFF_STAT_Down",                &muonSF_EFF_STAT_Down,                   &b_muonSF_EFF_STAT_Down);                   
   fChain->SetBranchAddress("muonSF_EFF_STAT_Up",                  &muonSF_EFF_STAT_Up,                     &b_muonSF_EFF_STAT_Up);                     
   fChain->SetBranchAddress("muonSF_EFF_SYS_Down",                 &muonSF_EFF_SYS_Down,                    &b_muonSF_EFF_SYS_Down);                    
   fChain->SetBranchAddress("muonSF_EFF_SYS_Up",                   &muonSF_EFF_SYS_Up,                      &b_muonSF_EFF_SYS_Up);                      
   fChain->SetBranchAddress("muonSF_EFF_STAT_LOWPT_Down",          &muonSF_EFF_STAT_LOWPT_Down,             &b_muonSF_EFF_STAT_LOWPT_Down);             
   fChain->SetBranchAddress("muonSF_EFF_STAT_LOWPT_Up",            &muonSF_EFF_STAT_LOWPT_Up,               &b_muonSF_EFF_STAT_LOWPT_Up);               
   fChain->SetBranchAddress("muonSF_EFF_SYS_LOWPT_Down",           &muonSF_EFF_SYS_LOWPT_Down,              &b_muonSF_EFF_SYS_LOWPT_Down);              
   fChain->SetBranchAddress("muonSF_EFF_SYS_LOWPT_Up",             &muonSF_EFF_SYS_LOWPT_Up,                &b_muonSF_EFF_SYS_LOWPT_Up);    
   /*            
   fChain->SetBranchAddress("muonSF_EFF_TrigStatUnc_Down",         &muonSF_EFF_TrigStatUnc_Down,            &b_muonSF_EFF_TrigStatUnc_Down);            
   fChain->SetBranchAddress("muonSF_EFF_TrigStatUnc_Up",           &muonSF_EFF_TrigStatUnc_Up,              &b_muonSF_EFF_TrigStatUnc_Up);              
   fChain->SetBranchAddress("muonSF_EFF_TrigSystUnc_Down",         &muonSF_EFF_TrigSystUnc_Down,            &b_muonSF_EFF_TrigSystUnc_Down);            
   fChain->SetBranchAddress("muonSF_EFF_TrigSystUnc_Up",           &muonSF_EFF_TrigSystUnc_Up,              &b_muonSF_EFF_TrigSystUnc_Up);   
   */           
   fChain->SetBranchAddress("muonSF_ISO_STAT_Down",                &muonSF_ISO_STAT_Down,                   &b_muonSF_ISO_STAT_Down);                   
   fChain->SetBranchAddress("muonSF_ISO_STAT_Up",                  &muonSF_ISO_STAT_Up,                     &b_muonSF_ISO_STAT_Up);                     
   fChain->SetBranchAddress("muonSF_ISO_SYS_Down",                 &muonSF_ISO_SYS_Down,                    &b_muonSF_ISO_SYS_Down);                    
   fChain->SetBranchAddress("muonSF_ISO_SYS_Up",                   &muonSF_ISO_SYS_Up,                      &b_muonSF_ISO_SYS_Up);                      
   fChain->SetBranchAddress("muonSF_TTVA_STAT_Down",               &muonSF_TTVA_STAT_Down,                  &b_muonSF_TTVA_STAT_Down);                  
   fChain->SetBranchAddress("muonSF_TTVA_STAT_Up",                 &muonSF_TTVA_STAT_Up,                    &b_muonSF_TTVA_STAT_Up);                    
   fChain->SetBranchAddress("muonSF_TTVA_SYS_Down",                &muonSF_TTVA_SYS_Down,                   &b_muonSF_TTVA_SYS_Down);                   
   fChain->SetBranchAddress("muonSF_TTVA_SYS_Up",                  &muonSF_TTVA_SYS_Up,                     &b_muonSF_TTVA_SYS_Up);                     
   fChain->SetBranchAddress("bEffUpWeight",                        &bEffUpWeight,                           &b_bEffUpWeight);                           
   fChain->SetBranchAddress("bEffDownWeight",                      &bEffDownWeight,                         &b_bEffDownWeight);                         
   fChain->SetBranchAddress("cEffUpWeight",                        &cEffUpWeight,                           &b_cEffUpWeight);                           
   fChain->SetBranchAddress("cEffDownWeight",                      &cEffDownWeight,                         &b_cEffDownWeight);                         
   fChain->SetBranchAddress("lEffUpWeight",                        &lEffUpWeight,                           &b_lEffUpWeight);                           
   fChain->SetBranchAddress("lEffDownWeight",                      &lEffDownWeight,                         &b_lEffDownWeight);                         
   /*
   fChain->SetBranchAddress("extrapolationUpWeight",               &extrapolationUpWeight,                  &b_extrapolationUpWeight);                  
   fChain->SetBranchAddress("extrapolationDownWeight",             &extrapolationDownWeight,                &b_extrapolationDownWeight);                
   */
   fChain->SetBranchAddress("jvtSFup",                             &jvtSFup ,                               &b_jvtSFup);                                                 
   fChain->SetBranchAddress("jvtSFdown",                           &jvtSFdown,                              &b_jvtSFdown );                                              

   fChain->SetBranchAddress("FT_EFF_extrapolationFromCharm_UP",       &FT_EFF_extrapolationFromCharm_UP ,                               &b_FT_EFF_extrapolationFromCharm_UP);                                                 
   fChain->SetBranchAddress("FT_EFF_extrapolationFromCharm_DOWN",    &FT_EFF_extrapolationFromCharm_DOWN,                              &b_FT_EFF_extrapolationFromCharm_DOWN );                                                 

   fChain->SetBranchAddress("Pass2ElTrigger", &Pass2ElTrigger, &b_Pass2ElTrigger);
   fChain->SetBranchAddress("Pass2El24Trigger", &Pass2El24Trigger, &b_Pass2El24Trigger);
   fChain->SetBranchAddress("Pass2MuTrigger", &Pass2MuTrigger, &b_Pass2MuTrigger);
   fChain->SetBranchAddress("PassElMuTrigger", &PassElMuTrigger, &b_PassElMuTrigger);
   fChain->SetBranchAddress("Pass1ElTrigger", &Pass1ElTrigger,&b_Pass1ElTrigger);
   fChain->SetBranchAddress("Pass1MuTrigger", &Pass1MuTrigger,&b_Pass1MuTrigger);
   fChain->SetBranchAddress("PassDiLepTriggerMatch", &PassDiLepTriggerMatch,&b_PassDiLepTriggerMatch);
   Notify();
}

Bool_t AnalysisBase::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void AnalysisBase::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t AnalysisBase::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef AnalysisBase_cxx
