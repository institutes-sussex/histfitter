//Andreas Petridis
//Andreas.Petridis@cern.ch
//Stockholm University
//2013

#ifndef TOOLS
#define TOOLS

#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath> 
#include "TH1F.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "MakeHFT/mt2_bisect.h"

using namespace std;

class Tools{

 public:

  Tools();
  //~Tools();
  
  Double_t GetMETrel(vector<TLorentzVector> lepton, vector<TLorentzVector> jet, TLorentzVector met);
  Double_t GetMt2(vector<TLorentzVector> lepton, TLorentzVector met);
  bool SortingPredicate(const TLorentzVector &lv1, const TLorentzVector &lv2);
  Double_t calc_mct(TLorentzVector v1, TLorentzVector v2);

  Double_t ComputeTransverseSphericity(vector<TLorentzVector> leptons, vector<TLorentzVector> jets);
  Double_t TranverseSphericity(Double_t sumpx2, Double_t sumpy2, Double_t sumpxpy);

  TLorentzVector FindZ(vector<TLorentzVector> leptons, vector<int> lepCharges);

  mt2_bisect::mt2 mt2calculator;
  


 private:
  
  Double_t m_mt2;
  Double_t m_METrel;
  Double_t m_TransverseSphericity;
  
 
};


#endif
