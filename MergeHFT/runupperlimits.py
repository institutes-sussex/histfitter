import os,subprocess,shlex,sys

try:
    import signals
except:
    print "include tools/ into your python path"
    sys.exit(1)

grid_in = "N2N3Stau8TeV"
finalstate = "L4"
#srs = "SR301_SR302_SR303_SR304_SR305_SR313"
#sr = "SR400_SR401_SR402_SR403_SR404_SR405_SR406_SR407"
sr='SR403_SR404_SR405_SR406_SR407_SR408'
#sr='BIN16'
runcoment = "d75"
make1dplot = False

grid = grid_in


print "Good luck"
#os.makedirs()

# get points from module
if 'Slepr' in grid:
        #for the right handed sleptons use the same points/files as the left handed ones
    sigSamples=signals.getSignals(grid.replace('Slepr','Slepl'))
else:
    sigSamples=signals.getSignals(grid)

if grid.endswith('ulxs'): grid=grid[:-4]

for entry in sigSamples:
    if not "_0" in entry and make1dplot:
        continue
    fname = 'job_'+str(entry)+'.sh'
    with open(fname,"w") as sfile:
        print "running point",entry
        runstr = 'HistFitter.py -t -w -l -u "--signalGrid '+str(grid)+' --signalRegions '+str(sr)+' --finalState '+str(finalstate)+' --vxOrp0 3 --runPoint '
        runstr += entry
        runstr +='" tools/3L_config.py'
        print runstr
        print >> sfile, runstr



# anoying, needed due to PWC splitting
if 'Slepr' in grid_in:
    sigSamples=signals.getSignals(grid_in.replace('Slepr','Slepl'))
else:
    sigSamples=signals.getSignals(grid_in)

print "Starting run"
for entry in sigSamples:
    if not "_0" in entry and make1dplot:
        continue
    fname = 'job_'+str(entry)+'.sh'
    #runcmd = ['nohup', 'sh',str(fname),'> out'+str(entry)+'.txt 2>&1 &']
    runcmd = 'nohup sh '+str(fname) +' &'
    args = shlex.split(runcmd)
    print runcmd
    p = subprocess.Popen(args)
    p.wait()
    os.rename('nohup.out','out'+str(entry)+'.txt')
    os.rename('results/3L_Output_upperlimit.root','results/3L_Output_upperlimit_'+str(entry)+'.root')
    print "Done with",entry


print "Creating output folders now"
dirname = "output/UpperLimits_"+str(grid_in)+"_"+runcoment
os.mkdir(dirname)
os.mkdir(dirname+"/jobs")
os.mkdir(dirname+"/logs")
os.mkdir(dirname+"/rootfiles")
os.mkdir(dirname+"/upperlimits")
os.mkdir(dirname+"/plots")
os.mkdir(dirname+"/results")

print "moving output now"
print "moving jobs"
os.system("mv ./job_*.sh "+str(dirname)+"/jobs")
print "moving logs"
os.system("mv out*.txt "+str(dirname)+"/logs")
print "moving plots"
os.system("mv results/upperlimit_cls_poi*.eps "+str(dirname)+"/plots")
print "moving upperlimits"
os.system("mv results/3L_Output_upperlimit*.root "+str(dirname)+"/upperlimits")
print "moving rootfiles"
os.system("mv results/*CLs_grid_ts3.root "+str(dirname)+"/rootfiles")
print "moving ws"
os.system("mv results/3L/* "+str(dirname)+"/results")
print "all done"









