#!/usr/bin/python

from ROOT import *
from array import array
import time
import sys
#from sigColorPalette import sigColorPalette

gROOT.SetBatch(True)
gROOT.LoadMacro("./macros/AtlasStyle.C")
SetAtlasStyle()

folder=sys.argv[-1]

path = 'output/'+folder+'/contour/NoSys.txt'


x_axis = 'm_{#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}} [GeV]'
y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'

#  CHECK THE LATEST HISTFITTER VERSION AND ITS COMPATIBILITY ON THE FOLLOWING!!!
##

#listItems = [1,2,3,7,11,12,13,14] #+24,25 und 26 = nb.of toys
listItems = [3,7,11,12,103,107]

### This script creates a plot for each element in ListItems. The keys are (not all of them make necessarily sense):
###  0 - not used
###  1 - p0
###  2 - p1
###  3 - CLs
###  4 - mode
###  5 - nexp
###  6 - seed
###  7 - CLsexp
###  8 - fID
###  9 - sigma0
### 10 - sigma1
### 11 - clsu1s
### 12 - clsd1s
### 13 - clsu2s
### 14 - clsd2s
### 15 - upperLimit
### 16 - upperLimitEstimatedError
### 17 - expectedUpperLimit
### 18 - expectedUpperLimitPlus1Sig
### 19 - expectedUpperLimitPlus2Sig
### 20 - expectedUpperLimitMinus1Sig
### 21 - expectedUpperLimitMinus2Sig
### 22 - xsec
### 23 - excludedXsec
### 24 - failedfit (hf26)
### 25 - failedcov (hf26)
### 26 - failedp0 (hf26)
### 27 - fitstatus (hf26)
### 28 - x
### 29 - y
### 30 - nofit (hf26)
### 31 - name of grid
### 32 - nbOfToys
### 103- same as 3, but plotting significance instead of CL
### 107- same as 7, but plotting significance instead of CL

outFile = 'output/'+folder+'/'+'_'.join(folder.split('_')[2:])
outFileStats = outFile+'_stats.txt'

### listDict[listItem] = ['output_file_name.eps','histogram_title','z_axis']
listDict = {}
listDict[1]  = [outFile+'_p0.eps','p0','']
listDict[2]  = [outFile+'_p1.eps','p1','']
listDict[3]  = [outFile+'_CLobs.eps','CL observed','CL obs']
listDict[4]  = [outFile+'_mode.eps','mode','']
listDict[5]  = [outFile+'_nexp.eps','nexp','']
listDict[6]  = [outFile+'_seed.eps','seed','']
listDict[7]  = [outFile+'_CLexp.eps','CL expected','CL exp']
listDict[8]  = [outFile+'_fID.eps','fID','']
listDict[9]  = [outFile+'_sigma0.eps','sigma0','']
listDict[10] = [outFile+'_sigma1.eps','sigma1','']
listDict[11] = [outFile+'_CLu1s.eps','CL + 1 \sigma','CL obs+1 \sigma']
listDict[12] = [outFile+'_CLd1s.eps','CL - 1 \sigma','CL obs-1 \sigma']
listDict[13] = [outFile+'_CLu2s.eps','CL + 2 \sigma','CL obs+2 \sigma']
listDict[14] = [outFile+'_CLd2s.eps','CL - 2 \sigma','CL obs-2 \sigma']
listDict[15] = [outFile+'_upperLimit.eps','upperLimit','']
listDict[16] = [outFile+'_upperLimitEstimatedError.eps','upperLimitEstimatedError','']
listDict[17] = [outFile+'_expectedUpperLimit.eps','expectedUpperLimit','']
listDict[18] = [outFile+'_expectedUpperLimitPlus1Sig.eps','expectedUpperLimitPlus1Sig','']
listDict[19] = [outFile+'_expectedUpperLimitPlus2Sig.eps','expectedUpperLimitPlus2Sig','']
listDict[20] = [outFile+'_expectedUpperLimitMinus1Sig.eps','expectedUpperLimitMinus1Sig','']
listDict[21] = [outFile+'_expectedUpperLimitMinus2Sig.eps','expectedUpperLimitMinus2Sig','']
listDict[22] = [outFile+'_xsec.eps','xsec','']
listDict[23] = [outFile+'_excludedXsec.eps','excludedXsec','']
listDict[24] = [outFile+'failedfit.eps','failedfit','']
listDict[25] = [outFile+'failedcov.eps','failedcov','']
listDict[26] = [outFile+'failedp0.eps','failedp0','']
listDict[27] = [outFile+'fitstatus.eps','fitstatus','']
listDict[28] = [outFile+'_x.eps','x','']
listDict[29] = [outFile+'_y.eps','y','']
listDict[30] = [outFile+'_nofit.eps','nofit','']
listDict[31] = [outFile+'_grid.eps','grid','']
listDict[32] = [outFile+'_nbOfToys.eps','nbOfToys','# Toys (x1000)']
listDict[103]= [outFile+'_Zobs.eps','Z observed','Z']
listDict[107]= [outFile+'_Zexp.eps','Z expected','Z']

#gStyle.SetOptStat(00000000)
gStyle.SetPaintTextFormat('5.2f')
gStyle.SetPalette(1)

listFile=[]
#f = open(path, 'r')
#lines = f.readlines()
#for line in lines:
#  listFile.append(line)
#f.close()

with open(path) as f:
    for line in f:
        listFile.append(line)

### Fill the x and y values into an array
x=array('f')
y=array('f')
for listLine in listFile:
    x.append(float(listLine.split(' ')[28]))
    y.append(float(listLine.split(' ')[29]))

#print x[10]
#print y

#x.remove(x[10]) # for staus only!
#y.remove(y[10])

#print min(x)
#print max(x)
#print min(y)
#print max(y)
#print len(x)
#print len(y)


if 'L4' in folder:
    infile_ut = TFile('tools/upperTriangle.root','READ')
    triangle = TGraph(infile_ut.Get('upperTriangle'))
    triangle.SetLineStyle(10)
    triangle.SetLineWidth(3)
    triangle.SetLineColor(14)

    diagonal = TLatex()
    diagonal.SetTextSize(0.050)
    diagonal.SetTextColor(15)
    diagonal.SetTextFont(42)

    x_central = 0.5 * (max(x) + min(x))
    width = max(x)-min(x)
    x_text = x_central - 0.1 * width
    y_range = max(y) - min(y)
    y_text = x_text + 0.07 * y_range
    angle = TMath.RadToDeg() * TMath.ATan(width/y_range)
    diagonal.SetTextAngle(angle)

for listItem in listItems:
    if listItem==32: gStyle.SetPaintTextFormat('g')
    ### Get the values from the dictionaries
    output_file_name    = listDict[listItem][0]
    histogram_title     = listDict[listItem][1]
    z_axis              = listDict[listItem][2]
    ### Fill the z values into an array
    z=array('f')
    for listLine in listFile:
        if listItem>100: z.append(TMath.NormQuantile(1.-float(listLine.split(' ')[listItem-100])))
        else: z.append(float(listLine.split(' ')[listItem]))

    #print z[10]
    #z.remove(z[10]) #for staus only!
    ### Parent histogram out of which we clone a histogram for printing the numbers (not interpolated) and one for the colors (interpolated)
    h_parent = TH2F(histogram_title,histogram_title,100,min(x)-1,max(x)+1,100,min(y)-1,max(y)+1)
    h_parent.SetXTitle(x_axis)
    h_parent.SetYTitle(y_axis)
    h_parent.SetZTitle(z_axis)
    h_numbers = h_parent.Clone()
    for i,element in enumerate(x):
        if x[i]==150.: print x[i],y[i],z[i]
        if listItem>100 and z[i]<0: continue
        if listItem==32: z[i]=z[i]/1000.  # For the # of toys
        h_numbers.Fill(x[i],y[i],z[i])
    ### First define graph and then derive histogram from graph. This way the the colors between the points are interpolated
    g = TGraph2D(len(x),x,y,z)
    g.SetHistogram(h_parent)
    #h_colors = TH2F('a','b',100,min(x)-1,max(x)+1,100,min(y)-1,max(y)+1)
    h_colors = g.GetHistogram()

    c = TCanvas(histogram_title,histogram_title,0,0,800,600)
    #gPad.SetPadTopMargin(0.05)
    gPad.SetRightMargin(0.15)
    #gPad.SetPadBottomMargin(0.16)
    #gPad.SetPadLeftMargin(0.16)
    #gPad.SetTopMargin(0.07)
    #gPad.SetBottomMargin(0.120)
    #h_colors.GetYaxis().SetTitleOffset(1.65)
    #gPad.SetRightMargin(0.18)
    #h_colors.GetXaxis().SetTitleOffset(1.25)
    #h_colors.GetYaxis().SetTitleOffset(1.65)
    if 'DGnoslep' in folder:
        h_colors.GetXaxis().SetRangeUser(100, 500) #for the DGnoSL
        h_colors.GetYaxis().SetRangeUser(100, 500) #for the DGnoSL
    if 'GGM' in folder:
        h_colors.GetXaxis().SetRangeUser(200, 900)
    if 'N2N3Stau' in folder:
        h_colors.GetXaxis().SetRangeUser(100, 400)
    if 'N2N3Z' in folder:
        h_colors.GetXaxis().SetRangeUser(100, 500)
    if listItem>100:
        h_colors.SetAxisRange(0.,5.,'Z')
        #for i in range(0,h_colors.GetNbinsX()):
        #    for j in range(0,h_colors.GetNbinsY()):
        #        zContent=h_colors.GetBinContent(i,j)
        #        if abs(zContent)<.12 and zContent!=0.: h_colors.SetBinContent(i,j,-.01)
    elif listItem==32:
        h_colors.SetAxisRange(0.,100.,'Z')
    else: h_colors.SetAxisRange(0.,1.,'Z')
    ### Define color palette in order to paint 0 values white
    #aRed    = array('d',[0.,1.,1.])
    #aGreen  = array('d',[0.,1.,0.])
    #aBlue   = array('d',[1.,1.,0.])
    #aStops = array('d',[0.,.5,1.])
    #iContours=50
    #TColor.CreateGradientColorTable(3,aStops,aRed,aGreen,aBlue,iContours)
    #h_colors.SetContour(iContours)
    #lColors=[]
    #lContours=[]
    #for i in range(0,501):
    #    #print i, (i-250.)/50.
    #    if listItem>100: lContours.append((i-250.)/50.)
    #    else: lContours.append(i/500.)
    #    if   i== 250 and listItem>100: lColors.append(TColor.GetColor(1.,1.,1.))
    #    elif i <  10: lColors.append(TColor.GetColor(.45,0.,1.))
    #    elif i <  20: lColors.append(TColor.GetColor(.4,0.,1.))
    #    elif i <  30: lColors.append(TColor.GetColor(.35,0.,1.))
    #    elif i <  40: lColors.append(TColor.GetColor(.3,0.,1.))
    #    elif i <  50: lColors.append(TColor.GetColor(.25,0.,1.))
    #    elif i <  60: lColors.append(TColor.GetColor(.2,0.,1.))
    #    elif i <  70: lColors.append(TColor.GetColor(.15,0.,1.))
    #    elif i <  80: lColors.append(TColor.GetColor(.1,0.,1.))
    #    elif i <  90: lColors.append(TColor.GetColor(.05,0.,1.))
    #    elif i < 100: lColors.append(TColor.GetColor(0.,0.,1.))
    #    elif i < 110: lColors.append(TColor.GetColor(0.,.1,1.))
    #    elif i < 120: lColors.append(TColor.GetColor(0.,.2,1.))
    #    elif i < 130: lColors.append(TColor.GetColor(0.,.3,1.))
    #    elif i < 140: lColors.append(TColor.GetColor(0.,.4,1.))
    #    elif i < 150: lColors.append(TColor.GetColor(0.,.5,1.))
    #    elif i < 160: lColors.append(TColor.GetColor(0.,.6,1.))
    #    elif i < 170: lColors.append(TColor.GetColor(0.,.7,1.))
    #    elif i < 180: lColors.append(TColor.GetColor(0.,.8,1.))
    #    elif i < 190: lColors.append(TColor.GetColor(0.,.9,1.))
    #    elif i < 200: lColors.append(TColor.GetColor(0.,1.,1.))
    #    elif i < 210: lColors.append(TColor.GetColor(0.,1.,.9))
    #    elif i < 220: lColors.append(TColor.GetColor(0.,1.,.8))
    #    elif i < 230: lColors.append(TColor.GetColor(0.,1.,.7))
    #    elif i < 240: lColors.append(TColor.GetColor(0.,1.,.6))
    #    elif i < 250: lColors.append(TColor.GetColor(0.,1.,.5))
    #    elif i < 260: lColors.append(TColor.GetColor(0.,1.,.4))
    #    elif i < 270: lColors.append(TColor.GetColor(0.,1.,.3))
    #    elif i < 280: lColors.append(TColor.GetColor(0.,1.,.2))
    #    elif i < 290: lColors.append(TColor.GetColor(0.,1.,.1))
    #    elif i < 300: lColors.append(TColor.GetColor(0.,1.,0.))
    #    elif i < 310: lColors.append(TColor.GetColor(.1,1.,0.))
    #    elif i < 320: lColors.append(TColor.GetColor(.2,1.,0.))
    #    elif i < 330: lColors.append(TColor.GetColor(.3,1.,0.))
    #    elif i < 340: lColors.append(TColor.GetColor(.4,1.,0.))
    #    elif i < 350: lColors.append(TColor.GetColor(.5,1.,0.))
    #    elif i < 360: lColors.append(TColor.GetColor(.6,1.,0.))
    #    elif i < 370: lColors.append(TColor.GetColor(.7,1.,0.))
    #    elif i < 380: lColors.append(TColor.GetColor(.8,1.,0.))
    #    elif i < 390: lColors.append(TColor.GetColor(.9,1.,0.))
    #    elif i < 400: lColors.append(TColor.GetColor(1.,1.,0.))
    #    elif i < 410: lColors.append(TColor.GetColor(1.,.9,0.))
    #    elif i < 420: lColors.append(TColor.GetColor(1.,.8,0.))
    #    elif i < 430: lColors.append(TColor.GetColor(1.,.7,0.))
    #    elif i < 440: lColors.append(TColor.GetColor(1.,.6,0.))
    #    elif i < 450: lColors.append(TColor.GetColor(1.,.5,0.))
    #    elif i < 460: lColors.append(TColor.GetColor(1.,.4,0.))
    #    elif i < 470: lColors.append(TColor.GetColor(1.,.3,0.))
    #    elif i < 480: lColors.append(TColor.GetColor(1.,.2,0.))
    #    elif i < 490: lColors.append(TColor.GetColor(1.,.1,0.))
    #    else:         lColors.append(TColor.GetColor(1.,.0,0.))
    #aColors=array('i',lColors)
    #aContours=array('d',lContours)
    #gStyle.SetPalette(len(aColors),aColors)
    #h_colors.SetContour(len(aContours),aContours)

    #sigColorPalette(h_colors)
    h_colors.Draw('COLZ')

    latexAtlas = TLatex()
    latexAtlas.SetNDC()
    #latexAtlas.SetTextSize(.05)
    latexAtlas.SetTextFont(72)
    latexAtlas.DrawLatex(.19,.87,"ATLAS")
    #latexAtlas.SetTextSize(.04)
    latexAtlas.SetTextFont(52)
    latexAtlas.DrawLatex(.32,.87,"Internal")
    latexAtlas.SetTextSize(.032)
    latexAtlas.DrawLatex(.19,.79,"#int L dt = %s fb^{-1}, #sqrt{s} = 8 TeV" % '20.3')

    if 'L4' in folder and not 'GGM' in folder and not 'stop' in folder:
        triangle.Draw('lsame')
        diagonal.DrawLatex(x_text, y_text, 'm_{'+nlsp+'} < m_{#tilde{#chi}^{0}_{1}}')
    h_numbers.Draw('TEXT,SAME')
    if listItem>100:
        h_contour = h_colors.Clone()
        aExclusion=array('d',[TMath.NormQuantile(.95)])
        h_contour.SetContour(1,aExclusion)
        h_contour.Draw('CONT1,SAME')

    c.Print(output_file_name)

    if listItem==107:

        ### Write some statistics into a text file
        pointsBelow5=0
        #pointsBelow20=0
        #pointsBelow50=0
        totalSignif=0.
        totalPoints=0
        for element in z:
            if element<0: continue
            if element > TMath.NormQuantile(1-.05):
                pointsBelow5+=1
            totalSignif+=element
            totalPoints+=1
            #if element<=0.05:
            #    pointsBelow5+=1
            #    pointsBelow20+=1
            #    pointsBelow50+=1
            #elif element<=0.2:
            #    pointsBelow20+=1
            #    pointsBelow50+=1
            #elif element<=0.5:
            #    pointsBelow50+=1
            #total+=element
        if totalPoints>0: average=totalSignif/totalPoints
        else: average=0.

        with open(outFileStats, 'w') as fStats:
            fStats.write(listDict[listItem][1]+':\n')
            fStats.write('%-35s %4i \n' % ('Points above 1.64 sigma:', pointsBelow5))
            #fStats.write('%-35s %4i \n' % ('Points below 20 %:', pointsBelow20))
            #fStats.write('%-35s %4i \n' % ('Points below 50 %:', pointsBelow50))
            fStats.write('%-35s %4i \n' % ('Number of points:', totalPoints))
            fStats.write('%-35s %8.3f \n' % ('Total significance:', totalSignif))
            fStats.write('%-35s %8.3f \n\n\n' % ('Average significance per point:', average))

    del z,h_parent,h_numbers,g,h_colors,c,output_file_name,histogram_title

print 'Bye.'




















