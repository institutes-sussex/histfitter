import os,sys

fname = {}
entrynr = {}
# set file with data
fname["SMAwslep13TeV"]="data/C1N2_Slep_massInfo.txt"
fname["SMAwnoslep13TeV"]="data/C1N2_WZ_massInfo.txt"
fname["SMCwslep13TeV"]="data/C1C1_Slep_massInfo.txt"

# set position of x,y in line when .split() is done
entrynr["SMAwslep13TeV"]=[1,2]
entrynr["SMAwnoslep13TeV"]=[1,2]
entrynr["SMCwslep13TeV"]=[1,2]

def getCoordfromMCID(mcid,grid):

    try:
        fname[grid.strip("Cont")]
    except:
        print "Could not identify your grid ("+str(grid)+") Possibilities are:"
        print fname.keys()
        return (-1,-1)

    with open(fname[grid.strip("Cont")]) as dataf:
        for line in dataf:
            if len(line.split())==0: continue
            if line.split()[0]==mcid:
                #print "found",mcid,"coord:",line.split()[entrynr[grid][0]],line.split()[entrynr[grid][1]]
                return (line.split()[entrynr[grid.strip("Cont")][0]],line.split()[entrynr[grid.strip("Cont")][1]])
        print "Could not find mcid",mcid
    return (-1,-1)


def getXsecFromMasses(grid,ind=3):
    try:
        fname[grid]
    except:
        if not "Slepr" in grid:
            print "Could not identify your grid ("+str(grid)+") Possibilities are:"
            print fname.keys()
            return -1
    textf_name = fname[grid]
    with open(textf_name) as dataf:
        xsec_dict = {}
        for i,line in enumerate(dataf):
            if i==0 or not line.strip():
                continue
            try:
                xsec_dict[int(line.split()[entrynr[grid][0]]),int(line.split()[entrynr[grid][1]])]=eval(line.split()[ind])
            except:
                print "Possible issues with ints and floats at",line.split(),entrynr[grid]
                xsec_dict[float(line.split()[entrynr[grid][0]]),float(line.split()[entrynr[grid][1]])]=eval(line.split()[ind])
        return xsec_dict
