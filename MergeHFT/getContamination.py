# script to run before running the full 3l_config for the rpv
# this script should make a list with the points where the wm is 0
# it should also provide the strings for the systematics in this case
from ROOT import *
import os,sys,time
verbose = False

# to be run on your input data before the limit
def writeFileofWM0(inpath, srlist, gridlist):
    print "starting"
    if not inpath.endswith("/"):
        inpath+="/"
    f_bg = TFile(inpath+"BG8TeV.root")
    # get wm prediciton, tot_wm[sr]=n
    tot_wm={}
    for sr in srlist:
        if verbose:
            print "Looping WM in sr",sr
        t_wm = f_bg.Get("WM_CENTRAL")
        try:
            int(sr.strip("SR"))
        except:
            print "It seems the sr is odd",sr
        sr=int(sr.strip("SR"))
        tot_wm[sr]=0
        for entry in t_wm:
            if entry.SR==sr:
                tot_wm[sr]+=entry.eventweight
    if verbose:
        print "done, wm results",tot_wm

    # get dict of contamination, d[x,y]=n
    for grid in gridlist:
        print "Looping",grid
        with open(inpath+"info_crS_"+grid+".txt","w") as outf:
            for sr in srlist:
                print "Looping",sr
                try:
                    int(sr.strip("SR"))
                except:
                    print "It seems the sr is odd",sr
                sr=int(sr.strip("SR"))
                d = {}
                if not os.path.exists(inpath+"Cont"+grid+".root"):
                    print "did not find",inpath+"Cont"+grid+".root, skipping"
                f_cr = TFile(inpath+"Cont"+grid+".root")
                points = f_cr.GetListOfKeys()
                for akey in points:
                    t = f_cr.Get(akey.GetName())
                    x = akey.GetName().split("_")[1]
                    y = akey.GetName().split("_")[2]
                    tmp_tot=0
                    for entry in t:
                        if entry.SR==sr:
                            tmp_tot+=entry.eventweight
                    d[x,y]=tmp_tot
                    # write down findings for point
                    # write out file, x,y,sr,contaminated wm, wm.
                    print >> outf, x,y,sr,d[x,y],tot_wm[sr]
    print "Wrote",inpath+"info_crS_"+grid+".txt"
    print "done"
    return


# to be called from 3l_config. Will check:
# - whether the contaminated wm prediction < 0
# - if this is the case it will get the corresponding systematic
# function returns (status,syst):
# if contamination>wm => (1,wm_uncontamined for syst)
# if contamination<wm => everything is fine, return (-1, wm-cont)
# if no match found   => (-99,-99)
def checkContForPoint(x,y,sr,grid,inpath):
    if not inpath.endswith("/"):
        inpath+="/"
    try:
        int(sr.strip("SR"))
    except:
        print "It seems the sr is odd",sr
    sr = int(sr.strip("SR"))
    with open(inpath+"info_crS_"+grid+".txt","r") as inf:
        for line in inf:
            x_file = float(line.split()[0])
            y_file = float(line.split()[1])
            sr_file= int(line.split()[2])
            if float(x)==x_file and float(y)==y_file and sr==sr_file:
                if verbose:
                    print "found match for point",x,y
                wm_cont = float(line.split()[3])
                wm_uncont=float(line.split()[4])
                if wm_cont<0 or wm_cont==0:
                    if verbose:
                        print "got a 0 wm at sr",sr,"x",x,"y",y,"in",inpath+"info_crS_"+grid+".txt"
                    return (1,wm_uncont)
                else:
                    if verbose:
                        print "wm>cont at sr",sr,"x",x,"y",y,"in, cn =", (wm_uncont-wm_cont)/wm_cont
                    return (-1,(wm_uncont-wm_cont)/wm_cont)

    print "I did not find a match for sr",sr,"x",x,"y",y,"in",inpath+"info_crS_"+grid+".txt"
    return (-99,-99)



if __name__ == "__main__":
    print "Script to manage external part of signal contamination in 4l cr"
    if not len(sys.argv)>2:
        print "arg1 is the inpath, e.g. /ngse5/atlas/HistFitterTree/Summer2013/L4/data14/"
        print "arg2 is the list of grids, e.g. Winol1218TeV_Winol1228TeV"
        sys.exit()
    inpath_input = sys.argv[1]
    gridlist_input = sys.argv[2]
    if not inpath_input.endswith("/"):
        inpath_input+="/"
    gridlist = gridlist_input.split("_")
    print "I will run:"
    print "writeFileofWM0('"+inpath_input+"', ['SR400','SR401','SR402','SR403','SR404','SR405','SR406','SR407','SR408'], "+str(gridlist)+")"
    print "If this is not what you intended to do, exit manually and use the functions interactively"
    time.sleep(3)
    writeFileofWM0(inpath_input, ['SR400','SR401','SR402','SR403','SR404','SR405','SR406','SR407','SR408'], gridlist)
    print "done"


