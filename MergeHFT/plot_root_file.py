from ROOT import *
from array import array
import time
import sys,os

gROOT.SetBatch(True)
gROOT.LoadMacro("./macros/AtlasStyle.C")
SetAtlasStyle()

doDebug = False

detailedBGOutput = False
BGOutputCoordinates = [0.79,0.955]
bgText = False

folder=sys.argv[-2]
srs=sys.argv[-1].split('_')

path = 'output/'+folder+'/data/3L.root'


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False




x_axis = 'm_{#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}} [GeV]'
y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'

outFile = 'output/'+folder+'/nSig/'+'_'.join(folder.split('_')[2:])
try: os.remove(outFile+'_nSig.txt')
except: pass

#gStyle.SetOptStat(00000000)
gStyle.SetPaintTextFormat('5.2f')
gStyle.SetPalette(1)

listFile=[]
#f = open(path, 'r')
#lines = f.readlines()
#for line in lines:
#  listFile.append(line)
#f.close()

f = TFile(path)

### Loop over signal regions and then over bins in a signal region.
### First bin is always underflow, last bin overflow
for sr in srs:
    srBinMax=f.Get(f.GetListOfKeys()[0].GetName()).GetNbinsX()+2
    for srBin in range(srBinMax):
        x=array('f')
        y=array('f')
        z=array('f')
        bgs=[]
        if doDebug: print 'sr = '+sr
        for key in f.GetListOfKeys():
            if 'hData' in key.GetName() or 'hTopLvlXML' in key.GetName() or not sr in key.GetName() or "Cont" in key.GetName(): continue
            if doDebug: print 'key =',key
            # Skip systematics
            if 'High' in key.GetName() or 'Low' in key.GetName(): continue
            xVal=key.GetName().split('_')[1]
            yVal=key.GetName().split('_')[2].rstrip('Nom')
            zVal=f.Get(key.GetName()).GetBinContent(srBin)
            f.Get(key.GetName()).GetBinContent(srBin)
            if is_number(xVal) and is_number(yVal):
                if not zVal==0:
                    x.append(float(xVal))
                    y.append(float(yVal))
                    z.append(float(zVal))
            else:
                bgs.append(key.GetName())
        if len(x)==0: continue

        ### bgArr = [[name of bg, prediction of bg, error up of bg, error down of bg],
        ###          [name of other bg, prediction of other bg], ...]
        bgArr=[]
        for bg in bgs:
            bgArr.append([bg.split('_')[0].strip('hNom'),f.Get(bg).GetBinContent(srBin),f.Get(bg).GetBinErrorUp(srBin),f.Get(bg).GetBinErrorLow(srBin)])
        bgTotal=0
        bgTexts=[]
        for bg in bgArr:
            bgName=bg[0]
            bgValue=bg[1]
            bgTotal+=bgValue
            if detailedBGOutput: bgTexts.append(bgName+': '+str("%0.3f" % bgValue))
            if doDebug: print 'bgName: '+bgName+' - bgValue: '+str(bgValue)
        if doDebug: print 'bgTotal: '+str(bgTotal)
        bgTexts.append('BG: '+str("%0.3f" % bgTotal))

        if doDebug: print 'bgTexts:',bgTexts

        if doDebug:
            for i,e in enumerate(x):
                print 'x: '+str(x[i])+' - y: '+str(y[i])+' - z: '+str(z[i])

        if 'L4' in folder and not 'stop' in folder:
            infile_ut = TFile('tools/upperTriangle.root','READ')
            triangle = TGraph(infile_ut.Get('upperTriangle'))
            triangle.SetLineStyle(10)
            triangle.SetLineWidth(3)
            triangle.SetLineColor(14)

            diagonal = TLatex()
            diagonal.SetTextSize(0.050)
            diagonal.SetTextColor(15)
            diagonal.SetTextFont(42)

            nlsp = 'NLSP'
 
            x_central = 0.5 * (max(x) + min(x))
            width = max(x)-min(x)
            x_text = x_central - 0.1 * width
            y_range = max(y) - min(y)
            y_text = x_text + 0.07 * y_range
            angle = TMath.RadToDeg() * TMath.ATan(width/y_range)
            diagonal.SetTextAngle(angle)

        if srBin==0: srBinText='underflow'
        elif srBin==srBinMax: srBinText='overflow'
        else: srBinText='bin_'+str(srBin)

        ### Get the values from the dictionaries
        output_file_name    = outFile+'_'+sr+'_'+str(srBin)+'_nSig.eps'
        histogram_title     = 'nSig_'+sr
        ### Fill the z values into an array
        ### Parent histogram out of which we clone a histogram for printing the numbers (not interpolated) and one for the colors (interpolated)
        h_parent = TH2F(histogram_title,histogram_title,100,min(x)-1,max(x)+1,100,min(y)-1,max(y)+1)
        h_parent.SetXTitle(x_axis)
        h_parent.SetYTitle(y_axis)
        h_parent.SetZTitle('N')
        h_numbers = h_parent.Clone()
        for i,element in enumerate(x):
            h_numbers.Fill(x[i],y[i],z[i])
        ### First define graph and then derive histogram from graph. This way the the colors between the points are interpolated
        g = TGraph2D(len(x),x,y,z)
        g.SetHistogram(h_parent)
        h_colors = g.GetHistogram()
        c = TCanvas(histogram_title,histogram_title,0,0,800,600)
        #gPad.SetTopMargin(0.07)
        #gPad.SetBottomMargin(0.120)
        #gPad.SetRightMargin(0.10)
        #gPad.SetLeftMargin(0.12)
        #h_colors.GetXaxis().SetTitleOffset(1.25)
        #h_colors.GetYaxis().SetTitleOffset(1.65)
        gPad.SetRightMargin(0.15)
        h_colors.SetAxisRange(0.1,1000.,'Z')
        gPad.SetLogz()

        #sigColorPalette(h_colors)
        h_colors.Draw('COLZ')

        latexAtlas = TLatex()
        latexAtlas.SetNDC()
        #latexAtlas.SetTextSize(.05)
        latexAtlas.SetTextFont(72)
        latexAtlas.DrawLatex(.19,.87,"ATLAS")
        #latexAtlas.SetTextSize(.04)
        latexAtlas.SetTextFont(52)
        latexAtlas.DrawLatex(.32,.87,"Internal")
        latexAtlas.SetTextSize(.032)
        latexAtlas.DrawLatex(.19,.79,"#int L dt = %s fb^{-1}, #sqrt{s} = 8 TeV" % '20.3')

        if 'L4' in folder and not 'stop' in folder:
            triangle.Draw('lsame')
            diagonal.DrawLatex(x_text, y_text, 'm_{'+nlsp+'} < m_{#tilde{#chi}^{0}_{1}}')
        if bgText:
            bgTextLatex = TLatex()
            bgTextLatex.SetNDC()
            bgTextLatex.SetTextSize(0.022)
            bgTextLatex.SetTextFont(42)
            for i,bgText in enumerate(bgTexts):
                bgTextLatex.DrawLatex(BGOutputCoordinates[0],BGOutputCoordinates[1]-i*0.023,bgText)
        h_numbers.Draw('TEXT,SAME')

        c.Print(output_file_name)

        ### Print the info also in a text file
        with open(outFile+'_nBg.txt','a') as txtf:
            txtf.write('Background prediction for '+sr+', '+srBinText+':\n'+'-'*45+'\n')
            for bg in bgArr: txtf.write('%-15s %9.3f + %9.3f - %9.3f\n' % (bg[0],bg[1],bg[2],bg[3]))
            txtf.write('%-15s %9.3f \n\n' % ('Total', bgTotal))
            txtf.write('\n'+'='*45+'\n\n')
        with open(outFile+'_nBg_latex.txt','a') as txtf:
            txtf.write('\\tiny\n')
            txtf.write('\\begin{tabular}{lrcr}\n')
            for bg in bgArr: txtf.write('%s & %.3f & $\pm$ & %.3f \\\\ \n' % (bg[0],bg[1],bg[2]))
            txtf.write('%s & %.3f \\\\ \n' % ('Total', bgTotal))
            txtf.write('\\end{tabular}')
        with open(outFile+'_nSig.txt','a') as txtf:
            txtf.write('Background prediction for '+sr+', '+srBinText+':\n'+'-'*45+'\n')
            for bg in bgArr: txtf.write('%-15s %9.3f + %9.3f - %9.3f\n' % (bg[0],bg[1],bg[2],bg[3]))
            txtf.write('%-15s %9.3f \n\n' % ('Total', bgTotal))
            txtf.write('Signal prediction for '+sr+', '+srBinText+':\n'+'-'*45+'\n')
            txtf.write('%7s %7s %10s \n' % ('x','y','events'))
            for i,e in enumerate(x): txtf.write('%7s %7s %9.3f \n' % (x[i],y[i],z[i]))
            txtf.write('\n'+'='*45+'\n\n')

        del z,h_parent,h_numbers,g,h_colors,c,output_file_name,histogram_title

print outFile+'_'+sr+'_nSig.txt written.'
print outFile+'_'+sr+'_nBg.txt written.'
print outFile+'_'+sr+'_nBg_latex.txt written.'

f.Close()


print 'Bye.'




















