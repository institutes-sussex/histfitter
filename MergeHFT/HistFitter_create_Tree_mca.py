from ROOT import *
from getCoordfromMCID import getCoordfromMCID
import sys,os.path

'''
Adapted by N. Santoyo from B. Schneider script in Run1

Usage: python HistFitter_create_Tree.py L3_BG13TeV_e
'''

### This list is not used, it's just for bookkeeping
grids=[  'L3_BG13TeV_e','L3_FakesMM13TeV','L3_BG13TeV_d', 'L3_BG13TeV_e','L_DATA13TeV','L3_SMAwslep13TeV', 'L3_SMAwnoslep13TeV','L3_SMWh13TeV_a','L3_SMWZ13TeV_a','L2_BG13TeV','L2_SMCwslep13TeV'    ]

if len(sys.argv)==2:
    grid=sys.argv[1]
else:
    print 'Need an argument which grid. Options are',grids
    print 'Exit.'
    sys.exit()

# isantoyo >> specify the path to your hadded trees per sys, and the outtput path (which can be the same)


#path="/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/outputs_JER/Trees_mce/"
path="/mnt/lustre/scratch/epp_test/ft81/outputs_mcx_JES30/Trees_mca/"
#path="/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/outputs_JER_Wh/Trees_mce/"

#path="/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/outputs_mcx/Trees_mce/"
#path="/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/outputs_Wh/Trees_mce/"

# if 'L2_' in grid:
#     #path="/lustre/scratch/epp/atlas/is86/Run2/Trees/SUSYApproval/2L/"
#     #path="/lustre/scratch/epp/atlas/is86/Run2/Trees/SUSYApproval/sigmaBand/"
#     path="/home/i/is86/2013_analysis/UCI_n0145/macros/Run2/HistFitterTreeMaking/outputs/Trees/"
# elif 'L3_' in grid:
#     #path="/lustre/scratch/epp/atlas/is86/Run2/Trees/SUSYApproval/3L/"
#     path="/home/i/is86/2013_analysis/UCI_n0145/macros/Run2/HistFitterTreeMaking/outputs/Trees/"

#output_file='/lustre/scratch/epp/atlas/is86/Run2/Limits_Moriond2017/Trees/'+grid+'.root'
output_file='/mnt/lustre/scratch/epp_test/ft81/HFtrees/'+grid+'.root' #lxplus
#if "L_DATA13TeV" in grid : output_file='/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_data/'+grid+'.root' #lxplus


f_out2 = TFile(output_file,'RECREATE')
f_out2.Close()
print "preparing output file"


if not path.endswith("/"):
    path+=("/")

systematicsMC               = [
    'CENTRAL',
#    'EG_SCALE_ALL_UP','EG_SCALE_ALL_DN', # negligible
#    'JER_DataVsMC_MC16_UP','JER_DataVsMC_MC16_DN',
#    'JER_EffectiveNP_1_UP','JER_EffectiveNP_1_DN',
#    'JER_EffectiveNP_2_UP','JER_EffectiveNP_2_DN',
#    'JER_EffectiveNP_3_UP','JER_EffectiveNP_3_DN',
#    'JER_EffectiveNP_4_UP','JER_EffectiveNP_4_DN',
#    'JER_EffectiveNP_5_UP','JER_EffectiveNP_5_DN',
#    'JER_EffectiveNP_6_UP','JER_EffectiveNP_6_DN',
#    'JER_EffectiveNP_7restTerm_UP','JER_EffectiveNP_7restTerm_DN',
#    'JET_GroupedNP_1_UP','JET_GroupedNP_1_DN', #negligible
#    'JET_GroupedNP_2_UP','JET_GroupedNP_2_DN',
#    'JET_GroupedNP_3_UP','JET_GroupedNP_3_DN',
#    'MET_SoftTrk_ResoPara','MET_SoftTrk_ResoPerp',
#    'MET_SoftTrk_ScaleUp','MET_SoftTrk_ScaleDown', # negligible
#    'MUON_ID_UP','MUON_ID_DN',
#    'MUON_MS_UP','MUON_MS_DN',
#    'MUON_SCALE_UP','MUON_SCALE_DN', # negligible in SR
#    'MUON_SAGITTA_RESBIAS_UP','MUON_SAGITTA_RESBIAS_DN', # negligible in SR
#    'MUON_SAGITTA_RHO_UP','MUON_SAGITTA_RHO_DN',

#    'PRW_DATASF_DN','PRW_DATASF_UP',
#'''
        'EG_SCALE_ALL_DN','EG_SCALE_ALL_UP',
        'JET_BJES_Response_DN','JET_BJES_Response_UP',
        'JET_EffectiveNP_Detector1_DN','JET_EffectiveNP_Detector1_UP',
        'JET_EffectiveNP_Detector2_DN','JET_EffectiveNP_Detector2_UP',
        'JET_EffectiveNP_Mixed1_DN','JET_EffectiveNP_Mixed1_UP',
        'JET_EffectiveNP_Mixed2_DN','JET_EffectiveNP_Mixed2_UP',
        'JET_EffectiveNP_Mixed3_DN','JET_EffectiveNP_Mixed3_UP',
        'JET_EffectiveNP_Modelling1_DN','JET_EffectiveNP_Modelling1_UP',
        'JET_EffectiveNP_Modelling2_DN','JET_EffectiveNP_Modelling2_UP',
        'JET_EffectiveNP_Modelling3_DN','JET_EffectiveNP_Modelling3_UP',
        'JET_EffectiveNP_Modelling4_DN','JET_EffectiveNP_Modelling4_UP',
        'JET_EffectiveNP_Statistical1_DN','JET_EffectiveNP_Statistical1_UP',
        'JET_EffectiveNP_Statistical2_DN','JET_EffectiveNP_Statistical2_UP',
        'JET_EffectiveNP_Statistical3_DN','JET_EffectiveNP_Statistical3_UP',
        'JET_EffectiveNP_Statistical4_DN','JET_EffectiveNP_Statistical4_UP',
        'JET_EffectiveNP_Statistical5_DN','JET_EffectiveNP_Statistical5_UP',
        'JET_EffectiveNP_Statistical6_DN','JET_EffectiveNP_Statistical6_UP',
        'JET_EtaIntercalibration_Modelling_DN','JET_EtaIntercalibration_Modelling_UP',
        'JET_EtaIntercalibration_NonClosure_2018data_DN','JET_EtaIntercalibration_NonClosure_2018data_UP',
        'JET_EtaIntercalibration_NonClosure_highE_DN','JET_EtaIntercalibration_NonClosure_highE_UP',
        'JET_EtaIntercalibration_NonClosure_negEta_DN','JET_EtaIntercalibration_NonClosure_negEta_UP',
        'JET_EtaIntercalibration_NonClosure_posEta_DN','JET_EtaIntercalibration_NonClosure_posEta_UP',
        'JET_EtaIntercalibration_TotalStat_DN','JET_EtaIntercalibration_TotalStat_UP',
        'JET_Flavor_Composition_DN','JET_Flavor_Composition_UP',
        'JET_Flavor_Response_DN','JET_Flavor_Response_UP',
        'JET_PunchThrough_AFII_DN','JET_PunchThrough_AFII_UP',
	'JET_RelativeNonClosure_AFII_DN','JET_RelativeNonClosure_AFII_UP',
	'JET_JER_DataVsMC_MC16_DN','JET_JER_DataVsMC_MC16_UP',
	'JET_JER_DataVsMC_AFII_DN', 'JET_JER_DataVsMC_AFII_UP',
        'JET_JER_EffectiveNP_1_DN','JET_JER_EffectiveNP_1_UP',
        'JET_JER_EffectiveNP_2_DN','JET_JER_EffectiveNP_2_UP',
        'JET_JER_EffectiveNP_3_DN','JET_JER_EffectiveNP_3_UP',
        'JET_JER_EffectiveNP_4_DN','JET_JER_EffectiveNP_4_UP',
        'JET_JER_EffectiveNP_5_DN','JET_JER_EffectiveNP_5_UP',
        'JET_JER_EffectiveNP_6_DN','JET_JER_EffectiveNP_6_UP',
        'JET_JER_EffectiveNP_7restTerm_DN','JET_JER_EffectiveNP_7restTerm_UP',
        'JET_Pileup_OffsetMu_DN','JET_Pileup_OffsetMu_UP',
        'JET_Pileup_OffsetNPV_DN','JET_Pileup_OffsetNPV_UP',
        'JET_Pileup_PtTerm_DN','JET_Pileup_PtTerm_UP',
        'JET_Pileup_RhoTopology_DN','JET_Pileup_RhoTopology_UP',
        'JET_SingleParticle_HighPt_DN','JET_SingleParticle_HighPt_UP',
        'MET_SoftTrk_ResoPara','MET_SoftTrk_ResoPerp',
        'MET_SoftTrk_ScaleDown','MET_SoftTrk_ScaleUp',
        'MUON_ID_DN','MUON_ID_UP',
        'MUON_MS_DN','MUON_MS_UP',
        'MUON_SAGITTA_RESBIAS_DN','MUON_SAGITTA_RESBIAS_UP',
        'MUON_SAGITTA_RHO_DN','MUON_SAGITTA_RHO_UP',
        'MUON_SCALE_DN','MUON_SCALE_UP',
    ]



systematicsSignal           = systematicsMC
systematicsData             = ['CENTRAL']

processesMC                 = {}
processesSignal             = {}
processesData               = {}




#----------------------------------------------------------------------------------------------------


if 'L3_otherBG13TeV' in grid:
    processesMC['ttvNLO']    = ['id_410155',                        'id_410081','id_410218','id_410219','id_410220',                                     'id_410050']
    processesMC['higgs4L']        = [ 'id_341471'  ] #complete

if 'L3_FakesMM13TeV' in grid:
    processesData['Fakes']    = ['id_Fakes']

if 'L3_BG13TeV_e' in grid:

    #processesMC['WZ0j']     = ['id_0jWZ']
    #processesMC['WZ1j']     = ['id_1jWZ']
    processesMC['Dibosons_2L_e']       = ['id_Dibosons_2L_e']
    processesMC['Dibosons_3L_nJ0_e']   = ['id_Dibosons_3L_nJ0_e']
    processesMC['Dibosons_3L_LowHT_e']  = ['id_Dibosons_3L_LowHT_e']
    processesMC['Dibosons_3L_HighHT_e']  = ['id_Dibosons_3L_HightHT_e']
    processesMC['Dibosons_4L_e']       = ['id_Dibosons_4L_e']
#    processesMC['Dibosons_3L_LowHT_d']       = ['id_Dibosons_3L_LowHT_d']
#    processesMC['Dibosons_4L_d']       = ['id_Dibosons_4L_d']
#    processesMC['Dibosons_3L_LowHT_e']       = ['id_Dibosons_3L_LowHT_e']
#    processesMC['Dibosons_4L_e']       = ['id_Dibosons_4L_e']
#    processesMC['Zjets_e']       = ['id_ZJets_e']
    processesMC['ttV_e']      = ['id_ttV_e']
    processesMC['VVV_e']      = ['id_VVV_e']
    processesMC['Higgs_e']    = ['id_Higgs_e']
#    processesMC['Wjets']    = ['id_Wjets']
#    processesMC['Zjets']    = ['id_Zjets']
#    processesMC['Vgamma_e']   = ['id_Vgamma_e']
    processesMC['ttbar_e']    = ['id_ttbar_e']
    processesMC['SingleT_e']  = ['id_SingleT_e']
#    processesMC['MultiT']   = ['id_multitop']
#    processesMC['MCfakes']  = ['id_MCfakes']    

#----------------------------------------------------------------------------------------------------
if 'L_DATA13TeV' in grid:
	processesData['Data']      = ['id_2018']





#----------------------------------------------------------------------------------------------------
# -------------------------- Specify the MCid for each signal grid-----------------------------------
#----------------------------------------------------------------------------------------------------

if 'L3_SMWh13TeV_a' in grid:
	processesSignal['SMAwh13TeV_150_0_a'] = ['id_Wh_150_0_a']
	processesSignal['SMAwh13TeV_152_22_a'] = ['id_Wh_152_22_a']
	processesSignal['SMAwh13TeV_162_12_a'] = ['id_Wh_162_12_a']
	processesSignal['SMAwh13TeV_165_35_a'] = ['id_Wh_165_35_a']
	processesSignal['SMAwh13TeV_175_0_a'] = ['id_Wh_175_0_a']
	processesSignal['SMAwh13TeV_175_25_a'] = ['id_Wh_175_25_a']
	processesSignal['SMAwh13TeV_177_47_a'] = ['id_Wh_177_47_a']
	processesSignal['SMAwh13TeV_187_37_a'] = ['id_Wh_187_37_a']
	processesSignal['SMAwh13TeV_190_60_a'] = ['id_Wh_190_60_a']
	processesSignal['SMAwh13TeV_200_25_a'] = ['id_Wh_200_25_a']
	processesSignal['SMAwh13TeV_200_50_a'] = ['id_Wh_200_50_a']
	processesSignal['SMAwh13TeV_202_72_a'] = ['id_Wh_202_72_a']
	processesSignal['SMAwh13TeV_212_37_a'] = ['id_Wh_212_37_a']
	processesSignal['SMAwh13TeV_212_62_a'] = ['id_Wh_212_62_a']
	processesSignal['SMAwh13TeV_225_0_a'] = ['id_Wh_225_0_a']
	processesSignal['SMAwh13TeV_225_25_a'] = ['id_Wh_225_25_a']
	processesSignal['SMAwh13TeV_225_50_a'] = ['id_Wh_225_50_a']
	processesSignal['SMAwh13TeV_225_75_a'] = ['id_Wh_225_75_a']
	processesSignal['SMAwh13TeV_237_62_a'] = ['id_Wh_237_62_a']
	processesSignal['SMAwh13TeV_250_0_a'] = ['id_Wh_250_0_a']
	processesSignal['SMAwh13TeV_250_100_a'] = ['id_Wh_250_100_a']
	processesSignal['SMAwh13TeV_250_25_a'] = ['id_Wh_250_25_a']
	processesSignal['SMAwh13TeV_250_50_a'] = ['id_Wh_250_50_a']
	processesSignal['SMAwh13TeV_250_75_a'] = ['id_Wh_250_75_a']
	processesSignal['SMAwh13TeV_275_0_a'] = ['id_Wh_275_0_a']
	processesSignal['SMAwh13TeV_275_25_a'] = ['id_Wh_275_25_a']
	processesSignal['SMAwh13TeV_275_50_a'] = ['id_Wh_275_50_a']
	processesSignal['SMAwh13TeV_275_75_a'] = ['id_Wh_275_75_a']
	processesSignal['SMAwh13TeV_300_0_a'] = ['id_Wh_300_0_a']
	processesSignal['SMAwh13TeV_300_100_a'] = ['id_Wh_300_100_a']
	processesSignal['SMAwh13TeV_300_25_a'] = ['id_Wh_300_25_a']
	processesSignal['SMAwh13TeV_300_50_a'] = ['id_Wh_300_50_a']
	processesSignal['SMAwh13TeV_300_75_a'] = ['id_Wh_300_75_a']
	processesSignal['SMAwh13TeV_325_0_a'] = ['id_Wh_325_0_a']
	processesSignal['SMAwh13TeV_325_50_a'] = ['id_Wh_325_50_a']
	processesSignal['SMAwh13TeV_350_0_a'] = ['id_Wh_350_0_a']
	processesSignal['SMAwh13TeV_350_100_a'] = ['id_Wh_350_100_a']
	processesSignal['SMAwh13TeV_350_25_a'] = ['id_Wh_350_25_a']
	processesSignal['SMAwh13TeV_350_50_a'] = ['id_Wh_350_50_a']
	processesSignal['SMAwh13TeV_350_75_a'] = ['id_Wh_350_75_a']
	processesSignal['SMAwh13TeV_375_0_a'] = ['id_Wh_375_0_a']
	processesSignal['SMAwh13TeV_375_50_a'] = ['id_Wh_375_50_a']
	processesSignal['SMAwh13TeV_400_0_a'] = ['id_Wh_400_0_a']
	processesSignal['SMAwh13TeV_400_25_a'] = ['id_Wh_400_25_a']
	processesSignal['SMAwh13TeV_425_0_a'] = ['id_Wh_425_0_a']

if 'L3_SMWZ13TeV_a' in grid:
	processesSignal['SMAwz13TeV_100_0_a'] = ['id_WZ_100_0_a']
	processesSignal['SMAwz13TeV_100_10_a'] = ['id_WZ_100_10_a']
	processesSignal['SMAwz13TeV_100_20_a'] = ['id_WZ_100_20_a']
	processesSignal['SMAwz13TeV_100_30_a'] = ['id_WZ_100_30_a']
	processesSignal['SMAwz13TeV_100_40_a'] = ['id_WZ_100_40_a']
	processesSignal['SMAwz13TeV_100_60_a'] = ['id_WZ_100_60_a']
	processesSignal['SMAwz13TeV_100_80_a'] = ['id_WZ_100_80_a']
	processesSignal['SMAwz13TeV_100_90_a'] = ['id_WZ_100_90_a']
	processesSignal['SMAwz13TeV_110_20_a'] = ['id_WZ_110_20_a']
	processesSignal['SMAwz13TeV_110_30_a'] = ['id_WZ_110_30_a']
	processesSignal['SMAwz13TeV_110_40_a'] = ['id_WZ_110_40_a']
	processesSignal['SMAwz13TeV_110_50_a'] = ['id_WZ_110_50_a']
	processesSignal['SMAwz13TeV_110_60_a'] = ['id_WZ_110_60_a']
	processesSignal['SMAwz13TeV_110_70_a'] = ['id_WZ_110_70_a']
	processesSignal['SMAwz13TeV_110_85_a'] = ['id_WZ_110_85_a']
	processesSignal['SMAwz13TeV_110_95_a'] = ['id_WZ_110_95_a']
	processesSignal['SMAwz13TeV_125_100_a'] = ['id_WZ_125_100_a']
	processesSignal['SMAwz13TeV_125_110_a'] = ['id_WZ_125_110_a']
	processesSignal['SMAwz13TeV_125_115_a'] = ['id_WZ_125_115_a']
	processesSignal['SMAwz13TeV_125_35_a'] = ['id_WZ_125_35_a']
	processesSignal['SMAwz13TeV_125_45_a'] = ['id_WZ_125_45_a']
	processesSignal['SMAwz13TeV_125_55_a'] = ['id_WZ_125_55_a']
	processesSignal['SMAwz13TeV_125_65_a'] = ['id_WZ_125_65_a']
	processesSignal['SMAwz13TeV_125_85_a'] = ['id_WZ_125_85_a']
	processesSignal['SMAwz13TeV_140_100_a'] = ['id_WZ_140_100_a']
	processesSignal['SMAwz13TeV_140_115_a'] = ['id_WZ_140_115_a']
	processesSignal['SMAwz13TeV_140_125_a'] = ['id_WZ_140_125_a']
	processesSignal['SMAwz13TeV_140_130_a'] = ['id_WZ_140_130_a']
	processesSignal['SMAwz13TeV_140_50_a'] = ['id_WZ_140_50_a']
	processesSignal['SMAwz13TeV_140_60_a'] = ['id_WZ_140_60_a']
	processesSignal['SMAwz13TeV_140_70_a'] = ['id_WZ_140_70_a']
	processesSignal['SMAwz13TeV_140_80_a'] = ['id_WZ_140_80_a']
	processesSignal['SMAwz13TeV_150_110_a'] = ['id_WZ_150_110_a']
	processesSignal['SMAwz13TeV_150_130_a'] = ['id_WZ_150_130_a']
	processesSignal['SMAwz13TeV_150_140_a'] = ['id_WZ_150_140_a']
	processesSignal['SMAwz13TeV_150_1_a'] = ['id_WZ_150_1_a']
	processesSignal['SMAwz13TeV_150_50_a'] = ['id_WZ_150_50_a']
	processesSignal['SMAwz13TeV_150_60_a'] = ['id_WZ_150_60_a']
	processesSignal['SMAwz13TeV_150_70_a'] = ['id_WZ_150_70_a']
	processesSignal['SMAwz13TeV_150_80_a'] = ['id_WZ_150_80_a']
	processesSignal['SMAwz13TeV_150_90_a'] = ['id_WZ_150_90_a']
	processesSignal['SMAwz13TeV_200_100_a'] = ['id_WZ_200_100_a']
	processesSignal['SMAwz13TeV_200_105_a'] = ['id_WZ_200_105_a']
	processesSignal['SMAwz13TeV_200_108_a'] = ['id_WZ_200_108_a']
	processesSignal['SMAwz13TeV_200_110_a'] = ['id_WZ_200_110_a']
	processesSignal['SMAwz13TeV_200_120_a'] = ['id_WZ_200_120_a']
	processesSignal['SMAwz13TeV_200_130_a'] = ['id_WZ_200_130_a']
	processesSignal['SMAwz13TeV_200_140_a'] = ['id_WZ_200_140_a']
	processesSignal['SMAwz13TeV_200_160_a'] = ['id_WZ_200_160_a']
	processesSignal['SMAwz13TeV_200_180_a'] = ['id_WZ_200_180_a']
	processesSignal['SMAwz13TeV_200_190_a'] = ['id_WZ_200_190_a']
	processesSignal['SMAwz13TeV_200_1_a'] = ['id_WZ_200_1_a']
	processesSignal['SMAwz13TeV_200_50_a'] = ['id_WZ_200_50_a']
	processesSignal['SMAwz13TeV_250_100_a'] = ['id_WZ_250_100_a']
	processesSignal['SMAwz13TeV_250_150_a'] = ['id_WZ_250_150_a']
	processesSignal['SMAwz13TeV_250_155_a'] = ['id_WZ_250_155_a']
	processesSignal['SMAwz13TeV_250_158_a'] = ['id_WZ_250_158_a']
	processesSignal['SMAwz13TeV_250_160_a'] = ['id_WZ_250_160_a']
	processesSignal['SMAwz13TeV_250_170_a'] = ['id_WZ_250_170_a']
	processesSignal['SMAwz13TeV_250_190_a'] = ['id_WZ_250_190_a']
	processesSignal['SMAwz13TeV_250_1_a'] = ['id_WZ_250_1_a']
	processesSignal['SMAwz13TeV_250_210_a'] = ['id_WZ_250_210_a']
	processesSignal['SMAwz13TeV_250_230_a'] = ['id_WZ_250_230_a']
	processesSignal['SMAwz13TeV_250_240_a'] = ['id_WZ_250_240_a']
	processesSignal['SMAwz13TeV_250_50_a'] = ['id_WZ_250_50_a']
	processesSignal['SMAwz13TeV_300_100_a'] = ['id_WZ_300_100_a']
	processesSignal['SMAwz13TeV_300_205_a'] = ['id_WZ_300_205_a']
	processesSignal['SMAwz13TeV_300_208_a'] = ['id_WZ_300_208_a']
	processesSignal['SMAwz13TeV_300_150_a'] = ['id_WZ_300_150_a']
	processesSignal['SMAwz13TeV_300_200_a'] = ['id_WZ_300_200_a']
	processesSignal['SMAwz13TeV_300_220_a'] = ['id_WZ_300_220_a']
	processesSignal['SMAwz13TeV_300_240_a'] = ['id_WZ_300_240_a']
	processesSignal['SMAwz13TeV_300_260_a'] = ['id_WZ_300_260_a']
	processesSignal['SMAwz13TeV_300_280_a'] = ['id_WZ_300_280_a']
	processesSignal['SMAwz13TeV_300_290_a'] = ['id_WZ_300_290_a']
	processesSignal['SMAwz13TeV_300_50_a'] = ['id_WZ_300_50_a']
	processesSignal['SMAwz13TeV_350_0_a'] = ['id_WZ_350_0_a']
	processesSignal['SMAwz13TeV_350_100_a'] = ['id_WZ_350_100_a']
	processesSignal['SMAwz13TeV_350_150_a'] = ['id_WZ_350_150_a']
	processesSignal['SMAwz13TeV_350_200_a'] = ['id_WZ_350_200_a']
	processesSignal['SMAwz13TeV_350_250_a'] = ['id_WZ_350_250_a']
	processesSignal['SMAwz13TeV_350_270_a'] = ['id_WZ_350_270_a']
	processesSignal['SMAwz13TeV_350_290_a'] = ['id_WZ_350_290_a']
	processesSignal['SMAwz13TeV_350_310_a'] = ['id_WZ_350_310_a']
	processesSignal['SMAwz13TeV_350_330_a'] = ['id_WZ_350_330_a']
	processesSignal['SMAwz13TeV_350_340_a'] = ['id_WZ_350_340_a']
	processesSignal['SMAwz13TeV_350_50_a'] = ['id_WZ_350_50_a']
	processesSignal['SMAwz13TeV_400_0_a'] = ['id_WZ_400_0_a']
	processesSignal['SMAwz13TeV_400_100_a'] = ['id_WZ_400_100_a']
	processesSignal['SMAwz13TeV_400_200_a'] = ['id_WZ_400_200_a']
	processesSignal['SMAwz13TeV_400_250_a'] = ['id_WZ_400_250_a']
	processesSignal['SMAwz13TeV_400_300_a'] = ['id_WZ_400_300_a']
	processesSignal['SMAwz13TeV_400_320_a'] = ['id_WZ_400_320_a']
	processesSignal['SMAwz13TeV_400_340_a'] = ['id_WZ_400_340_a']
	processesSignal['SMAwz13TeV_400_360_a'] = ['id_WZ_400_360_a']
	processesSignal['SMAwz13TeV_400_380_a'] = ['id_WZ_400_380_a']
	processesSignal['SMAwz13TeV_400_390_a'] = ['id_WZ_400_390_a']
	processesSignal['SMAwz13TeV_450_150_a'] = ['id_WZ_450_150_a']
	processesSignal['SMAwz13TeV_450_200_a'] = ['id_WZ_450_200_a']
	processesSignal['SMAwz13TeV_450_250_a'] = ['id_WZ_450_250_a']
	processesSignal['SMAwz13TeV_450_300_a'] = ['id_WZ_450_300_a']
	processesSignal['SMAwz13TeV_450_350_a'] = ['id_WZ_450_350_a']
	processesSignal['SMAwz13TeV_450_50_a'] = ['id_WZ_450_50_a']
	processesSignal['SMAwz13TeV_500_0_a'] = ['id_WZ_500_0_a']
	processesSignal['SMAwz13TeV_500_100_a'] = ['id_WZ_500_100_a']
	processesSignal['SMAwz13TeV_500_150_a'] = ['id_WZ_500_150_a']
	processesSignal['SMAwz13TeV_500_200_a'] = ['id_WZ_500_200_a']
	processesSignal['SMAwz13TeV_500_250_a'] = ['id_WZ_500_250_a']
	processesSignal['SMAwz13TeV_500_300_a'] = ['id_WZ_500_300_a']
	processesSignal['SMAwz13TeV_500_350_a'] = ['id_WZ_500_350_a']
	processesSignal['SMAwz13TeV_500_400_a'] = ['id_WZ_500_400_a']
	processesSignal['SMAwz13TeV_500_50_a'] = ['id_WZ_500_50_a']
	processesSignal['SMAwz13TeV_550_0_a'] = ['id_WZ_550_0_a']
	processesSignal['SMAwz13TeV_550_100_a'] = ['id_WZ_550_100_a']
	processesSignal['SMAwz13TeV_550_150_a'] = ['id_WZ_550_150_a']
	processesSignal['SMAwz13TeV_550_200_a'] = ['id_WZ_550_200_a']
	processesSignal['SMAwz13TeV_550_250_a'] = ['id_WZ_550_250_a']
	processesSignal['SMAwz13TeV_550_300_a'] = ['id_WZ_550_300_a']
	processesSignal['SMAwz13TeV_550_350_a'] = ['id_WZ_550_350_a']
	processesSignal['SMAwz13TeV_550_400_a'] = ['id_WZ_550_400_a']
	processesSignal['SMAwz13TeV_550_50_a'] = ['id_WZ_550_50_a']
	processesSignal['SMAwz13TeV_600_0_a'] = ['id_WZ_600_0_a']
	processesSignal['SMAwz13TeV_600_100_a'] = ['id_WZ_600_100_a']
	processesSignal['SMAwz13TeV_600_150_a'] = ['id_WZ_600_150_a']
	processesSignal['SMAwz13TeV_600_200_a'] = ['id_WZ_600_200_a']
	processesSignal['SMAwz13TeV_600_250_a'] = ['id_WZ_600_250_a']
	processesSignal['SMAwz13TeV_600_300_a'] = ['id_WZ_600_300_a']
	processesSignal['SMAwz13TeV_600_350_a'] = ['id_WZ_600_350_a']
	processesSignal['SMAwz13TeV_600_400_a'] = ['id_WZ_600_400_a']
	processesSignal['SMAwz13TeV_600_50_a'] = ['id_WZ_600_50_a']
	processesSignal['SMAwz13TeV_650_0_a'] = ['id_WZ_650_0_a']
	processesSignal['SMAwz13TeV_650_100_a'] = ['id_WZ_650_100_a']
	processesSignal['SMAwz13TeV_650_150_a'] = ['id_WZ_650_150_a']
	processesSignal['SMAwz13TeV_650_200_a'] = ['id_WZ_650_200_a']
	processesSignal['SMAwz13TeV_650_250_a'] = ['id_WZ_650_250_a']
	processesSignal['SMAwz13TeV_650_300_a'] = ['id_WZ_650_300_a']
	processesSignal['SMAwz13TeV_650_350_a'] = ['id_WZ_650_350_a']
	processesSignal['SMAwz13TeV_650_400_a'] = ['id_WZ_650_400_a']
	processesSignal['SMAwz13TeV_650_50_a'] = ['id_WZ_650_50_a']
	processesSignal['SMAwz13TeV_700_0_a'] = ['id_WZ_700_0_a']
	processesSignal['SMAwz13TeV_700_100_a'] = ['id_WZ_700_100_a']
	processesSignal['SMAwz13TeV_700_150_a'] = ['id_WZ_700_150_a']
	processesSignal['SMAwz13TeV_700_200_a'] = ['id_WZ_700_200_a']
	processesSignal['SMAwz13TeV_700_250_a'] = ['id_WZ_700_250_a']
	processesSignal['SMAwz13TeV_700_300_a'] = ['id_WZ_700_300_a']
	processesSignal['SMAwz13TeV_700_350_a'] = ['id_WZ_700_350_a']
	processesSignal['SMAwz13TeV_700_400_a'] = ['id_WZ_700_400_a']
	processesSignal['SMAwz13TeV_700_50_a'] = ['id_WZ_700_50_a']
	processesSignal['SMAwz13TeV_750_0_a'] = ['id_WZ_750_0_a']
	processesSignal['SMAwz13TeV_750_100_a'] = ['id_WZ_750_100_a']
	processesSignal['SMAwz13TeV_750_150_a'] = ['id_WZ_750_150_a']
	processesSignal['SMAwz13TeV_750_200_a'] = ['id_WZ_750_200_a']
	processesSignal['SMAwz13TeV_750_250_a'] = ['id_WZ_750_250_a']
	processesSignal['SMAwz13TeV_750_300_a'] = ['id_WZ_750_300_a']
	processesSignal['SMAwz13TeV_750_350_a'] = ['id_WZ_750_350_a']
	processesSignal['SMAwz13TeV_750_400_a'] = ['id_WZ_750_400_a']
	processesSignal['SMAwz13TeV_750_50_a'] = ['id_WZ_750_50_a']
	processesSignal['SMAwz13TeV_800_0_a'] = ['id_WZ_800_0_a']
	processesSignal['SMAwz13TeV_800_100_a'] = ['id_WZ_800_100_a']
	processesSignal['SMAwz13TeV_800_150_a'] = ['id_WZ_800_150_a']
	processesSignal['SMAwz13TeV_800_200_a'] = ['id_WZ_800_200_a']
	processesSignal['SMAwz13TeV_800_250_a'] = ['id_WZ_800_250_a']
	processesSignal['SMAwz13TeV_800_300_a'] = ['id_WZ_800_300_a']
	processesSignal['SMAwz13TeV_800_350_a'] = ['id_WZ_800_350_a']
	processesSignal['SMAwz13TeV_800_400_a'] = ['id_WZ_800_400_a']
	processesSignal['SMAwz13TeV_800_50_a'] = ['id_WZ_800_50_a']

#----------------------------------------------------------------------------------------------------
def group(systematics,processes):
    print "---In group function"
    f_out = TFile(output_file,'UPDATE')
    for systematic in systematics:
        if not os.path.isfile(path+systematic+'.root'):
            if len(processes)!=0:
                errFiles.append(systematic+'.root')
                print 'Could not find file '+path+systematic+'.root. Skipping...'
            continue
        print "fetching :",path+systematic+'.root'
        f_in = TFile(path+systematic+'.root','READ')

        for process, samples in processes.iteritems():
            name_tree=''
                    #check if process starts with lookup, if so, then lookup the x and y coordinates
            if process.split('_')[0] == 'lookup':
                tmpgrid=process.split('_')[1]
                x,y=getCoordfromMCID(process.split('_')[2],process.split('_')[1])
                name_tree=process.split('_')[1]+'_'+str(x)+'_'+str(y)+'_'+systematic
                if tmpgrid=="SMCwslep13TeV":
                    name_tree="c1c1_slep"+'_'+str(x)+'_'+str(y)+'_'+systematic

            else:
                name_tree=process+'_'+systematic

            list_sample = TList()
            for sample in samples:
                if (f_in.Get(sample)):
                    list_sample.Add(f_in.Get(sample))
                else:
                    if systematic=='CENTRAL': errSamples.append(sample)
                    print '    '+sample+' for '+name_tree+' ('+process+'_'+systematic+') is a null pointer, does the TTree exist? Skipping...'
                    #print systematic,process,samples,list_sample.At(0)
            f_out.cd()
        #upto here closing is OK!
        #####f_in.Close()


            if list_sample.At(0):
                #print list_sample.At(0)
                tree= list_sample.At(0).MergeTrees(list_sample)      #somewhat strange implementation: t.MergeTrees(TList l) creates a new tree in the memory
                #print tree
                #print list_sample
                if tree:
                    tree.SetNameTitle(name_tree,name_tree)
                    tree.Write(name_tree)
                    if 'data' in name_tree.lower():
                        print '    Tree '+name_tree+' ('+process+'_'+systematic+') written with ? events.'
                    else:
                        print '    Tree '+name_tree+' ('+process+'_'+systematic+') written with '+str(tree.GetEntries())+' events.'
                else:
                    print '    Tree '+name_tree+' ('+process+'_'+systematic+') not written due to 0 events.'
                #del tree
            else:
                print '    '+name_tree+' ('+process+'_'+systematic+') is a null pointer, does any TTree in the list exist? Skipping...'
            print "deleting list_sample"
            del list_sample

        print "closing input file"
        f_in.Close()
    print "closing output file"
    f_out.Close()



errSamples=[]
errFiles=[]


if 'BG13TeV' in grid:
    print 'Grouping samples to processes and assigning systematics for MC.'
    group(systematicsMC,processesMC)
elif 'L_DATA13TeV' in grid:
    print 'Grouping samples for Data.'
    group(systematicsData,processesData)
elif 'L3_FakesMM13TeV' in grid:
    print 'Grouping samples for Fakes.'
    group(systematicsData,processesData)
else:

    print 'Grouping samples to processes and assigning systematics for Signal.'

    group(systematicsSignal,processesSignal)

print 'Created '+output_file

print
print 'The following files were missing:'
print ','.join(errFiles)
print
print 'The following samples were missing:'
print ','.join(errSamples)
print

print 'Bye.'
