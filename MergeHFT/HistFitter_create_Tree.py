from ROOT import *
from getCoordfromMCID import getCoordfromMCID
import sys,os.path

'''
Adapted by N. Santoyo from B. Schneider script in Run1

Usage: python HistFitter_create_Tree.py L3_BG13TeV_a
'''

### This list is not used, it's just for bookkeeping
grids=[  'L3_BG13TeV_a','L3_FakesMM13TeV','L3_BG13TeV_d', 'L3_BG13TeV_e','L_DATA13TeV','L3_SMAwslep13TeV', 'L3_SMAwnoslep13TeV','L3_SMWh13TeV','L2_BG13TeV','L2_SMCwslep13TeV'    ]

if len(sys.argv)==2:
    grid=sys.argv[1]
else:
    print 'Need an argument which grid. Options are',grids
    print 'Exit.'
    sys.exit()

# isantoyo >> specify the path to your hadded trees per sys, and the outtput path (which can be the same)

path="/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/outputs_Signal/Trees_mca/"

if "L_DATA13TeV" in grid : path="/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/outputs_data/"

# if 'L2_' in grid:
#     #path="/lustre/scratch/epp/atlas/is86/Run2/Trees/SUSYApproval/2L/"
#     #path="/lustre/scratch/epp/atlas/is86/Run2/Trees/SUSYApproval/sigmaBand/"
#     path="/home/i/is86/2013_analysis/UCI_n0145/macros/Run2/HistFitterTreeMaking/outputs/Trees/"
# elif 'L3_' in grid:
#     #path="/lustre/scratch/epp/atlas/is86/Run2/Trees/SUSYApproval/3L/"
#     path="/home/i/is86/2013_analysis/UCI_n0145/macros/Run2/HistFitterTreeMaking/outputs/Trees/"

#output_file='/lustre/scratch/epp/atlas/is86/Run2/Limits_Moriond2017/Trees/'+grid+'.root'
output_file='/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_signal/'+grid+'.root' #lxplus
if "L_DATA13TeV" in grid : output_file='/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_data/'+grid+'.root' #lxplus
f_out2 = TFile(output_file,'RECREATE')
f_out2.Close()
print "preparing output file"


if not path.endswith("/"):
    path+=("/")

systematicsMC               = [
    'CENTRAL',
    'EG_SCALE_ALL_UP','EG_SCALE_ALL_DN', # negligible
    'JER_EffectiveNP_1_UP','JER_EffectiveNP_1_DN',
    'JER_EffectiveNP_2_UP','JER_EffectiveNP_2_DN',
    'JER_EffectiveNP_3_UP','JER_EffectiveNP_3_DN',
    'JER_EffectiveNP_4_UP','JER_EffectiveNP_4_DN',
    'JER_EffectiveNP_5_UP','JER_EffectiveNP_5_DN',
    'JER_EffectiveNP_6_UP','JER_EffectiveNP_6_DN',
    'JER_EffectiveNP_7_UP','JER_EffectiveNP_7_DN',
    'JET_GroupedNP_1_UP','JET_GroupedNP_1_DN', #negligible
    'JET_GroupedNP_2_UP','JET_GroupedNP_2_DN',
    'JET_GroupedNP_3_UP','JET_GroupedNP_3_DN',
    'MET_SoftTrk_ResoPara','MET_SoftTrk_ResoPerp',
    'MET_SoftTrk_ScaleUp','MET_SoftTrk_ScaleDown', # negligible
    'MUONS_ID_UP','MUONS_ID_DN',
    'MUONS_MS_UP','MUONS_MS_DN',
    'MUONS_SCALE_UP','MUONS_SCALE_DN', # negligible in SR
    'MUONS_SAGITTA_UP','MUONS_SAGITTA_DN', # negligible in SR
#    'PRW_DATASF_DN','PRW_DATASF_UP',
    ]



systematicsSignal           = systematicsMC
systematicsData             = ['CENTRAL']

processesMC                 = {}
processesSignal             = {}
processesData               = {}




#----------------------------------------------------------------------------------------------------


if 'L3_otherBG13TeV' in grid:
    processesMC['ttvNLO']    = ['id_410155',                        'id_410081','id_410218','id_410219','id_410220',                                     'id_410050']
    processesMC['higgs4L']        = [ 'id_341471'  ] #complete

if 'L3_FakesMM13TeV' in grid:
    processesData['Fakes']    = ['id_Fakes']

if 'L3_BG13TeV_d' in grid:

    #processesMC['WZ0j']     = ['id_0jWZ']
    #processesMC['WZ1j']     = ['id_1jWZ']
    processesMC['Dibosons_2L_d']       = ['id_Dibosons_2L_d']
    processesMC['Dibosons_3L_nJ0_d']       = ['id_Dibosons_3L_nJ0_d']
    processesMC['Dibosons_3L_LowHT_d']       = ['id_Dibosons_3L_LowHT_d']
    processesMC['Dibosons_3L_HighHT_d']       = ['id_Dibosons_3L_HightHT_d']
    processesMC['Dibosons_4L_d']       = ['id_Dibosons_4L_d']
#    processesMC['Dibosons_3L_LowHT_d']       = ['id_Dibosons_3L_LowHT_d']
#    processesMC['Dibosons_4L_d']       = ['id_Dibosons_4L_d']
#    processesMC['Dibosons_3L_LowHT_e']       = ['id_Dibosons_3L_LowHT_e']
#    processesMC['Dibosons_4L_e']       = ['id_Dibosons_4L_e']
    processesMC['ZZ_d']       = ['id_ZJets_d']
    processesMC['ttV_d']      = ['id_ttV_d']
    processesMC['VVV_d']      = ['id_VVV_d']
    processesMC['Higgs_d']    = ['id_Higgs_d']
#    processesMC['Wjets']    = ['id_Wjets']
#    processesMC['Zjets']    = ['id_Zjets']
    processesMC['Vgamma_d']   = ['id_Vgamma_d']
    processesMC['ttbar_d']    = ['id_ttbar_d']
    processesMC['SingleT_d']  = ['id_SingleT_d']
#    processesMC['MultiT']   = ['id_multitop']
#    processesMC['MCfakes']  = ['id_MCfakes']    

#----------------------------------------------------------------------------------------------------
if 'L_DATA13TeV' in grid:

    processesData['Data']      = ['id_2018']




#----------------------------------------------------------------------------------------------------
# -------------------------- Specify the MCid for each signal grid-----------------------------------
#----------------------------------------------------------------------------------------------------

if 'L3_SMWh13TeV' in grid:
    processesSignal['SMAwh13TeV_150_0'] = ['id_150_0']
    processesSignal['SMAwh13TeV_152_22'] = ['id_152_22']
    processesSignal['SMAwh13TeV_162_12'] = ['id_162_12']
    processesSignal['SMAwh13TeV_175_0'] = ['id_175_0']
    processesSignal['SMAwh13TeV_175_25'] = ['id_175_25']
    processesSignal['SMAwh13TeV_177_47'] = ['id_177_47']
    processesSignal['SMAwh13TeV_187_12'] = ['id_187_12']
    processesSignal['SMAwh13TeV_187_37'] = ['id_187_37']
    processesSignal['SMAwh13TeV_190_60'] = ['id_190_60']
    processesSignal['SMAwh13TeV_200_0'] = ['id_200_0']
    processesSignal['SMAwh13TeV_200_25'] = ['id_200_25']
    processesSignal['SMAwh13TeV_200_50'] = ['id_200_50']
    processesSignal['SMAwh13TeV_202_72'] = ['id_202_72']
    processesSignal['SMAwh13TeV_212_12'] = ['id_212_12']
    processesSignal['SMAwh13TeV_212_37'] = ['id_212_37']
    processesSignal['SMAwh13TeV_212_62'] = ['id_212_62']
    processesSignal['SMAwh13TeV_215_85'] = ['id_215_85']
    processesSignal['SMAwh13TeV_225_0'] = ['id_225_0']
    processesSignal['SMAwh13TeV_225_25'] = ['id_225_25']
    processesSignal['SMAwh13TeV_225_50'] = ['id_225_50']
    processesSignal['SMAwh13TeV_225_75'] = ['id_225_75']
    processesSignal['SMAwh13TeV_227_97'] = ['id_227_97']
    processesSignal['SMAwh13TeV_237_37'] = ['id_237_37']
    processesSignal['SMAwh13TeV_237_62'] = ['id_237_62']
    processesSignal['SMAwh13TeV_237_87'] = ['id_237_87']
    processesSignal['SMAwh13TeV_240_110'] = ['id_240_110']
    processesSignal['SMAwh13TeV_250_0'] = ['id_250_0']
    processesSignal['SMAwh13TeV_250_100'] = ['id_250_100']
    processesSignal['SMAwh13TeV_250_25'] = ['id_250_25']
    processesSignal['SMAwh13TeV_250_50'] = ['id_250_50']
    processesSignal['SMAwh13TeV_250_75'] = ['id_250_75']
    processesSignal['SMAwh13TeV_262_12'] = ['id_262_12']
    processesSignal['SMAwh13TeV_262_37'] = ['id_262_37']
    processesSignal['SMAwh13TeV_262_62'] = ['id_262_62']
    processesSignal['SMAwh13TeV_262_87'] = ['id_262_87']
    processesSignal['SMAwh13TeV_275_0'] = ['id_275_0']
    processesSignal['SMAwh13TeV_275_25'] = ['id_275_25']
    processesSignal['SMAwh13TeV_275_50'] = ['id_275_50']
    processesSignal['SMAwh13TeV_275_75'] = ['id_275_75']
    processesSignal['SMAwh13TeV_287_12'] = ['id_287_12']
    processesSignal['SMAwh13TeV_287_37'] = ['id_287_37']
    processesSignal['SMAwh13TeV_287_62'] = ['id_287_62']
    processesSignal['SMAwh13TeV_300_0'] = ['id_300_0']
    processesSignal['SMAwh13TeV_300_100'] = ['id_300_100']
    processesSignal['SMAwh13TeV_300_25'] = ['id_300_25']
    processesSignal['SMAwh13TeV_300_50'] = ['id_300_50']
    processesSignal['SMAwh13TeV_300_75'] = ['id_300_75']
    processesSignal['SMAwh13TeV_312_12'] = ['id_312_12']
    processesSignal['SMAwh13TeV_312_37'] = ['id_312_37']
    processesSignal['SMAwh13TeV_325_0'] = ['id_325_0']
    processesSignal['SMAwh13TeV_325_100'] = ['id_325_100']
    processesSignal['SMAwh13TeV_325_25'] = ['id_325_25']
    processesSignal['SMAwh13TeV_325_50'] = ['id_325_50']
    processesSignal['SMAwh13TeV_325_75'] = ['id_325_75']
    processesSignal['SMAwh13TeV_337_12'] = ['id_337_12']
    processesSignal['SMAwh13TeV_350_0'] = ['id_350_0']
    processesSignal['SMAwh13TeV_350_100'] = ['id_350_100']
    processesSignal['SMAwh13TeV_350_25'] = ['id_350_25']
    processesSignal['SMAwh13TeV_350_50'] = ['id_350_50']
    processesSignal['SMAwh13TeV_350_75'] = ['id_350_75']
    processesSignal['SMAwh13TeV_375_0'] = ['id_375_0']
    processesSignal['SMAwh13TeV_375_25'] = ['id_375_25']
    processesSignal['SMAwh13TeV_375_50'] = ['id_375_50']
    processesSignal['SMAwh13TeV_375_75'] = ['id_375_75']
    processesSignal['SMAwh13TeV_400_0'] = ['id_400_0']
    processesSignal['SMAwh13TeV_400_100'] = ['id_400_100']
    processesSignal['SMAwh13TeV_400_25'] = ['id_400_25']
    processesSignal['SMAwh13TeV_400_50'] = ['id_400_50']
    processesSignal['SMAwh13TeV_400_75'] = ['id_400_75']
    processesSignal['SMAwh13TeV_425_0'] = ['id_425_0']
    processesSignal['SMAwh13TeV_425_100'] = ['id_425_100']
    processesSignal['SMAwh13TeV_425_25'] = ['id_425_25']
    processesSignal['SMAwh13TeV_425_50'] = ['id_425_50']
    processesSignal['SMAwh13TeV_425_75'] = ['id_425_75']
    processesSignal['SMAwh13TeV_450_0'] = ['id_450_0']
    processesSignal['SMAwh13TeV_450_100'] = ['id_450_100']
    processesSignal['SMAwh13TeV_450_25'] = ['id_450_25']
    processesSignal['SMAwh13TeV_450_50'] = ['id_450_50']
    processesSignal['SMAwh13TeV_450_75'] = ['id_450_75']
    processesSignal['SMAwh13TeV_475_0'] = ['id_475_0']
    processesSignal['SMAwh13TeV_475_100'] = ['id_475_100']
    processesSignal['SMAwh13TeV_475_25'] = ['id_475_25']
    processesSignal['SMAwh13TeV_475_50'] = ['id_475_50']
    processesSignal['SMAwh13TeV_475_75'] = ['id_475_75']
    processesSignal['SMAwh13TeV_500_0'] = ['id_500_0']
    processesSignal['SMAwh13TeV_500_25'] = ['id_500_25']
    processesSignal['SMAwh13TeV_500_50'] = ['id_500_50']
    processesSignal['SMAwh13TeV_500_75'] = ['id_500_75']
    processesSignal['SMAwh13TeV_525_0'] = ['id_525_0']
    processesSignal['SMAwh13TeV_525_25'] = ['id_525_25']
    processesSignal['SMAwh13TeV_525_50'] = ['id_525_50']
    processesSignal['SMAwh13TeV_550_0'] = ['id_550_0']
    processesSignal['SMAwh13TeV_550_25'] = ['id_550_25']
    processesSignal['SMAwh13TeV_575_25'] = ['id_575_25']





#----------------------------------------------------------------------------------------------------
def group(systematics,processes):
    print "---In group function"
    f_out = TFile(output_file,'UPDATE')
    for systematic in systematics:
        if not os.path.isfile(path+systematic+'.root'):
            if len(processes)!=0:
                errFiles.append(systematic+'.root')
                print 'Could not find file '+path+systematic+'.root. Skipping...'
            continue
        print "fetching :",path+systematic+'.root'
        f_in = TFile(path+systematic+'.root','READ')

        for process, samples in processes.iteritems():
            name_tree=''
                    #check if process starts with lookup, if so, then lookup the x and y coordinates
            if process.split('_')[0] == 'lookup':
                tmpgrid=process.split('_')[1]
                x,y=getCoordfromMCID(process.split('_')[2],process.split('_')[1])
                name_tree=process.split('_')[1]+'_'+str(x)+'_'+str(y)+'_'+systematic
                if tmpgrid=="SMCwslep13TeV":
                    name_tree="c1c1_slep"+'_'+str(x)+'_'+str(y)+'_'+systematic

            else:
                name_tree=process+'_'+systematic

            list_sample = TList()
            for sample in samples:
                if (f_in.Get(sample)):
                    list_sample.Add(f_in.Get(sample))
                else:
                    if systematic=='CENTRAL': errSamples.append(sample)
                    print '    '+sample+' for '+name_tree+' ('+process+'_'+systematic+') is a null pointer, does the TTree exist? Skipping...'
                    #print systematic,process,samples,list_sample.At(0)
            f_out.cd()
        #upto here closing is OK!
        #####f_in.Close()


            if list_sample.At(0):
                #print list_sample.At(0)
                tree= list_sample.At(0).MergeTrees(list_sample)      #somewhat strange implementation: t.MergeTrees(TList l) creates a new tree in the memory
                #print tree
                #print list_sample
                if tree:
                    tree.SetNameTitle(name_tree,name_tree)
                    tree.Write(name_tree)
                    if 'data' in name_tree.lower():
                        print '    Tree '+name_tree+' ('+process+'_'+systematic+') written with ? events.'
                    else:
                        print '    Tree '+name_tree+' ('+process+'_'+systematic+') written with '+str(tree.GetEntries())+' events.'
                else:
                    print '    Tree '+name_tree+' ('+process+'_'+systematic+') not written due to 0 events.'
                #del tree
            else:
                print '    '+name_tree+' ('+process+'_'+systematic+') is a null pointer, does any TTree in the list exist? Skipping...'
            print "deleting list_sample"
            del list_sample

        print "closing input file"
        f_in.Close()
    print "closing output file"
    f_out.Close()



errSamples=[]
errFiles=[]


if 'BG13TeV' in grid:
    print 'Grouping samples to processes and assigning systematics for MC.'
    group(systematicsMC,processesMC)
elif 'L_DATA13TeV' in grid:
    print 'Grouping samples for Data.'
    group(systematicsData,processesData)
elif 'L3_FakesMM13TeV' in grid:
    print 'Grouping samples for Fakes.'
    group(systematicsData,processesData)
else:

    print 'Grouping samples to processes and assigning systematics for Signal.'

    group(systematicsSignal,processesSignal)

print 'Created '+output_file

print
print 'The following files were missing:'
print ','.join(errFiles)
print
print 'The following samples were missing:'
print ','.join(errSamples)
print

print 'Bye.'
