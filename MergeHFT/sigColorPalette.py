#!/usr/bin/python

def sigColorPalette(h):

    from ROOT import TColor,gStyle
    from array import array

    zFrom = h.GetMinimum()
    zTo   = h.GetMaximum()
    if abs(zFrom)<0.01 and abs(zTo)<0.01: zTo = 1.

    ### Define color palette in order to paint 0 values white
    #aRed    = array('d',[0.,1.,1.])
    #aGreen  = array('d',[0.,1.,0.])
    #aBlue   = array('d',[1.,1.,0.])
    #aStops = array('d',[0.,.5,1.])
    #iContours=50
    #TColor.CreateGradientColorTable(3,aStops,aRed,aGreen,aBlue,iContours)
    #h.SetContour(iContours)
    lColors=[]
    lContours=[]
    for i in range(0,501):
        #print i, (i-250.)/50.
        #lContours.append((i-250.)/50.)
        contourLevel = ((i+(zFrom)*500./(zTo-zFrom))*(zTo-zFrom)/500.)
        lContours.append(contourLevel)
        if contourLevel==0.: lColors.append(TColor.GetColor(1.,1.,1.))
        elif i <  10: lColors.append(TColor.GetColor(.45,0.,1.))
        elif i <  20: lColors.append(TColor.GetColor(.4,0.,1.))
        elif i <  30: lColors.append(TColor.GetColor(.35,0.,1.))
        elif i <  40: lColors.append(TColor.GetColor(.3,0.,1.))
        elif i <  50: lColors.append(TColor.GetColor(.25,0.,1.))
        elif i <  60: lColors.append(TColor.GetColor(.2,0.,1.))
        elif i <  70: lColors.append(TColor.GetColor(.15,0.,1.))
        elif i <  80: lColors.append(TColor.GetColor(.1,0.,1.))
        elif i <  90: lColors.append(TColor.GetColor(.05,0.,1.))
        elif i < 100: lColors.append(TColor.GetColor(0.,0.,1.))
        elif i < 110: lColors.append(TColor.GetColor(0.,.1,1.))
        elif i < 120: lColors.append(TColor.GetColor(0.,.2,1.))
        elif i < 130: lColors.append(TColor.GetColor(0.,.3,1.))
        elif i < 140: lColors.append(TColor.GetColor(0.,.4,1.))
        elif i < 150: lColors.append(TColor.GetColor(0.,.5,1.))
        elif i < 160: lColors.append(TColor.GetColor(0.,.6,1.))
        elif i < 170: lColors.append(TColor.GetColor(0.,.7,1.))
        elif i < 180: lColors.append(TColor.GetColor(0.,.8,1.))
        elif i < 190: lColors.append(TColor.GetColor(0.,.9,1.))
        elif i < 200: lColors.append(TColor.GetColor(0.,1.,1.))
        elif i < 210: lColors.append(TColor.GetColor(0.,1.,.9))
        elif i < 220: lColors.append(TColor.GetColor(0.,1.,.8))
        elif i < 230: lColors.append(TColor.GetColor(0.,1.,.7))
        elif i < 240: lColors.append(TColor.GetColor(0.,1.,.6))
        elif i < 250: lColors.append(TColor.GetColor(0.,1.,.5))
        elif i < 260: lColors.append(TColor.GetColor(0.,1.,.4))
        elif i < 270: lColors.append(TColor.GetColor(0.,1.,.3))
        elif i < 280: lColors.append(TColor.GetColor(0.,1.,.2))
        elif i < 290: lColors.append(TColor.GetColor(0.,1.,.1))
        elif i < 300: lColors.append(TColor.GetColor(0.,1.,0.))
        elif i < 310: lColors.append(TColor.GetColor(.1,1.,0.))
        elif i < 320: lColors.append(TColor.GetColor(.2,1.,0.))
        elif i < 330: lColors.append(TColor.GetColor(.3,1.,0.))
        elif i < 340: lColors.append(TColor.GetColor(.4,1.,0.))
        elif i < 350: lColors.append(TColor.GetColor(.5,1.,0.))
        elif i < 360: lColors.append(TColor.GetColor(.6,1.,0.))
        elif i < 370: lColors.append(TColor.GetColor(.7,1.,0.))
        elif i < 380: lColors.append(TColor.GetColor(.8,1.,0.))
        elif i < 390: lColors.append(TColor.GetColor(.9,1.,0.))
        elif i < 400: lColors.append(TColor.GetColor(1.,1.,0.))
        elif i < 410: lColors.append(TColor.GetColor(1.,.9,0.))
        elif i < 420: lColors.append(TColor.GetColor(1.,.8,0.))
        elif i < 430: lColors.append(TColor.GetColor(1.,.7,0.))
        elif i < 440: lColors.append(TColor.GetColor(1.,.6,0.))
        elif i < 450: lColors.append(TColor.GetColor(1.,.5,0.))
        elif i < 460: lColors.append(TColor.GetColor(1.,.4,0.))
        elif i < 470: lColors.append(TColor.GetColor(1.,.3,0.))
        elif i < 480: lColors.append(TColor.GetColor(1.,.2,0.))
        elif i < 490: lColors.append(TColor.GetColor(1.,.1,0.))
        else:         lColors.append(TColor.GetColor(1.,.0,0.))

    aColors=array('i',lColors)
    aContours=array('d',lContours)
    gStyle.SetPalette(len(aColors),aColors)
    h.SetContour(len(aContours),aContours)

    for i in range(0,h.GetNbinsX()):
        for j in range(0,h.GetNbinsY()):
            zContent=h.GetBinContent(i,j)
            #if abs(zContent)<.12 and zContent!=0.: h.SetBinContent(i,j,-.01)

