#!/usr/bin/env python
#from sys import exit
#from ROOT import gSystem
#gSystem.Load("libCombinationTools")

from ROOT import gROOT,gSystem,gDirectory
gSystem.Load("libSusyFitter.so")
gROOT.Reset()

from ROOT import TFile, RooWorkspace, TObject, TString, RooAbsReal, RooRealVar, RooFitResult, RooDataSet, RooAddition, RooArgSet, RooFormulaVar, RooAbsData, RooRandom 
from ROOT import Util, TMath, TMap, RooExpandedFitResult

from TexYieldsTable import *
import os
import sys
import time

# Main function calls are defined below.

def latexfitresults(filename = os.environ['SUSYFITTER'] + '/results/MyOneLeptonKtScaleFit_BkgOnlyKt_combined_NormalMeasurement_model_afterFit.root',regionList='3jL', dataname='obsData'):

  print "hallo"
  ####
  # SEt THOSE !!!
  print "Have you set the samples?"
  irred = "ZZ"
  red = "ttbar"
  tot = irred+","+red+",WZ"
  print "Reducible:",red
  print "Irreducible:",irred
  time.sleep(2)

  w = Util.GetWorkspaceFromFile(filename,'w')

  if w==None:
    print "ERROR : Cannot open workspace : ", workspacename
    sys.exit(1) 


  resultname = 'RooExpandedFitResult_afterFit'
  result = w.obj(resultname)

  if result==None:
    print "ERROR : Cannot open fit result : ", resultname
    sys.exit(1)

  snapshot =  'snapshot_paramsVals_' + resultname
  w.loadSnapshot(snapshot)

  if not w.loadSnapshot(snapshot):
    print "ERROR : Cannot load snapshot : ", snapshot
    sys.exit(1)

    
  data_set = w.data(dataname)
  if data_set==None:
    print "ERROR : Cannot open dataset : ", "data_set"+suffix
    sys.exit(1)
      
  regionCat = w.obj("channelCat")
  data_set.table(regionCat).Print("v");

  regionFullNameList = [ Util.GetFullRegionName(regionCat, region) for region in regionList]
  print " \n Requested regions = ",  regionFullNameList, "\n\n"


  ###

  tablenumbers = {}
  tablenumbers['names'] = regionList 
 
  regionCatList = [ 'channelCat==channelCat::' +region.Data() for region in regionFullNameList]
  
  regionDatasetList = [data_set.reduce(regioncat) for regioncat in regionCatList]
  for index, data in  enumerate(regionDatasetList):
    data.SetName("data_" + regionList[index])
    data.SetTitle("data_" + regionList[index])
    
  nobs_regionList = [ data.sumEntries() for data in regionDatasetList]
  tablenumbers['nobs'] = nobs_regionList
 
  ####

  bkginRegionList = [ Util.GetComponent(w,"WZ,ttbar,ZZ",region) for region in regionList]
  nbkginRegionList = [  bkginRegion.getVal() for bkginRegion in bkginRegionList]
  [region.Print() for region in bkginRegionList]
  print "\n N bkgs in regions is in total  = ", nbkginRegionList

  nbkgerrinRegionList = [ Util.GetPropagatedError(bkginRegion, result)  for bkginRegion in bkginRegionList]
  print "\n error N bkgs in regions = ", nbkgerrinRegionList

  redinRegionList = [ Util.GetComponent(w,red,region) for region in regionList]
  nredinRegionList = [  redinRegion.getVal() for redinRegion in redinRegionList]
  [region.Print() for region in redinRegionList]
  print "\n N red in regions = ", nredinRegionList
  nrederrinRegionList = [ Util.GetPropagatedError(redinRegion, result)  for redinRegion in redinRegionList]
  print "\n error N red in regions = ", nrederrinRegionList

  irredinRegionList = [ Util.GetComponent(w,irred,region) for region in regionList]
  nirredinRegionList = [  irredinRegion.getVal() for irredinRegion in irredinRegionList]
  [region.Print() for region in irredinRegionList]
  print "\n N irred in regions = ", nirredinRegionList
  nirrederrinRegionList = [ Util.GetPropagatedError(irredinRegion, result)  for irredinRegion in irredinRegionList]
  print "\n error N irred in regions = ", nirrederrinRegionList

  WZinRegionList = [ Util.GetComponent(w,"WZ",region) for region in regionList]
  nWZinRegionList = [  WZinRegion.getVal() for WZinRegion in WZinRegionList]
  print "\n N WZ in regions = ", nWZinRegionList
  nWZerrinRegionList = [ Util.GetPropagatedError(WZinRegion, result)  for WZinRegion in WZinRegionList]
  print "\n error N WZ in regions = ", nWZerrinRegionList

  ZZinRegionList = [ Util.GetComponent(w,"ZZ",region) for region in regionList]
  nZZinRegionList = [  ZZinRegion.getVal() for ZZinRegion in ZZinRegionList]
  print "\n N ZZ in regions = ", nZZinRegionList
  nZZerrinRegionList = [ Util.GetPropagatedError(ZZinRegion, result)  for ZZinRegion in ZZinRegionList]
  print "\n error N ZZ in regions = ", nZZerrinRegionList

  ttbarinRegionList = [ Util.GetComponent(w,"ttbar",region) for region in regionList]
  nttbarinRegionList = [  ttbarinRegion.getVal() for ttbarinRegion in ttbarinRegionList]
  print "\n N ttbar in regions = ", nttbarinRegionList
  nttbarerrinRegionList = [ Util.GetPropagatedError(ttbarinRegion, result)  for ttbarinRegion in ttbarinRegionList]
  print "\n error N ttbar in regions = ", nttbarerrinRegionList
  #================================================================================================================0

  ######
  # Example how to add multiple backgrounds in multiple(!) regions
  ttbarWZinRegionList = [ RooAddition( ("ttbarWZin" + regionList[index]),("ttbarWZin" + regionList[index]),RooArgSet(ttbarinRegionList[index],WZinRegionList[index]))  for index, region in enumerate (regionList)]
  nttbarWZinRegionList = [ ttbarWZinRegion.getVal() for ttbarWZinRegion in ttbarWZinRegionList]
  nttbarWZerrinRegionList = [ Util.GetPropagatedError(ttbarWZinRegion, result)   for ttbarWZinRegion in ttbarWZinRegionList]

   

  ######
  ######
  ######  FROM HERE ON OUT WE CALCULATE THE EXPECTED NUMBER OF EVENTS __BEFORE__ THE FIT
  ######
  ######

  #  FROM HERE ON OUT WE CALCULATE THE EXPECTED NUMBER OF EVENTS BEFORE THE FIT
  w.loadSnapshot('snapshot_paramsVals_RooExpandedFitResult_afterFit')
  
  pdfinRegionList = [ Util.GetRegionPdf(w, region)  for region in regionList]

  varinRegionList =  [ Util.GetRegionVar(w, region) for region in regionList]

  nexpinRegionList =  [ pdf.expectedEvents(RooArgSet(varinRegionList[index])) for index, pdf in enumerate(pdfinRegionList)]
  print "\n N expected in regions = ", nexpinRegionList
  time.sleep(5)
  

  fracsiginRegionList   = [ Util.GetComponentFracInRegion(w,"M12U_100_300_300",region)  for region in regionList]
  fracWZinRegionList   = [ Util.GetComponentFracInRegion(w,"WZ",region)  for region in regionList]
  fracTopinRegionList   = [ Util.GetComponentFracInRegion(w,"ttbar",region)  for region in regionList] ###
  fraczjinRegionList   = [ Util.GetComponentFracInRegion(w,"z-jets",region)  for region in regionList] ###
  fraczzinRegionList   = [ Util.GetComponentFracInRegion(w,"ZZ",region)  for region in regionList] ###
  fracttwinRegionList   = [ Util.GetComponentFracInRegion(w,"ttbarW",region)  for region in regionList] ###
  fracttzinRegionList   = [ Util.GetComponentFracInRegion(w,"ttbarZ",region)  for region in regionList] ###
  fracttwwinRegionList   = [ Util.GetComponentFracInRegion(w,"ttbarWW",region)  for region in regionList] ###

  mcsiginRegionList    = [  fracsiginRegionList[index]*nexpinRegionList[index]   for index, region in enumerate(regionList)]
  mcWZinRegionList    = [  fracWZinRegionList[index]*nexpinRegionList[index]   for index, region in enumerate(regionList)]
  mcZZinRegionList    = [  fraczzinRegionList[index]*nexpinRegionList[index]   for index, region in enumerate(regionList)]
  mcttwinRegionList    = [  fracttwinRegionList[index]*nexpinRegionList[index]   for index, region in enumerate(regionList)]
  mcttzinRegionList    = [  fracttzinRegionList[index]*nexpinRegionList[index]   for index, region in enumerate(regionList)]
  mcttwwinRegionList    = [  fracttwwinRegionList[index]*nexpinRegionList[index]   for index, region in enumerate(regionList)]
  mczjinRegionList    = [  fraczjinRegionList[index]*nexpinRegionList[index]   for index, region in enumerate(regionList)]
  mcTopinRegionList    = [  fracTopinRegionList[index]*nexpinRegionList[index]   for index, region in enumerate(regionList)]

  mcBGinRegionList    = [  (fracTopinRegionList[index]+fraczzinRegionList[index])*nexpinRegionList[index]   for index, region in enumerate(regionList)]
  
  #  mcSMinRegionList = [ mcWZinRegionList[index] + mcTopinRegionList[index] +  mcQCDinRegionList[index] + mcBGinRegionList[index]  for index, region in enumerate(regionList)]
  mcSMinRegionList = [ mcWZinRegionList[index]+mcZZinRegionList[index]+mcttwinRegionList[index]+mcttzinRegionList[index]+mcttwwinRegionList[index]+mczjinRegionList[index]+mcTopinRegionList[index] for index, region in enumerate(regionList)]
  mcIRREDwoWZinRegionList = [ mcttwinRegionList[index]+mcttzinRegionList[index]+mcttwwinRegionList[index]+mcZZinRegionList[index] for index, region in enumerate(regionList)]
  mcIRREDinRegionList = [ mcWZinRegionList[index]+mcttwinRegionList[index]+mcttzinRegionList[index]+mcttwwinRegionList[index]+mcZZinRegionList[index] for index, region in enumerate(regionList)]
  mcREDinRegionList = [ mcTopinRegionList[index]+mczjinRegionList[index] for index, region in enumerate(regionList)]
  print "\n N expected WZ in regions = ", mcWZinRegionList   
  
#  tablenumbers['MC_exp_top_WZ_events'] = [ mcWZinRegionList[index] + mcTopinRegionList[index]   for index, region in enumerate(regionList)]
  tablenumbers['MC_exp_sig_events']    = [ mcsiginRegion  for mcsiginRegion  in mcsiginRegionList ]
  tablenumbers['MC_exp_WZ_events']    = [ mcWZinRegion  for mcWZinRegion  in mcWZinRegionList ]
  tablenumbers['MC_exp_IRREDwoWZ_events']    = [ mcIRREDwoWZinRegion  for mcIRREDwoWZinRegion  in mcIRREDwoWZinRegionList ]
  tablenumbers['MC_exp_IRRED_events']    = [ mcIRREDinRegion  for mcIRREDinRegion  in mcIRREDinRegionList ]
  tablenumbers['MC_exp_RED_events']    = [ mcREDinRegion  for mcREDinRegion  in mcREDinRegionList ]
  tablenumbers['MC_exp_SM_events']    = [ mcSMinRegion  for mcSMinRegion  in mcSMinRegionList ]
  tablenumbers['MC_exp_BG_events']    = [ mcBGinRegion  for mcBGinRegion  in mcBGinRegionList ]
  
  tablenumbers['Fitted_bkg_events']    =  nbkginRegionList
  tablenumbers['Fitted_bkg_events_err']    =  nbkgerrinRegionList

  tablenumbers['Fitted_red_events']    =  nredinRegionList
  tablenumbers['Fitted_red_events_err']    =  nrederrinRegionList
  tablenumbers['Fitted_irred_events']    =  nirredinRegionList
  tablenumbers['Fitted_irred_events_err']    =  nirrederrinRegionList

  tablenumbers['Fitted_WZ_events']    = nWZinRegionList
  tablenumbers['Fitted_WZ_events_err']    = nWZerrinRegionList

  tablenumbers['Fitted_ZZ_events']    = nZZinRegionList
  tablenumbers['Fitted_ZZ_events_err']    = nZZerrinRegionList

  tablenumbers['Fitted_ttbar_events']    = nttbarinRegionList
  tablenumbers['Fitted_ttbar_events_err']    = nttbarerrinRegionList

  
  tablenumbers['Fitted_ttbar_WZ_events'] = nttbarWZinRegionList
  tablenumbers['Fitted_ttbar_WZ_events_err'] = nttbarWZerrinRegionList  
  #print tablenumbers['MC_exp_sig_events'],"========================================================="
  ###
  return tablenumbers




##################################
##################################
##################################

#### Main function calls start here ....
filename = os.environ['SUSYFITTER'] + '/results/3L_TopLvlXML_combined_NormalMeasurement_model_afterFit.root'
#filename = os.environ['SUSYFITTER'] + '/results/3L_TopLvlXML_Exclusion_M12U_100_300_300_combined_NormalMeasurement_model_afterFit.root'
regionsList = ['CR','SR']
signalregionsList = ['SR']
#regionsList = ['CF0_3L','CF1_SFOS','CF2_MET','CF3_Z','CF4_BVeto']
#signalregionsList = ['CF4_BVeto']

m3 = latexfitresults(filename,regionsList)

f = open('SLWR_SLTR_yields_data.tex', 'w')
f.write( tablestart() )
f.write( tablefragment(m3, '', signalregionsList) )
f.write( tableend4(regionsList, 'slwr.sltr') )
f.close()

