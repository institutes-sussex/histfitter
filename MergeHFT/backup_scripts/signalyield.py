"""
This script can be used to plot quantities in the signal grids, such as
 - signal values in the regions in all points
 - transfer factors
So far only 2 grids have been implemented and some of the naming is hard-coded
The input files used are:
 - file produced by histfitter with all the histograms, set as "pathtorootfiles"
 - file with the fitted values from histfitter, containing the workspaces from the fit, set in getfittedValuesInSR function
"""

from ROOT import *
ROOT.gROOT.SetBatch(True)
from math import sqrt, pow
import sys,time, os
import copy as pythoncopydude

# only needed for the getfittedValuesInSR funtion
from ROOT import gROOT,gSystem,gDirectory
gSystem.Load("libSusyFitter.so")
gROOT.Reset()
from ROOT import Util

#graphdict = {}
"""
# find file with points list. Don't rely on histo file
def getListOfPoints(grid):
  filedict = {}
  filedict["DGemt100"]="DGemt100"
  filedict["DGemt140"]="DGemt140"
  filedict["DGemt250"]="DGemt250"
  filedict["SMAnoslep8TeV"]="modeA_lepW_MC1eqMN2"
  filedict["SMAwslep8TeV"]="modeA_lightslep_MC1eqMN2"
  plist = []

  pathtof = "/terabig/lmarti/HistFitter/weak_svn_cfgs/trunk/8TeV/3L/data/"+filedict[grid]+".txt"
  if not os.path.isfile(pathtof):
    print "Did not find list of points file at",pathtof,", exiting"
    sys.exit()
  with open(pathtof , "r") as pointf:
    for i,line in enumerate(pointf):
      if i==0:
        continue
      # format is x_y
      if "DGemt" in grid:
        x = line.split()[2]
        y = line.split()[1]
      else:
        x = line.split()[1]
        y = line.split()[2]
      plist.append(str(x)+"_"+str(y))
  print "for grid",grid,"found points:",plist
  return plist
"""

#############################
def getfittedValuesInSR(signalpoint, region, grid):
  # go into the fitted workspaces and grap them values
  # find the right file name
  if "8TeV" in grid:
    filename = "/terabig/lmarti/svn_notes/plots_HCP/v22_app/raw_"+str(grid)+"/3L_TopLvlXML_Exclusion_"+str(grid)+"_"+str(signalpoint)+"_combined_NormalMeasurement_model_afterFit.root"
  else:
    filename = "/terabig/lmarti/svn_notes/plots_HCP/v22_app/raw_"+str(grid)+"/3L_TopLvlXML_Exclusion_"+str(grid)+"8TeV_"+str(signalpoint)+"_combined_NormalMeasurement_model_afterFit.root"
  #filename = "/terabig/lmarti/HistFitter/tags_hf12_user_02/HistFitter/output/output_signalyields_dgemt100/3L_TopLvlXML_Exclusion_"+str(grid)+"_"+str(signalpoint)+"_combined_NormalMeasurement_model_afterFit.root"
  if not os.path.isfile(filename):
    print "File",filename,"does not exist, skipping"
    missingf.append(filename)
    time.sleep(1)
    return (-1.,-1.)
  w = Util.GetWorkspaceFromFile(filename,'w')
  nWZinRegion = Util.GetComponent(w,"WZ",region).getVal()
  if "8TeV" in grid:
    nsiginRegion = Util.GetComponent(w,str(grid)+"_"+str(signalpoint),region).getVal()
  else:
    nsiginRegion = Util.GetComponent(w,str(grid)+"8TeV_"+str(signalpoint),region).getVal()
  print "nWZ",nWZinRegion,"nsig",nsiginRegion
  return (nWZinRegion, nsiginRegion)


#############################
# for text based output. Not in use at the moment
def interpretLimitFile(inputfile):
  
  regionLineDict = {}
  print "interpreting " + inputfile
  f = open(inputfile, 'r')
  lines = f.readlines()
  f.close()
  
  for li, line in enumerate(lines):
    currentline = line.split(';')  # currentline = array with [N_bla=34, N_bli=56, ...]
    lineDict = {}
    #print currentline
    for pair in currentline:
      if not '=' in pair: # want a N=23 in pair, otherwise don't make sense
        continue
      lineDict[pair.split('=')[0]] = pair.split('=')[1]
    gridpoint = lineDict["x"]+"_"+lineDict["y"]
    regionLineDict[gridpoint] = lineDict
  
  return regionLineDict

#############################
### functin to interprete the histograms produced by histfitter ###
def interpretHistFitterFile(inputfile,region, grid):
  print "writing dict for",region
  regionLineDict = {}
  print "interpreting Histfitter file" + inputfile
  f = TFile(inputfile)
  keyslist = f.GetListOfKeys()
  HistoDict = {}
  for key in keyslist:
    HistoDict[key.GetName()]=f.Get(key.GetName()).GetMaximum()
  linedict = {}

  ### Get Backgrounds in region ###
  linedict["WZSherpa"]=float(f.Get("hWZNom_"+str(region)+"_obs_cuts").GetMaximum())
  # use this when blinding was used in histfitter
  linedict["data"]=float(f.Get("hData_CENTRAL_"+str(region)+"_obs_cuts").GetMaximum()) 

  # blinding ########################

  ###if not "SR" in region:
  ###  linedict["data"]=float(f.Get("hData_CENTRAL_"+str(region)+"_obs_cuts").GetMaximum()) 
  ###else:
  ###  print "using blinded data at s+b"
  ###  linedict["data"]=float(f.Get("hTopLvlXMLData_CENTRALBlind_"+str(region)+"_obs_cuts").GetMaximum())

  #else:
  #  print "using real data"
  #  linedict["data"]=float(f.Get("hData_CENTRAL_"+str(region)+"_obs_cuts").GetMaximum())
  # if some bg has no contribution in a region it will crash => try block

  fakesum = 0

  try:
    fakesum+=float(f.Get("httbarNom_"+str(region)+"_obs_cuts").GetMaximum())
  except:
    print "did not find ttbar in",region
  try:
    fakesum+=float(f.Get("hWWNom_"+str(region)+"_obs_cuts").GetMaximum())
  except:
    print "did not find WW in",region
  try:
    fakesum+=float(f.Get("hZ-jetsNom_"+str(region)+"_obs_cuts").GetMaximum())
  except:
    print "did not find Z+jets in",region
  try:
    fakesum+=float(f.Get("hsingletopNom_"+str(region)+"_obs_cuts").GetMaximum())
  except:
    print "did not find t in",region
  try:
    fakesum+=float(f.Get("hW-jetsNom_"+str(region)+"_obs_cuts").GetMaximum())
  except:
    print "did not find W-jets in",region

  #### using matrix method #######
  fakesum = float(f.Get("hFakeNom_"+str(region)+"_obs_cuts").GetMaximum())

  # sum the non-zero contributions
  linedict["FakeBackground"]=fakesum

  irredsum = 0
  try:
    irredsum+=float(f.Get("hZZNom_"+str(region)+"_obs_cuts").GetMaximum())
  except:
    print "did not find ZZ in",region
  try:
    irredsum+=float(f.Get("httbarVNom_"+str(region)+"_obs_cuts").GetMaximum())
  except:
    print "did not find ttbarV in",region
  try:
    irredsum+=float(f.Get("htribosonNom_"+str(region)+"_obs_cuts").GetMaximum())
  except:
    print "did not find triboson in",region
  # sum irreducible bg
  linedict["OtherIrr"]=irredsum

  #try:
  #  linedict["FakeBackground"]=float(f.Get("httbarNom_"+str(region)+"_obs_cuts").GetMaximum())+float(f.Get("hZ-jetsNom_"+str(region)+"_obs_cuts").GetMaximum())
  #except:
  #  linedict["FakeBackground"]=float(f.Get("httbarNom_"+str(region)+"_obs_cuts").GetMaximum())
  #linedict["OtherIrr"]=float(f.Get("hZZNom_"+str(region)+"_obs_cuts").GetMaximum())

  ### Get Signal in region ###
  pointsused = []
  for akey in HistoDict.keys():
    if str(grid) in akey and "Nom" in akey and "_"+str(region)+"_" in akey:
      pointsused.append(akey)
      sigdict = {}

      # pmssm
      """
      sigdict["sig"]=float(f.Get(akey).GetMaximum())
      sigdict["x"]=akey.split("_")[2].strip("Nom")
      sigdict["y"]=akey.split("_")[1].strip("Nom")
      regionLineDict[sigdict["y"]+"_"+sigdict["x"]]=dict(sigdict.items()+linedict.items())
      """
      # SMA
      
      sigdict["sig"]=float(f.Get(akey).GetMaximum())
      sigdict["x"]=akey.split("_")[1].strip("Nom")
      sigdict["y"]=akey.split("_")[2].strip("Nom")
      regionLineDict[sigdict["x"]+"_"+sigdict["y"]]=dict(sigdict.items()+linedict.items())
      

  # now we have to convert this into the other dict's format dict["x_y"]
  print ""
  print "for grid",grid,"and region",region,"used points",pointsused
  print "N points:",len(pointsused)
  print ""
  return regionLineDict



if __name__ == "__main__":
  print "Starting signalyield"

  if not len(sys.argv) == 3:
    print "Usage:"
    print "python signalyield.py 'Grid' 'Regions'"
    print "regions have to be separated by underscores, e.g. SR1a_SR2"
    print "Exiting now.."
    sys.exit()

  print "Using MM prediction"
  time.sleep(2)

  #grids = ["DGemtR50"]
  grids = []
  regions = []
  missingf = []
  global missingf
  grids.append(sys.argv[-2])
  regions = sys.argv[-1].split("_")
  print "running: regions:",regions,"grids:", grids
  print "Directory of the source files", "/terabig/lmarti/svn_notes/plots_HCP/v18_app"
  #useBlindedData = True
  #grids = ["SMAnoslep7TeV"]
  #regions = ["SR2"]


  xRange = {}
  xRange["DGwSL100"] = [100, 360]
  xRange["DGwSL140"] = [100, 360]
  xRange["DGwSL250"] = [100, 360]
  xRange["DGemtR50"] = [100, 360]
  xRange["DGemt100"] = [90, 510]
  xRange["DGemt140"] = [90, 510]
  xRange["DGemt250"] = [90, 510]
  xRange["SMA0noW"] = [0, 750]
  xRange["SMAnoslep7TeV"] = [0, 350]
  xRange["SMAnoslep8TeV"] = [0, 500]
  xRange["SMAwslep8TeV"] = [0, 700]
  xRange["UED"] = [300, 900]
  xRange["GGM"] = [100, 800]

  yRange = {}
  yRange["DGwSL100"] = [100, 360]
  yRange["DGwSL140"] = [100, 360]
  yRange["DGwSL250"] = [100, 360]
  yRange["DGemtR50"] = [100, 360]
  yRange["DGemt100"] = [90, 510]
  yRange["DGemt140"] = [90, 510]
  yRange["DGemt250"] = [90, 510]
  yRange["SMA0noW"] = [0, 500]
  yRange["SMAnoslep7TeV"] = [-10, 240]
  yRange["SMAnoslep8TeV"] = [-10, 500]
  yRange["SMAwslep8TeV"] = [-10, 700]
  yRange["UED"] = [0, 45]
  yRange["GGM"] = [300, 1000]


  xAxis = {}
  xAxis["DGwSL100"] = "|#mu| [GeV]"
  xAxis["DGwSL140"] = "|#mu| [GeV]"
  xAxis["DGwSL250"] = "|#mu| [GeV]"
  xAxis["DGemtR50"] = "|#mu| [GeV]"
  xAxis["DGemt100"] = "#mu [GeV]"
  xAxis["DGemt140"] = "#mu [GeV]"
  xAxis["DGemt250"] = "#mu [GeV]"
  xAxis["SMA0noW"] = "#M_{C1} [GeV]"
  xAxis["SMAnoslep7TeV"] = "#M_{C1} [GeV]"
  xAxis["SMAnoslep8TeV"] = "#M_{C1} [GeV]"
  xAxis["SMAwslep8TeV"] = "#M_{C1} [GeV]"
  xAxis["UED"] = "R^{-1} [GeV]"
  xAxis["GGM"] = "M_{#tilde{H}} [GeV]"

  yAxis = {}
  yAxis["DGwSL100"] = "M2 [GeV]"
  yAxis["DGwSL140"] = "M2 [GeV]"
  yAxis["DGwSL250"] = "M2 [GeV]"
  yAxis["DGemtR50"] = "M2 [GeV]"
  yAxis["DGemt100"] = "M2 [GeV]"
  yAxis["DGemt140"] = "M2 [GeV]"
  yAxis["DGemt250"] = "M2 [GeV]"
  yAxis["SMA0noW"] = "M_{N1} [GeV]"
  yAxis["SMAnoslep7TeV"] = "M_{N1} [GeV]"
  yAxis["SMAnoslep8TeV"] = "M_{N1} [GeV]"
  yAxis["SMAwslep8TeV"] = "M_{N1} [GeV]"
  yAxis["UED"] = "#Lambda R"
  yAxis["GGM"] = "M_{#tilde{g}} [GeV]"
  
  
  histosdict = {}
  graphdict = {}
  summary = {}
  
  #pathtodatfiles = "/home/kruker/PhD/susy/multileptons/histfitter/HistFitter-00-00-05/data/mc_tina/20120529/"
  # should maybe also put the other filename and grid id and so on to set here instead of goofy implementing cases

    
  for gridi, grid in enumerate(grids):
    pathtorootfiles = "/terabig/lmarti/svn_notes/plots_HCP/v22_app/raw_"+str(grid)+"/3L.root"
    #pathtorootfiles = "/terabig/lmarti/HistFitter/tags_hf12_user_02/HistFitter/output/output_signalyields_dgemt100/3L.root"

    wzsinsr = []
    sinwznr = []
    #gridpoints = getListOfPoints(grid)

#   For text based output, example here (not tested)
#    filename = grid+"_WZNR_LimitInfo.dat"
#    wznrdict = interpretLimitFile(pathtodatfiles+filename)

    # for Histfitter output
    wznrdict = interpretHistFitterFile(pathtorootfiles, "L3CR", grid)
    onegp = wznrdict.keys()[0]
    dataWZNR = wznrdict[onegp]["data"]
    wzInWZNR = float(wznrdict[onegp]["WZSherpa"])
    fakeInWZNR = float(wznrdict[onegp]["FakeBackground"])
    otherIrrInWZNR = float(wznrdict[onegp]["OtherIrr"])
    totBGinWZNR = wzInWZNR + fakeInWZNR + otherIrrInWZNR
    summary["WZSherpaCR"] = wzInWZNR
    summary["dataCR"]=dataWZNR
    summary["FakeBackgroundCR"]=fakeInWZNR
    summary["OtherIrrCR"]= otherIrrInWZNR
    summary["totBGCR"]=totBGinWZNR

    binning = 0.5
    nbinsX = int((xRange[grid][1] - xRange[grid][0]) / binning)
    nbinsY = int((yRange[grid][1] - yRange[grid][0]) / binning)
    offsetX = float(xRange[grid][0])
    offsetY = float(yRange[grid][0])

    histosdict["signalInWZNR_"+grid] = TH2F("h_signalInWZNR_"+grid, grid+": signal in WZNR, wz(MC) = 139.9, tot BG = 148.0;"+xAxis[grid]+";"+yAxis[grid], nbinsX, xRange[grid][0], xRange[grid][1], nbinsY, yRange[grid][0], yRange[grid][1])
    histosdict["signalInWZNR_"+grid].SetStats(kFALSE)
    histosdict["signalOverSqrtBGInWZNR_"+grid] = TH2F("h_signalOverSqrtBGInWZNR_"+grid, grid+": signal/#sqrt{BG};"+xAxis[grid]+";"+yAxis[grid], nbinsX, xRange[grid][0], xRange[grid][1], nbinsY, yRange[grid][0], yRange[grid][1])
    histosdict["signalOverSqrtBGInWZNR_"+grid].SetStats(kFALSE)
    
    graphdict["signalInWZNR_"+grid] = TGraph2D()
    graphdict["signalInWZNR_"+grid].SetName("gr_signalInWZNR_"+grid)
    #graphdict["signalInWZNR_"+grid].GetXaxis().SetTitle()
    
    graphdict["signalOverSqrtBGInWZNR_"+grid] =  TGraph2D()
    graphdict["signalOverSqrtBGInWZNR_"+grid].SetName("gr_signalOverSqrtBGInWZNR_"+grid)

    point = 0
    for gp, gpdict in wznrdict.iteritems():
      sInWZNR = float(gpdict["sig"])
      binx = int( (float(gpdict["x"]) - offsetX)/binning)
      biny = int( (float(gpdict["y"]) - offsetY)/binning)
      pointx = float(gpdict["x"])
      pointy = float(gpdict["y"])
      histosdict["signalInWZNR_"+grid].SetBinContent(binx, biny, sInWZNR)
      histosdict["signalOverSqrtBGInWZNR_"+grid].SetBinContent(binx, biny, sInWZNR/sqrt(totBGinWZNR))
      graphdict["signalInWZNR_"+grid].SetPoint(point, pointx, pointy, sInWZNR)
      graphdict["signalOverSqrtBGInWZNR_"+grid].SetPoint(point, pointx, pointy, sInWZNR/sqrt(totBGinWZNR))
      point += 1
    
    for regi, reg in enumerate(regions):

      filename = grid+"_"+reg+"_LimitInfo.dat"
      #regionLineDict = interpretLimitFile(pathtodatfiles+filename)
      regionLineDict = interpretHistFitterFile(pathtorootfiles, reg, grid)
      try:
        wzInSR = float(regionLineDict[gp]["WZSherpa"])
      except:
        print "WARNING!!!!!, keyerror at ",gp
        try:
          wzInSR = float(regionLineDict[wznrdict.keys()[0]]["WZSherpa"])
          gp = wznrdict.keys()[0]
        except:
          print "giving up"
          continue
      fakeInSR = float(regionLineDict[gp]["FakeBackground"])
      otherIrrInSR = float(regionLineDict[gp]["OtherIrr"])
      totBGinSR = wzInSR +otherIrrInSR  + fakeInSR
      summary["wzMC"+str(reg)]=wzInSR
      summary["fake"+str(reg)]=fakeInSR
      summary["otherIrr"+str(reg)]=otherIrrInSR
      summary["totBGMC"+str(reg)]=totBGinSR

      histosdict["bias_"+grid+"_"+reg] = TH2F("h_bias_"+grid+"_"+reg, grid+", "+reg+": #frac{BG with signal contamination}{BG no signal contamination};"
                                              +xAxis[grid]+";"+yAxis[grid], nbinsX, xRange[grid][0], xRange[grid][1], nbinsY, yRange[grid][0], yRange[grid][1])
      histosdict["bias_"+grid+"_"+reg].SetStats(kFALSE)
      graphdict["bias_"+grid+"_"+reg] = TGraph2D()

      graphdict["scaleFactors_"+grid+"_"+reg] =  TGraph2D()
      histosdict["scaleFactors_"+grid+"_"+reg] = TH2F("h_scaleFactors_"+grid+"_"+reg, grid+","+reg+": c_WZ;"+xAxis[grid]+";"+yAxis[grid], nbinsX, 
                                                      xRange[grid][0], xRange[grid][1], nbinsY, yRange[grid][0], yRange[grid][1])
      histosdict["scaleFactors_"+grid+"_"+reg].SetStats(kFALSE)

      graphdict["transferFactors_"+grid+"_"+reg] =  TGraph2D()
      # HARDCODED transferfactors # value for 10fb
      minidict = {}
      minidict["L3SR1a"]=0.2337
      minidict["L3SR1b"]=0.008578
      minidict["L3SR2"]= 0.03288
      minidict["L3VR1"]= 1.117
      minidict["L3VR2"]= 0.03217
      minidict["L3VR3"]= 2.6791

      histosdict["transferFactors_"+grid+"_"+reg] = TH2F("h_transferFactors_"+grid+"_"+reg, "Transfer factor, wz="+str(minidict[reg])+";"+xAxis[grid]+";"+
                                                            yAxis[grid], nbinsX, xRange[grid][0], xRange[grid][1], nbinsY, yRange[grid][0], yRange[grid][1])
      histosdict["transferFactors_"+grid+"_"+reg].SetStats(kFALSE)


      graphdict["scaleFactors_ratio_"+grid+"_"+reg] =  TGraph2D()
      histosdict["scaleFactors_ratio_"+grid+"_"+reg] = TH2F("h_scaleFactors_ratio_"+grid+"_"+reg, grid+","+reg+": c_WZ / c_sig;"+xAxis[grid]+";"+yAxis[grid], 
                                                            nbinsX, xRange[grid][0], xRange[grid][1], nbinsY, yRange[grid][0], yRange[grid][1])
      histosdict["scaleFactors_ratio_"+grid+"_"+reg].SetStats(kFALSE)

      graphdict["diff_scale_fit_"+grid+"_"+reg] =  TGraph2D()
      histosdict["diff_scale_fit_"+grid+"_"+reg] = TH2F("h_diff_scale_fit_"+grid+"_"+reg, grid+","+reg+": wz_fit-wz_scale / wz_fit;"+xAxis[grid]+";"+yAxis[grid], 
                                                        nbinsX, xRange[grid][0], xRange[grid][1], nbinsY, yRange[grid][0], yRange[grid][1])
      histosdict["diff_scale_fit_"+grid+"_"+reg].SetStats(kFALSE)

      graphdict["signal_yield_"+grid+"_"+reg] =  TGraph2D()
      histosdict["signal_yield_"+grid+"_"+reg] = TH2F("h_signal_yield_"+grid+"_"+reg, grid+","+reg+": signal_yield(MC);"+xAxis[grid]+";"+yAxis[grid], nbinsX, 
                                                        xRange[grid][0], xRange[grid][1], nbinsY, yRange[grid][0], yRange[grid][1])
      histosdict["signal_yield_"+grid+"_"+reg].SetStats(kFALSE)

      # HACK SMAwslep8TeV.. set 700 0 to 0
      """
      histosdict["transferFactors_"+grid+"_"+reg].SetBinContent(700, 0, 0.)
      graphdict["transferFactors_"+grid+"_"+reg].SetPoint(0, 700, 0, 0.)
      histosdict["signal_yield_"+grid+"_"+reg].SetBinContent(700, 0, 0.)
      graphdict["signal_yield_"+grid+"_"+reg].SetPoint(0, 700, 0, 0.)
      histosdict["signalInWZNR_"+grid].SetBinContent(700, 0, 0)
      graphdict["signalInWZNR_"+grid].SetPoint(point, 700, 0, 0)
      ppp =1
      """

      # HACK DGemt100. set 100 500 to 0
      """
      histosdict["transferFactors_"+grid+"_"+reg].SetBinContent(100, 500, 0.)
      graphdict["transferFactors_"+grid+"_"+reg].SetPoint(0, 100, 500, 0.)
      histosdict["signal_yield_"+grid+"_"+reg].SetBinContent(100, 500, 0.)
      graphdict["signal_yield_"+grid+"_"+reg].SetPoint(0, 100, 500, 0.)
      histosdict["signalInWZNR_"+grid].SetBinContent(100, 500, 0)
      graphdict["signalInWZNR_"+grid].SetPoint(point, 100, 500, 0)
      """

      ppp = 1
      for gp, gpdict in regionLineDict.iteritems():
        
        if gpdict["x"] == "" or gpdict["y"] == "":
          continue
        
        sInSR = float(gpdict["sig"])
        try:
            sInWZNR = float(wznrdict[gp]["sig"])
        except:
             continue     
        print "Reading the workspace now: point",gp.strip("Nom"),"regions",reg,"grid",grid
        wzfromoutfile, sigfromoutfile = getfittedValuesInSR(gp.strip("Nom"), reg, grid)
        c = wzInSR/wzInWZNR
        cs = sInSR/sInWZNR
        summary["wzfitted"+str(reg)]=wzfromoutfile
        summary["sfitted"+str(reg)]=sInSR
        totBGinSRfit =wzfromoutfile +otherIrrInSR  + fakeInSR
        summary["totBGfitted"+str(reg)]=totBGinSRfit
        summary["Data"+str(reg)]=gpdict["data"]
        wzsinsr.append(wzfromoutfile)
        sinwznr.append(sInWZNR)

        
        # wz prediction not using simfit but scaling wz in wznr
        normWZinWZNR = dataWZNR - fakeInWZNR - otherIrrInWZNR
        normWZinWZNRwithSignalCont =  normWZinWZNR - sInWZNR
        normWZinSR = normWZinWZNR * c
        normWZinSRwithSignalCont = normWZinWZNRwithSignalCont * c

        normBgNoSignalCont = normWZinSR + otherIrrInSR + fakeInSR
        normBgWithSignalCont = normWZinSRwithSignalCont + otherIrrInSR + fakeInSR

        bias = normBgWithSignalCont/float(normBgNoSignalCont)

        binx = int((float(gpdict["x"]) - offsetX)/binning)
        biny = int((float(gpdict["y"]) - offsetY)/binning)
        pointx = float(gpdict["x"]) 
        pointy = float(gpdict["y"])


        histosdict["bias_"+grid+"_"+reg].SetBinContent(binx, biny, bias)
        graphdict["bias_"+grid+"_"+reg].SetPoint(ppp, pointx, pointy, bias)
        histosdict["scaleFactors_"+grid+"_"+reg].SetBinContent(binx, biny, wzfromoutfile/wzInSR)
        graphdict["scaleFactors_"+grid+"_"+reg].SetPoint(ppp, pointx, pointy, wzfromoutfile/wzInSR)
        histosdict["transferFactors_"+grid+"_"+reg].SetBinContent(binx, biny, sInSR/sInWZNR)
        graphdict["transferFactors_"+grid+"_"+reg].SetPoint(ppp, pointx, pointy, sInSR/sInWZNR)
        histosdict["scaleFactors_ratio_"+grid+"_"+reg].SetBinContent(binx, biny, (wzfromoutfile/wzInSR)/(sigfromoutfile/sInSR))
        graphdict["scaleFactors_ratio_"+grid+"_"+reg].SetPoint(ppp, pointx, pointy, (wzfromoutfile/wzInSR)/(sigfromoutfile/sInSR))
        histosdict["diff_scale_fit_"+grid+"_"+reg].SetBinContent(binx, biny, (wzfromoutfile-normWZinSRwithSignalCont)/(wzfromoutfile))
        graphdict["diff_scale_fit_"+grid+"_"+reg].SetPoint(ppp, pointx, pointy, (wzfromoutfile-normWZinSRwithSignalCont)/(wzfromoutfile))
        histosdict["signal_yield_"+grid+"_"+reg].SetBinContent(binx, biny, sInSR)
        graphdict["signal_yield_"+grid+"_"+reg].SetPoint(ppp, pointx, pointy,sInSR)
        ppp += 1


  newgraphdict = pythoncopydude.deepcopy(graphdict)
  rootfile = TFile("signalYields.root", "recreate")
  texfile = open("signalYieldsInWZNR.tex", 'w')
  texfile.write("\\documentclass[a4paper,11pt]{article}\n\\usepackage[utf8x]{inputenc}\n")
  texfile.write("\\usepackage{graphicx}\n\\usepackage{subfigure}\n\\usepackage[top=2cm, bottom=2cm, left=1.5cm, right=1.5cm]{geometry}\n\n\\begin{document}\n\n")
  texstring_1 = "\\begin{figure}[htp]\n \\begin{center} \n\\subfigure[]\n{\\includegraphics[scale=0.35]{"
  texstring_2 = "}} \\hfill\n \\subfigure[]\n {\includegraphics[scale=0.35]{"
  texstring_3 = "}} \n \\hfill \n \\end{center} \n \\end{figure}\n\n"
  texstr_a = "\\begin{figure}[htp]\n \\begin{center} \n\\includegraphics[scale=1]{"
  texstr_b = "}\n \\end{center} \n \\end{figure}\n\n"


  oldhistoname = ""
  nbhistos = 0
  for hi, histo in histosdict.iteritems():
    print "working on histogram",hi
    gStyle.SetPaintTextFormat("1.3g");
    #gStyle.SetTextSize(0.8)
    #gStyle.SetMarkerSize(1.2)
    c = TCanvas("c", "c", 800,600)
    #gPad.SetLogz()
    #c.SetLogz()
    
    histo.Draw("AXIS")

    # DGemt
    #htemp = TH2F("temp_2","",50,100,500,50,100,500)
    # SMA noslep
    #htemp = TH2F("temp_2","",50,0,500,50,-10,500)
    htemp = TH2F("temp_2","",50,xRange[grid][0],xRange[grid][1],50,yRange[grid][0],yRange[grid][1])

    newgraphdict[hi].SetHistogram(htemp)
    
    htemper = newgraphdict[hi].GetHistogram()


    htemper.Draw("same,COLZ")

    if "transfer" in hi:
        if "L3SR1a" in hi:
            if htemper.GetMaximum() > minidict["L3SR1a"]*1000:
                mymax = minidict["L3SR1a"]*1000
            else:
                mymax = htemper.GetMaximum()
            htemper.GetZaxis().SetRangeUser(minidict["L3SR1a"]*0.1, mymax)
        if "L3SR1b" in hi:
            if htemper.GetMaximum() > minidict["L3SR1b"]*1000:
                mymax = minidict["L3SR1b"]*1000
            else:
                mymax = htemper.GetMaximum()
            htemper.GetZaxis().SetRangeUser(minidict["L3SR1b"]*0.1, mymax)
        if "L3SR2" in hi:
            if htemper.GetMaximum() > minidict["L3SR2"]*1000:
                mymax = minidict["L3SR2"]*1000
            else:
                mymax = htemper.GetMaximum()
            htemper.GetZaxis().SetRangeUser(minidict["L3SR2"]*0.1, mymax)
        c.SetLogz()
    if "signalInWZNR" in hi:
        htemper.GetZaxis().SetRangeUser(0.0001,10.)
    if "scaleFactors_ratio" in hi:
        c.SetLogz()
    if "bias" in hi:
        htemper.GetZaxis().SetRangeUser(0.9, 1.1)
    # put the expected number of bg events into this one
    if "signal_yield" in hi:
        #c.SetLogz()
        htemper.GetZaxis().SetRangeUser(0.0001,100.)

    #htemper.Smooth(1,"k5a")

    #graphdict[hi].Draw("same,contz")
    #histo.Draw("CONT4Z")
    histo.Draw("text,same")


    histo.Write()
    c.Print(hi+".eps")
    c.Print(hi+".png")
    
    if not "bias" in hi:
      nbhistos += 1
      texfile.write(texstr_a+hi+".eps"+texstr_b)
    htemp.SetDirectory(0)
    
    del c

  # show some control numbers
  print "Summary:"
  print "CR:"
  for akey,anentry in summary.iteritems():
    if "CR" in akey:
      print akey,anentry
  print "SR's:"
  for akey,anentry in summary.iteritems():
    if "CR" not in akey:
      print akey,anentry
  print "lowest wz in sr:",sorted(wzsinsr)[0], "highestwz",sorted(wzsinsr)[-1]
  print "lowest s in wznr:",sorted(sinwznr)[0], "highestwz",sorted(sinwznr)[-1]
  print missingf

  texfile.write("\n\end{document}\n")
  texfile.close()
  rootfile.Close()

  print "All done, bye"
