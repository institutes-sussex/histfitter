"""
script to check whether the upper limit scan was successful or not.
"""

from os import *
from ROOT import *
import sys


# path to upperlimit files
inpath = sys.argv[-1]
# expected number of scans
n_def = 10
toys = False


def usage():
    print ""
    print "Usage:"
    print "checkUpperLimits.py PathToFiles"
    print ""

if len(sys.argv)<2:
    usage()
    sys.exit()

if not inpath.endswith("/"):
  inpath+="/"

print "Scanning",inpath

if not path.exists(inpath):
    print "exit, folder does not exist"
    sys.exit()

problems = []
info = []

# start looking through the files
ls = listdir(inpath)

for f in ls:
    roof = TFile(inpath+str(f))
    if not "upperlimit" in f:
        print "No upper limit file, skipping",f
        continue
    hypo = "hypo_"+f.split("_upperlimit_")[-1].strip(".root")
    if toys:
        hypo = "result_mu_SIG"
    try:
        n_scans = roof.Get(hypo).ArraySize()
        if n_def - n_scans > 1:
            problems.append([f,"scanned points found "+str(n_scans)])
        roof.Get(hypo).CLs(n_scans-1)
    except:
        # try to find the hypo with the unusual name
        print "Did not find hypo test result, checking for different naming"
        for akey in roof.GetListOfKeys():
            if "hypo" in akey.GetName():
                print "using",akey.GetName()
                hypo = akey.GetName()
                break
        try:
            roof.Get(hypo).CLs(n_scans-1)
        except:
            print "Skipping, no hypo ("+hypo+") result in",f
            problems.append(f)
            info.append([f,"unknown reason"])
            continue
    if roof.Get(hypo).CLs(1)<0.05 or roof.Get(hypo).CLs(n_scans-1)>0.05:
        details = []
        for i in range(n_scans):
            details.append('%.3f' % round(roof.Get(hypo).CLs(i),3))
        info.append([f,details])
        problems.append(f)
    del roof
print "\n ~~~ SUMMARY ~~~ \n"
print "problems"
for entry in problems:
    print entry
print ""
print "more info on those CLs values"
for entry in info:
    print entry
print "Done"

