from getCoordfromMCID import getXsecFromMasses
from ROOT import *
from array import array
import time
import sys,os
gROOT.SetBatch(True)

"""
script to plot the upper limit on the cross-section
and the signal strength for the simplified models.
Not joined with the limit curves at the moment.
Will print a file called numbers.root that can be fed to the contour.C
ToDo: fix annoying bug due to things being written to the same file
"""

grid_name = 'SMAnoslep8TeV'  # <======= set this
inpath="/ngse5/users/lmarti/upperlimits/L2L3Combination_1812_v3/upperlimits/"
runwith = "toys"

fb = 1000. # put this to 1 for pb
if 'SMAnoslep' in grid_name: fb=1000.
elif 'SMAwslep' in grid_name: fb=1000.
elif 'SMAwhiggs' in grid_name: fb=1000.
elif 'SMAwstaus' in grid_name: fb=1000.

# read out cross-sections to dictionary
xsec = getXsecFromMasses(grid_name)
# need file with -l output
mu_sig = {}

# ugly hack for the right handed sleptons that don't have their own input files
grid_name_original = ""
if "Slepr" in grid_name:
    grid_name_original = grid_name
    grid_name = grid_name.replace("Slepr","Slepl")

gROOT.SetBatch(True)
gROOT.LoadMacro("/terabig/schneider/SusyFitter/HistFitter/HistFitter-00-00-33_SLC6/macros/AtlasStyle.C")
SetAtlasStyle()
gStyle.SetPalette(1)

def colorHistoPadding(l, padding):
    lMin=min(l)
    lMax=max(l)
    for i in range(0,len(l)):
        if l[i]==lMin:
            l[i]=lMin-padding
        if l[i]==lMax:
            l[i]=lMax+padding
    return



print "xs dict", xsec
problems = []
if not inpath.endswith("/"):
  inpath+="/"

# read out the upper signal strength
for akey in xsec.keys():
    # hardcoded for the noslepton grid right now.
    hypo = ""
    print "opening root file " +str(inpath)+"3L_Output_upperlimit_"+str(grid_name)+"_"+str(akey[0])+"_"+str(akey[1])+".root"
    if os.path.exists(str(inpath)+"3L_Output_upperlimit_"+str(grid_name)+"_"+str(akey[0])+"_"+str(akey[1])+".root"):
        f = TFile(str(inpath)+"3L_Output_upperlimit_"+str(grid_name)+"_"+str(akey[0])+"_"+str(akey[1])+".root")
        hypo = "hypo_"+grid_name+"_"+str(akey[0])+"_"+str(akey[1])
    # some of the files mess up with int and float, try different naming too
    elif os.path.exists(str(inpath)+"3L_Output_upperlimit_"+str(grid_name)+"_"+str(akey[0]).replace(".0","")+"_"+str(akey[1]).replace(".0","")+".root"):
        f = TFile(str(inpath)+"3L_Output_upperlimit_"+str(grid_name)+"_"+str(akey[0]).replace(".0","")+"_"+str(akey[1]).replace(".0","")+".root")
        hypo = "hypo_"+grid_name+"_"+str(akey[0]).replace(".0","")+"_"+str(akey[1]).replace(".0","")
    else:
        print "could not find " +str(inpath)+"3L_Output_upperlimit_"+str(grid_name)+"_"+str(akey[0])+"_"+str(akey[1])+".root"
        continue
    if runwith == "toys":
        hypo = "result_mu_SIG"
    try:
        mu = f.Get(hypo).UpperLimit()
        if float(mu)==100.:
            problems.append([akey,"Failed scan"])
    except:
        print "we have a problem at",akey
        problems.append([akey,"No File"])
        continue
    mu_sig[akey]=mu
    f.Close()
    del f

# right handed sleptons, switch name back
if grid_name_original:
    print "Dealing with the right handed sleptons"
    grid_name = grid_name_original

# set up plotting
if "GGM" in grid_name:
    x_axis = '#mu [GeV]'
    y_axis = 'm_{#tilde{g}} [GeV]'
elif 'Wino' in grid_name:
    nlsp = '#tilde{#chi_{1}^{#pm}}'
    x_axis='m_{'+nlsp+'} [GeV]'
    y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'
elif 'Slepl' in grid_name:
    nlsp = '#tilde{l}'
    x_axis='m_{'+nlsp+'} [GeV]'
    y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'
elif 'Slepr' in grid_name:
    nlsp = '#tilde{l}'
    x_axis='m_{'+nlsp+'} [GeV]'
    y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'
elif 'Sneut' in grid_name:
    nlsp = '#tilde{#nu}'
    x_axis='m_{'+nlsp+'} [GeV]'
    y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'
elif 'Gluino' in grid_name:
    nlsp = '#tilde{g}'
    x_axis='m_{'+nlsp+'} [GeV]'
    y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'
elif 'N2N3' in grid_name:
    nlsp = '#tilde{#chi}_{2/3}^{0}'
    x_axis='m_{#tilde{#chi}_{2/3}^{0}} [GeV]'
    y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'
else:
    x_axis = 'm_{#tilde{#chi}^{0}_{2}, #tilde{#chi}^{#pm}_{1}} [GeV]'
    y_axis = 'm_{#tilde{#chi}_{1}^{0}} [GeV]'
gStyle.SetOptStat(00000000)
gStyle.SetPaintTextFormat('5.1f')
# upperlimit = excluded
plot_items = ["upperlimit","excluded_mu_sig","xs"]


### Get filter efficiencies (only needed for SM via WZ (?))
def getFE(akey):
    if   akey[0]==100.0 and akey[1]== 87.5: return 0.3181700000
    elif akey[0]==125.0 and akey[1]==112.5: return 0.3358500000
    elif akey[0]==150.0 and akey[1]==125.0: return 0.6749900000
    elif akey[0]==150.0 and akey[1]==137.5: return 0.3456800000
    elif akey[0]==175.0 and akey[1]==125.0: return 0.8759600000
    elif akey[0]==175.0 and akey[1]==150.0: return 0.6869100000
    elif akey[0]==175.0 and akey[1]==162.5: return 0.3453300000
    elif akey[0]==200.0 and akey[1]==125.0: return 0.9433100000
    elif akey[0]==200.0 and akey[1]==175.0: return 0.6961800000
    elif akey[0]==200.0 and akey[1]==187.5: return 0.3464500000
    else: return 1.

### Fill the x and y values into an array
x=array('f')
y=array('f')
for akey in mu_sig.keys():
    x.append(akey[0])
    y.append(akey[1])


for anitem in plot_items:
    ### Get the values from the dictionaries
    output_file_name    = "plots/test1/"+str(anitem)+"_"+grid_name+".eps"
    if anitem=="upperlimit":
        histogram_title     = "Excluded model cross sections"
    elif anitem=="excluded_mu_sig":
        histogram_title     = "Excluded mu_sig"
    else:
        histogram_title     = str(anitem)
    ### Fill the z values into an array
    z=array('f')
    for akey in mu_sig.keys():
        if float(mu_sig[akey])==0.:
            z.append(0.0001)
            continue
        if anitem == "upperlimit":
            br=1.
            if 'SMAnoslep' in grid_name:
                fe = getFE(akey)
                br=1000*0.324098*0.100788*fe
            z.append(float(fb*xsec[akey])*float(mu_sig[akey])/br)
        elif anitem == "excluded_mu_sig":
            z.append(float(mu_sig[akey]))
        else:
            z.append(float(fb*xsec[akey]))
    ### Parent histogram out of which we clone a histogram for printing the numbers (not interpolated) and one for the colors (interpolated)

    # those 100 are the number of bins
    if 'SMAwslep' in grid_name: padding=28
    else: padding=10
    h_parent = TH2F("upperlimit","",100,min(x)-padding,max(x)+padding,100,min(y)-padding,max(y)+padding)
    gStyle.SetTitleFontSize(0.035);
    h_parent.SetXTitle(x_axis)
    h_parent.SetYTitle(y_axis)
    if 'SMAnoslep' in grid_name:
        h_parent.SetZTitle("excluded model cross sections [pb]")
    else:
        h_parent.SetZTitle("excluded model cross sections [fb]")
    if anitem == "excluded_mu_sig":
        h_parent.GetZaxis().SetRangeUser(0., 10)
    # axis labeling for each grid
    if "GGM" in grid_name:
        h_parent.GetXaxis().SetRangeUser(200.,900.)
    h_numbers = h_parent.Clone()
    for i,element in enumerate(x):
        if anitem == "upperlimit":
            h_numbers.Fill(x[i],y[i],z[i])
        else:
            h_numbers.Fill(x[i],y[i],z[i])
    ### Draw the colors to the borders, i.e. move data points if they are at the border
    if anitem=="upperlimit": colorHistoPadding(x, padding)
    if anitem=="upperlimit": colorHistoPadding(y, padding)
    ### First define graph and then derive histogram from graph. This way the the colors between the points are interpolated
    g = TGraph2D(len(x),x,y,z)
    g.SetHistogram(h_parent)
    h_colors = g.GetHistogram()

    c = TCanvas(histogram_title,histogram_title,0,0,800,600)
    gPad.SetRightMargin(0.15)
    if anitem=="upperlimit":
        #gPad.SetLogz()
        if "SMAnoslep" in grid_name:
            h_parent.GetZaxis().SetRangeUser(0.01,1000.)
        else:
            if len(z)>10:
                h_parent.GetZaxis().SetRangeUser(0.,sorted(z)[-8])
    h_colors.GetXaxis().SetTitleOffset(1.25)
    h_colors.GetYaxis().SetTitleOffset(1.65)

    h_colors.Draw('COLZ')

    ### Draw contour line
    if 'SMAnoslep' in grid_name:
        fContour=TFile("plots/SMAnoslep8TeV_contour.root")
    else:
        fContour=TFile("plots/SMAwslep8TeV_contour.root")
    hContour=fContour.Get("contour_obs")
    ### Draw the TH2 to the border
    if 'SMAnoslep' in grid_name:
        for xContour in range(int(min(x)-padding), 365):
            hContour.Fill(xContour,min(y)-(padding/2.),.5)
        for yContour in range(int(min(y)-padding), 70):
            hContour.Fill(min(x)-(padding/2.),yContour,.5)
    elif 'SMAwslep' in grid_name:
        for xContour in range(int(min(x)), 740):
            hContour.Fill(xContour,min(y),.5)
        for yContour in range(int(min(y)), 80):
            hContour.Fill(min(x),yContour,.5)

    hContour.Draw('CONT1,SAME')



    if 'SMAwslep' in grid_name: h_numbers.SetMarkerSize(1.)
    h_numbers.Draw('TEXT45,SAME')

    #h_title = TLatex()
    #h_title.SetNDC()
    #h_title.SetTextSize(0.032)
    #h_title.SetTextFont(52)
    #h_title.DrawLatex(0.15,0.955,str(histogram_title))

    latexAtlas = TLatex()
    latexAtlas.SetNDC()
    #latexAtlas.SetTextSize(.05)
    latexAtlas.SetTextFont(72)
    latexAtlas.DrawLatex(.19,.87,"ATLAS")
    #latexAtlas.SetTextSize(.04)
    latexAtlas.SetTextFont(52)
    latexAtlas.SetTextSize(.03)
    latexAtlas.DrawLatex(.32,.87,"Internal")
    latexAtlas.SetTextSize(.032)
    latexAtlas.DrawLatex(.19,.79,"#int L dt = 20.3 fb^{-1}")
    latexAtlas.DrawLatex(.19,.73,"#sqrt{s} = 8 TeV")


    if anitem=="upperlimit":
      outf = TFile(grid_name+".root","RECREATE")
      h_numbers.Write()
      outf.Close()

    c.Print(output_file_name)

    del z,h_parent,h_numbers,g,h_colors,c,output_file_name,histogram_title,hContour,fContour

print "problems:"
print problems
print "mu's:"
print sorted(mu_sig.values())
print "done, in so many ways"
