# script to generate visible cross-section limits.
# output should be a latex table, as well as a readable 
# text file
#
# usage: 
# python tools/visibleXsec.py "FinalState" "SR"
# where Finalstate is either L4 or L3 and SR an underscore separated list, e.g. SR400_SR401

# Example tex output from seminar note:
# $N_{\rm signal}$ Excluded (exp) & 3.9 & 3.6 & 5.3 & 6.7 & 4.5 \\ % toys, 16.03
# $N_{\rm signal}$ Excluded (obs) & 4.7 & 3.7 & 7.5 & 10.4 & 6.5 \\
# $\sigma_{\rm visible}$ Excluded (exp) [fb] & 0.19 & 0.17 & 0.26 & 0.32 & 0.22 \\
# $\sigma_{\rm visible}$ Excluded (obs) [fb] & 0.23 & 0.18 & 0.36 & 0.50 & 0.31

import os,sys,subprocess
from time import strftime,sleep
from ROOT import *

### some settings
verbose = True
lumi = 20.3
### should be no need to touch the rest

def usage():
    print "usage"
    print  'python tools/visibleXsec.py "FinalState" "SR"'
    print  'where Finalstate is either L4 or L3 and SR an underscore separated list, e.g. SR400_SR401'



# main part starts here
if not len(sys.argv)==3:
    print "ERROR, not enough arguments"
    usage()
    sys.exit()

outtime = strftime("%d%m_2013_%H%M%S")
fs = sys.argv[-2]
if not (fs=="L4" or fs=="L3"):
    print "ERROR, finalstate not recognized"
    usage()
    sys.exit()

if os.path.isfile("results/3L_Output_upperlimit.root"):
    print "Doing a backup from your current upper limits file"
    os.rename("results/3L_Output_upperlimit.root", "results/3L_Output_upperlimit_backup_"+outtime+".root")

sr_str = sys.argv[-1]
sr_list = sr_str.split("_")
for sr in sr_list:
    cmd ='HistFitter.py -t -w -l -u "--signalYield 1 --signalRegions '+sr+' --finalState '+fs+' --vxOrp0 1" tools/3L_config.py'
    #HistFitter.py -t -w -l -u "--signalYield 1 --signalRegions SR400 --finalState L4 --vxOrp0 1" tools/3L_config.py
    p = subprocess.Popen(cmd,shell=True)
    p.wait()


print "Done running, collecting output"
sleep(2)
f = TFile("results/3L_Output_upperlimit.root")
# dictionare with the results. Format is results['SR']=("vx_exp","vx_obs")
results = {}
for i,sr in enumerate(sr_list):
    hypo = f.Get("hypo_DiscoveryMode_SIG;"+str(i+1))
    results[sr]=(hypo.GetExpectedUpperLimit(),hypo.UpperLimit())

if verbose:
    print results

# time to write the output
# rounding to 3 digits
print "Writing output to","vx_"+sr_str+"_"+outtime+".tex/.txt"
with open("results/vx_"+sr_str+"_"+outtime+".txt","w") as f_txt:
    for sr in sr_list:
        print >> f_txt, sr,"n_exp:",'%.3f' % round(results[sr][0],3),"sigma_exp:",'%.3f' % round(results[sr][0]/lumi,3),
        print >> f_txt,    "n_obs:",'%.3f' % round(results[sr][1],3),"sigma_obs:",'%.3f' % round(results[sr][1]/lumi,3)

table_exp = "\t & "
n_exp = " $N_{\\rm signal}$ Excluded (exp) & "
n_obs = " $N_{\\rm signal}$ Excluded (obs) & "
s_exp = " $\sigma_{\\rm visible}$ Excluded (exp) [fb] & "
s_obs = " $\sigma_{\\rm visible}$ Excluded (obs) [fb] & "

for sr in sr_list:
    table_exp+=sr+" & "
    n_exp+='%.2f' % round(results[sr][0],2)+" & "
    n_obs+='%.2f' % round(results[sr][1],2)+" & "
    s_exp+='%.2f' % round(results[sr][0]/lumi,2)+" & "
    s_obs+='%.2f' % round(results[sr][1]/lumi,2)+" & "

with open("results/vx_"+outtime+".tex","w") as f_tex:
    print >> f_tex, table_exp.rstrip(" & ")+" \\\\ "
    print >> f_tex, n_exp.rstrip(" & ")+" \\\\ "
    print >> f_tex, n_obs.rstrip(" & ")+" \\\\ "
    print >> f_tex, s_exp.rstrip(" & ")+" \\\\ "
    print >> f_tex, s_obs.rstrip(" & ")

print "All done"


