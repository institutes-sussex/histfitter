#!/bin/python
import sys, os
from ROOT import *
from time import strftime, localtime

theoryBands = True
showPlot = False
checkSR = False


gROOT.SetBatch(True)

finalStates=['L3']

#A new grid needs to be implemented:
# - in the list just below
# - in the 3L_config.py the list of the sigSamples
# - the format of the string in makelistfiles.C
# - the list entries at the top of contour.C
# - the corresponding root file must exist

grids=['SMAwslep13TeV',]

#A new signal region needs to be implemented:
# - in the list just below
# - as cuts in the 3L_config.py
srs = ['SR1a']

finalStates = ['L3']

# signalYield: only needed for discovery and exclusion of visible X-sec
signalYield = 1
doVisibleXsecORdiscovery = int(0)

if len(sys.argv)!=4 and len(sys.argv)!=5 and len(sys.argv)!=6:
    print 'run.py needs exactly 3 or 4 arguments. Run \"run.py arg1 arg2 arg3 arg4\", where arg1 is an element from the list'
    print finalStates
    print 'and arg2 is an element from the list'
    print grids
    print 'and arg3 is one or more (separated by underscore) elements from the list,'
    print srs
    print 'and arg4 will be appended to the output name (use it as description).'
    print 'and arg5 is the optional argument to pass an int to indicate which PWC should be done.'
    print 'Exit.'
    sys.exit()

finalState = sys.argv[1]
grid = sys.argv[2]
sr = sys.argv[3]
if len(sys.argv)==5: dscrptn='_'+sys.argv[4]
else: dscrptn=''


if not finalState in finalStates:
    print 'Could not determine final state. Possible options are',finalStates,'.'
    print 'Exit.'
    sys.exit()

if not grid in grids:
    print 'Could not determine grid. Possible options are',grids,'.'
    print 'Exit.'
    sys.exit()

if "PWC" in grid:
    print 'Use the normal signal grid name, e.g. Winol1218TeV and not the ones with the PWC superfix'
    print 'Exit.'
    sys.exit()

if sr=='SR0a': sr='BIN01_BIN02_BIN03_BIN04_BIN05_BIN06_BIN07_BIN08_BIN09_BIN10_BIN11_BIN12_BIN13_BIN14_BIN15_BIN16_BIN17_BIN18_BIN19_BIN20'

if checkSR:
    for s in sr.split('_'):
        if not s in srs:
            print 'Could not determine signal region. Possible options are one or more (separated by underscore) elements from the list,',srs,'.'
            print 'Exit.'
            sys.exit()



# use user argument of HF
optstring = ""
optstring +="--signalGridName "+str(grid)
optstring +=" --signalYield "+str(signalYield)
optstring +=" --theoryUncertMode "+str(theoryBands)
optstring +=" --signalRegions "+str(sr)
optstring +=" --finalState "+str(finalState)
optstring +=" --vxOrp0 "+str(doVisibleXsecORdiscovery)

outName=finalState+'_'+grid+'_'+sr+dscrptn
if len(outName)>100: outName=finalState+'_'+grid+dscrptn
dirOutName=strftime("%Y-%m-%d_%H-%M-%S", localtime())+'_'+outName
dirOut='output/'+dirOutName+'/'

print 'Running...'

os.system('rm -f data/3L.root')
os.system('rm -rf data/3L*')
os.system('rm -f config/3L*')
os.system('rm -rf config/3L')
os.system('rm -rf results/3L')
os.system('rm -f results/3L* results/*L3* ')

os.system('mkdir -p '+dirOut+' '+dirOut+'/results/ '+dirOut+'/config/ '+dirOut+'/data/ '+dirOut+'/cls/ '+dirOut+'/contour/ '+dirOut+'/nSig/')
with open(dirOut+'out.txt','w') as f:
    f.write('finalState: '+finalState+'\n')
    f.write('grid: '+grid+'\n')
    f.write('sr: '+sr+'\n')
    f.write('dscrptn: '+dscrptn+'\n\n')



# main call to HF
os.system('HistFitter.py -t -w -D allPlots -f -p -x -u "'+optstring+'" tools/3L_config.py 2>&1 | tee -a '+dirOut+'out.txt')

print 'HistFitter.py -t -w -D allPlots -f -p -x -u "'+optstring+'" tools/3L_config.py 2>&1 | tee -a '+dirOut+'out.txt'
# if theoryBands:
#     os.system('root -l -b -q \'tools/makelistfiles.C(\"'+grid+'\",\"results/3L_Output_fixSigXSecNominal_hypotest.root\")\''+' 2>&1 | tee -a '+dirOut+'out.txt')
#     os.system('root -l -b -q \'tools/makelistfiles.C(\"'+grid+'\",\"results/3L_Output_fixSigXSecUp_hypotest.root\")\''+' 2>&1 | tee -a '+dirOut+'out.txt')
#     os.system('root -l -b -q \'tools/makelistfiles.C(\"'+grid+'\",\"results/3L_Output_fixSigXSecDown_hypotest.root\")\''+' 2>&1 | tee -a '+dirOut+'out.txt')
#     os.system('root -l -b -q \'tools/m0_vs_m12_nofloat.C(\"3L_Output_fixSigXSecNominal_hypotest__1_harvest_list\")\''+' 2>&1 | tee -a '+dirOut+'out.txt')
#     os.system('root -l -b -q \'tools/m0_vs_m12_nofloat.C(\"3L_Output_fixSigXSecUp_hypotest__1_harvest_list\")\''+' 2>&1 | tee -a '+dirOut+'out.txt')
#     os.system('root -l -b -q \'tools/m0_vs_m12_nofloat.C(\"3L_Output_fixSigXSecDown_hypotest__1_harvest_list\")\''+' 2>&1 | tee -a '+dirOut+'out.txt')
# else:
#     os.system('root -l -b -q \'tools/makelistfiles.C(\"'+grid+'\")\''+' 2>&1 | tee -a '+dirOut+'out.txt')
#     os.system('root -l -b -q tools/m0_vs_m12_nofloat.C'+' 2>&1 | tee -a '+dirOut+'out.txt')

# print 'Moving your output...'
# os.system('mv results/3L* '+dirOut+'/results/')
# os.system('mv results/*L3* '+dirOut+'/results/')
# os.system('mv config/3L* '+dirOut+'/config/')
# os.system('mv data/3L* '+dirOut+'/data/')
# os.system('mv 3L_Output_* '+dirOut)
# if theoryBands:
#     os.system('cp '+dirOut+'3L_Output_fixSigXSecNominal_hypotest__1_harvest_list '+dirOut+'contour/NoSys.txt')
#     os.system('cp '+dirOut+'3L_Output_fixSigXSecNominal_hypotest__1_harvest_list.root '+dirOut+'contour/NoSys.root')
#     os.system('cp '+dirOut+'3L_Output_fixSigXSecUp_hypotest__1_harvest_list '+dirOut+'contour/up.txt')
#     os.system('cp '+dirOut+'3L_Output_fixSigXSecUp_hypotest__1_harvest_list.root '+dirOut+'contour/up.root')
#     os.system('cp '+dirOut+'3L_Output_fixSigXSecDown_hypotest__1_harvest_list '+dirOut+'contour/down.txt')
#     os.system('cp '+dirOut+'3L_Output_fixSigXSecDown_hypotest__1_harvest_list.root '+dirOut+'contour/down.root')
#     os.system('mv '+dirOut+'3L_Output_fixSigXSecNominal_hypotest__1_harvest_list '+dirOut+outName+'_NoSys.txt')
#     os.system('mv '+dirOut+'3L_Output_fixSigXSecNominal_hypotest__1_harvest_list.root '+dirOut+outName+'_NoSys.root')
#     os.system('mv '+dirOut+'3L_Output_fixSigXSecUp_hypotest__1_harvest_list '+dirOut+outName+'_up.txt')
#     os.system('mv '+dirOut+'3L_Output_fixSigXSecUp_hypotest__1_harvest_list.root '+dirOut+outName+'_up.root')
#     os.system('mv '+dirOut+'3L_Output_fixSigXSecDown_hypotest__1_harvest_list '+dirOut+outName+'_down.txt')
#     os.system('mv '+dirOut+'3L_Output_fixSigXSecDown_hypotest__1_harvest_list.root '+dirOut+outName+'_down.root')
# else:
#     os.system('cp '+dirOut+'3L_Output_hypotest__1_harvest_list '+dirOut+'contour/NoSys.txt')
#     os.system('cp '+dirOut+'3L_Output_hypotest__1_harvest_list.root '+dirOut+'contour/NoSys.root')
#     os.system('mv '+dirOut+'3L_Output_hypotest__1_harvest_list '+dirOut+outName+'_NoSys.txt')
#     os.system('mv '+dirOut+'3L_Output_hypotest__1_harvest_list.root '+dirOut+outName+'_NoSys.root')


# os.system('root -l -b -q \'tools/contour/contour.C(\"'+dirOut+'\",\"'+grid+'\",\"'+sr+'\")\''+' 2>&1 | tee -a '+dirOut+'out.txt')
# os.system('cp tools/3L_config.py '+dirOut)
# os.system('mv plots '+dirOut)
# os.system('cp '+dirOut+'plots/NoSys.eps '+dirOut+outName+'.eps')
# os.system('python tools/plot_list_file.py -b '+dirOutName)
# os.system('python tools/plot_root_file.py -b '+dirOutName+' '+sr)

# print '\nYou can find your output in '+dirOut+' and there\'s a symbolic link to this directory under last_output.'

# os.system('rm -f last_output')
# os.system('ln -s '+dirOut+' last_output')


# if showPlot:
#     os.system('evince '+dirOut+'/plots/3L_Output_hypotest__1_harvest_list.eps')

print 'Bye.'

