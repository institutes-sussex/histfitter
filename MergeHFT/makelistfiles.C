#include <string>

using namespace std;

void makelistfiles(string grid,string inputfile="results/3L_Output_hypotest.root")
{
	const char* format     = "";
	const char* interpretation = "m0:m12";
	if (string("SMAnoslep8TeV") == grid){
		format = "hypo_SMAnoslep8TeV_%f_%f";
	}else if (string("SMAwslep8TeV") == grid){
		format = "hypo_SMAwslep8TeV_%f_%f";
	}else if (string("SMAwhiggs8TeV") == grid){
		format = "hypo_SMAwhiggs8TeV_%f_%f";
	}else if (string("SMAwstaus8TeV") == grid){
		format = "hypo_SMAwstaus8TeV_%f_%f";
	}else{
		cout << "Could not determine hypotest root file." << endl << "Exit." << endl;
		abort();
	}
	gSystem->Load("libSusyFitter.so");
	const char* cutStr = "1"; // accept everything
	string outputfile = /*Combination::*/CollectAndWriteHypoTestResults( inputfile, format, interpretation, cutStr ) ;
}

