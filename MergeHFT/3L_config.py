1################################################################
## In principle all you have to setup is defined in this file ##
################################################################


from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,TCanvas,TLegend,TLegendEntry
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from optparse import OptionParser
from math import sqrt
import sys

try:
    import signals,signalRegions
except ImportError:
    print "ERROR, you need to add tools/ to your pythonpath, e.g. in setup.sh"
    print "export PYTHONPATH=tools/:$PYTHONPATH"
    sys.exit(1)


#Argument which will be passed by run.py to determine to run over which grid and to know

# HF23, use -u option
inputParser = OptionParser()
inputParser.add_option('', '--signalGridName', dest = 'signalGridName', default = 'SMAwslep13TeV', help='name of the grid(e.g. SMAwslep13TeV)')
inputParser.add_option('', '--signalYield', dest = 'signalYield', default = '0', help='0 for exclusion, 1 for discovery, visible cross-section limit')
inputParser.add_option('', '--theoryUncertMode', dest = 'theoryUncertMode', default = "NoSys", help='sets theory bands NoSys/up/down')
inputParser.add_option('', '--signalRegions', dest = 'signalRegions', default = 'SR1a', help='sets which signal regions to use,(one or several, seperate with underscore)')
inputParser.add_option('', '--finalState', dest = 'finalState', default = 'L3', help='final state, e.g. L3')
inputParser.add_option('', '--vxOrp0', dest = 'vxOrp0', default = '0', help='do visible cross-section (1) or discovery p-value (2) or upperlimit cross-section (3)')
inputParser.add_option('', '--runOnly1Point', dest = 'runOnly1Point', default = '0', help='when 1 will abort after first point of the grid')
inputParser.add_option('', '--runPoint', dest = 'runPoint', default = '', help='put name of point you want to run in here')

configArgs = []


userArgs= configMgr.userArg.split(' ')
for i in userArgs:
    configArgs.append(i)

(options, args) = inputParser.parse_args(configArgs)
srs = options.signalRegions
doVisibleXsecORdiscovery = int(options.vxOrp0)
signalYield = float(options.signalYield)
sig_uncert = options.theoryUncertMode
finalState = options.finalState
grid = options.signalGridName
runOnly1Point = int(options.runOnly1Point)
runPoint = str(options.runPoint)

print "Your options are",(options, args)

# also use doValidation to get the BG only fit
# note: for discovery and exclusion of visible x-sec these flags are set automatically below.
doValidation=False
doDiscovery=False
doExclusion=True
doCutflow = False
useFakePrediction = True
allSysts = True

includeSystematics = False
whichConference = "ICHEP"

splitMCsystsIntoSamples=False
doSimFit=False

#----------------------------------------------------------------------------------------------------
if int(doVisibleXsecORdiscovery) == 1 or int(doVisibleXsecORdiscovery) == 2:
  doValidation = False
  doDiscovery = True
  doExclusion= False
  doCutflow = False
  doSimFit = False

#-------------------------------
# Parameters for hypothesis test
#-------------------------------
#configMgr.doHypoTest=False
if doDiscovery: configMgr.doExclusion=False
elif doExclusion: configMgr.doExclusion=True # True=exclusion, False=discovery

configMgr.nTOYs=10000
configMgr.calculatorType=2 # use 2 for asymptotic, 0 for toys
configMgr.testStatType=3
configMgr.nPoints=10
if sig_uncert=='True':
    configMgr.fixSigXSec=True
else:
    configMgr.fixSigXSec=False


#configManager options
# works with histfitter15+
if finalState=='L3': configMgr.blindSR=False
else: configMgr.blindSR=False
#configMgr.blindCR=True
#configMgr.useSignalInBlindedData=True



# There might be a smarter way

if doValidation and doExclusion:
  print "Can't do Validation and Exclusion, setting validation to false"
  doValidation = False



#-------------------------------------
# Now we start to build the data model
#-------------------------------------

# First define HistFactory attributes
configMgr.analysisName = "3L"
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 3.2 #2015
if whichConference=="ICHEP":
    configMgr.outputLumi = 5.8
else:
    print "not clear what lumi to scale to"
configMgr.setLumiUnits("fb-1")

# Set the files to read from
bgdFiles = []
if configMgr.readFromTree:
    bgdFiles.append("/lustre/scratch/epp/atlas/is86/Run2/Trees/SUSYcirculation/L3_BG13TeV.root")
else:
    bgdFiles = [configMgr.histCacheFile]
    pass
configMgr.setFileList(bgdFiles)

# to add signal regions go to tools/signalRegions
configMgr.cutsDict = signalRegions.getSignalRegions(whichConference)

configMgr.weights = ["eventweight"]


#--------------------
# List of systematics
#--------------------


configMgr.nomName = '_CENTRAL'
# AR_all_JES      = Systematic('AR_all_JES','_CENTRAL','_JESUP','_JESDOWN','tree','overallSys')
# AR_all_JVF      = Systematic('AR_all_JVF','_CENTRAL','_JVFUP','_JVFDOWN','tree','overallSys')
# AR_all_JER      = Systematic('AR_all_JER','_CENTRAL','_JER','_JER','tree','histoSysOneSideSym')
# AR_all_EES      = Systematic('AR_all_EES','_CENTRAL','_EESUP','_EESDOWN','tree','overallSys')
# AR_all_EESLOW   = Systematic('AR_all_EESLOW','_CENTRAL','_EESLOWUP','_EESLOWDOWN','tree','overallSys')
# AR_all_EESMAT   = Systematic('AR_all_EESMAT','_CENTRAL','_EESMATUP','_EESMATDOWN','tree','overallSys')
# AR_all_EESPS    = Systematic('AR_all_EESPS','_CENTRAL','_EESPSUP','_EESPSDOWN','tree','overallSys')
# AR_all_EESZ     = Systematic('AR_all_EESZ','_CENTRAL','_EESZUP','_EESZDOWN','tree','overallSys')
# AR_all_EER      = Systematic('AR_all_EER','_CENTRAL','_EERUP','_EERDOWN','tree','overallSys')
# AR_all_MES      = Systematic('AR_all_MES','_CENTRAL','_MESUP','_MESDOWN','tree','overallSys')
# AR_all_MID      = Systematic('AR_all_MID','_CENTRAL','_MIDUP','_MIDDOWN','tree','overallSys')
# AR_all_MMS      = Systematic('AR_all_MMS','_CENTRAL','_MMSUP','_MMSDOWN','tree','overallSys')
# AR_all_ETRIGREW = Systematic('AR_all_ETRIGREW',configMgr.weights, 1.05,0.95, "user","userOverallSys")
# AR_all_ETRIGREW4L=Systematic('AR_all_ETRIGREW4L',configMgr.weights, 1.05,0.95, "user","userOverallSys")
# AR_all_TRIGSF   = Systematic('AR_all_TRIGSF','_CENTRAL','_TRIGSF_UP','_TRIGSF_DN','tree','overallSys')
# AR_all_MTRIGREW = Systematic('AR_all_MTRIGREW',configMgr.weights, 1.05,0.95, "user","userOverallSys")
# AR_all_GEN      = Systematic('AR_all_GEN','_CENTRAL','_GENUP','_GENDOWN','tree','histoSysOneSideSym')
# AR_all_SCALEST  = Systematic('AR_all_SCALEST','_CENTRAL','_SCALESTUP','_SCALESTDOWN','tree','overallSys')
# AR_all_RESOST   = Systematic('AR_all_RESOST','_CENTRAL','_RESOST','_RESOST','tree','histoSysOneSideSym')
# AR_all_TES      = Systematic('AR_all_TES','_CENTRAL','_TESUP','_TESDOWN','tree','overallSys')
# # fake systematics
# AR_fake_mFr     = Systematic('AR_fake_mFr','_CENTRAL','_muonFrUp','_muonFrDo','tree','overallSys')
# # Upper errors for 0 wm prediction
# sr400_wm_upp = Systematic("sr400_wm_upp",configMgr.weights, [2100.],[0.9], "user","userHistoSys") #
# sr403_wm_upp = Systematic("sr403_wm_upp",configMgr.weights, [1700.],[0.9], "user","userHistoSys") #
# sr404_wm_upp = Systematic("sr404_wm_upp",configMgr.weights, [800.],[0.9], "user","userHistoSys") #
# # weights
# AR_all_ESF      = Systematic('AR_all_ESF',configMgr.weights,("eventweight","syst_ESFUP"),("eventweight","syst_ESFDOWN"),'weight','overallSys')
# AR_all_MEFF     = Systematic('AR_all_MEFF',configMgr.weights,("eventweight","syst_MEFFUP"),("eventweight","syst_MEFFDOWN"),'weight','overallSys')
# AR_all_PILEUP     = Systematic('AR_all_PILEUP',configMgr.weights,("eventweight","syst_PILEUPUP"),("eventweight","syst_PILEUPDOWN"),'weight','overallSys')
# AR_all_TIDSF     = Systematic('AR_all_TIDSF',configMgr.weights,("eventweight","syst_TIDSFUP"),("eventweight","syst_TIDSFDOWN"),'weight','overallSys')
# #pdf
# AR_zjets_PDFERR = Systematic('AR_z-jets_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_wjets_PDFERR = Systematic('AR_w-jets_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_ttbar_PDFERR = Systematic('AR_ttbar_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_stop_PDFERR  = Systematic('AR_stop_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_vgamma_PDFERR  = Systematic('AR_vgamma_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_ttbarV_PDFERR= Systematic('AR_ttbarV_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_sig_PDFERR   = Systematic('AR_sig_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_wz_PDFERR    = Systematic('AR_wz_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_zz_PDFERR    = Systematic('AR_zz_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_triboson_PDFERR    = Systematic('AR_triboson_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_ttbarV_PDFERR    = Systematic('AR_ttbarV_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_Higgs_PDFERR    = Systematic('AR_Higgs_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_Signal_PDFERR    = Systematic('AR_Signal_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# AR_ww_PDFERR    = Systematic('AR_ww_PDFERR',configMgr.weights,("eventweight","syst_PDFERRUP"),("eventweight","syst_PDFERRDOWN"),'weight','overallSys')
# # generator
# AR_wz_GEN       = Systematic('AR_wz_GEN',configMgr.weights,("eventweight","syst_GEN"),("eventweight","syst_GEN"),'weight','histoSysOneSideSym')
# AR_zz_GEN       = Systematic('AR_zz_GEN',configMgr.weights,("eventweight","syst_GEN"),("eventweight","syst_GEN"),'weight','histoSysOneSideSym')
# AR_ttbarV_GEN       = Systematic('AR_ttbarV_GEN',configMgr.weights,("eventweight","syst_GEN"),("eventweight","syst_GEN"),'weight','histoSysOneSideSym')
# AR_triboson_GEN       = Systematic('AR_triboson_GEN',configMgr.weights,("eventweight","syst_GEN"),("eventweight","syst_GEN"),'weight','histoSysOneSideSym')
# AR_ttbarZ_GEN       = Systematic('AR_ttbarZ_GEN',configMgr.weights,("eventweight","syst_GEN"),("eventweight","syst_GEN"),'weight','histoSysOneSideSym')
# # xsec
# AR_wz_XS        = Systematic('AR_wz_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_zz_XS        = Systematic('AR_zz_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_ww_XS        = Systematic('AR_ww_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_zjets_XS     = Systematic('AR_z-jets_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_wjets_XS     = Systematic('AR_w-jets_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_ttbar_XS     = Systematic('AR_ttbar_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_stop_XS      = Systematic('AR_stop_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_vgamma_XS    = Systematic('AR_vgamma_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_ttbarV_XS    = Systematic('AR_ttbarV_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# AR_Higgs_XS    = Systematic('AR_Higgs_XS',configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),'weight','overallSys')
# # b-tag. only apply in b-vetoing regions
# AR_all_BJET     = Systematic('AR_all_BJET',configMgr.weights,("eventweight","syst_BJETUP"),("eventweight","syst_BJETDOWN"),'weight','overallSys')
# AR_all_CJET     = Systematic('AR_all_CJET',configMgr.weights,("eventweight","syst_CJETUP"),("eventweight","syst_CJETDOWN"),'weight','overallSys')
# AR_all_BMISTAG  = Systematic('AR_all_BMISTAG',configMgr.weights,("eventweight","syst_BMISTAGUP"),("eventweight","syst_BMISTAGDOWN"),'weight','overallSys')
# # combine all triboson uncertainties to a conservative 100%
# AR_triboson_theory = Systematic("AR_triboson_theory",configMgr.weights, 2.,0.01, "user","userOverallSys")
# #
# theoryBand = Systematic("SigXSec",configMgr.weights,("eventweight","syst_XSUP"),("eventweight","syst_XSDOWN"),"weight","overallSys")
# # use overall systs for now
# AR_sr1a_MM_CLO = Systematic("AR_sr1a_MM_CLO",configMgr.weights, 1.26,1., "user","userOverallSys") # ok
# AR_sr1b_MM_CLO = Systematic("AR_sr1b_MM_CLO",configMgr.weights, 1.,1., "user","userOverallSys") # ok (used to be 0.3)
# AR_sr2_MM_CLO = Systematic("AR_sr2_MM_CLO",configMgr.weights, 270.,1.,"user","userOverallSys") # ok
# # Flat 30 % systematic for cross check
# AR_all_STD = Systematic("AR_all_STD",configMgr.weights,1.3,0.7,"user","userOverallSys")


#----------------------------------------------------------------------------------------------------
#Fit config instance
tlx = configMgr.addFitConfig("TopLvlXML")
#tlx = configMgr.addTopLevelXML("BkgOnly")
meas=tlx.addMeasurement(name="NormalMeasurement",lumi=1.,lumiErr=0.05)
meas.addPOI("mu_SIG")

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------

# disable standard mc stat to get contributions for each sample correct


useStat=True

if finalState=='L3':
    VVSample = Sample("VV",kAzure-4)
    VVSample.setStatConfig(useStat)
    VVSample.setNormByTheory()
    VVVSample = Sample("VVV",kCyan+4 )
    VVVSample.setStatConfig(useStat)
    VVVSample.setNormByTheory()
    ZSample = Sample("Z-jets",kViolet-9  )
    ZSample.setStatConfig(useStat)
    ZSample.setNormByTheory()               # when the numbers for the reducible background are coming from the matrix method, deactivate this
    WSample = Sample("W-jets",kViolet-7  )
    WSample.setStatConfig(useStat)
    WSample.setNormByTheory()               # when the numbers for the reducible background are coming from the matrix method, deactivate this
    ttbarSample = Sample("ttbar",kOrange-2 )
    ttbarSample.setStatConfig(useStat)
    ttbarSample.setNormByTheory()
    singletopSample = Sample("singletop",kYellow-9 )
    singletopSample.setStatConfig(useStat)
    singletopSample.setNormByTheory()
    #vgammaSample = Sample("V-gamma",kViolet-1 )
    #vgammaSample.setStatConfig(useStat)
    #vgammaSample.setNormByTheory()
    ttvSample = Sample("ttv",kCyan-1 )
    ttvSample.setStatConfig(useStat)
    ttvSample.setNormByTheory()
    fakeSample = Sample("MM",kOrange-2 )
    fakeSample.setStatConfig(useStat)
    #fakeSample.setWeights(("eventweight","1./13.02"))
    #fakeSample.setNormByTheory()
    higgsSample = Sample("higgs",kWhite)
    higgsSample.setStatConfig(useStat)
    higgsSample.setNormByTheory()


    dataSample = Sample("Data_CENTRAL",kBlack)
    dataSample.setData()

    #Samples
    # the order in which you add them determines the way they are stacked in the plots
    if doCutflow:
      tlx.addSamples([ttvSample,VVVSample,VVSample,fakeSample,higgsSample,dataSample])
    elif useFakePrediction:
      tlx.addSamples([ttvSample,VVVSample,VVSample,fakeSample,higgsSample,dataSample])
    else:
      tlx.addSamples([singletopSample,ZSample,WSample,ttvSample,VVVSample,ttbarSample,VVSample,higgsSample,dataSample])

    # Systematics
    # fakes have their own systs, nothing in common with mc bg..
    if includeSystematics:
      tlx.getSample('VV').addSystematic(AR_VV_GEN)
      tlx.getSample('ttv').addSystematic(AR_ttbarV_GEN)
      #tlx.getSample('VVV').addSystematic(AR_triboson_theory)
      if not doSimFit:
        tlx.getSample('VV').addSystematic(AR_VV_PDFERR)
        tlx.getSample('VV').addSystematic(AR_VV_XS)
      tlx.getSample('ZZ').addSystematic(AR_zz_XS)
      tlx.getSample('ttv').addSystematic(AR_ttbarV_XS)
      tlx.getSample('VVV').addSystematic(AR_triboson_XS)
      tlx.getSample('Higgs').addSystematic(AR_Higgs_XS)
      if useFakePrediction:
        tlx.getSample('VV').addSystematic(AR_all_EESLOW)
        tlx.getSample('VV').addSystematic(AR_all_EESMAT)
        tlx.getSample('VV').addSystematic(AR_all_EESPS)
        tlx.getSample('VV').addSystematic(AR_all_EESZ)
        tlx.getSample('VV').addSystematic(AR_all_JES)
        tlx.getSample('VV').addSystematic(AR_all_JVF)
        tlx.getSample('VV').addSystematic(AR_all_JER)
        tlx.getSample('VV').addSystematic(AR_all_EER)
        tlx.getSample('VV').addSystematic(AR_all_MID)
        tlx.getSample('VV').addSystematic(AR_all_MMS)
        tlx.getSample('VV').addSystematic(AR_all_ETRIGREW)
        tlx.getSample('VV').addSystematic(AR_all_SCALEST)
        tlx.getSample('VV').addSystematic(AR_all_RESOST)
        tlx.getSample('VV').addSystematic(AR_all_ESF)
        tlx.getSample('VV').addSystematic(AR_all_MEFF)
        tlx.getSample('VV').addSystematic(AR_all_TES)
        tlx.getSample('VV').addSystematic(AR_all_TIDSF)
        tlx.getSample('VV').addSystematic(AR_all_BJET)
        tlx.getSample('VV').addSystematic(AR_all_CJET)
        tlx.getSample('VV').addSystematic(AR_all_BMISTAG)
        tlx.getSample('VV').addSystematic(AR_all_PILEUP)


        tlx.getSample('VVV').addSystematic(AR_all_EESLOW)
        tlx.getSample('VVV').addSystematic(AR_all_EESMAT)
        tlx.getSample('VVV').addSystematic(AR_all_EESPS)
        tlx.getSample('VVV').addSystematic(AR_all_EESZ)
        tlx.getSample('VVV').addSystematic(AR_all_JES)
        tlx.getSample('VVV').addSystematic(AR_all_JVF)
        tlx.getSample('VVV').addSystematic(AR_all_JER)
        tlx.getSample('VVV').addSystematic(AR_all_EER)
        tlx.getSample('VVV').addSystematic(AR_all_MID)
        tlx.getSample('VVV').addSystematic(AR_all_MMS)
        tlx.getSample('VVV').addSystematic(AR_all_ETRIGREW)
        tlx.getSample('VVV').addSystematic(AR_all_SCALEST)
        tlx.getSample('VVV').addSystematic(AR_all_RESOST)
        tlx.getSample('VVV').addSystematic(AR_all_ESF)
        tlx.getSample('VVV').addSystematic(AR_all_MEFF) 
        tlx.getSample('VVV').addSystematic(AR_all_TES)
        tlx.getSample('VVV').addSystematic(AR_all_TIDSF)
        tlx.getSample('VVV').addSystematic(AR_all_BJET)
        tlx.getSample('VVV').addSystematic(AR_all_CJET)
        tlx.getSample('VVV').addSystematic(AR_all_BMISTAG)
        tlx.getSample('VVV').addSystematic(AR_triboson_PDFERR)
        tlx.getSample('VVV').addSystematic(AR_all_PILEUP)

        tlx.getSample('ttv').addSystematic(AR_all_EESLOW)
        tlx.getSample('ttv').addSystematic(AR_all_EESMAT)
        tlx.getSample('ttv').addSystematic(AR_all_EESPS)
        tlx.getSample('ttv').addSystematic(AR_all_EESZ)
        tlx.getSample('ttv').addSystematic(AR_all_JES)
        tlx.getSample('ttv').addSystematic(AR_all_JVF)
        tlx.getSample('ttv').addSystematic(AR_all_JER)
        tlx.getSample('ttv').addSystematic(AR_all_EER)
        tlx.getSample('ttv').addSystematic(AR_all_MID)
        tlx.getSample('ttv').addSystematic(AR_all_MMS)
        tlx.getSample('ttv').addSystematic(AR_all_ETRIGREW)
        tlx.getSample('ttv').addSystematic(AR_all_SCALEST)
        tlx.getSample('ttv').addSystematic(AR_all_RESOST)
        tlx.getSample('ttv').addSystematic(AR_all_ESF)
        tlx.getSample('ttv').addSystematic(AR_all_MEFF)
        tlx.getSample('ttv').addSystematic(AR_all_TES)
        tlx.getSample('ttv').addSystematic(AR_all_TIDSF)
        tlx.getSample('ttv').addSystematic(AR_all_BJET)
        tlx.getSample('ttv').addSystematic(AR_all_CJET)
        tlx.getSample('ttv').addSystematic(AR_all_BMISTAG)
        tlx.getSample('ttv').addSystematic(AR_ttbarV_PDFERR)
        tlx.getSample('ttv').addSystematic(AR_all_PILEUP)

        tlx.getSample('Higgs').addSystematic(AR_all_EESLOW)
        tlx.getSample('Higgs').addSystematic(AR_all_EESMAT)
        tlx.getSample('Higgs').addSystematic(AR_all_EESPS)
        tlx.getSample('Higgs').addSystematic(AR_all_EESZ)
        tlx.getSample('Higgs').addSystematic(AR_all_JES)
        tlx.getSample('Higgs').addSystematic(AR_all_JVF)
        tlx.getSample('Higgs').addSystematic(AR_all_JER)
        tlx.getSample('Higgs').addSystematic(AR_all_EER)
        tlx.getSample('Higgs').addSystematic(AR_all_MID)
        tlx.getSample('Higgs').addSystematic(AR_all_MMS)
        tlx.getSample('Higgs').addSystematic(AR_all_ETRIGREW)
        tlx.getSample('Higgs').addSystematic(AR_all_SCALEST)
        tlx.getSample('Higgs').addSystematic(AR_all_RESOST)
        tlx.getSample('Higgs').addSystematic(AR_all_ESF)
        tlx.getSample('Higgs').addSystematic(AR_all_MEFF)
        tlx.getSample('Higgs').addSystematic(AR_all_TES)
        tlx.getSample('Higgs').addSystematic(AR_all_TIDSF)
        tlx.getSample('Higgs').addSystematic(AR_all_BJET)
        tlx.getSample('Higgs').addSystematic(AR_all_CJET)
        tlx.getSample('Higgs').addSystematic(AR_all_BMISTAG)
        tlx.getSample('Higgs').addSystematic(AR_Higgs_PDFERR)
        tlx.getSample('Higgs').addSystematic(AR_all_PILEUP)
      else:
        print ''
        tlx.addSystematic(AR_all_EESLOW)
        tlx.addSystematic(AR_all_EESMAT)
        tlx.addSystematic(AR_all_EESPS)
        tlx.addSystematic(AR_all_EESZ)
        tlx.addSystematic(AR_all_JES)
        tlx.addSystematic(AR_all_JVF)
        tlx.addSystematic(AR_all_JER)
        tlx.addSystematic(AR_all_EER)
        tlx.addSystematic(AR_all_MID)
        tlx.addSystematic(AR_all_MMS)
        tlx.addSystematic(AR_all_ESF)
        tlx.addSystematic(AR_all_MEFF)
        tlx.addSystematic(AR_all_ETRIGREW)
        tlx.addSystematic(AR_all_SCALEST)
        tlx.addSystematic(AR_all_RESOST)
        tlx.addSystematic(AR_all_BJET)
        tlx.addSystematic(AR_all_CJET)
        tlx.addSystematic(AR_all_BMISTAG)
        tlx.addSystematic(AR_all_PILEUP)
        tlx.addSystematic(AR_all_TES)
        tlx.addSystematic(AR_all_TIDSF)
        tlx.getSample('WW').addSystematic(AR_ww_XS)
        tlx.getSample('Z-jets').addSystematic(AR_zjets_XS)
        tlx.getSample('W-jets').addSystematic(AR_wjets_XS)
        tlx.getSample('ttbar').addSystematic(AR_ttbar_XS)
        tlx.getSample('singletop').addSystematic(AR_stop_XS)
        tlx.getSample('V-gamma').addSystematic(AR_vgamma_XS)



# Need to set some threshold to enable MC stat error
if useStat:
    tlx.statErrThreshold=0.05
elif splitMCsystsIntoSamples:
    tlx.statErrThreshold=0.05
else:
    tlx.statErrThreshold=None

# Cosmetics

# Set global plotting colors/styles
tlx.dataColor = dataSample.color
tlx.totalPdfColor = kRed
tlx.errorFillColor = kBlack
tlx.errorFillStyle = 3004
tlx.errorLineStyle = kDashed
tlx.errorLineColor = kBlue-5

# Create TLegend (AK: TCanvas is needed for that, but it gets deleted afterwards)
c = TCanvas()
compFillStyle = 1001 # see ROOT for Fill styles
leg = TLegend(0.75,0.47,0.94,0.94,"")
leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
leg.SetTextSize(0.045)




if splitMCsystsIntoSamples:
  mcstat = Systematic("mcstat", "_CENTRAL", "_CENTRAL", "_CENTRAL", "tree", "shapeStat")


# Set legend for TopLevelXML
tlx.tLegend = leg
c.Close()
if doSimFit:
  crBin = tlx.addChannel("cuts",["L3CR"],1,40,400)
  if splitMCsystsIntoSamples:
    crBin.addSystematic(mcstat)
  if includeSystematics:
    crBin.addSystematic(CR_all_JES)
    crBin.addSystematic(CR_all_JER)
    crBin.addSystematic(AR_all_BMISTAG)
    crBin.addSystematic(AR_all_BJET)
    crBin.addSystematic(AR_all_CJET)
    if useFakePrediction:
      crBin.getSample("fake").removeSystematic('CR_all_JER')
      crBin.getSample("fake").removeSystematic('CR_all_JES')
      crBin.getSample("fake").addSystematic(AR_CR_MM_CLO)
      crBin.getSample("fake").removeSystematic('AR_all_BMISTAG')
      crBin.getSample("fake").removeSystematic('AR_all_BJET')
      crBin.getSample("fake").removeSystematic('AR_all_CJET')
  crBin.useOverflowBin=True
  crBin.useUnderflowBin=True
  tlx.setBkgConstrainChannels([crBin])

  # Set Channel titleX, titleY, minY, maxY, logY
  crBin.minY = 0.05
  crBin.maxY = 10000
  crBin.titleX = "pT"
  crBin.titleY = "Entries"
  crBin.logY = True
  crBin.ATLASLabelX = 0.25
  crBin.ATLASLabelY = 0.85
  crBin.ATLASLabelText = "Work in progress"

if not doDiscovery:
  srList=[]
  #if splitMCsystsIntoSamples:
  #  statSRwz = []
  if finalState == "L3":
    for i,sr in enumerate(srs.split('_')):
      if whichConference=='HCP':
        srList.append(tlx.addChannel("cuts",[finalState+sr],1,0,1000))
      elif whichConference=='MORIOND':
        if sr=='SR3xx': srList.append(tlx.addChannel("SR",[finalState+sr],12,300.5,312.5))
        elif sr=='SR3LL': srList.append(tlx.addChannel("SR",[finalState+sr],5,300.5,305.5))
        elif sr=='SR3TT': srList.append(tlx.addChannel("SR",[finalState+sr],7,305.5,312.5))
        elif sr=='SR3T1': srList.append(tlx.addChannel("SR",[finalState+sr],6,305.5,311.5))
        else: srList.append(tlx.addChannel("SR",[finalState+sr],1,300.5,312.5))
      else:
        #if sr=='SR3xx': srList.append(tlx.addChannel("SR",[finalState+sr],12,300.5,312.5))
        #else: srList.append(tlx.addChannel("cuts",[finalState+sr],1,0,1000))
        srList.append(tlx.addChannel("cuts",[finalState+sr],1,0,1000))
      if splitMCsystsIntoSamples:
        srList[i].addSystematic(mcstat)
      if includeSystematics:
        #if "L3SR1a" in finalState+sr or "L3SR1b" in finalState+sr:
        #  srList[i].addSystematic(AR_all_BMISTAG)
        #  srList[i].addSystematic(AR_all_BJET)
        #  srList[i].addSystematic(AR_all_CJET)
        if useFakePrediction:
            if 'L3BIN' in finalState+sr or finalState+sr=='L3SR0b':
                srList[i].getSample('fake').addSystematic(AR_fake_mFr)
                srList[i].getSample('fake').addSystematic(AR_fake_eFr)
                srList[i].getSample('fake').addSystematic(AR_fake_mRe)
                srList[i].getSample('fake').addSystematic(AR_fake_eRe)
            elif finalState+sr=='L3SR1SS':
                srList[i].getSample('fake').addSystematic(AR_fake_mFr)
                srList[i].getSample('fake').addSystematic(AR_fake_eFr)
                srList[i].getSample('fake').addSystematic(AR_fake_tFr)
                srList[i].getSample('fake').addSystematic(AR_fake_mRe)
                srList[i].getSample('fake').addSystematic(AR_fake_eRe)
                srList[i].getSample('fake').addSystematic(AR_fake_tRe)
            elif finalState+sr=='L3SR2a' or finalState+sr=='L3SR2b':
                srList[i].getSample('fake').addSystematic(AR_fake_tFr)
                srList[i].getSample('fake').addSystematic(AR_fake_tRe)
      srList[i].useOverflowBin=True
      srList[i].useUnderflowBin=True

      # Set Channel titleX, titleY, minY, maxY, logY
      srList[i].minY = 0.05
      srList[i].maxY = 1000000
      srList[i].titleX = " "
      srList[i].titleY = "Entries / SR"
      srList[i].logY = True
      srList[i].ATLASLabelX = 0.25
      srList[i].ATLASLabelY = 0.85
      srList[i].ATLASLabelText = "Internal"
  if doValidation:
    valchanlist = srList
  else:
    tlx.setSignalChannels(srList)


if doValidation:
  vr1met = tlx.addChannel("MET",["L3VR1"],25,0,500)
  vr1met.useOverflowBin=False
  vr1met.useUnderflowBin=True
  if includeSystematics:
    vr1met.addSystematic(VR1_all_JES)
    vr1met.addSystematic(VR1_all_JER)
    if useFakePrediction:
      vr1met.getSample("fake").removeSystematic('VR1_all_JER')
      vr1met.getSample("fake").removeSystematic('VR1_all_JES')
      vr1met.getSample("fake").addSystematic(AR_VR1_MM_CLO)

  # Set Channel titleX, titleY, minY, maxY, logY

  vr1met.minY = 0.05
  vr1met.maxY = 1000000
  vr1met.titleX = "E_{T}^{miss} [GeV]"
  vr1met.titleY = "Entries"
  vr1met.logY = True
  vr1met.ATLASLabelX = 0.25
  vr1met.ATLASLabelY = 0.85
  vr1met.ATLASLabelText = "Work in progress"


  tlx.setValidationChannels([vr1met]+valchanlist)
"""
if doValidation:
  tlx.setValidationChannels(valchanlist)
"""
if doCutflow:
  cf0chan = tlx.addChannel("cuts",["CF0_3L"],1,40,400)
  cf1chan = tlx.addChannel("cuts",["CF1_SFOS"],1,40,400)
  cf2chan = tlx.addChannel("cuts",["CF2_MET"],1,40,400)
  cf3chan = tlx.addChannel("cuts",["CF3_Z"],1,40,400)
  cf4chan = tlx.addChannel("cuts",["CF4_BVeto"],1,40,400)

  tlx.setValidationChannels([cf0chan,cf1chan,cf2chan,cf3chan,cf4chan])

# Don't add a seperate discovery clone because of crashes with the empty tlx when
# not doing SimFit
if doDiscovery:
    if len(srs.split('_'))>1:
        print "Don't do Discovery fit for multiple signal regions at once. Keeping",srs.split('_')[0], "removing rest."
        print "In fact I exit here. Rerun with 1 region to avoid mess"
        sys.exit()
    discoChannel = tlx.addChannel("cuts",[finalState+srs.split('_')[0]],1,40,400)
    discoChannel.addDiscoverySamples(["SIG"],[signalYield],[0.],[100.],[kMagenta])


    if splitMCsystsIntoSamples:
        discoChannel.addSystematic(mcstat)
    tlx.setSignalChannels(discoChannel)


if doExclusion:
    # to add signal samples, go to tools/signals.py
    sigSamples=signals.getSignals(grid)
    if sigSamples=='NONE':
        print "Could not identify grid"
        sys.exit()


    if runPoint:
        sigSamples = [str(runPoint)]
    #sigSamples = ['SMAnoslep8TeV_150.0_62.5']
    for s in sigSamples:
        exclusion = configMgr.addFitConfigClone(tlx,'TopLvlXML_Exclusion_%s'%s)
        sigSample = Sample(s,kViolet+5)
        # is now read from trees.
        if 'Slepr' in grid:
            mnlsp = s.split("_")[1] # right handed sleptons get a correction depending on their MNLSP
            #Load correction factors
            if float(mnlsp)==75: sf_lr = 0.354
            sigSample.setWeights(("eventweight",str(sf_lr)))
        sigSample.setStatConfig(useStat)
        sigSample.setFileList(["/lustre/scratch/epp/atlas/is86/Run2/Trees/L3_"+grid+".root"])
        sigSample.setNormByTheory()


        sigSample.setNormFactor('mu_SIG',1.,0.,100.)
        if includeSystematics:
            if not int(doVisibleXsecORdiscovery)==3:
                sigSample.addSystematic(theoryBand)
            if allSysts:
                sigSample.addSystematic(AR_all_EESLOW)
                sigSample.addSystematic(AR_all_EESMAT)
                sigSample.addSystematic(AR_all_EESPS)
                sigSample.addSystematic(AR_all_EESZ)
                sigSample.addSystematic(AR_all_EER)
                sigSample.addSystematic(AR_all_JES)
                sigSample.addSystematic(AR_all_JER)
                sigSample.addSystematic(AR_all_JVF)
                sigSample.addSystematic(AR_all_MID)
                sigSample.addSystematic(AR_all_MMS)
                sigSample.addSystematic(AR_all_TES)
                sigSample.addSystematic(AR_all_TIDSF)
                sigSample.addSystematic(AR_all_RESOST)
                sigSample.addSystematic(AR_all_BJET)
                sigSample.addSystematic(AR_all_CJET)
                sigSample.addSystematic(AR_all_BMISTAG)
                sigSample.addSystematic(AR_all_PILEUP)
                sigSample.addSystematic(AR_Signal_PDFERR)
                sigSample.addSystematic(AR_all_MEFF)
                sigSample.addSystematic(AR_all_SCALEST)
                sigSample.addSystematic(AR_all_ESF)
            if finalState=='L3': sigSample.addSystematic(AR_all_ETRIGREW)



        exclusion.addSamples(sigSample)
        exclusion.setSignalSample(sigSample)
        exclusion.setSignalChannels(srList)
        if runOnly1Point:
            break

