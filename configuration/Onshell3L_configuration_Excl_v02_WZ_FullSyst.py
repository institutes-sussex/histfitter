from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,TCanvas,TLegend,TLegendEntry
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from optparse import OptionParser
from copy import deepcopy

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
import ROOT
GeV = 1000.;

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    newList.append(newWeight)
    return newList

#-------------------------------
# Parse command line
#-------------------------------
myUserArgs= configMgr.userArg.split(' ')
myInputParser = OptionParser()
myInputParser.add_option('', '--point', dest = 'point', default = '')
myInputParser.add_option('', '--signalUncert', dest = 'signalUncert', default = 'Nom')
myInputParser.add_option('', '--SR', dest = 'SR', default = 'onshell')
myInputParser.add_option('', '--doFullGrid', dest = 'doFullGrid', default = 'True')
myInputParser.add_option('', '--useSysts', dest = 'useSysts', default = 'False')
myInputParser.add_option('', '--addFakes', dest = 'addFakes', default = 'True')
myInputParser.add_option('', '--doThUncert', dest = 'doThUncert', default = 'True')
myInputParser.add_option('', '--doFakeUncert', dest = 'doFakeUncert', default = 'True')
myInputParser.add_option('', '--ShapeFit', dest = 'ShapeFit', default = 'False')
myInputParser.add_option('', '--makePlots', dest = 'makePlots', default = 'False')
myInputParser.add_option('', '--runToys', dest = 'runToys', default = 'False')
myInputParser.add_option('', '--toyIndex', dest = 'toyIndex', default = '3')
myInputParser.add_option('', '--Name',dest = 'Name',default='EWK_Wh3L')

(options, args) = myInputParser.parse_args(myUserArgs)
whichPoint = options.point
whichSR = options.SR
signalXSec=options.signalUncert
name = options.Name
toyindex= options.toyIndex

#---------------------------------------
# Flags to control options
#---------------------------------------

fullGrid=False
if options.doFullGrid== "True" or options.doFullGrid == "true":
	fullGrid=True
doToys=False
if options.runToys== "True" or options.runToys == "true":
	doToys=True
useFakes=False
if options.addFakes== "True" or options.addFakes == "true":
	useFakes=True
doShapeFit = False
if options.ShapeFit == "True" or options.ShapeFit == "true":
	doShapeFit = True
doThUncert=False
if options.doThUncert == "True" or options.doThUncert == "true":
	doThUncert=True
doFakeUncert=False
if options.doFakeUncert == "True" or options.doFakeUncert == "true":
        doFakeUncert=True
noSysts=True
if options.useSysts== "True" or options.useSysts == "true":
        print "disabling systematics"
        noSysts=False

useStat=True
testSignalYield=False

print "########## Running Exclusion fit #############"


#-------------------------------
# Parameters for hypothesis test
#-------------------------------

# have been running with this commented out and set to false
#configMgr.doHypoTest=True
configMgr.nTOYs=3000
if not doToys:
	configMgr.calculatorType=2 # 0=toys, 2=asymptotic calc.
else:
	configMgr.calculatorType=0 # 0=toys, 2=asymptotic calc.

configMgr.testStatType=3
configMgr.nPoints=20
#configMgr.scanRange = (1., 4.)
#configMgr.drawBeforeAfterFit = False
configMgr.doExclusion=True
configMgr.fixSigXSec= True

configMgr.blindSR = False #CAUTION: SRs in background only fit are seen as VRs so for BkgOnly they are unblinded! FIXME need to unblind them ONE BY ONE
configMgr.blindCR = False
configMgr.blindVR = False 

#--------------------------------
# Now we start to build the model
#--------------------------------

# First define HistFactory attributes
if not noSysts:
	name = name + "_syst"

if not doToys:
	configMgr.analysisName = name
else:
	configMgr.analysisName = name + "_"+ whichPoint+"_withToys_"+toyindex

configMgr.inputLumi = 1.	# Luminosity of input TTree
configMgr.outputLumi = 1.  # Luminosity required for output histograms

configMgr.setLumiUnits("fb-1")
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

configMgr.ReduceCorrMatrix=True #CorrMatrix to be given in a reduced version
# Set the files to read from
bgdFiles_a = []
bgdFiles_d = []
bgdFiles_e = []
dataFiles = []
fakeFiles = []
sigFiles_a = []
sigFiles_d = []
sigFiles_e = []

ntupDir = ""

#Systematics pruning
configMgr.prun = True
configMgr.prunThreshold = 0.01 #Set at 1%

# Recycle histograms --------------------------- FIXME for first iteration we are gonna reproduce all histograms from scratch but will keep the lines here for later

configMgr.useCacheToTreeFallback = True # enable the fallback to trees
configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
configMgr.histBackupCacheFile = "/its/home/ft81/Stats/HistFitter_63/data/backup_WZ_onshell.root"

# Input files ----------------------------------------------------------------------------------------------------

if configMgr.readFromTree or configMgr.useCacheToTreeFallback:
	bgdFiles_a.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_mcaJES/L3_BG13TeV_a.root")
        bgdFiles_d.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_mcdJES/L3_BG13TeV_d.root")
        bgdFiles_e.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_mceJES/L3_BG13TeV_e.root")
	#bgdFiles_a.append("/mnt/lustre/scratch/epp_test/shared/JES30/Trees_mcaJES/L3_BG13TeV_a.root")
        #bgdFiles_d.append("/mnt/lustre/scratch/epp_test/shared/JES30/Trees_mcdJES/L3_BG13TeV_d.root")
        #bgdFiles_e.append("/mnt/lustre/scratch/epp_test/shared/JES30/Trees_mceJES/L3_BG13TeV_e.root")
        sigFiles_a.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/HFtrees/L3_SMWZ13TeV_a.root")
        sigFiles_d.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/HFtrees/L3_SMWZ13TeV_d.root")
        sigFiles_e.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/HFtrees/L3_SMWZ13TeV_e.root")
        #sigFiles_a.append("/mnt/lustre/scratch/epp_test/shared/HFtrees/L3_SMWZ13TeV_a.root")
        #sigFiles_d.append("/mnt/lustre/scratch/epp_test/shared/HFtrees/L3_SMWZ13TeV_d.root")
        #sigFiles_e.append("/mnt/lustre/scratch/epp_test/shared/HFtrees/L3_SMWZ13TeV_e.root")
	dataFiles.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_datav91/L_DATA13TeV.root")
        fakeFiles.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_fakes/L3_FakesFF13TeV.root")
        #dataFiles.append("/mnt/lustre/scratch/epp_test/shared/JES30/Trees_datav91/L_DATA13TeV.root")
        #fakeFiles.append("/mnt/lustre/scratch/epp_test/shared/JES30/Trees_fakes/L3_FakesFF13TeV.root")

# Regions ----------------------------------------------------------------------------------------------------

print "Defying 3L regions"

# common preselection for ALL the regions
configMgr.cutsDict["baseThreeLep"]= "cleaning && n_comblep==3 && LepPt1>20000. && LepPt0>25000. && pass3l && matchtrigger" #trig matching

#######################
# SRs for WZ analysis #
#######################

#0 jets
configMgr.cutsDict["SR1_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>100000 && Mt<160000 && MET<100000)"
configMgr.cutsDict["SR2_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>100000 && Mt<160000 && MET>100000 && MET<150000)"
configMgr.cutsDict["SR3_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>100000 && Mt<160000 && MET>150000 && MET<200000)"
configMgr.cutsDict["SR4_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>100000 && Mt<160000 && MET>200000)"
configMgr.cutsDict["SR5_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>160000 && MET<150000)"
configMgr.cutsDict["SR6_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>160000 && MET>150000 && MET<200000)"
configMgr.cutsDict["SR7_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>160000 && MET>200000 && MET<350000)"
configMgr.cutsDict["SR8_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>160000 && MET>350000)"

#low HT
configMgr.cutsDict["SR9_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT<200000 && Mt>100000 && Mt<160000 && MET>100000 && MET<150000)"
configMgr.cutsDict["SR10_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT<200000 && Mt>100000 && Mt<160000 && MET>150000 && MET<250000)"
configMgr.cutsDict["SR11_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT<200000 && Mt>100000 && Mt<160000 && MET>250000 && MET<300000)"
configMgr.cutsDict["SR12_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT<200000 && Mt>100000 && Mt<160000 && MET>300000)"
configMgr.cutsDict["SR13_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT<200000 && Mt>160000 && MET<150000)"
configMgr.cutsDict["SR14_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT<200000 && Mt>160000 && MET>150000 && MET<250000)"
configMgr.cutsDict["SR15_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT<200000 && Mt>160000 && MET>250000 && MET<400000)"
configMgr.cutsDict["SR16_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT<200000 && Mt>160000 && MET>400000)"

#high HT
configMgr.cutsDict["SR17_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT>200000 && HTLep<350000 && Mt>100000 && MET>150000 && MET<200000)"
configMgr.cutsDict["SR18_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT>200000 && HTLep<350000 && Mt>100000 && MET>200000 && MET<300000)"
configMgr.cutsDict["SR19_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT>200000 && HTLep<350000 && Mt>100000 && MET>300000 && MET<400000)"
configMgr.cutsDict["SR20_WZ"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15. && mSFOS>75000. && mSFOS<105000. && njets>0 && HT>200000 && HTLep<350000 && Mt>100000 && MET>400000)"

#######################
# SRs for Wh analysis #
#######################

#low mll - 0 jets
configMgr.cutsDict["SR1_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt<100000 && MET>50000 && MET<100000)"
configMgr.cutsDict["SR2_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt<100000 && MET>100000 && MET<150000)"
configMgr.cutsDict["SR3_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt<100000 && MET>150000)"
configMgr.cutsDict["SR4_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt>100000 && Mt<160000 && MET>50000 && MET<100000)"
configMgr.cutsDict["SR5_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt>100000 && Mt<160000 && MET>100000)"
configMgr.cutsDict["SR6_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt>160000 && MET>50000 && MET<100000)"
configMgr.cutsDict["SR7_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt>160000 && MET>100000)"

#low mll - low HT
configMgr.cutsDict["SR8_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt<50000 && MET>50000 && MET<100000)"
configMgr.cutsDict["SR9_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>50000 && Mt<100000 && MET>50000 && MET<100000)"
configMgr.cutsDict["SR10_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>50000 && Mt<100000 && MET>100000 && MET<150000)"
configMgr.cutsDict["SR11_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>50000 && Mt<100000 && MET>150000)"
configMgr.cutsDict["SR12_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>100000 && Mt<160000 && MET>50000 && MET<100000)"
configMgr.cutsDict["SR13_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>100000 && Mt<160000 && MET>100000 && MET<150000)"
configMgr.cutsDict["SR14_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>100000 && Mt<160000 && MET>150000)"
configMgr.cutsDict["SR15_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>160000 && MET>50000 && MET<150000)"
configMgr.cutsDict["SR16_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>160000 && MET>150000)"

#high mll
configMgr.cutsDict["SR17_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>105000. && njets==0 && Mt>100000 && MET>50000 && MET<100000)"
configMgr.cutsDict["SR18_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>105000. && njets==0 && Mt>100000 && MET>100000 && MET<200000)"
configMgr.cutsDict["SR19_Wh"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>105000. && njets==0 && Mt>100000 && MET>200000)"

#DFOS
configMgr.cutsDict["SR_DFOS_0j"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passDF && nbjets==0 && MET>50000. && METSig>8 && LepPt2>15000 && deltaRmin<1.2 && njets==0)"
configMgr.cutsDict["SR_DFOS_1j"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passDF && nbjets==0 && MET>50000. && METSig>8 && LepPt2>20000 && deltaRmin<1.0 && njets>0 && njets<3)"


#VR and CR

configMgr.cutsDict["WZ_VR_HighHT"]="("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>100000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt>20000 && Mt<100000 && njets>0 && HT>200000)"
configMgr.cutsDict["WZ_VR_LowHT"]="("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>100000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt>20000 && Mt<100000 && njets>0 && HT<200000)"
configMgr.cutsDict["WZ_VR_nJ0"]="("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>100000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt>20000 && Mt<100000 && njets==0)"
configMgr.cutsDict["WZ_CR_0jets"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt<100000 && Mt>20000 && MET<100000 && njets==0)"
configMgr.cutsDict["WZ_CR_LowHT"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt<100000 && Mt>20000 && MET<100000 && njets>0 && HT<200000)"
configMgr.cutsDict["WZ_CR_HighHT"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt<100000 && Mt>20000 && MET<100000 && njets>0 && HT>200000)"
configMgr.cutsDict["top_VR"] = "("+str( configMgr.cutsDict["baseThreeLep"])+"&& nbjets>0 && nbjets<3 && passDF && MET>50000)"
#configMgr.cutsDict["top_VR"] = "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passDF && MET>50000 && METSig<8)"
configMgr.cutsDict["top_VRAl"] = "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passDF && MET>50000 && METSig<8)"
configMgr.cutsDict["fakes_VR"] = "("+str(configMgr.cutsDict["baseThreeLep"])+"&& nbjets==0 && passSFOS && MET>50000 && MET<100000 && mSFOS>75000 && mSFOS<105000 && Mlll>105000 && Mlll<160000 && Mt<20000)"

# Weights ----------------------------------------------------------------------------------------------------
if 'Nom' in signalXSec:
	weights = ["bTagWeight","pileupWeight","JVTSF", "ElecSF","MuonSF","EvtWeight","NormWeight"]
	configMgr.weights = weights		
elif 'up' in signalXSec:
	configMgr.weights =["eventweightUp"]
elif 'down' in signalXSec:
	configMgr.weights =["eventweightDown"]
configMgr.nomName = "_CENTRAL"

# Systematics ----------------------------------------------------------------------------------------------------
#FIXME no systematics yet for now. We will add them later on, leaving a couple of examples below for future references

####===> TREE BASED

#jets
#syst_jes_1_MC = Systematic("syst_jes_1","_CENTRAL","_JET_GroupedNP_1_UP","_JET_GroupedNP_1_DN","tree","histoSys")
#syst_jes_2_MC = Systematic("syst_jes_2","_CENTRAL","_JET_GroupedNP_2_UP","_JET_GroupedNP_2_DN","tree","histoSys")
#syst_jes_3_MC = Systematic("syst_jes_3","_CENTRAL","_JET_GroupedNP_3_UP","_JET_GroupedNP_3_DN","tree","histoSys")

#syst_jes_1_norm = Systematic("syst_jes_1","_CENTRAL","_JET_GroupedNP_1_UP","_JET_GroupedNP_1_DN","tree","normHistoSys")
#syst_jes_2_norm = Systematic("syst_jes_2","_CENTRAL","_JET_GroupedNP_2_UP","_JET_GroupedNP_2_DN","tree","normHistoSys")
#syst_jes_3_norm = Systematic("syst_jes_3","_CENTRAL","_JET_GroupedNP_3_UP","_JET_GroupedNP_3_DN","tree","normHistoSys")

syst_jes_1_MC = Systematic("syst_Bjes_resp","_CENTRAL","_JET_BJES_Response_UP","_JET_BJES_Response_DN","tree","histoSys")
syst_jes_2_MC = Systematic("syst_jes_det1","_CENTRAL","_JET_EffectiveNP_Detector1_UP","_JET_EffectiveNP_Detector1_DN","tree","histoSys")
syst_jes_3_MC = Systematic("syst_jes_det2","_CENTRAL","_JET_EffectiveNP_Detector2_UP","_JET_EffectiveNP_Detector2_DN","tree","histoSys")
syst_jes_4_MC = Systematic("syst_jes_mix1","_CENTRAL","_JET_EffectiveNP_Mixed1_UP","_JET_EffectiveNP_Mixed1_DN","tree","histoSys")
syst_jes_5_MC = Systematic("syst_jes_mix2","_CENTRAL","_JET_EffectiveNP_Mixed2_UP","_JET_EffectiveNP_Mixed2_DN","tree","histoSys")
syst_jes_6_MC = Systematic("syst_jes_mix3","_CENTRAL","_JET_EffectiveNP_Mixed3_UP","_JET_EffectiveNP_Mixed3_DN","tree","histoSys")
syst_jes_7_MC = Systematic("syst_jes_mod1","_CENTRAL","_JET_EffectiveNP_Modelling1_UP","_JET_EffectiveNP_Modelling1_DN","tree","histoSys")
syst_jes_8_MC = Systematic("syst_jes_mod2","_CENTRAL","_JET_EffectiveNP_Modelling2_UP","_JET_EffectiveNP_Modelling2_DN","tree","histoSys")
syst_jes_9_MC = Systematic("syst_jes_mod3","_CENTRAL","_JET_EffectiveNP_Modelling3_UP","_JET_EffectiveNP_Modelling3_DN","tree","histoSys")
syst_jes_10_MC = Systematic("syst_jes_mod4","_CENTRAL","_JET_EffectiveNP_Modelling4_UP","_JET_EffectiveNP_Modelling4_DN","tree","histoSys")
syst_jes_11_MC = Systematic("syst_jes_stat1","_CENTRAL","_JET_EffectiveNP_Statistical1_UP","_JET_EffectiveNP_Statistical1_DN","tree","histoSys")
syst_jes_12_MC = Systematic("syst_jes_stat2","_CENTRAL","_JET_EffectiveNP_Statistical2_UP","_JET_EffectiveNP_Statistical2_DN","tree","histoSys")
syst_jes_13_MC = Systematic("syst_jes_stat3","_CENTRAL","_JET_EffectiveNP_Statistical3_UP","_JET_EffectiveNP_Statistical3_DN","tree","histoSys")
syst_jes_14_MC = Systematic("syst_jes_stat4","_CENTRAL","_JET_EffectiveNP_Statistical4_UP","_JET_EffectiveNP_Statistical4_DN","tree","histoSys")
syst_jes_15_MC = Systematic("syst_etaInter_mod","_CENTRAL","_JET_EtaIntercalibration_Modelling_UP","_JET_EtaIntercalibration_Modelling_DN","tree","histoSys")
syst_jes_16_MC = Systematic("syst_etaInter_NC2018","_CENTRAL","_JET_EtaIntercalibration_NonClosure_2018data_UP","_JET_EtaIntercalibration_NonClosure_2018data_DN","tree","histoSys")
syst_jes_17_MC = Systematic("syst_etaInter_NChighE","_CENTRAL","_JET_EtaIntercalibration_NonClosure_highE_UP","_JET_EtaIntercalibration_NonClosure_highE_DN","tree","histoSys")
syst_jes_18_MC = Systematic("syst_etaInter_negEta","_CENTRAL","_JET_EtaIntercalibration_NonClosure_negEta_UP","_JET_EtaIntercalibration_NonClosure_negEta_DN","tree","histoSys")
syst_jes_19_MC = Systematic("syst_etaInter_posEta","_CENTRAL","_JET_EtaIntercalibration_NonClosure_posEta_UP","_JET_EtaIntercalibration_NonClosure_posEta_DN","tree","histoSys")
syst_jes_20_MC = Systematic("syst_etaInter_stat","_CENTRAL","_JET_EtaIntercalibration_TotalStat_UP","_JET_EtaIntercalibration_TotalStat_DN","tree","histoSys")
syst_jes_21_MC = Systematic("syst_flav_comp","_CENTRAL","_JET_Flavor_Composition_UP","_JET_Flavor_Composition_DN","tree","histoSys")
syst_jes_22_MC = Systematic("syst_flav_resp","_CENTRAL","_JET_Flavor_Response_UP","_JET_Flavor_Response_DN","tree","histoSys")
syst_jes_23_MC = Systematic("syst_PU_offsetMu","_CENTRAL","_JET_Pileup_OffsetMu_UP","_JET_Pileup_OffsetMu_DN","tree","histoSys")
syst_jes_24_MC = Systematic("syst_PU_offsetNPV","_CENTRAL","_JET_Pileup_OffsetNPV_UP","_JET_Pileup_OffsetNPV_DN","tree","histoSys")
syst_jes_25_MC = Systematic("syst_PU_pt","_CENTRAL","_JET_Pileup_PtTerm_UP","_JET_Pileup_PtTerm_DN","tree","histoSys")
syst_jes_26_MC = Systematic("syst_PU_rho","_CENTRAL","_JET_Pileup_RhoTopology_UP","_JET_Pileup_RhoTopology_DN","tree","histoSys")
syst_jes_27_MC = Systematic("syst_singleP_pt","_CENTRAL","_JET_SingleParticle_HighPt_UP","_JET_SingleParticle_HighPt_DN","tree","histoSys")

syst_PunchThrough_AFII_MC = Systematic("JET_PunchThrough_AFII","_CENTRAL","_JET_PunchThrough_AFII_UP","_JET_PunchThrough_AFII_DN","tree","histoSys")
syst_RelNonCl_AFII_MC = Systematic("JET_RelativeNonClosure_AFII","_CENTRAL","_JET_RelativeNonClosure_AFII_UP","_JET_RelativeNonClosure_AFII_DN","tree","histoSys")

syst_jes_1_MC_norm = Systematic("syst_Bjes_resp","_CENTRAL","_JET_BJES_Response_UP","_JET_BJES_Response_DN","tree","normHistoSys")
syst_jes_2_MC_norm = Systematic("syst_jes_det1","_CENTRAL","_JET_EffectiveNP_Detector1_UP","_JET_EffectiveNP_Detector1_DN","tree","normHistoSys")
syst_jes_3_MC_norm = Systematic("syst_jes_det2","_CENTRAL","_JET_EffectiveNP_Detector2_UP","_JET_EffectiveNP_Detector2_DN","tree","normHistoSys")
syst_jes_4_MC_norm = Systematic("syst_jes_mix1","_CENTRAL","_JET_EffectiveNP_Mixed1_UP","_JET_EffectiveNP_Mixed1_DN","tree","normHistoSys")
syst_jes_5_MC_norm = Systematic("syst_jes_mix2","_CENTRAL","_JET_EffectiveNP_Mixed2_UP","_JET_EffectiveNP_Mixed2_DN","tree","normHistoSys")
syst_jes_6_MC_norm = Systematic("syst_jes_mix3","_CENTRAL","_JET_EffectiveNP_Mixed3_UP","_JET_EffectiveNP_Mixed3_DN","tree","normHistoSys")
syst_jes_7_MC_norm = Systematic("syst_jes_mod1","_CENTRAL","_JET_EffectiveNP_Modelling1_UP","_JET_EffectiveNP_Modelling1_DN","tree","normHistoSys")
syst_jes_8_MC_norm = Systematic("syst_jes_mod2","_CENTRAL","_JET_EffectiveNP_Modelling2_UP","_JET_EffectiveNP_Modelling2_DN","tree","normHistoSys")
syst_jes_9_MC_norm = Systematic("syst_jes_mod3","_CENTRAL","_JET_EffectiveNP_Modelling3_UP","_JET_EffectiveNP_Modelling3_DN","tree","normHistoSys")
syst_jes_10_MC_norm = Systematic("syst_jes_mod4","_CENTRAL","_JET_EffectiveNP_Modelling4_UP","_JET_EffectiveNP_Modelling4_DN","tree","normHistoSys")
syst_jes_11_MC_norm = Systematic("syst_jes_stat1","_CENTRAL","_JET_EffectiveNP_Statistical1_UP","_JET_EffectiveNP_Statistical1_DN","tree","normHistoSys")
syst_jes_12_MC_norm = Systematic("syst_jes_stat2","_CENTRAL","_JET_EffectiveNP_Statistical2_UP","_JET_EffectiveNP_Statistical2_DN","tree","normHistoSys")
syst_jes_13_MC_norm = Systematic("syst_jes_stat3","_CENTRAL","_JET_EffectiveNP_Statistical3_UP","_JET_EffectiveNP_Statistical3_DN","tree","normHistoSys")
syst_jes_14_MC_norm = Systematic("syst_jes_stat4","_CENTRAL","_JET_EffectiveNP_Statistical4_UP","_JET_EffectiveNP_Statistical4_DN","tree","normHistoSys")
syst_jes_15_MC_norm = Systematic("syst_etaInter_mod","_CENTRAL","_JET_EtaIntercalibration_Modelling_UP","_JET_EtaIntercalibration_Modelling_DN","tree","normHistoSys")
syst_jes_16_MC_norm = Systematic("syst_etaInter_NC2018","_CENTRAL","_JET_EtaIntercalibration_NonClosure_2018data_UP","_JET_EtaIntercalibration_NonClosure_2018data_DN","tree","normHistoSys")
syst_jes_17_MC_norm = Systematic("syst_etaInter_NChighE","_CENTRAL","_JET_EtaIntercalibration_NonClosure_highE_UP","_JET_EtaIntercalibration_NonClosure_highE_DN","tree","normHistoSys")
syst_jes_18_MC_norm = Systematic("syst_etaInter_negEta","_CENTRAL","_JET_EtaIntercalibration_NonClosure_negEta_UP","_JET_EtaIntercalibration_NonClosure_negEta_DN","tree","normHistoSys")
syst_jes_19_MC_norm = Systematic("syst_etaInter_posEta","_CENTRAL","_JET_EtaIntercalibration_NonClosure_posEta_UP","_JET_EtaIntercalibration_NonClosure_posEta_DN","tree","normHistoSys")
syst_jes_20_MC_norm = Systematic("syst_etaInter_stat","_CENTRAL","_JET_EtaIntercalibration_TotalStat_UP","_JET_EtaIntercalibration_TotalStat_DN","tree","normHistoSys")
syst_jes_21_MC_norm = Systematic("syst_flav_comp","_CENTRAL","_JET_Flavor_Composition_UP","_JET_Flavor_Composition_DN","tree","normHistoSys")
syst_jes_22_MC_norm = Systematic("syst_flav_resp","_CENTRAL","_JET_Flavor_Response_UP","_JET_Flavor_Response_DN","tree","normHistoSys")
syst_jes_23_MC_norm = Systematic("syst_PU_offsetMu","_CENTRAL","_JET_Pileup_OffsetMu_UP","_JET_Pileup_OffsetMu_DN","tree","normHistoSys")
syst_jes_24_MC_norm = Systematic("syst_PU_offsetNPV","_CENTRAL","_JET_Pileup_OffsetNPV_UP","_JET_Pileup_OffsetNPV_DN","tree","normHistoSys")
syst_jes_25_MC_norm = Systematic("syst_PU_pt","_CENTRAL","_JET_Pileup_PtTerm_UP","_JET_Pileup_PtTerm_DN","tree","normHistoSys")
syst_jes_26_MC_norm = Systematic("syst_PU_rho","_CENTRAL","_JET_Pileup_RhoTopology_UP","_JET_Pileup_RhoTopology_DN","tree","normHistoSys")
syst_jes_27_MC_norm = Systematic("syst_singleP_pt","_CENTRAL","_JET_SingleParticle_HighPt_UP","_JET_SingleParticle_HighPt_DN","tree","normHistoSys")

syst_jer_dataMC_MC = Systematic("syst_jer_dataMC","_CENTRAL","_JET_JER_DataVsMC_MC16_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_1_MC = Systematic("syst_jer_1","_CENTRAL","_JET_JER_EffectiveNP_1_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_2_MC = Systematic("syst_jer_2","_CENTRAL","_JET_JER_EffectiveNP_2_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_3_MC = Systematic("syst_jer_3","_CENTRAL","_JET_JER_EffectiveNP_3_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_4_MC = Systematic("syst_jer_4","_CENTRAL","_JET_JER_EffectiveNP_4_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_5_MC = Systematic("syst_jer_5","_CENTRAL","_JET_JER_EffectiveNP_5_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_6_MC = Systematic("syst_jer_6","_CENTRAL","_JET_JER_EffectiveNP_6_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_7_MC = Systematic("syst_jer_7","_CENTRAL","_JET_JER_EffectiveNP_7restTerm_UP","_CENTRAL","tree","histoSysOneSideSym")

syst_jer_dataMC_AFII_MC = Systematic("JET_JER_DataVsMC_AFII","_CENTRAL","_JET_JER_DataVsMC_AFII_UP","_CENTRAL","tree","histoSysOneSideSym")

syst_jer_dataMC_norm = Systematic("syst_jer_dataMC","_CENTRAL","_JET_JER_DataVsMC_MC16_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_1_norm = Systematic("syst_jer_1","_CENTRAL","_JET_JER_EffectiveNP_1_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_2_norm = Systematic("syst_jer_2","_CENTRAL","_JET_JER_EffectiveNP_2_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_3_norm = Systematic("syst_jer_3","_CENTRAL","_JET_JER_EffectiveNP_3_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_4_norm = Systematic("syst_jer_4","_CENTRAL","_JET_JER_EffectiveNP_4_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_5_norm = Systematic("syst_jer_5","_CENTRAL","_JET_JER_EffectiveNP_5_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_6_norm = Systematic("syst_jer_6","_CENTRAL","_JET_JER_EffectiveNP_6_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_7_norm = Systematic("syst_jer_7","_CENTRAL","_JET_JER_EffectiveNP_7restTerm_UP","_CENTRAL","tree","normHistoSysOneSideSym")

#MET
syst_MET_SoftTrk_ResoPara_MC = Systematic("MET_SoftTrk_ResoPara","_CENTRAL","_MET_SoftTrk_ResoPara","_CENTRAL","tree","histoSysOneSideSym")
syst_MET_SoftTrk_ResoPerp_MC = Systematic("MET_SoftTrk_ResoPerp","_CENTRAL","_MET_SoftTrk_ResoPerp","_CENTRAL","tree","histoSysOneSideSym")
syst_MET_SoftTrk_Scale_MC = Systematic("MET_SoftTrk_Scale","_CENTRAL","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","histoSys")

syst_MET_SoftTrk_ResoPara_norm = Systematic("MET_SoftTrk_ResoPara","_CENTRAL","_MET_SoftTrk_ResoPara","_CENTRAL","tree","normHistoSysOneSideSym")
syst_MET_SoftTrk_ResoPerp_norm = Systematic("MET_SoftTrk_ResoPerp","_CENTRAL","_MET_SoftTrk_ResoPerp","_CENTRAL","tree","normHistoSysOneSideSym")
syst_MET_SoftTrk_Scale_norm = Systematic("MET_SoftTrk_Scale","_CENTRAL","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","normHistoSys")

#Egamma
syst_EG_Scale_MC = Systematic("EG_Scale","_CENTRAL","_EG_SCALE_ALL_UP","_EG_SCALE_ALL_DN","tree","histoSys")

syst_EG_Scale_norm = Systematic("EG_Scale","_CENTRAL","_EG_SCALE_ALL_UP","_EG_SCALE_ALL_DN","tree","normHistoSys")

syst_EG_Scale_AF2 = Systematic("EG_Scale_AF2", 1., 1.01,0.99,"user","userOverallSys")

#Muon
syst_MuID_MC = Systematic("Muon_ID","_CENTRAL","_MUON_ID_UP","_MUON_ID_DN","tree","histoSys")
syst_MuMS_MC = Systematic("Muon_MS","_CENTRAL","_MUON_MS_UP","_MUON_MS_DN","tree","histoSys")
syst_MuScale_MC = Systematic("Muon_Scale","_CENTRAL","_MUON_SCALE_UP","_MUON_SCALE_DN","tree","histoSys")
syst_MuSagResBias_MC = Systematic("Muon_Sag_Resbias","_CENTRAL","_MUON_SAGITTA_RESBIAS_UP","_MUON_SAGITTA_RESBIAS_DN","tree","histoSys")
syst_MuSagRho_MC = Systematic("Muon_SagRho","_CENTRAL","_MUON_SAGITTA_RHO_UP","_MUON_SAGITTA_RHO_DN","tree","histoSys")

syst_MuID_norm = Systematic("Muon_ID","_CENTRAL","_MUON_ID_UP","_MUON_ID_DN","tree","normHistoSys")
syst_MuMS_norm = Systematic("Muon_MS","_CENTRAL","_MUON_MS_UP","_MUON_MS_DN","tree","normHistoSys")
syst_MuScale_norm = Systematic("Muon_Scale","_CENTRAL","_MUON_SCALE_UP","_MUON_SCALE_DN","tree","normHistoSys")
syst_MuSagResBias_norm = Systematic("Muon_Sag_Resbias","_CENTRAL","_MUON_SAGITTA_RESBIAS_UP","_MUON_SAGITTA_RESBIAS_DN","tree","normHistoSys")
syst_MuSagRho_norm = Systematic("Muon_SagRho","_CENTRAL","_MUON_SAGITTA_RHO_UP","_MUON_SAGITTA_RHO_DN","tree","normHistoSys")

####===> WEIGHTS

#electrons
syst_elecSF_EFF_Iso_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up")
syst_elecSF_EFF_Iso_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down")
syst_elecSF_EFF_Iso = Systematic("syst_elecSF_Iso", configMgr.weights, syst_elecSF_EFF_Iso_UP, syst_elecSF_EFF_Iso_DOWN, 'weight', 'overallHistoSys')

syst_elecSF_EFF_Iso_norm = Systematic("syst_elecSF_Iso", configMgr.weights, syst_elecSF_EFF_Iso_UP, syst_elecSF_EFF_Iso_DOWN, 'weight', 'overallNormHistoSys')


syst_elecSF_EFF_ID_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up")
syst_elecSF_EFF_ID_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down")
syst_elecSF_EFF_ID = Systematic("syst_elecSF_ID", configMgr.weights, syst_elecSF_EFF_ID_UP, syst_elecSF_EFF_ID_DOWN, 'weight', 'overallHistoSys')

syst_elecSF_EFF_ID_norm = Systematic("syst_elecSF_ID", configMgr.weights, syst_elecSF_EFF_ID_UP, syst_elecSF_EFF_ID_DOWN, 'weight', 'overallNormHistoSys')

syst_elecSF_EFF_Reco_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up")
syst_elecSF_EFF_Reco_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down")
syst_elecSF_EFF_Reco = Systematic("syst_elecSF_Reco", configMgr.weights, syst_elecSF_EFF_Reco_UP, syst_elecSF_EFF_Reco_DOWN, 'weight', 'overallHistoSys')

syst_elecSF_EFF_Reco_norm = Systematic("syst_elecSF_Reco", configMgr.weights, syst_elecSF_EFF_Reco_UP, syst_elecSF_EFF_Reco_DOWN, 'weight', 'overallNormHistoSys')

syst_elecSF_EFF_TriggerEff_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_TriggerEff_up")
syst_elecSF_EFF_TriggerEff_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_TriggerEff_down")
syst_elecSF_EFF_TriggerEff = Systematic("syst_elecSF_TrigEff", configMgr.weights, syst_elecSF_EFF_TriggerEff_UP, syst_elecSF_EFF_TriggerEff_DOWN, 'weight', 'overallHistoSys')

syst_elecSF_EFF_TriggerEff_norm = Systematic("syst_elecSF_TrigEff", configMgr.weights, syst_elecSF_EFF_TriggerEff_UP, syst_elecSF_EFF_TriggerEff_DOWN, 'weight', 'overallNormHistoSys')

#muons
syst_muonSF_EFF_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_up")
syst_muonSF_EFF_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_down")
syst_muonSF_EFF_Stat = Systematic("syst_muonSF_Eff_Stat", configMgr.weights, syst_muonSF_EFF_Stat_UP, syst_muonSF_EFF_Stat_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_EFF_Stat_norm = Systematic("syst_muonSF_Eff_Stat", configMgr.weights, syst_muonSF_EFF_Stat_UP, syst_muonSF_EFF_Stat_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_EFF_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_up")
syst_muonSF_EFF_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_down")
syst_muonSF_EFF_Sys = Systematic("syst_muonSF_Eff_Sys", configMgr.weights, syst_muonSF_EFF_Sys_UP, syst_muonSF_EFF_Sys_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_EFF_Sys_norm = Systematic("syst_muonSF_Eff_Sys", configMgr.weights, syst_muonSF_EFF_Sys_UP, syst_muonSF_EFF_Sys_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_EFF_Stat_lowPt_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_LOWPT_up")
syst_muonSF_EFF_Stat_lowPt_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_LOWPT_down")
syst_muonSF_EFF_Stat_lowPt = Systematic("syst_muonSF_Eff_Stat_lowPt", configMgr.weights, syst_muonSF_EFF_Stat_lowPt_UP, syst_muonSF_EFF_Stat_lowPt_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_EFF_Stat_lowPt_norm = Systematic("syst_muonSF_Eff_Stat_lowPt", configMgr.weights, syst_muonSF_EFF_Stat_lowPt_UP, syst_muonSF_EFF_Stat_lowPt_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_EFF_Sys_lowPt_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_up")
syst_muonSF_EFF_Sys_lowPt_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_down")
syst_muonSF_EFF_Sys_lowPt = Systematic("syst_muonSF_Eff_Sys_lowPt", configMgr.weights, syst_muonSF_EFF_Sys_lowPt_UP, syst_muonSF_EFF_Sys_lowPt_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_EFF_Sys_lowPt_norm = Systematic("syst_muonSF_Eff_Sys_lowPt", configMgr.weights, syst_muonSF_EFF_Sys_lowPt_UP, syst_muonSF_EFF_Sys_lowPt_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_ISO_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_STAT_up")
syst_muonSF_ISO_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_STAT_down")
syst_muonSF_ISO_Stat = Systematic("syst_muonSF_Iso_Stat", configMgr.weights, syst_muonSF_ISO_Stat_UP, syst_muonSF_ISO_Stat_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_ISO_Stat_norm = Systematic("syst_muonSF_Iso_Stat", configMgr.weights, syst_muonSF_ISO_Stat_UP, syst_muonSF_ISO_Stat_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_ISO_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_SYS_up")
syst_muonSF_ISO_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_SYS_down")
syst_muonSF_ISO_Sys = Systematic("syst_muonSF_Iso_Sys", configMgr.weights, syst_muonSF_ISO_Sys_UP, syst_muonSF_ISO_Sys_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_ISO_Sys_norm = Systematic("syst_muonSF_Iso_Sys", configMgr.weights, syst_muonSF_ISO_Sys_UP, syst_muonSF_ISO_Sys_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_TTVA_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_STAT_up")
syst_muonSF_TTVA_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_STAT_down")
syst_muonSF_TTVA_Stat = Systematic("syst_muonSF_TTVA_Stat", configMgr.weights, syst_muonSF_TTVA_Stat_UP, syst_muonSF_TTVA_Stat_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_TTVA_Stat_norm = Systematic("syst_muonSF_TTVA_Stat", configMgr.weights, syst_muonSF_TTVA_Stat_UP, syst_muonSF_TTVA_Stat_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_TTVA_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_SYS_up")
syst_muonSF_TTVA_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_SYS_down")
syst_muonSF_TTVA_Sys = Systematic("syst_muonSF_TTVA_Sys", configMgr.weights, syst_muonSF_TTVA_Sys_UP, syst_muonSF_TTVA_Sys_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_TTVA_Sys_norm = Systematic("syst_muonSF_TTVA_Sys", configMgr.weights, syst_muonSF_TTVA_Sys_UP, syst_muonSF_TTVA_Sys_DOWN, 'weight', 'overallNormHistoSys')

#Flavour tagging
syst_FT_EFF_B_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_B_up")
syst_FT_EFF_B_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_B_down")
syst_FT_EFF_B = Systematic("syst_FT_Eff_B", configMgr.weights, syst_FT_EFF_B_UP, syst_FT_EFF_B_DOWN, 'weight', 'overallHistoSys')

syst_FT_EFF_B_norm = Systematic("syst_FT_Eff_B", configMgr.weights, syst_FT_EFF_B_UP, syst_FT_EFF_B_DOWN, 'weight', 'overallNormHistoSys')

syst_FT_EFF_C_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_C_up")
syst_FT_EFF_C_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_C_down")
syst_FT_EFF_C = Systematic("syst_FT_Eff_C", configMgr.weights, syst_FT_EFF_C_UP, syst_FT_EFF_C_DOWN, 'weight', 'overallHistoSys')

syst_FT_EFF_C_norm = Systematic("syst_FT_Eff_C", configMgr.weights, syst_FT_EFF_C_UP, syst_FT_EFF_C_DOWN, 'weight', 'overallNormHistoSys')

syst_FT_EFF_L_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_Light_up")
syst_FT_EFF_L_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_Light_down")
syst_FT_EFF_L = Systematic("syst_FT_Eff_L", configMgr.weights, syst_FT_EFF_L_UP, syst_FT_EFF_L_DOWN, 'weight', 'overallHistoSys')

syst_FT_EFF_L_norm = Systematic("syst_FT_Eff_L", configMgr.weights, syst_FT_EFF_L_UP, syst_FT_EFF_L_DOWN, 'weight', 'overallNormHistoSys')

syst_FT_EFF_extrCharm_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_extrapolationFromCharm_up")
syst_FT_EFF_extrCharm_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_extrapolationFromCharm_down")
syst_FT_EFF_extrCharm = Systematic("syst_FT_Eff_extrCharm", configMgr.weights, syst_FT_EFF_extrCharm_UP, syst_FT_EFF_extrCharm_DOWN, 'weight', 'overallHistoSys')

syst_FT_EFF_extrCharm_norm = Systematic("syst_FT_Eff_extrCharm", configMgr.weights, syst_FT_EFF_extrCharm_UP, syst_FT_EFF_extrCharm_DOWN, 'weight', 'overallNormHistoSys')

#jets
syst_jvtSF_UP = replaceWeight(configMgr.weights, "JVTSF","syst_jvtSF_up")
syst_jvtSF_DOWN = replaceWeight(configMgr.weights, "JVTSF","syst_jvtSF_down")
syst_jvtSF = Systematic("syst_jvtSF", configMgr.weights, syst_jvtSF_UP, syst_jvtSF_DOWN, 'weight', 'overallHistoSys')

syst_jvtSF_norm = Systematic("syst_jvtSF", configMgr.weights, syst_jvtSF_UP, syst_jvtSF_DOWN, 'weight', 'overallNormHistoSys')

#cross-section syst
syst_xSec_ZZ_flat = Systematic("syst_xSec_ZZ_flat", 1.,1.06,0.94,"user","userOverallSys")
syst_xSec_VVV_flat = Systematic("syst_xSec_VVV_flat", 1.,1.2,0.8,"user","userOverallSys")

syst_xSec_ttW_UP = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.+(runNumber==410155)*0.12)")
syst_xSec_ttW_DN = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.-(runNumber==410155)*0.12)")
syst_xSec_ttW_flat = Systematic("syst_xSec_ttW_flat", 1., syst_xSec_ttW_UP, syst_xSec_ttW_DN, "weight", "overallSys")

syst_xSec_ttZ_UP = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.+(runNumber==410156 || runNumber==410157 || runNumber==410218 || runNumber==410219 || runNumber==410220 || runNumber==410276 || runNumber==410277 || runNumber==410278)*0.13)")
syst_xSec_ttZ_DN = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.-(runNumber==410156 || runNumber==410157 || runNumber==410218 || runNumber==410219 || runNumber==410220 || runNumber==410276 || runNumber==410277 || runNumber==410278)*0.13)")
syst_xSec_ttZ_flat = Systematic("syst_xSec_ttZ_flat", 1., syst_xSec_ttZ_UP, syst_xSec_ttZ_DN, "weight", "overallSys")

syst_xSec_ttH_flat = Systematic("syst_xSec_ttH_flat", 1.,1.1,0.9,"user","userOverallSys")
syst_xSec_Higgs_flat = Systematic("syst_xSec_Higgs_flat", 1.,1.07,0.93,"user","userOverallSys")
syst_xSec_ttbar_flat = Systematic("syst_xSec_ttbar_flat", 1.,1.06,0.94,"user","userOverallSys")

syst_xSec_other_UP = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.+(runNumber==304014 || runNumber==410080 || runNumber==410081 || runNumber==410560 || runNumber==410408)*0.5)")
syst_xSec_other_DN = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.-(runNumber==304014 || runNumber==410080 || runNumber==410081 || runNumber==410560 || runNumber==410408)*0.5)")
syst_xSec_other_flat = Systematic("syst_xSec_other_flat", 1., syst_xSec_other_UP, syst_xSec_other_DN, "weight", "overallSys")

# theory syst 3L - WZ
syst_Theory_WZ_VRWZ_0j_QCD_muR = Systematic("syst_Theory_WZ_QCD", 1., 1.0072,0.9941,"user","userOverallSys")
syst_Theory_WZ_VRWZ_lowht_QCD_muR = Systematic("syst_Theory_WZ_QCD", 1., 1.0028,0.9949,"user","userOverallSys")
syst_Theory_WZ_VRWZ_highht_QCD_muR = Systematic("syst_Theory_WZ_QCD", 1., 1.0121,0.9861,"user","userOverallSys")

syst_Theory_WZ_VRWZ_0j_QCD_muF = Systematic("syst_Theory_WZ_QCD", 1., 1.0095,0.9921,"user","userOverallSys")
syst_Theory_WZ_VRWZ_lowht_QCD_muF = Systematic("syst_Theory_WZ_QCD", 1., 1.0032,0.9985,"user","userOverallSys")
syst_Theory_WZ_VRWZ_highht_QCD_muF = Systematic("syst_Theory_WZ_QCD", 1., 1.0035,0.9976,"user","userOverallSys")

syst_Theory_WZ_VRWZ_0j_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD", 1., 0.9970,0.9879,"user","userOverallSys")
syst_Theory_WZ_VRWZ_lowht_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD", 1., 1.0014,0.9904,"user","userOverallSys")
syst_Theory_WZ_VRWZ_highht_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD", 1., 1.0084,0.9868,"user","userOverallSys")

syst_Theory_WZ_VRWZ_0j_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0070,0.9930,"user","userOverallSys")
syst_Theory_WZ_VRWZ_lowht_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0029,0.9971,"user","userOverallSys")
syst_Theory_WZ_VRWZ_highht_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0089,0.9911,"user","userOverallSys")

syst_Theory_WZ_VRWZ_0j_AltSample = Systematic("syst_Theory_WZ_VRWZ_0j_AltSample", 1., 1.0920638,0.9079362,"user","userOverallSys")
syst_Theory_WZ_VRWZ_lowht_AltSample = Systematic("syst_Theory_WZ_VRWZ_lowht_AltSample", 1., 1.0510995,0.9904,"user","userOverallSys")
syst_Theory_WZ_VRWZ_highht_AltSample = Systematic("syst_Theory_WZ_VRWZ_highht_AltSample", 1., 1.105405,0.894595,"user","userOverallSys")

syst_Theory_WZ_SRWZ1_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0479,0.9693,"user","userOverallSys")
syst_Theory_WZ_SRWZ2_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0070,0.9812,"user","userOverallSys")
syst_Theory_WZ_SRWZ3_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0296,0.9824,"user","userOverallSys")
syst_Theory_WZ_SRWZ4_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0296,0.9824,"user","userOverallSys")
syst_Theory_WZ_SRWZ5_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0530,0.9685,"user","userOverallSys")
syst_Theory_WZ_SRWZ6_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0280,0.9812,"user","userOverallSys")
syst_Theory_WZ_SRWZ7_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0878,0.9507,"user","userOverallSys")
syst_Theory_WZ_SRWZ8_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0878,0.9507,"user","userOverallSys")
syst_Theory_WZ_SRWZ9_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0274,0.9784,"user","userOverallSys")
syst_Theory_WZ_SRWZ10_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0054,0.9915,"user","userOverallSys")
syst_Theory_WZ_SRWZ11_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0054,0.9915,"user","userOverallSys")
syst_Theory_WZ_SRWZ12_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0232,0.9683,"user","userOverallSys")
syst_Theory_WZ_SRWZ13_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0117,0.9866,"user","userOverallSys")
syst_Theory_WZ_SRWZ14_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0074,0.9768,"user","userOverallSys")
syst_Theory_WZ_SRWZ15_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0074,0.9768,"user","userOverallSys")
syst_Theory_WZ_SRWZ16_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0074,0.9768,"user","userOverallSys")
syst_Theory_WZ_SRWZ17_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0010,0.9979,"user","userOverallSys")
syst_Theory_WZ_SRWZ18_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0140,0.9839,"user","userOverallSys")
syst_Theory_WZ_SRWZ19_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0101,0.9935,"user","userOverallSys")
syst_Theory_WZ_SRWZ20_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0227,0.9806,"user","userOverallSys")

syst_Theory_WZ_SRWZ1_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0090,0.9896,"user","userOverallSys")
syst_Theory_WZ_SRWZ2_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0055,0.9946,"user","userOverallSys")
syst_Theory_WZ_SRWZ3_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0086,0.9944,"user","userOverallSys")
syst_Theory_WZ_SRWZ4_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0086,0.9944,"user","userOverallSys")
syst_Theory_WZ_SRWZ5_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0043,0.9968,"user","userOverallSys")
syst_Theory_WZ_SRWZ6_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0245,0.9727,"user","userOverallSys")
syst_Theory_WZ_SRWZ7_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0150,0.9889,"user","userOverallSys")
syst_Theory_WZ_SRWZ8_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0150,0.9889,"user","userOverallSys")
syst_Theory_WZ_SRWZ9_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0004,0.9980,"user","userOverallSys")
syst_Theory_WZ_SRWZ10_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0044,0.9999,"user","userOverallSys")
syst_Theory_WZ_SRWZ11_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0044,0.9999,"user","userOverallSys")
syst_Theory_WZ_SRWZ12_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0123,0.9714,"user","userOverallSys")
syst_Theory_WZ_SRWZ13_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0061,0.9954,"user","userOverallSys")
syst_Theory_WZ_SRWZ14_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0035,0.9996,"user","userOverallSys")
syst_Theory_WZ_SRWZ15_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0035,0.9996,"user","userOverallSys")
syst_Theory_WZ_SRWZ16_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0035,0.9996,"user","userOverallSys")
syst_Theory_WZ_SRWZ17_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 0.9992,0.9986,"user","userOverallSys")
syst_Theory_WZ_SRWZ18_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0026,0.9997,"user","userOverallSys")
syst_Theory_WZ_SRWZ19_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0082,0.9990,"user","userOverallSys")
syst_Theory_WZ_SRWZ20_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0107,0.9915,"user","userOverallSys")

syst_Theory_WZ_SRWZ1_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0326,0.9760,"user","userOverallSys")
syst_Theory_WZ_SRWZ2_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0100,0.9737,"user","userOverallSys")
syst_Theory_WZ_SRWZ3_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0094,0.9827,"user","userOverallSys")
syst_Theory_WZ_SRWZ4_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0094,0.9827,"user","userOverallSys")
syst_Theory_WZ_SRWZ5_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0399,0.9680,"user","userOverallSys")
syst_Theory_WZ_SRWZ6_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 0.9945,0.9832,"user","userOverallSys")
syst_Theory_WZ_SRWZ7_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0568,0.9548,"user","userOverallSys")
syst_Theory_WZ_SRWZ8_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0568,0.9548,"user","userOverallSys")
syst_Theory_WZ_SRWZ9_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0268,0.9762,"user","userOverallSys")
syst_Theory_WZ_SRWZ10_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 0.9993,0.9881,"user","userOverallSys")
syst_Theory_WZ_SRWZ11_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 0.9993,0.9881,"user","userOverallSys")
syst_Theory_WZ_SRWZ12_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0275,0.9566,"user","userOverallSys")
syst_Theory_WZ_SRWZ13_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0165,0.9800,"user","userOverallSys")
syst_Theory_WZ_SRWZ14_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0026,0.9727,"user","userOverallSys")
syst_Theory_WZ_SRWZ15_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0026,0.9727,"user","userOverallSys")
syst_Theory_WZ_SRWZ16_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0026,0.9727,"user","userOverallSys")
syst_Theory_WZ_SRWZ17_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0004,0.9978,"user","userOverallSys")
syst_Theory_WZ_SRWZ18_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0123,0.9807,"user","userOverallSys")
syst_Theory_WZ_SRWZ19_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0062,0.9925,"user","userOverallSys")
syst_Theory_WZ_SRWZ20_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0096,0.9855,"user","userOverallSys")

syst_Theory_WZ_SRWZ1_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0051,0.9949,"user","userOverallSys")
syst_Theory_WZ_SRWZ2_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0042,0.9958,"user","userOverallSys")
syst_Theory_WZ_SRWZ3_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0109,0.9891,"user","userOverallSys")
syst_Theory_WZ_SRWZ4_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0109,0.9891,"user","userOverallSys")
syst_Theory_WZ_SRWZ5_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0054,0.9946,"user","userOverallSys")
syst_Theory_WZ_SRWZ6_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0092,0.9908,"user","userOverallSys")
syst_Theory_WZ_SRWZ7_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0108,0.9892,"user","userOverallSys")
syst_Theory_WZ_SRWZ8_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0108,0.9892,"user","userOverallSys")
syst_Theory_WZ_SRWZ9_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0018,0.9982,"user","userOverallSys")
syst_Theory_WZ_SRWZ10_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0044,0.9956,"user","userOverallSys")
syst_Theory_WZ_SRWZ11_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0044,0.9956,"user","userOverallSys")
syst_Theory_WZ_SRWZ12_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0891,0.9109,"user","userOverallSys")
syst_Theory_WZ_SRWZ13_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0031,0.9969,"user","userOverallSys")
syst_Theory_WZ_SRWZ14_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0055,0.9945,"user","userOverallSys")
syst_Theory_WZ_SRWZ15_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0055,0.9945,"user","userOverallSys")
syst_Theory_WZ_SRWZ16_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0055,0.9945,"user","userOverallSys")
syst_Theory_WZ_SRWZ17_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.01,0.99,"user","userOverallSys")
syst_Theory_WZ_SRWZ18_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.01,0.99,"user","userOverallSys")
syst_Theory_WZ_SRWZ19_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.01,0.99,"user","userOverallSys")
syst_Theory_WZ_SRWZ20_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.01,0.99,"user","userOverallSys")

syst_Theory_WZ_SROn0jets_AltSample = Systematic("syst_Theory_WZ_SROn0jets_AltSample", 1., 1.0409681,0.9590319,"user","userOverallSys")
syst_Theory_WZ_SROnJets_AltSample = Systematic("syst_Theory_WZ_SROnJets_AltSample", 1., 1.0697464,0.9302536,"user","userOverallSys")

syst_Theory_WZ_SRWh1_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0319,0.9816,"user","userOverallSys")
syst_Theory_WZ_SRWh2_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0230,0.9524,"user","userOverallSys")
syst_Theory_WZ_SRWh3_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0173,0.9909,"user","userOverallSys")
syst_Theory_WZ_SRWh4_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0332,0.9817,"user","userOverallSys")
syst_Theory_WZ_SRWh5_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0399,0.9776,"user","userOverallSys")
syst_Theory_WZ_SRWh6_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0127,0.9767,"user","userOverallSys")
syst_Theory_WZ_SRWh7_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0069,0.9953,"user","userOverallSys")
syst_Theory_WZ_SRWh8_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0259,0.9795,"user","userOverallSys")
syst_Theory_WZ_SRWh9_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0089,0.9841,"user","userOverallSys")
syst_Theory_WZ_SRWh10_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0172,0.9857,"user","userOverallSys")
syst_Theory_WZ_SRWh11_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0037,0.9933,"user","userOverallSys")
syst_Theory_WZ_SRWh12_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0258,0.9572,"user","userOverallSys")
syst_Theory_WZ_SRWh13_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0192,0.9850,"user","userOverallSys")
syst_Theory_WZ_SRWh14_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0034,0.9978,"user","userOverallSys")
syst_Theory_WZ_SRWh15_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0066,0.9900,"user","userOverallSys")
syst_Theory_WZ_SRWh16_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0169,0.9509,"user","userOverallSys")
syst_Theory_WZ_SRWh17_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0571,1.0546,"user","userOverallSys")
syst_Theory_WZ_SRWh18_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0302,0.9830,"user","userOverallSys")
syst_Theory_WZ_SRWh19_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0199,0.9588,"user","userOverallSys")

syst_Theory_WZ_SRWh1_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0082,0.9877,"user","userOverallSys")
syst_Theory_WZ_SRWh2_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0326,0.9705,"user","userOverallSys")
syst_Theory_WZ_SRWh3_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0073,0.9827,"user","userOverallSys")
syst_Theory_WZ_SRWh4_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0071,0.9913,"user","userOverallSys")
syst_Theory_WZ_SRWh5_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0140,0.9826,"user","userOverallSys")
syst_Theory_WZ_SRWh6_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0061,0.9990,"user","userOverallSys")
syst_Theory_WZ_SRWh7_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0085,0.9886,"user","userOverallSys")
syst_Theory_WZ_SRWh8_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0018,0.9985,"user","userOverallSys")
syst_Theory_WZ_SRWh9_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0183,0.9795,"user","userOverallSys")
syst_Theory_WZ_SRWh10_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0025,1.0001,"user","userOverallSys")
syst_Theory_WZ_SRWh11_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0045,0.9989,"user","userOverallSys")
syst_Theory_WZ_SRWh12_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0609,0.9384,"user","userOverallSys")
syst_Theory_WZ_SRWh13_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 0.9972,0.9967,"user","userOverallSys")
syst_Theory_WZ_SRWh14_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0123,0.9926,"user","userOverallSys")
syst_Theory_WZ_SRWh15_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0089,0.9956,"user","userOverallSys")
syst_Theory_WZ_SRWh16_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0007,0.9930,"user","userOverallSys")
syst_Theory_WZ_SRWh17_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0487,1.0069,"user","userOverallSys")
syst_Theory_WZ_SRWh18_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0150,0.9894,"user","userOverallSys")
syst_Theory_WZ_SRWh19_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0274,0.9796,"user","userOverallSys")

syst_Theory_WZ_SRWh1_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0159,0.9893,"user","userOverallSys")
syst_Theory_WZ_SRWh2_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0470,0.9090,"user","userOverallSys")
syst_Theory_WZ_SRWh3_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 0.9878,0.9735,"user","userOverallSys")
syst_Theory_WZ_SRWh4_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0199,0.9869,"user","userOverallSys")
syst_Theory_WZ_SRWh5_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0142,0.9862,"user","userOverallSys")
syst_Theory_WZ_SRWh6_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0062,0.9810,"user","userOverallSys")
syst_Theory_WZ_SRWh7_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 0.9964,0.9793,"user","userOverallSys")
syst_Theory_WZ_SRWh8_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0269,0.9780,"user","userOverallSys")
syst_Theory_WZ_SRWh9_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0116,0.9941,"user","userOverallSys")
syst_Theory_WZ_SRWh10_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0144,0.9839,"user","userOverallSys")
syst_Theory_WZ_SRWh11_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 0.9973,0.9879,"user","userOverallSys")
syst_Theory_WZ_SRWh12_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0395,0.9789,"user","userOverallSys")
syst_Theory_WZ_SRWh13_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0158,0.9855,"user","userOverallSys")
syst_Theory_WZ_SRWh14_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0001,0.9845,"user","userOverallSys")
syst_Theory_WZ_SRWh15_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0106,0.9791,"user","userOverallSys")
syst_Theory_WZ_SRWh16_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0022,0.9387,"user","userOverallSys")
syst_Theory_WZ_SRWh17_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0476,0.9416,"user","userOverallSys")
syst_Theory_WZ_SRWh18_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0003,0.9873,"user","userOverallSys")
syst_Theory_WZ_SRWh19_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0279,0.9067,"user","userOverallSys")


syst_Theory_WZ_SRWhDFOS0j_QCD = Systematic("syst_Theory_WZ_QCD", 1., 1.0377,0.9057,"user","userOverallSys")
syst_Theory_WZ_SRWhDFOS1j_QCD = Systematic("syst_Theory_WZ_QCD", 1., 1.0377,0.9057,"user","userOverallSys")

syst_Theory_WZ_SRWh1_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0032,0.9968,"user","userOverallSys")
syst_Theory_WZ_SRWh2_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0078,0.9922,"user","userOverallSys")
syst_Theory_WZ_SRWh3_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.1486,0.8514,"user","userOverallSys")
syst_Theory_WZ_SRWh4_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0030,0.9970,"user","userOverallSys")
syst_Theory_WZ_SRWh5_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0040,0.9960,"user","userOverallSys")
syst_Theory_WZ_SRWh6_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0048,0.9952,"user","userOverallSys")
syst_Theory_WZ_SRWh7_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0635,0.9365,"user","userOverallSys")
syst_Theory_WZ_SRWh8_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0016,0.9984,"user","userOverallSys")
syst_Theory_WZ_SRWh9_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0016,0.9984,"user","userOverallSys")
syst_Theory_WZ_SRWh10_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0025,0.9975,"user","userOverallSys")
syst_Theory_WZ_SRWh11_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0045,0.9955,"user","userOverallSys")
syst_Theory_WZ_SRWh12_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0021,0.9979,"user","userOverallSys")
syst_Theory_WZ_SRWh13_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0025,0.9975,"user","userOverallSys")
syst_Theory_WZ_SRWh14_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0038,0.9962,"user","userOverallSys")
syst_Theory_WZ_SRWh15_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0036,0.9964,"user","userOverallSys")
syst_Theory_WZ_SRWh16_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0087,0.9913,"user","userOverallSys")
syst_Theory_WZ_SRWh17_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0553,0.9447,"user","userOverallSys")
syst_Theory_WZ_SRWh18_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0075,0.9925,"user","userOverallSys")
syst_Theory_WZ_SRWh19_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0314,0.9686,"user","userOverallSys")

syst_Theory_WZ_SRWhDFOS0j_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0156,0.9844,"user","userOverallSys")
syst_Theory_WZ_SRWhDFOS1j_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0156,0.9844,"user","userOverallSys")

syst_Theory_WZ_SRLow0jets_AltSample = Systematic("syst_Theory_WZ_SRLow0jets_AltSample", 1., 1.110437,0.889563,"user","userOverallSys")
syst_Theory_WZ_SRLowJets_AltSample = Systematic("syst_Theory_WZ_SRLowJets_AltSample", 1., 1.0978209,0.9021791,"user","userOverallSys")
syst_Theory_WZ_SRHigh0jets_AltSample = Systematic("syst_Theory_WZ_SRHigh0jets_AltSample", 1., 1.113117,0.886883,"user","userOverallSys")
syst_Theory_WZ_SRDFOS_AltSample = Systematic("syst_Theory_WZ_SRDFOS_AltSample", 1.,1.104972,0.895028,"user","userOverallSys")

# theory syst 3L - ZZ
syst_Theory_ZZ_VRWZ_0j_QCD_muR = Systematic("syst_Theory_ZZ_QCD", 1., 1.0059,0.9632,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_lowht_QCD_muR = Systematic("syst_Theory_ZZ_QCD", 1., 1.1029,0.9115,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_highht_QCD_muR = Systematic("syst_Theory_ZZ_QCD", 1., 1.2588,1.2588,"user","userOverallSys")

yst_Theory_ZZ_VRWZ_0j_QCD_muF = Systematic("syst_Theory_ZZ_QCD", 1., 1.0119,0.9869,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_lowht_QCD_muF = Systematic("syst_Theory_ZZ_QCD", 1., 1.0043,0.9952,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_highht_QCD_muF = Systematic("syst_Theory_ZZ_QCD", 1., 1.0084,0.9916,"user","userOverallSys")

syst_Theory_ZZ_VRWZ_0j_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD", 1., 1.0166,0.9477,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_lowht_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD", 1., 1.0997,0.9168,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_highht_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD", 1., 1.2686,1.2686,"user","userOverallSys")

syst_Theory_ZZ_VRWZ_0j_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.1120,0.8880,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_lowht_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0635,0.9365,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_highht_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0739,0.9261,"user","userOverallSys")

syst_Theory_ZZ_VRWZ_0j_AltSample = Systematic("syst_Theory_ZZ_VRWZ_0j_AltSample", 1., 1.14085,0.85915,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_lowht_AltSample = Systematic("syst_Theory_ZZ_VRWZ_lowht_AltSample", 1., 1.26219,0.73781,"user","userOverallSys")
syst_Theory_ZZ_VRWZ_highht_AltSample = Systematic("syst_Theory_ZZ_VRWZ_highht_AltSample", 1., 1.14075,0.85925,"user","userOverallSys")

syst_Theory_ZZ_SRWZ1_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 0.9980,0.9838,"user","userOverallSys")
syst_Theory_ZZ_SRWZ2_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0116,0.9302,"user","userOverallSys")
syst_Theory_ZZ_SRWZ3_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0000,0.9167,"user","userOverallSys")
syst_Theory_ZZ_SRWZ4_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0000,0.9167,"user","userOverallSys")
syst_Theory_ZZ_SRWZ5_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0101,0.9764,"user","userOverallSys")
syst_Theory_ZZ_SRWZ6_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0714,0.8571,"user","userOverallSys")
syst_Theory_ZZ_SRWZ7_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0714,0.8571,"user","userOverallSys")
syst_Theory_ZZ_SRWZ8_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0714,0.8571,"user","userOverallSys")
syst_Theory_ZZ_SRWZ9_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0950,0.9174,"user","userOverallSys")
syst_Theory_ZZ_SRWZ10_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ11_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ12_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ13_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ14_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ15_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ16_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ17_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ18_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ19_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")
syst_Theory_ZZ_SRWZ20_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1613,0.8710,"user","userOverallSys")

syst_Theory_ZZ_SRWZ1_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0034,0.9966,"user","userOverallSys")
syst_Theory_ZZ_SRWZ2_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9884,"user","userOverallSys")
syst_Theory_ZZ_SRWZ3_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9167,"user","userOverallSys")
syst_Theory_ZZ_SRWZ4_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9167,"user","userOverallSys")
syst_Theory_ZZ_SRWZ5_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0034,1.0034,"user","userOverallSys")
syst_Theory_ZZ_SRWZ6_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0714,1.0714,"user","userOverallSys")
syst_Theory_ZZ_SRWZ7_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0714,1.0714,"user","userOverallSys")
syst_Theory_ZZ_SRWZ8_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0714,1.0714,"user","userOverallSys")
syst_Theory_ZZ_SRWZ9_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0124,0.9835,"user","userOverallSys")
syst_Theory_ZZ_SRWZ10_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ11_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ12_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ13_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ14_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ15_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ16_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ17_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ18_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ19_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWZ20_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")

syst_Theory_ZZ_SRWZ1_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0135,0.9865,"user","userOverallSys")
syst_Theory_ZZ_SRWZ2_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0116,0.9419,"user","userOverallSys")
syst_Theory_ZZ_SRWZ3_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0000,0.9167,"user","userOverallSys")
syst_Theory_ZZ_SRWZ4_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0000,0.9167,"user","userOverallSys")
syst_Theory_ZZ_SRWZ5_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0135,0.9764,"user","userOverallSys")
syst_Theory_ZZ_SRWZ6_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1429,0.9286,"user","userOverallSys")
syst_Theory_ZZ_SRWZ7_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1429,0.9286,"user","userOverallSys")
syst_Theory_ZZ_SRWZ8_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1429,0.9286,"user","userOverallSys")
syst_Theory_ZZ_SRWZ9_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0868,0.9298,"user","userOverallSys")
syst_Theory_ZZ_SRWZ10_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ11_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ12_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ13_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ14_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ15_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ16_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ17_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ18_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ19_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")
syst_Theory_ZZ_SRWZ20_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1290,0.9032,"user","userOverallSys")

syst_Theory_ZZ_SRWZ1_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0137,0.9863,"user","userOverallSys")
syst_Theory_ZZ_SRWZ2_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.1012,0.8988,"user","userOverallSys")
syst_Theory_ZZ_SRWZ3_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0815,0.9185,"user","userOverallSys")
syst_Theory_ZZ_SRWZ4_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0815,0.9185,"user","userOverallSys")
syst_Theory_ZZ_SRWZ5_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0112,0.9888,"user","userOverallSys")
syst_Theory_ZZ_SRWZ6_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0457,0.9543,"user","userOverallSys")
syst_Theory_ZZ_SRWZ7_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0457,0.9543,"user","userOverallSys")
syst_Theory_ZZ_SRWZ8_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0457,0.9543,"user","userOverallSys")
syst_Theory_ZZ_SRWZ9_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0870,0.9130,"user","userOverallSys")
syst_Theory_ZZ_SRWZ10_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ11_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ12_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ13_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ14_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ15_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ16_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ17_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ18_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ19_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")
syst_Theory_ZZ_SRWZ20_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0180,0.9820,"user","userOverallSys")

syst_Theory_ZZ_SROn0jets_AltSample = Systematic("syst_Theory_ZZ_SROn0jets_AltSample", 1., 1.165436,0.834564,"user","userOverallSys")
syst_Theory_ZZ_SROnJets_AltSample = Systematic("syst_Theory_ZZ_SROnJets_AltSample", 1., 1.391625,0.608375,"user","userOverallSys")

syst_Theory_ZZ_SRWh1_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0264,0.9711,"user","userOverallSys")
syst_Theory_ZZ_SRWh2_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWh3_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0000,0.9231,"user","userOverallSys")
syst_Theory_ZZ_SRWh4_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0747,0.9425,"user","userOverallSys")
syst_Theory_ZZ_SRWh5_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0747,0.9425,"user","userOverallSys")
syst_Theory_ZZ_SRWh6_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0169,0.9661,"user","userOverallSys")
syst_Theory_ZZ_SRWh7_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0169,0.9661,"user","userOverallSys")
syst_Theory_ZZ_SRWh8_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh9_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh10_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh11_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh12_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh13_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh14_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh15_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh16_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1268,0.9025,"user","userOverallSys")
syst_Theory_ZZ_SRWh17_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0000,0.9758,"user","userOverallSys")
syst_Theory_ZZ_SRWh18_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0000,0.9758,"user","userOverallSys")
syst_Theory_ZZ_SRWh19_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0000,0.9758,"user","userOverallSys")

syst_Theory_ZZ_SRWh1_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0176,0.9774,"user","userOverallSys")
syst_Theory_ZZ_SRWh2_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0233,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWh3_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWh4_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0115,0.9885,"user","userOverallSys")
syst_Theory_ZZ_SRWh5_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0115,0.9885,"user","userOverallSys")
syst_Theory_ZZ_SRWh6_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9831,"user","userOverallSys")
syst_Theory_ZZ_SRWh7_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9831,"user","userOverallSys")
syst_Theory_ZZ_SRWh8_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh9_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh10_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh11_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh12_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh13_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh14_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh15_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh16_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0052,0.9874,"user","userOverallSys")
syst_Theory_ZZ_SRWh17_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9758,"user","userOverallSys")
syst_Theory_ZZ_SRWh18_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9758,"user","userOverallSys")
syst_Theory_ZZ_SRWh19_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9758,"user","userOverallSys")

syst_Theory_ZZ_SRWh1_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0088,0.9925,"user","userOverallSys")
syst_Theory_ZZ_SRWh2_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SRWh3_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0000,0.9231,"user","userOverallSys")
syst_Theory_ZZ_SRWh4_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0632,0.9598,"user","userOverallSys")
syst_Theory_ZZ_SRWh5_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0632,0.9598,"user","userOverallSys")
syst_Theory_ZZ_SRWh6_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0169,0.9661,"user","userOverallSys")
syst_Theory_ZZ_SRWh7_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0169,0.9661,"user","userOverallSys")
syst_Theory_ZZ_SRWh8_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh9_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh10_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh11_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh12_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh13_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh14_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh15_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh16_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1247,0.9140,"user","userOverallSys")
syst_Theory_ZZ_SRWh17_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 0.9919,0.9839,"user","userOverallSys")
syst_Theory_ZZ_SRWh18_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 0.9919,0.9839,"user","userOverallSys")
syst_Theory_ZZ_SRWh19_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 0.9919,0.9839,"user","userOverallSys")

syst_Theory_ZZ_SRWhDFOS0j_QCD = Systematic("syst_Theory_ZZ_QCD", 1., 1.1429,0.8571,"user","userOverallSys")
syst_Theory_ZZ_SRWhDFOS1j_QCD = Systematic("syst_Theory_ZZ_QCD", 1., 1.1429,0.8571,"user","userOverallSys")

syst_Theory_ZZ_SRWh1_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0134,0.9866,"user","userOverallSys")
syst_Theory_ZZ_SRWh2_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0154,0.9846,"user","userOverallSys")
syst_Theory_ZZ_SRWh3_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0313,0.9687,"user","userOverallSys")
syst_Theory_ZZ_SRWh4_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0128,0.9872,"user","userOverallSys")
syst_Theory_ZZ_SRWh5_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0128,0.9872,"user","userOverallSys")
syst_Theory_ZZ_SRWh6_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0195,0.9805,"user","userOverallSys")
syst_Theory_ZZ_SRWh7_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0195,0.9805,"user","userOverallSys")
syst_Theory_ZZ_SRWh8_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh9_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh10_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh11_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh12_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh13_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh14_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh15_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh16_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0111,0.9889,"user","userOverallSys")
syst_Theory_ZZ_SRWh17_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0122,0.9878,"user","userOverallSys")
syst_Theory_ZZ_SRWh18_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0122,0.9878,"user","userOverallSys")
syst_Theory_ZZ_SRWh19_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0122,0.9878,"user","userOverallSys")

syst_Theory_ZZ_SRWhDFOS0j_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0581,0.9419,"user","userOverallSys")
syst_Theory_ZZ_SRWhDFOS1j_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0581,0.9419,"user","userOverallSys")

syst_Theory_ZZ_SRLow0jets_AltSample = Systematic("syst_Theory_ZZ_SRLow0jets_AltSample", 1., 1.168288,0.831712,"user","userOverallSys")
syst_Theory_ZZ_SRLowJets_AltSample = Systematic("syst_Theory_ZZ_SRLowJets_AltSample", 1., 1.0671805,0.9328195,"user","userOverallSys")
syst_Theory_ZZ_SRHigh0jets_AltSample = Systematic("syst_Theory_ZZ_SRHigh0jets_AltSample", 1., 1.33822,0.66178,"user","userOverallSys")
syst_Theory_ZZ_SRDFOS_AltSample = Systematic("syst_Theory_ZZ_SRDFOS_AltSample", 1.,1.10888,0.89112,"user","userOverallSys")

# theory syst 3L - VVV

syst_Theory_VVV_flat = Systematic("syst_Theory_VVV_flat", 1.,1.2,0.8,"user","userOverallSys")

# theory syst 3L - ttV

syst_Theory_ttV_flat = Systematic("syst_Theory_ttV_flat", 1., 1.2,0.8,"user","userOverallSys")

# Theory syst - 3L ttbar
syst_Theory_ttbar_SRWh8_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.0000,0.9886,"user","userOverallSys")
syst_Theory_ttbar_SRWh9_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.1311,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SRWh10_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.1533,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SRWh11_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.1533,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SRWh12_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.1108,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SRWh13_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.0000,0.9943,"user","userOverallSys")
syst_Theory_ttbar_SRWh14_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.0000,0.9943,"user","userOverallSys")
syst_Theory_ttbar_SRWh15_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.2394,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SRWh16_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.2394,1.0000,"user","userOverallSys")

syst_Theory_ttbar_SRWh8_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7366,"user","userOverallSys")
syst_Theory_ttbar_SRWh9_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7629,"user","userOverallSys")
syst_Theory_ttbar_SRWh10_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.8318,"user","userOverallSys")
syst_Theory_ttbar_SRWh11_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.8318,"user","userOverallSys")
syst_Theory_ttbar_SRWh12_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7640,"user","userOverallSys")
syst_Theory_ttbar_SRWh13_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7855,"user","userOverallSys")
syst_Theory_ttbar_SRWh14_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7855,"user","userOverallSys")
syst_Theory_ttbar_SRWh15_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7907,"user","userOverallSys")
syst_Theory_ttbar_SRWh16_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7907,"user","userOverallSys")

syst_Theory_ttbar_SRWh8_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.8947,"user","userOverallSys")
syst_Theory_ttbar_SRWh9_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9602,"user","userOverallSys")
syst_Theory_ttbar_SRWh10_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0320,0.8586,"user","userOverallSys")
syst_Theory_ttbar_SRWh11_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0320,0.8586,"user","userOverallSys")
syst_Theory_ttbar_SRWh12_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9883,"user","userOverallSys")
syst_Theory_ttbar_SRWh13_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9247,"user","userOverallSys")
syst_Theory_ttbar_SRWh14_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9247,"user","userOverallSys")
syst_Theory_ttbar_SRWh15_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9962,"user","userOverallSys")
syst_Theory_ttbar_SRWh16_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9962,"user","userOverallSys")

syst_Theory_ttbar_SRWh8_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0217,0.9977,"user","userOverallSys")
syst_Theory_ttbar_SRWh9_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0929,0.9908,"user","userOverallSys")
syst_Theory_ttbar_SRWh10_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0685,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SRWh11_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0685,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SRWh12_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0483,0.9608,"user","userOverallSys")
syst_Theory_ttbar_SRWh13_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0000,0.9809,"user","userOverallSys")
syst_Theory_ttbar_SRWh14_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0000,0.9809,"user","userOverallSys")
syst_Theory_ttbar_SRWh15_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0000,0.9970,"user","userOverallSys")
syst_Theory_ttbar_SRWh16_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0000,0.9970,"user","userOverallSys")

syst_Theory_ttbar_SRWh1_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0143,0.9857,"user","userOverallSys")
syst_Theory_ttbar_SRWh2_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0148,0.9852,"user","userOverallSys")
syst_Theory_ttbar_SRWh3_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0470,0.9530,"user","userOverallSys")
syst_Theory_ttbar_SRWh4_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0124,0.9876,"user","userOverallSys")
syst_Theory_ttbar_SRWh5_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0163,0.9837,"user","userOverallSys")
syst_Theory_ttbar_SRWh6_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0116,0.9884,"user","userOverallSys")
syst_Theory_ttbar_SRWh7_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0109,0.9891,"user","userOverallSys")
syst_Theory_ttbar_SRWh8_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0132,0.9868,"user","userOverallSys")
syst_Theory_ttbar_SRWh9_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0125,0.9875,"user","userOverallSys")
syst_Theory_ttbar_SRWh10_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0141,0.9859,"user","userOverallSys")
syst_Theory_ttbar_SRWh11_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0164,0.9836,"user","userOverallSys")
syst_Theory_ttbar_SRWh12_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0121,0.9879,"user","userOverallSys")
syst_Theory_ttbar_SRWh13_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0133,0.9867,"user","userOverallSys")
syst_Theory_ttbar_SRWh14_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0176,0.9824,"user","userOverallSys")
syst_Theory_ttbar_SRWh15_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0130,0.9870,"user","userOverallSys")
syst_Theory_ttbar_SRWh16_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0096,0.9904,"user","userOverallSys")
syst_Theory_ttbar_SRWh17_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0151,0.9849,"user","userOverallSys")
syst_Theory_ttbar_SRWh18_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0142,0.9858,"user","userOverallSys")
syst_Theory_ttbar_SRWh19_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0960,0.9040,"user","userOverallSys")

syst_Theory_ttbar_flat = Systematic("syst_Theory_ttbar_flat", 1., 1.2000,0.8000,"user","userOverallSys")

# Fake syst        
syst_FFstat_CRWZ_0j = Systematic("syst_FFstat_CRWZ_0j", 1., 0.9927 , 0.9747, "user","userOverallSys")
syst_FFstat_CRWZ_lowht = Systematic("syst_FFstat_CRWZ_lowht", 1., 1.1454 , 0.869, "user","userOverallSys")
syst_FFstat_CRWZ_highht = Systematic("syst_FFstat_CRWZ_highht", 1., 1.5184 , 0.6795, "user","userOverallSys")

syst_FFstat_VRWZ_0j = Systematic("syst_FFstat_VRWZ_0j", 1., 1.1775 , 0.8798, "user","userOverallSys")
syst_FFstat_VRWZ_lowht = Systematic("syst_FFstat_VRWZ_lowht", 1., 1.4541 , 0.6658, "user","userOverallSys")
syst_FFstat_VRWZ_highht = Systematic("syst_FFstat_VRWZ_highht", 1., 1.568 , 0.4523, "user","userOverallSys")
syst_FFstat_VRTop = Systematic("syst_FFstat_VRTop", 1., 0.6793 , 1.1155, "user","userOverallSys")
syst_FFstat_VRFakes = Systematic("syst_FFstat_VRFakes", 1., 1.0786 , 0.915, "user","userOverallSys")
syst_FFstat_VRTopAl = Systematic("syst_FFstat_VRTopAl", 1., 0.6859 , 1.0912, "user","userOverallSys")

syst_FFstat_SRWZ1 = Systematic("syst_FFstat_SRWZ1", 1., 3.1877 , -0.324, "user","userOverallSys")
syst_FFstat_SRWZ2 = Systematic("syst_FFstat_SRWZ2", 1., 1.7348 , -0.7639, "user","userOverallSys")
syst_FFstat_SRWZ3 = Systematic("syst_FFstat_SRWZ3", 1., 2.0099 , -0.097, "user","userOverallSys")
syst_FFstat_SRWZ4 = Systematic("syst_FFstat_SRWZ4", 1., 1.312 , 0.7067, "user","userOverallSys")
syst_FFstat_SRWZ5 = Systematic("syst_FFstat_SRWZ5", 1., 1.6145 , 0.3321, "user","userOverallSys")
syst_FFstat_SRWZ6 = Systematic("syst_FFstat_SRWZ6", 1., 2.4333 , -0.5102, "user","userOverallSys")
syst_FFstat_SRWZ7 = Systematic("syst_FFstat_SRWZ7", 1., 1.2816 , 0.4605, "user","userOverallSys")
syst_FFstat_SRWZ8 = Systematic("syst_FFstat_SRWZ8", 1., 1.494 , 0.4909, "user","userOverallSys")
syst_FFstat_SRWZ9 = Systematic("syst_FFstat_SRWZ9", 1., 2.3604 , 0.2594, "user","userOverallSys")
syst_FFstat_SRWZ10 = Systematic("syst_FFstat_SRWZ10", 1., 3.4864 , -0.6533, "user","userOverallSys")
syst_FFstat_SRWZ11 = Systematic("syst_FFstat_SRWZ11", 1., 1.9234 , 0.0847, "user","userOverallSys")
syst_FFstat_SRWZ12 = Systematic("syst_FFstat_SRWZ12", 1., 1.6553 , 0.3447, "user","userOverallSys")
syst_FFstat_SRWZ13 = Systematic("syst_FFstat_SRWZ13", 1., 3.0008 , -0.2271, "user","userOverallSys")
syst_FFstat_SRWZ14 = Systematic("syst_FFstat_SRWZ14", 1., 6.2878 , 0.4402, "user","userOverallSys")
syst_FFstat_SRWZ15 = Systematic("syst_FFstat_SRWZ15", 1., 1.6554 , 0.2527, "user","userOverallSys")
syst_FFstat_SRWZ16 = Systematic("syst_FFstat_SRWZ16", 1., 2.0347 , -0.043, "user","userOverallSys")
syst_FFstat_SRWZ17 = Systematic("syst_FFstat_SRWZ17", 1., 2.0533 , 0.1861, "user","userOverallSys")
syst_FFstat_SRWZ18 = Systematic("syst_FFstat_SRWZ18", 1., 2.5171 , -0.1411, "user","userOverallSys")
syst_FFstat_SRWZ19 = Systematic("syst_FFstat_SRWZ19", 1., 2.4801 , -0.4456, "user","userOverallSys")
syst_FFstat_SRWZ20 = Systematic("syst_FFstat_SRWZ20", 1., 0.8124 , 1.209, "user","userOverallSys")

syst_FFstat_SRWh1 = Systematic("syst_FFstat_SRWh1", 1., -0.1045 , 2.1388, "user","userOverallSys")
syst_FFstat_SRWh2 = Systematic("syst_FFstat_SRWh2", 1., 1.6755 , 0.3501, "user","userOverallSys")
syst_FFstat_SRWh3 = Systematic("syst_FFstat_SRWh3", 1., 1.8852 , 0.1078, "user","userOverallSys")
syst_FFstat_SRWh4 = Systematic("syst_FFstat_SRWh4", 1., 1.7067 , 0.4046, "user","userOverallSys")
syst_FFstat_SRWh5 = Systematic("syst_FFstat_SRWh5", 1., 0.1596 , 2.7886, "user","userOverallSys")
syst_FFstat_SRWh6 = Systematic("syst_FFstat_SRWh6", 1., 0.6784 , 1.0324, "user","userOverallSys")
syst_FFstat_SRWh7 = Systematic("syst_FFstat_SRWh7", 1., 0.1807 , 1.0673, "user","userOverallSys")
syst_FFstat_SRWh8 = Systematic("syst_FFstat_SRWh8", 1., 1.1262 , 0.8621, "user","userOverallSys")
syst_FFstat_SRWh9 = Systematic("syst_FFstat_SRWh9", 1., 0.4812 , 1.7865, "user","userOverallSys")
syst_FFstat_SRWh10 = Systematic("syst_FFstat_SRWh10", 1., 2.3956 , -0.1709, "user","userOverallSys")
syst_FFstat_SRWh11 = Systematic("syst_FFstat_SRWh11", 1., -2.0834 , 4.2228, "user","userOverallSys")
syst_FFstat_SRWh12 = Systematic("syst_FFstat_SRWh12", 1., 3.1399 , 0.3485, "user","userOverallSys")
syst_FFstat_SRWh13 = Systematic("syst_FFstat_SRWh13", 1., 1.2643 , 0.823, "user","userOverallSys")
syst_FFstat_SRWh14 = Systematic("syst_FFstat_SRWh14", 1., 3.2348 , -1.3471, "user","userOverallSys")
syst_FFstat_SRWh15 = Systematic("syst_FFstat_SRWh15", 1., 1.7904 , 0.4124, "user","userOverallSys")
syst_FFstat_SRWh16 = Systematic("syst_FFstat_SRWh16", 1., 7.525 , -0.3447, "user","userOverallSys")
syst_FFstat_SRWh17 = Systematic("syst_FFstat_SRWh17", 1., 2.257 , 0.0261, "user","userOverallSys")
syst_FFstat_SRWh18 = Systematic("syst_FFstat_SRWh18", 1., 3.5606 , 0.1215, "user","userOverallSys")
syst_FFstat_SRWh19 = Systematic("syst_FFstat_SRWh19", 1., 1.6683 , 0.5174, "user","userOverallSys")

syst_FFstat_SRWhDFOS0j = Systematic("syst_FFstat_SRWhDFOS0j", 1., -0.8784 , 1.7478, "user","userOverallSys")
syst_FFstat_SRWhDFOS1j = Systematic("syst_FFstat_SRWhDFOS1j", 1., 1.804 , 0.1884, "user","userOverallSys")

syst_FakeClosure_VRWZ = Systematic("syst_FakeClosure_VRWZ", 1., 1.24, 0.76, "user","userOverallSys")
syst_FakeClosure_VRFakes = Systematic("syst_FakeClosure_VRfakes", 1., 1.22, 0.78, "user","userOverallSys")
syst_FakeClosure_CR = Systematic("syst_FakeClosure_CR", 1., 1.30, 0.7, "user","userOverallSys")
syst_FakeClosure_SR = Systematic("syst_FakeClosure_SR_SFOS", 1., 1.5, 0.5, "user","userOverallSys")
syst_FakeClosure_VRTop = Systematic("syst_FakeClosure_VRtop", 1., 1.08, 0.92, "user","userOverallSys")
syst_FakeClosure_VRTopAl = Systematic("syst_FakeClosure_VRtopAl", 1., 1.08, 0.92, "user","userOverallSys")
syst_FakeClosure_SRDF = Systematic("syst_FakeClosure_SR_DFOS", 1., 1.2, 0.8, "user","userOverallSys")

# Signal systematics
syst_Theory_Signal_flat = Systematic("syst_Theory_Signal_flat", 1., 1.1, 0.9, "user","userOverallSys")
syst_XSec_Signal_flat = Systematic("SigXSec", 1., 1.1, 0.9, "user","userOverallSys")

systList = [
	syst_jes_1_MC,
	syst_jes_2_MC,
	syst_jes_3_MC,
        syst_jes_4_MC,
        syst_jes_5_MC,
        syst_jes_6_MC,
        syst_jes_7_MC,
        syst_jes_8_MC,
        syst_jes_9_MC,
        syst_jes_10_MC,
        syst_jes_11_MC,
        syst_jes_12_MC,
        syst_jes_13_MC,
        syst_jes_14_MC,
        syst_jes_15_MC,
        syst_jes_16_MC,
        syst_jes_17_MC,
        syst_jes_18_MC,
        syst_jes_19_MC,
        syst_jes_20_MC,
        syst_jes_21_MC,
        syst_jes_22_MC,
        syst_jes_23_MC,
        syst_jes_24_MC,
        syst_jes_25_MC,
        syst_jes_26_MC,
        syst_jes_27_MC,
	syst_jer_dataMC_MC,
	syst_jer_1_MC,
	syst_jer_2_MC,
	syst_jer_3_MC,
        syst_jer_4_MC,
	syst_jer_5_MC,
        syst_jer_6_MC,
	syst_jer_7_MC,
        syst_MET_SoftTrk_ResoPara_MC,  
        syst_MET_SoftTrk_ResoPerp_MC,    
        syst_MET_SoftTrk_Scale_MC,
	syst_EG_Scale_MC,
	syst_MuID_MC,
	syst_MuMS_MC,
	syst_MuScale_MC,
	syst_MuSagResBias_MC,
	syst_MuSagRho_MC,
	syst_elecSF_EFF_Iso,
	syst_elecSF_EFF_ID,
	syst_elecSF_EFF_Reco,
	syst_elecSF_EFF_TriggerEff,
	syst_muonSF_EFF_Stat,
	syst_muonSF_EFF_Sys,
	syst_muonSF_EFF_Stat_lowPt,
	syst_muonSF_EFF_Sys_lowPt,
	syst_muonSF_ISO_Stat,
	syst_muonSF_ISO_Sys,
	syst_muonSF_TTVA_Stat,
	syst_muonSF_TTVA_Sys,
	syst_FT_EFF_B,
	syst_FT_EFF_C,
	syst_FT_EFF_L,
	syst_FT_EFF_extrCharm,
	syst_jvtSF
	]	


normSystList = [
        syst_jes_1_MC_norm,
        syst_jes_2_MC_norm,
        syst_jes_3_MC_norm,
	syst_jes_4_MC_norm,
        syst_jes_5_MC_norm,
        syst_jes_6_MC_norm,
        syst_jes_7_MC_norm,
        syst_jes_8_MC_norm,
        syst_jes_9_MC_norm,
        syst_jes_10_MC_norm,
        syst_jes_11_MC_norm,
        syst_jes_12_MC_norm,
        syst_jes_13_MC_norm,
        syst_jes_14_MC_norm,
        syst_jes_15_MC_norm,
        syst_jes_16_MC_norm,
        syst_jes_17_MC_norm,
        syst_jes_18_MC_norm,
        syst_jes_19_MC_norm,
        syst_jes_20_MC_norm,
        syst_jes_21_MC_norm,
        syst_jes_22_MC_norm,
        syst_jes_23_MC_norm,
        syst_jes_24_MC_norm,
        syst_jes_25_MC_norm,
        syst_jes_26_MC_norm,
        syst_jes_27_MC_norm,
        syst_jer_dataMC_norm,
        syst_jer_1_norm,
        syst_jer_2_norm,
        syst_jer_3_norm,
        syst_jer_4_norm,
        syst_jer_5_norm,
        syst_jer_6_norm,
        syst_jer_7_norm,
        syst_MET_SoftTrk_ResoPara_norm,
        syst_MET_SoftTrk_ResoPerp_norm,
        syst_MET_SoftTrk_Scale_norm,
        syst_EG_Scale_norm,
        syst_MuID_norm,
        syst_MuMS_norm,
        syst_MuScale_norm,
        syst_MuSagResBias_norm,
        syst_MuSagRho_norm,
        syst_elecSF_EFF_Iso_norm,
        syst_elecSF_EFF_ID_norm,
        syst_elecSF_EFF_Reco_norm,
        syst_elecSF_EFF_TriggerEff_norm,
        syst_muonSF_EFF_Stat_norm,
        syst_muonSF_EFF_Sys_norm,
        syst_muonSF_EFF_Stat_lowPt_norm,
        syst_muonSF_EFF_Sys_lowPt_norm,
        syst_muonSF_ISO_Stat_norm,
        syst_muonSF_ISO_Sys_norm,
        syst_muonSF_TTVA_Stat_norm,
        syst_muonSF_TTVA_Sys_norm,
        syst_FT_EFF_B_norm,
        syst_FT_EFF_C_norm,
        syst_FT_EFF_L_norm,
        syst_FT_EFF_extrCharm_norm,
        syst_jvtSF_norm
        ]

sig_systList = [
	syst_jes_1_MC,
	syst_jes_2_MC,
	syst_jes_3_MC,
        syst_jes_4_MC,
        syst_jes_5_MC,
        syst_jes_6_MC,
        syst_jes_7_MC,
        syst_jes_8_MC,
        syst_jes_9_MC,
        syst_jes_10_MC,
        syst_jes_11_MC,
        syst_jes_12_MC,
        syst_jes_13_MC,
        syst_jes_14_MC,
        syst_jes_15_MC,
        syst_jes_16_MC,
        syst_jes_17_MC,
        syst_jes_18_MC,
        syst_jes_19_MC,
        syst_jes_20_MC,
        syst_jes_21_MC,
        syst_jes_22_MC,
        syst_jes_23_MC,
        syst_jes_24_MC,
        syst_jes_25_MC,
        syst_jes_26_MC,
        syst_jes_27_MC,
	syst_PunchThrough_AFII_MC,
	syst_RelNonCl_AFII_MC,
	syst_jer_dataMC_AFII_MC,
	syst_jer_1_MC,
	syst_jer_2_MC,
	syst_jer_3_MC,
        syst_jer_4_MC,
	syst_jer_5_MC,
        syst_jer_6_MC,
	syst_jer_7_MC,
        syst_MET_SoftTrk_ResoPara_MC,  
        syst_MET_SoftTrk_ResoPerp_MC,    
        syst_MET_SoftTrk_Scale_MC,
	syst_EG_Scale_MC,
	syst_EG_Scale_AF2,
	syst_MuID_MC,
	syst_MuMS_MC,
	syst_MuScale_MC,
	syst_MuSagResBias_MC,
	syst_MuSagRho_MC,
	syst_elecSF_EFF_Iso,
	syst_elecSF_EFF_ID,
	syst_elecSF_EFF_Reco,
	syst_elecSF_EFF_TriggerEff,
	syst_muonSF_EFF_Stat,
	syst_muonSF_EFF_Sys,
	syst_muonSF_EFF_Stat_lowPt,
	syst_muonSF_EFF_Sys_lowPt,
	syst_muonSF_ISO_Stat,
	syst_muonSF_ISO_Sys,
	syst_muonSF_TTVA_Stat,
	syst_muonSF_TTVA_Sys,
	syst_FT_EFF_B,
	syst_FT_EFF_C,
	syst_FT_EFF_L,
	syst_FT_EFF_extrCharm,
	syst_jvtSF
	]

# 3L configuration ----------------------------------------------------------------------------------------------------
#FIXME check naming convention

Diboson3L0jSample=Sample("Dibosons_3L_0j", kPink)
Diboson3L0jSample.setStatConfig(useStat)
Diboson3L0jSample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
Diboson3L0jSample.setNormRegions([("WZ_CR_0jets","cuts")])
Diboson3L0jSample.addInputs(bgdFiles_a,"Dibosons_3L_nJ0_a")
Diboson3L0jSample.addInputs(bgdFiles_d,"Dibosons_3L_nJ0_d")
Diboson3L0jSample.addInputs(bgdFiles_e,"Dibosons_3L_nJ0_e")

Diboson3LLowHTSample=Sample("Dibosons_3L_lowHT", kPink)
Diboson3LLowHTSample.setStatConfig(useStat)
Diboson3LLowHTSample.setNormFactor("mu_WZSF_LowHT",1.,0.,5.)
Diboson3LLowHTSample.setNormRegions([("WZ_CR_LowHT","cuts")])
Diboson3LLowHTSample.addInputs(bgdFiles_a,"Dibosons_3L_LowHT_a")
Diboson3LLowHTSample.addInputs(bgdFiles_d,"Dibosons_3L_LowHT_d")
Diboson3LLowHTSample.addInputs(bgdFiles_e,"Dibosons_3L_LowHT_e")

Diboson3LHighHTSample=Sample("Dibosons_3L_highHT", kPink)
Diboson3LHighHTSample.setStatConfig(useStat)
Diboson3LHighHTSample.setNormFactor("mu_WZSF_HighHT",1.,0.,5.)
Diboson3LHighHTSample.setNormRegions([("WZ_CR_HighHT","cuts")])
Diboson3LHighHTSample.addInputs(bgdFiles_a,"Dibosons_3L_HighHT_a")
Diboson3LHighHTSample.addInputs(bgdFiles_d,"Dibosons_3L_HighHT_d")
Diboson3LHighHTSample.addInputs(bgdFiles_e,"Dibosons_3L_HighHT_e")

Diboson4LSample=Sample("Dibosons_4L", kGreen)
Diboson4LSample.setStatConfig(useStat)
Diboson4LSample.setNormByTheory()
Diboson4LSample.addInputs(bgdFiles_a,"Dibosons_4L_a")
Diboson4LSample.addInputs(bgdFiles_d,"Dibosons_4L_d")
Diboson4LSample.addInputs(bgdFiles_e,"Dibosons_4L_e")
if not noSysts and doThUncert: Diboson4LSample.addSystematic(syst_xSec_ZZ_flat)

Diboson2LSample=Sample("Dibosons_2L", kGreen)
Diboson2LSample.setStatConfig(useStat)
Diboson2LSample.setNormByTheory()
Diboson2LSample.addInputs(bgdFiles_a,"Dibosons_2L_a")
Diboson2LSample.addInputs(bgdFiles_d,"Dibosons_2L_d")
Diboson2LSample.addInputs(bgdFiles_e,"Dibosons_2L_e")
if not noSysts and doThUncert: Diboson2LSample.addSystematic(syst_xSec_ZZ_flat)

higgsSample=Sample("Higgs_VH", kPink)
higgsSample.setStatConfig(useStat)
higgsSample.setNormByTheory()
higgsSample.addInputs(bgdFiles_a,"Higgs_a")
higgsSample.addInputs(bgdFiles_d,"Higgs_d")
higgsSample.addInputs(bgdFiles_e,"Higgs_e")
higgsSample.addSampleSpecificWeight("!(runNumber==346343 || runNumber==346344 || runNumber==346345)")
if not noSysts and doThUncert: higgsSample.addSystematic(syst_xSec_Higgs_flat)

ttHSample=Sample("Higgs_ttH", kPink)
ttHSample.setStatConfig(useStat)
ttHSample.setNormByTheory()
ttHSample.addInputs(bgdFiles_a,"Higgs_a")
ttHSample.addInputs(bgdFiles_d,"Higgs_d")
ttHSample.addInputs(bgdFiles_e,"Higgs_e")
ttHSample.addSampleSpecificWeight("(runNumber==346343 || runNumber==346344 || runNumber==346345)")
if not noSysts and doThUncert: ttHSample.addSystematic(syst_xSec_ttH_flat)

ttVSample=Sample("ttV", kOrange)
ttVSample.setStatConfig(useStat)
ttVSample.setNormByTheory()
ttVSample.addInputs(bgdFiles_a,"ttV_a")
ttVSample.addInputs(bgdFiles_d,"ttV_d")
ttVSample.addInputs(bgdFiles_e,"ttV_e")
if not noSysts and doThUncert: ttVSample.addSystematic(syst_xSec_ttW_flat)
if not noSysts and doThUncert: ttVSample.addSystematic(syst_xSec_ttZ_flat)
if not noSysts and doThUncert: ttVSample.addSystematic(syst_xSec_other_flat)
#if not noSysts and doThUncert: ttVSample.addSystematic(syst_Theory_ttV_flat)

tribosonSample=Sample("VVV", kCyan+2)
tribosonSample.setStatConfig(useStat)
tribosonSample.setNormByTheory()
tribosonSample.addInputs(bgdFiles_a,"VVV_a")
tribosonSample.addInputs(bgdFiles_d,"VVV_d")
tribosonSample.addInputs(bgdFiles_e,"VVV_e")
#if not noSysts and doThUncert: tribosonSample.addSystematic(syst_xSec_VVV_flat)
if not noSysts and doThUncert: tribosonSample.addSystematic(syst_Theory_VVV_flat)

VgammaSample=Sample("MCFakes_Vg", kCyan+2)
VgammaSample.setStatConfig(useStat)
VgammaSample.setNormByTheory()
VgammaSample.addInputs(bgdFiles_a,"Vgamma_a")
VgammaSample.addInputs(bgdFiles_d,"Vgamma_d")
VgammaSample.addInputs(bgdFiles_e,"Vgamma_e")

ttbarSample=Sample("ttbar", kCyan-9)
ttbarSample.setStatConfig(useStat)
ttbarSample.setNormByTheory()
ttbarSample.addInputs(bgdFiles_a,"ttbar_a")
ttbarSample.addInputs(bgdFiles_d,"ttbar_d")
ttbarSample.addInputs(bgdFiles_e,"ttbar_e")
if whichSR=="offshell" :
	if not noSysts and doThUncert: ttbarSample.addSystematic(syst_xSec_ttbar_flat)
#multitopSample.buildHisto([0.01], "SR3LSFOS0ja", "cuts")

singleTSample=Sample("SingleT", kCyan+4)
singleTSample.setStatConfig(useStat)
singleTSample.setNormByTheory()
singleTSample.addInputs(bgdFiles_a,"SingleT_a")
singleTSample.addInputs(bgdFiles_d,"SingleT_d")
singleTSample.addInputs(bgdFiles_e,"SingleT_e")

if not useFakes:

        allmcSamples=['Dibosons_3L_0j','Dibosons_3L_lowHT','Dibosons_3L_highHT','Dibosons_4L','Dibosons_2L','MCFakes','MCFakes_Vg','VVV','Higgs','ttV', 'ttbar','SingleT']
        mconlySamples=['Dibosons_4L','Dibosons_2L','ZZ','VVV','Higgs','ttV', 'ttbar','SingleT']

        mcfakesSample=Sample("MCFakes", kMagenta+6)
        mcfakesSample.setStatConfig(useStat)
        mcfakesSample.setNormByTheory()
        mcfakesSample.addInputs(bgdFiles_a,"Zjets_a")
        mcfakesSample.addInputs(bgdFiles_d,"ZZ_d")
        mcfakesSample.addInputs(bgdFiles_e,"Zjets_e")

if useFakes:

        allmcSamples=['Dibosons_3L_0j','Dibosons_3L_lowHT','Dibosons_3L_highHT','Dibosons_4L','Dibosons_2L','VVV','Higgs','ttV', 'ttbar','SingleT']
        mconlySamples=['Dibosons_4L','Dibosons_2L','VVV','Higgs_VH','Higgs_ttH','ttV', 'ttbar','SingleT']
        normSamples=['Dibosons_3L_0j','Dibosons_3L_lowHT','Dibosons_3L_highHT']

        fakeSample=Sample("Fakes",kMagenta+6)
        #fakeSample.setNormByTheory()
        fakeSample.setStatConfig(False)
        #fakeSample.addSampleSpecificWeight(")
        fakeSample.addInputs(fakeFiles)

# Data setup ----------------------------------------------------------------------------------------------------

dataSample = Sample("Data_CENTRAL",kBlack)
dataSample.setData()
#dataSample.setFileList(dataFiles)
dataSample.addInputs(dataFiles)

#dataSample.buildHisto([0.01], "SR3LDFOS0j", "cuts")
#dataSample.buildHisto([0.01], "SR3LSFOS0ja", "cuts")

# ----------------------------------------------------------------------------------------------------
#Here we are basically saying which backgrounds we want to include in the analysis, adding the input from the files

#if not useFakes:
#	
#	for sam in [mcfakesSample,Diboson3L0jSample,Diboson4LSample,Diboson2LSample,ttVSample,higgsSample,ttbarSample]:
#		#sam.setFileList(bgdFiles)
#		sam.setSuffixTreeName("_a_CENTRAL")
#		sam.addInputs(bgdFiles_a)
#		sam.setSuffixTreeName("_d_CENTRAL")
#		sam.addInputs(bgdFiles_d)
#		sam.setSuffixTreeName("_e_CENTRAL")
#		sam.addInputs(bgdFiles_e)
#else:
#	for sam in [wzdibosonSample,zzdibosonSample,tribosonSample,ttVSample,higgsSample,multitopSample]:
#		#sam.setFileList(bgdFiles)
#		sam.addInputs(bgdFiles)
#	#fakeSample.setFileList(fakeFiles)
#	fakeSample.addInputs(fakeFiles)
        

# Fits ----------------------------------------------------------------------------------------------------
#Binnings
crNBins	     = 1
crBinLow     = 0.5
crBinHigh    = 1.5

srNBins	  = 1
srBinLow  = 0.5
srBinHigh = 1.5

#---------------------------------------------------background only ------------------------------------------------
#
bkt = configMgr.addFitConfig("BkgOnly")
#Not sure what the following lines are doing, but nothing major, will keep them commented

#if useStat:
#	bkt.statErrThreshold = None #0.01
#else:
#	bkt.statErrThreshold=None

#*****************************************************************************************************************
#Adding systematics to the samples
if useFakes:
        bkt.addSamples([tribosonSample,higgsSample,ttHSample,ttVSample,Diboson3L0jSample,Diboson3LLowHTSample,Diboson3LHighHTSample,Diboson4LSample,Diboson2LSample,singleTSample,ttbarSample,fakeSample,dataSample])

else:
        bkt.addSamples([tribosonSample,higgsSample,ttHSample,ttVSample,Diboson3L0jSample,Diboson3LLowHTSample,Diboson3LHighHTSample,Diboson4LSample,Diboson2LSample,mcfakesSample,singleTSample,ttbarSample,VgammaSample,dataSample])

if not noSysts:
        for sample in mconlySamples:
                for syst in systList:
                        bkt.getSample(sample).addSystematic(syst)

        for sample in normSamples:
                for syst in normSystList:
                        bkt.getSample(sample).addSystematic(syst)

meas=bkt.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.017) #Lumi uncertainties is here!
meas.addPOI("mu_SIG")
meas.addParamSetting("mu_BG",True,1)
        
#---------------------------------------------------------------------------------------------------- 3L CA fits

#LET'S START WITH THE FIT

CR_WZ_0jets = bkt.addChannel("cuts",["WZ_CR_0jets"],srNBins,srBinLow,srBinHigh)
CR_WZ_LowHT = bkt.addChannel("cuts",["WZ_CR_LowHT"],srNBins,srBinLow,srBinHigh)
CR_WZ_HighHT = bkt.addChannel("cuts",["WZ_CR_HighHT"],srNBins,srBinLow,srBinHigh)

if doFakeUncert :
	if not noSysts: 
		CR_WZ_0jets.getSample("Fakes").addSystematic(syst_FFstat_CRWZ_0j)
        	CR_WZ_0jets.getSample("Fakes").addSystematic(syst_FakeClosure_CR)

		CR_WZ_LowHT.getSample("Fakes").addSystematic(syst_FFstat_CRWZ_lowht)
                CR_WZ_LowHT.getSample("Fakes").addSystematic(syst_FakeClosure_CR)

		CR_WZ_HighHT.getSample("Fakes").addSystematic(syst_FFstat_CRWZ_highht)
                CR_WZ_HighHT.getSample("Fakes").addSystematic(syst_FakeClosure_CR)

##################
# Signal Regions #
##################

if "AllSRs" in whichSR:

	SR1_WZ= bkt.addChannel("cuts",["SR1_WZ"],srNBins,srBinLow,srBinHigh)
	#SR1_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR1_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ1_QCD)
		SR1_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ1_QCD)
		SR1_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ1_QCD)
		SR1_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ1_QCD)

		SR1_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ1_PDF)
		SR1_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ1_PDF)
		SR1_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ1_PDF)
		SR1_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ1_PDF)

		SR1_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR1_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR1_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR1_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR1_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR1_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ1)
		SR1_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR2_WZ= bkt.addChannel("cuts",["SR2_WZ"],srNBins,srBinLow,srBinHigh)
	#SR2_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR2_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ2_QCD)
		SR2_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ2_QCD)
		SR2_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ2_QCD)
		SR2_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ2_QCD)

		SR2_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ2_PDF)
		SR2_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ2_PDF)
		SR2_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ2_PDF)
		SR2_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ2_PDF)

		SR2_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR2_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR2_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR2_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR2_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR2_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ2)
		SR2_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR3_WZ= bkt.addChannel("cuts",["SR3_WZ"],srNBins,srBinLow,srBinHigh)
	#SR3_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR3_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ3_QCD)
		SR3_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ3_QCD)
		SR3_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ3_QCD)
		SR3_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ3_QCD)

		SR3_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ3_PDF)
		SR3_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ3_PDF)
		SR3_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ3_PDF)
		SR3_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ3_PDF)

		SR3_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR3_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR3_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR3_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR3_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR3_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ3)
		SR3_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)
		
	SR4_WZ= bkt.addChannel("cuts",["SR4_WZ"],srNBins,srBinLow,srBinHigh)
	#SR4_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR4_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ4_QCD)
		SR4_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ4_QCD)
		SR4_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ4_QCD)
		SR4_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ4_QCD)

		SR4_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ4_PDF)
		SR4_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ4_PDF)
		SR4_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ4_PDF)
		SR4_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ4_PDF)

		SR4_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR4_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR4_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR4_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR4_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ4)
		SR4_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR5_WZ= bkt.addChannel("cuts",["SR5_WZ"],srNBins,srBinLow,srBinHigh)
	#SR5_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR5_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ5_QCD)
		SR5_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ5_QCD)
		SR5_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ5_QCD)
		SR5_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ5_QCD)

		SR5_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ5_PDF)
		SR5_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ5_PDF)
		SR5_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ5_PDF)
		SR5_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ5_PDF)

		SR5_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR5_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR5_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR5_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR5_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ5)
		SR5_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR6_WZ= bkt.addChannel("cuts",["SR6_WZ"],srNBins,srBinLow,srBinHigh)
	#SR6_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR6_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ6_QCD)
		SR6_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ6_QCD)
		SR6_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ6_QCD)
		SR6_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ6_QCD)

		SR6_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ6_PDF)
		SR6_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ6_PDF)
		SR6_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ6_PDF)
		SR6_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ6_PDF)

		SR6_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR6_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR6_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR6_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR6_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ6)
		SR6_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR7_WZ= bkt.addChannel("cuts",["SR7_WZ"],srNBins,srBinLow,srBinHigh)
	#SR7_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR7_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ7_QCD)
		SR7_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ7_QCD)
		SR7_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ7_QCD)
		SR7_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ7_QCD)

		SR7_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ7_PDF)
		SR7_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ7_PDF)
		SR7_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ7_PDF)
		SR7_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ7_PDF)

		SR7_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR7_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR7_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR7_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR7_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ7)
		SR7_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR8_WZ= bkt.addChannel("cuts",["SR8_WZ"],srNBins,srBinLow,srBinHigh)
	#SR8_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR8_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ8_QCD)
		SR8_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ8_QCD)
		SR8_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ8_QCD)
		SR8_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ8_QCD)

		SR8_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ8_PDF)
		SR8_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ8_PDF)
		SR8_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ8_PDF)
		SR8_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ8_PDF)

		SR8_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR8_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR8_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR8_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR8_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ8)
		SR8_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR9_WZ= bkt.addChannel("cuts",["SR9_WZ"],srNBins,srBinLow,srBinHigh)
	#SR9_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR9_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ9_QCD)
		SR9_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ9_QCD)
		SR9_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ9_QCD)
		SR9_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ9_QCD)

		SR9_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ9_PDF)
		SR9_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ9_PDF)
		SR9_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ9_PDF)
		SR9_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ9_PDF)

		SR9_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR9_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR9_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR9_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR9_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ9)
		SR9_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR10_WZ= bkt.addChannel("cuts",["SR10_WZ"],srNBins,srBinLow,srBinHigh)
	#SR10_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR10_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ10_QCD)
		SR10_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ10_QCD)
		SR10_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ10_QCD)
		SR10_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ10_QCD)

		SR10_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ10_PDF)
		SR10_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ10_PDF)
		SR10_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ10_PDF)
		SR10_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ10_PDF)

		SR10_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR10_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR10_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR10_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR10_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ10)
		SR10_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR11_WZ= bkt.addChannel("cuts",["SR11_WZ"],srNBins,srBinLow,srBinHigh)
	#SR11_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR11_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ11_QCD)
		SR11_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ11_QCD)
		SR11_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ11_QCD)
		SR11_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ11_QCD)

		SR11_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ11_PDF)
		SR11_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ11_PDF)
		SR11_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ11_PDF)
		SR11_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ11_PDF)

		SR11_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR11_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR11_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR11_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR11_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ11)
		SR11_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR12_WZ= bkt.addChannel("cuts",["SR12_WZ"],srNBins,srBinLow,srBinHigh)
	#SR12_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR12_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ12_QCD)
		SR12_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ12_QCD)
		SR12_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ12_QCD)
		SR12_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ12_QCD)

		SR12_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ12_PDF)
		SR12_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ12_PDF)
		SR12_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ12_PDF)
		SR12_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ12_PDF)

		SR12_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR12_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR12_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR12_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR12_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ12)
		SR12_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR13_WZ= bkt.addChannel("cuts",["SR13_WZ"],srNBins,srBinLow,srBinHigh)
	#SR13_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR13_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ13_QCD)
		SR13_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ13_QCD)
		SR13_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ13_QCD)
		SR13_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ13_QCD)

		SR13_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ13_PDF)
		SR13_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ13_PDF)
		SR13_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ13_PDF)
		SR13_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ13_PDF)

		SR13_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR13_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR13_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR13_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR13_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ13)
		SR13_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR14_WZ= bkt.addChannel("cuts",["SR14_WZ"],srNBins,srBinLow,srBinHigh)
	#SR14_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR14_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ14_QCD)
		SR14_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ14_QCD)
		SR14_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ14_QCD)
		SR14_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ14_QCD)

		SR14_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ14_PDF)
		SR14_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ14_PDF)
		SR14_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ14_PDF)
		SR14_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ14_PDF)

		SR14_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR14_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR14_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR14_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR14_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ14)
		SR14_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR15_WZ= bkt.addChannel("cuts",["SR15_WZ"],srNBins,srBinLow,srBinHigh)
	#SR15_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR15_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ15_QCD)
		SR15_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ15_QCD)
		SR15_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ15_QCD)
		SR15_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ15_QCD)

		SR15_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ15_PDF)
		SR15_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ15_PDF)
		SR15_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ15_PDF)
		SR15_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ15_PDF)

		SR15_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR15_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR15_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR15_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR15_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ15)
		SR15_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR16_WZ= bkt.addChannel("cuts",["SR16_WZ"],srNBins,srBinLow,srBinHigh)
	#SR16_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR16_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ16_QCD)
		SR16_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ16_QCD)
		SR16_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ16_QCD)
		SR16_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ16_QCD)

		SR16_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ16_PDF)
		SR16_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ16_PDF)
		SR16_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ16_PDF)
		SR16_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ16_PDF)

		SR16_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR16_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR16_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR16_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR16_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ16)
		SR16_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR17_WZ= bkt.addChannel("cuts",["SR17_WZ"],srNBins,srBinLow,srBinHigh)
	#SR17_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR17_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ17_QCD)
		SR17_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ17_QCD)
		SR17_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ17_QCD)
		SR17_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ17_QCD)

		SR17_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ17_PDF)
		SR17_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ17_PDF)
		SR17_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ17_PDF)
		SR17_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ17_PDF)

		SR17_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR17_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR17_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR17_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR17_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ17)
		SR17_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR18_WZ= bkt.addChannel("cuts",["SR18_WZ"],srNBins,srBinLow,srBinHigh)
	#SR18_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR18_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ18_QCD)
		SR18_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ18_QCD)
		SR18_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ18_QCD)
		SR18_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ18_QCD)

		SR18_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ18_PDF)
		SR18_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ18_PDF)
		SR18_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ18_PDF)
		SR18_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ18_PDF)

		SR18_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR18_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR18_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR18_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)	

		SR18_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ18)
		SR18_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR19_WZ= bkt.addChannel("cuts",["SR19_WZ"],srNBins,srBinLow,srBinHigh)
	#SR19_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR19_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ19_QCD)
		SR19_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ19_QCD)
		SR19_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ19_QCD)
		SR19_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ19_QCD)

		SR19_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ19_PDF)
		SR19_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ19_PDF)
		SR19_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ19_PDF)
		SR19_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ19_PDF)
		
		SR19_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR19_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR19_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR19_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR19_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ19)
		SR19_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR20_WZ= bkt.addChannel("cuts",["SR20_WZ"],srNBins,srBinLow,srBinHigh)
	#SR19_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR20_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ20_QCD)
		SR20_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ20_QCD)
		SR20_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ20_QCD)
		SR20_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ20_QCD)

		SR20_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ20_PDF)
		SR20_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ20_PDF)
		SR20_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ20_PDF)
		SR20_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ20_PDF)

		SR20_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR20_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR20_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR20_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR20_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ20)
		SR20_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)	


	SR1_Wh= bkt.addChannel("cuts",["SR1_Wh"],srNBins,srBinLow,srBinHigh)
	#SR1_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR1_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh1_QCD)
		SR1_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh1_QCD)
		SR1_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh1_QCD)
		SR1_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh1_QCD)

		SR1_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh1_PDF)
		SR1_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh1_PDF)
		SR1_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh1_PDF)
		SR1_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh1_PDF)
		
		SR1_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR1_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR1_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR1_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR1_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh1)
		SR1_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR2_Wh= bkt.addChannel("cuts",["SR2_Wh"],srNBins,srBinLow,srBinHigh)
	#SR2_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR2_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh2_QCD)
		SR2_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh2_QCD)
		SR2_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh2_QCD)
		SR2_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh2_QCD)

		SR2_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh2_PDF)
		SR2_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh2_PDF)
		SR2_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh2_PDF)
		SR2_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh2_PDF)
		
		SR2_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR2_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR2_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR2_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR2_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh2)
		SR2_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR3_Wh= bkt.addChannel("cuts",["SR3_Wh"],srNBins,srBinLow,srBinHigh)
	#SR3_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR3_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh3_QCD)
		SR3_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh3_QCD)
		SR3_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh3_QCD)
		SR3_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh3_QCD)

		SR3_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh3_PDF)
		SR3_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh3_PDF)
		SR3_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh3_PDF)
		SR3_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh3_PDF)
		
		SR3_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR3_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR3_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR3_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR3_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh3)
		SR3_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)
		
	SR4_Wh= bkt.addChannel("cuts",["SR4_Wh"],srNBins,srBinLow,srBinHigh)
	#SR4_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR4_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh4_QCD)
		SR4_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh4_QCD)
		SR4_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh4_QCD)
		SR4_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh4_QCD)

		SR4_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh4_PDF)
		SR4_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh4_PDF)
		SR4_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh4_PDF)
		SR4_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh4_PDF)
		
		SR4_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR4_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR4_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR4_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR4_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh4)
		SR4_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR5_Wh= bkt.addChannel("cuts",["SR5_Wh"],srNBins,srBinLow,srBinHigh)
	#SR5_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR5_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh5_QCD)
		SR5_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh5_QCD)
		SR5_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh5_QCD)
		SR5_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh5_QCD)

		SR5_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh5_PDF)
		SR5_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh5_PDF)
		SR5_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh5_PDF)
		SR5_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh5_PDF)

		SR5_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR5_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR5_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR5_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR5_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh5)
		SR5_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR6_Wh= bkt.addChannel("cuts",["SR6_Wh"],srNBins,srBinLow,srBinHigh)
	#SR6_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR6_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh6_QCD)
		SR6_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh6_QCD)
		SR6_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh6_QCD)
		SR6_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh6_QCD)

		SR6_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh6_PDF)
		SR6_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh6_PDF)
		SR6_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh6_PDF)
		SR6_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh6_PDF)
		
		SR6_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR6_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR6_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR6_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR6_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh6)
		SR6_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR7_Wh= bkt.addChannel("cuts",["SR7_Wh"],srNBins,srBinLow,srBinHigh)
	#SR7_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR7_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh7_QCD)
		SR7_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh7_QCD)
		SR7_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh7_QCD)
		SR7_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh7_QCD)

		SR7_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh7_PDF)
		SR7_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh7_PDF)
		SR7_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh7_PDF)
		SR7_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh7_PDF)
		
		SR7_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR7_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR7_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR7_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR7_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh7)
		SR7_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR8_Wh= bkt.addChannel("cuts",["SR8_Wh"],srNBins,srBinLow,srBinHigh)
	#SR8_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR8_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh8_QCD)
		SR8_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh8_QCD)
		SR8_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh8_QCD)
		SR8_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh8_QCD)

		SR8_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh8_PDF)
		SR8_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh8_PDF)
		SR8_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh8_PDF)
		SR8_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh8_PDF)
		
		SR8_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR8_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR8_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR8_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR8_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh8)
		SR8_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR9_Wh= bkt.addChannel("cuts",["SR9_Wh"],srNBins,srBinLow,srBinHigh)
	#SR9_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR9_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh9_QCD)
		SR9_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh9_QCD)
		SR9_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh9_QCD)
		SR9_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh9_QCD)

		SR9_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh9_PDF)
		SR9_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh9_PDF)
		SR9_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh9_PDF)
		SR9_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh9_PDF)
		
		SR9_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR9_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR9_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR9_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR9_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh9)
		SR9_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR10_Wh= bkt.addChannel("cuts",["SR10_Wh"],srNBins,srBinLow,srBinHigh)
	#SR10_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR10_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh10_QCD)
		SR10_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh10_QCD)
		SR10_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh10_QCD)
		SR10_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh10_QCD)

		SR10_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh10_PDF)
		SR10_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh10_PDF)
		SR10_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh10_PDF)
		SR10_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh10_PDF)
		
		SR10_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR10_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR10_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR10_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR10_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh10)
		SR10_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR11_Wh= bkt.addChannel("cuts",["SR11_Wh"],srNBins,srBinLow,srBinHigh)
	#SR11_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR11_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh11_QCD)
		SR11_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh11_QCD)
		SR11_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh11_QCD)
		SR11_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh11_QCD)

		SR11_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh11_PDF)
		SR11_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh11_PDF)
		SR11_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh11_PDF)
		SR11_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh11_PDF)
		
		SR11_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR11_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR11_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR11_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR11_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh11)
		SR11_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR12_Wh= bkt.addChannel("cuts",["SR12_Wh"],srNBins,srBinLow,srBinHigh)
	#SR12_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR12_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh12_QCD)
		SR12_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh12_QCD)
		SR12_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh12_QCD)
		SR12_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh12_QCD)

		SR12_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh12_PDF)
		SR12_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh12_PDF)
		SR12_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh12_PDF)
		SR12_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh12_PDF)
		
		SR12_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR12_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR12_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR12_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR12_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh12)
		SR12_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR13_Wh= bkt.addChannel("cuts",["SR13_Wh"],srNBins,srBinLow,srBinHigh)
	#SR13_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR13_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh13_QCD)
		SR13_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh13_QCD)
		SR13_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh13_QCD)
		SR13_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh13_QCD)

		SR13_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh13_PDF)
		SR13_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh13_PDF)
		SR13_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh13_PDF)
		SR13_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh13_PDF)
		
		SR13_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR13_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR13_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR13_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR13_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh13)
		SR13_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR14_Wh= bkt.addChannel("cuts",["SR14_Wh"],srNBins,srBinLow,srBinHigh)
	#SR14_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR14_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh14_QCD)
		SR14_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh14_QCD)
		SR14_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh14_QCD)
		SR14_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh14_QCD)

		SR14_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh14_PDF)
		SR14_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh14_PDF)
		SR14_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh14_PDF)
		SR14_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh14_PDF)
		
		SR14_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR14_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR14_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR14_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR14_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh14)
		SR14_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR15_Wh= bkt.addChannel("cuts",["SR15_Wh"],srNBins,srBinLow,srBinHigh)
	#SR15_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR15_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh15_QCD)
		SR15_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh15_QCD)
		SR15_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh15_QCD)
		SR15_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh15_QCD)

		SR15_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh15_PDF)
		SR15_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh15_PDF)
		SR15_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh15_PDF)
		SR15_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh15_PDF)
		
		SR15_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR15_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR15_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR15_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR15_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh15)
		SR15_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR16_Wh= bkt.addChannel("cuts",["SR16_Wh"],srNBins,srBinLow,srBinHigh)
	#SR16_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR16_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh16_QCD)
		SR16_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh16_QCD)
		SR16_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh16_QCD)
		SR16_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh16_QCD)

		SR16_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh16_PDF)
		SR16_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh16_PDF)
		SR16_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh16_PDF)
		SR16_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh16_PDF)
		
		SR16_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR16_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR16_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR16_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR16_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh16)
		SR16_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR17_Wh= bkt.addChannel("cuts",["SR17_Wh"],srNBins,srBinLow,srBinHigh)
	#SR17_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR17_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh17_QCD)
		SR17_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh17_QCD)
		SR17_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh17_QCD)
		SR17_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh17_QCD)

		SR17_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh17_PDF)
		SR17_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh17_PDF)
		SR17_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh17_PDF)
		SR17_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh17_PDF)
		
		SR17_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR17_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR17_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR17_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRHigh0jets_AltSample)

		SR17_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh17)
		SR17_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR18_Wh= bkt.addChannel("cuts",["SR18_Wh"],srNBins,srBinLow,srBinHigh)
	#SR18_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR18_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh18_QCD)
		SR18_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh18_QCD)
		SR18_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh18_QCD)
		SR18_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh18_QCD)

		SR18_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh18_PDF)
		SR18_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh18_PDF)
		SR18_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh18_PDF)
		SR18_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh18_PDF)
		
		SR18_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR18_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR18_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR18_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRHigh0jets_AltSample)
		
		SR18_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh18)
		SR18_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR19_Wh= bkt.addChannel("cuts",["SR19_Wh"],srNBins,srBinLow,srBinHigh)
	#SR19_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR19_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh19_QCD)
		SR19_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh19_QCD)
		SR19_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh19_QCD)
		SR19_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh19_QCD)

		SR19_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh19_PDF)
		SR19_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh19_PDF)
		SR19_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh19_PDF)
		SR19_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh19_PDF)
		
		SR19_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR19_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR19_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR19_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRHigh0jets_AltSample)

		SR19_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh19)
		SR19_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	bkt.setSignalChannels([
                                SR1_WZ,
                                SR2_WZ,
                                SR3_WZ,
                                SR4_WZ,
                                SR5_WZ,
                                SR6_WZ,
                                SR7_WZ,
                                SR8_WZ,
                                SR9_WZ,
                                SR10_WZ,
                                SR11_WZ,
                                SR12_WZ,
                                SR13_WZ,
                                SR14_WZ,
                                SR15_WZ,
                                SR16_WZ,
                                SR17_WZ,
                                SR18_WZ,
                                SR19_WZ,
                                SR20_WZ,
                                SR1_Wh,
                                SR2_Wh,
                                SR3_Wh,
                                SR4_Wh,
                                SR5_Wh,
                                SR6_Wh,
                                SR7_Wh,
                                SR8_Wh,
                                SR9_Wh,
                                SR10_Wh,
                                SR11_Wh,
                                SR12_Wh,
                                SR13_Wh,
                                SR14_Wh,
                                SR15_Wh,
                                SR16_Wh,
                                SR17_Wh,
                                SR18_Wh,
                                SR19_Wh
                                ])


if "onshell" in whichSR:

	SR1_WZ= bkt.addChannel("cuts",["SR1_WZ"],srNBins,srBinLow,srBinHigh)
	#SR1_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR1_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muR)
		SR1_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muR)
		SR1_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muR)
		SR1_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ1_QCD_muR)

		SR1_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muF)
                SR1_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muF)
                SR1_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muF)
                SR1_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ1_QCD_muF)

		SR1_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muRmuF)
                SR1_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muRmuF)
                SR1_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ1_QCD_muRmuF)
                SR1_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ1_QCD_muRmuF)

		SR1_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ1_PDF)
		SR1_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ1_PDF)
		SR1_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ1_PDF)
		SR1_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ1_PDF)

		SR1_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR1_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR1_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR1_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR1_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR1_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ1)
		SR1_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR2_WZ= bkt.addChannel("cuts",["SR2_WZ"],srNBins,srBinLow,srBinHigh)
	#SR2_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR2_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muR)
                SR2_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muR)
                SR2_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muR)
                SR2_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ2_QCD_muR)

                SR2_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muF)
                SR2_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muF)
                SR2_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muF)
                SR2_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ2_QCD_muF)

                SR2_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muRmuF)
                SR2_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muRmuF)
                SR2_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ2_QCD_muRmuF)
                SR2_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ2_QCD_muRmuF)
	
		SR2_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ2_PDF)
		SR2_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ2_PDF)
		SR2_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ2_PDF)
		SR2_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ2_PDF)

		SR2_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR2_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR2_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR2_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR2_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR2_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ2)
		SR2_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR3_WZ= bkt.addChannel("cuts",["SR3_WZ"],srNBins,srBinLow,srBinHigh)
	#SR3_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR3_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muR)
                SR3_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muR)
                SR3_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muR)
                SR3_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ3_QCD_muR)

                SR3_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muF)
                SR3_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muF)
                SR3_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muF)
                SR3_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ3_QCD_muF)

                SR3_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muRmuF)
                SR3_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muRmuF)
                SR3_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ3_QCD_muRmuF)
                SR3_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ3_QCD_muRmuF)

		SR3_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ3_PDF)
		SR3_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ3_PDF)
		SR3_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ3_PDF)
		SR3_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ3_PDF)

		SR3_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR3_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR3_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR3_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR3_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR3_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ3)
		SR3_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)
		
	SR4_WZ= bkt.addChannel("cuts",["SR4_WZ"],srNBins,srBinLow,srBinHigh)
	#SR4_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR4_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muR)
                SR4_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muR)
                SR4_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muR)
                SR4_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ4_QCD_muR)

                SR4_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muF)
                SR4_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muF)
                SR4_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muF)
                SR4_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ4_QCD_muF)

                SR4_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muRmuF)
                SR4_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muRmuF)
                SR4_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ4_QCD_muRmuF)
                SR4_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ4_QCD_muRmuF)

		SR4_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ4_PDF)
		SR4_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ4_PDF)
		SR4_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ4_PDF)
		SR4_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ4_PDF)

		SR4_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR4_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR4_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR4_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR4_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR4_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ4)
		SR4_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR5_WZ= bkt.addChannel("cuts",["SR5_WZ"],srNBins,srBinLow,srBinHigh)
	#SR5_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR5_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muR)
                SR5_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muR)
                SR5_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muR)
                SR5_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ5_QCD_muR)

                SR5_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muF)
                SR5_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muF)
                SR5_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muF)
                SR5_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ5_QCD_muF)

                SR5_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muRmuF)
                SR5_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muRmuF)
                SR5_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ5_QCD_muRmuF)
                SR5_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ5_QCD_muRmuF)

		SR5_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ5_PDF)
		SR5_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ5_PDF)
		SR5_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ5_PDF)
		SR5_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ5_PDF)

		SR5_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR5_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR5_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR5_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR5_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR5_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ5)
		SR5_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR6_WZ= bkt.addChannel("cuts",["SR6_WZ"],srNBins,srBinLow,srBinHigh)
	#SR6_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR6_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muR)
                SR6_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muR)
                SR6_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muR)
                SR6_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ6_QCD_muR)

                SR6_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muF)
                SR6_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muF)
                SR6_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muF)
                SR6_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ6_QCD_muF)

                SR6_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muRmuF)
                SR6_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muRmuF)
                SR6_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ6_QCD_muRmuF)
                SR6_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ6_QCD_muRmuF)

		SR6_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ6_PDF)
		SR6_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ6_PDF)
		SR6_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ6_PDF)
		SR6_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ6_PDF)

		SR6_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR6_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR6_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR6_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR6_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR6_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ6)
		SR6_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR7_WZ= bkt.addChannel("cuts",["SR7_WZ"],srNBins,srBinLow,srBinHigh)
	#SR7_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR7_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muR)
                SR7_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muR)
                SR7_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muR)
                SR7_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ7_QCD_muR)

                SR7_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muF)
                SR7_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muF)
                SR7_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muF)
                SR7_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ7_QCD_muF)

                SR7_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muRmuF)
                SR7_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muRmuF)
                SR7_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ7_QCD_muRmuF)
                SR7_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ7_QCD_muRmuF)

		SR7_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ7_PDF)
		SR7_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ7_PDF)
		SR7_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ7_PDF)
		SR7_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ7_PDF)

		SR7_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR7_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR7_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR7_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR7_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR7_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ7)
		SR7_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR8_WZ= bkt.addChannel("cuts",["SR8_WZ"],srNBins,srBinLow,srBinHigh)
	#SR8_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR8_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muR)
                SR8_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muR)
                SR8_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muR)
                SR8_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ8_QCD_muR)

                SR8_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muF)
                SR8_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muF)
                SR8_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muF)
                SR8_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ8_QCD_muF)

                SR8_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muRmuF)
                SR8_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muRmuF)
                SR8_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ8_QCD_muRmuF)
                SR8_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ8_QCD_muRmuF)		

		SR8_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ8_PDF)
		SR8_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ8_PDF)
		SR8_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ8_PDF)
		SR8_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ8_PDF)

		SR8_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR8_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR8_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
		SR8_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

		SR8_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR8_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ8)
		SR8_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR9_WZ= bkt.addChannel("cuts",["SR9_WZ"],srNBins,srBinLow,srBinHigh)
	#SR9_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR9_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muR)
                SR9_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muR)
                SR9_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muR)
                SR9_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ9_QCD_muR)

                SR9_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muF)
                SR9_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muF)
                SR9_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muF)
                SR9_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ9_QCD_muF)

                SR9_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muRmuF)
                SR9_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muRmuF)
                SR9_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ9_QCD_muRmuF)
                SR9_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ9_QCD_muRmuF)

		SR9_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ9_PDF)
		SR9_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ9_PDF)
		SR9_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ9_PDF)
		SR9_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ9_PDF)

		SR9_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR9_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR9_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR9_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR9_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR9_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ9)
		SR9_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR10_WZ= bkt.addChannel("cuts",["SR10_WZ"],srNBins,srBinLow,srBinHigh)
	#SR10_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR10_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muR)
                SR10_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muR)
                SR10_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muR)
                SR10_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ10_QCD_muR)

                SR10_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muF)
                SR10_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muF)
                SR10_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muF)
                SR10_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ10_QCD_muF)

                SR10_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muRmuF)
                SR10_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muRmuF)
                SR10_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ10_QCD_muRmuF)
                SR10_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ10_QCD_muRmuF)

		SR10_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ10_PDF)
		SR10_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ10_PDF)
		SR10_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ10_PDF)
		SR10_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ10_PDF)

		SR10_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR10_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR10_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR10_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR10_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR10_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ10)
		SR10_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR11_WZ= bkt.addChannel("cuts",["SR11_WZ"],srNBins,srBinLow,srBinHigh)
	#SR11_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR11_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muR)
                SR11_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muR)
                SR11_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muR)
                SR11_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ11_QCD_muR)

                SR11_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muF)
                SR11_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muF)
                SR11_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muF)
                SR11_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ11_QCD_muF)

                SR11_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muRmuF)
                SR11_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muRmuF)
                SR11_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ11_QCD_muRmuF)
                SR11_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ11_QCD_muRmuF)

		SR11_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ11_PDF)
		SR11_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ11_PDF)
		SR11_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ11_PDF)
		SR11_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ11_PDF)

		SR11_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR11_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR11_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR11_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR11_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR11_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ11)
		SR11_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR12_WZ= bkt.addChannel("cuts",["SR12_WZ"],srNBins,srBinLow,srBinHigh)
	#SR12_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR12_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muR)
                SR12_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muR)
                SR12_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muR)
                SR12_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ12_QCD_muR)

                SR12_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muF)
                SR12_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muF)
                SR12_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muF)
                SR12_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ12_QCD_muF)

                SR12_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muRmuF)
                SR12_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muRmuF)
                SR12_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ12_QCD_muRmuF)
                SR12_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ12_QCD_muRmuF)

		SR12_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ12_PDF)
		SR12_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ12_PDF)
		SR12_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ12_PDF)
		SR12_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ12_PDF)

		SR12_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR12_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR12_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR12_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR12_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR12_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ12)
		SR12_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR13_WZ= bkt.addChannel("cuts",["SR13_WZ"],srNBins,srBinLow,srBinHigh)
	#SR13_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR13_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muR)
                SR13_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muR)
                SR13_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muR)
                SR13_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ13_QCD_muR)

                SR13_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muF)
                SR13_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muF)
                SR13_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muF)
                SR13_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ13_QCD_muF)

                SR13_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muRmuF)
                SR13_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muRmuF)
                SR13_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ13_QCD_muRmuF)
                SR13_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ13_QCD_muRmuF)

		SR13_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ13_PDF)
		SR13_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ13_PDF)
		SR13_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ13_PDF)
		SR13_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ13_PDF)

		SR13_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR13_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR13_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR13_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR13_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR13_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ13)
		SR13_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR14_WZ= bkt.addChannel("cuts",["SR14_WZ"],srNBins,srBinLow,srBinHigh)
	#SR14_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR14_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muR)
                SR14_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muR)
                SR14_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muR)
                SR14_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ14_QCD_muR)

                SR14_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muF)
                SR14_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muF)
                SR14_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muF)
                SR14_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ14_QCD_muF)

                SR14_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muRmuF)
                SR14_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muRmuF)
                SR14_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ14_QCD_muRmuF)
                SR14_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ14_QCD_muRmuF)

		SR14_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ14_PDF)
		SR14_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ14_PDF)
		SR14_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ14_PDF)
		SR14_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ14_PDF)

		SR14_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR14_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR14_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR14_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR14_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR14_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ14)
		SR14_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR15_WZ= bkt.addChannel("cuts",["SR15_WZ"],srNBins,srBinLow,srBinHigh)
	#SR15_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR15_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muR)
                SR15_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muR)
                SR15_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muR)
                SR15_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ15_QCD_muR)

                SR15_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muF)
                SR15_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muF)
                SR15_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muF)
                SR15_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ15_QCD_muF)

                SR15_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muRmuF)
                SR15_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muRmuF)
                SR15_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ15_QCD_muRmuF)
                SR15_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ15_QCD_muRmuF)

		SR15_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ15_PDF)
		SR15_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ15_PDF)
		SR15_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ15_PDF)
		SR15_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ15_PDF)

		SR15_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR15_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR15_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR15_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR15_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR15_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ15)
		SR15_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR16_WZ= bkt.addChannel("cuts",["SR16_WZ"],srNBins,srBinLow,srBinHigh)
	#SR16_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR16_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muR)
                SR16_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muR)
                SR16_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muR)
                SR16_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ16_QCD_muR)

                SR16_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muF)
                SR16_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muF)
                SR16_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muF)
                SR16_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ16_QCD_muF)

                SR16_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muRmuF)
                SR16_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muRmuF)
                SR16_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ16_QCD_muRmuF)
                SR16_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ16_QCD_muRmuF)
	
		SR16_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ16_PDF)
		SR16_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ16_PDF)
		SR16_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ16_PDF)
		SR16_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ16_PDF)

		SR16_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR16_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR16_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR16_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR16_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR16_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ16)
		SR16_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR17_WZ= bkt.addChannel("cuts",["SR17_WZ"],srNBins,srBinLow,srBinHigh)
	#SR17_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR17_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muR)
                SR17_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muR)
                SR17_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muR)
                SR17_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ17_QCD_muR)

                SR17_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muF)
                SR17_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muF)
                SR17_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muF)
                SR17_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ17_QCD_muF)

                SR17_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muRmuF)
                SR17_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muRmuF)
                SR17_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ17_QCD_muRmuF)
                SR17_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ17_QCD_muRmuF)

		SR17_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ17_PDF)
		SR17_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ17_PDF)
		SR17_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ17_PDF)
		SR17_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ17_PDF)

		SR17_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR17_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR17_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR17_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR17_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR17_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ17)
		SR17_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR18_WZ= bkt.addChannel("cuts",["SR18_WZ"],srNBins,srBinLow,srBinHigh)
	#SR18_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR18_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muR)
                SR18_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muR)
                SR18_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muR)
                SR18_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ18_QCD_muR)

                SR18_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muF)
                SR18_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muF)
                SR18_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muF)
                SR18_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ18_QCD_muF)

                SR18_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muRmuF)
                SR18_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muRmuF)
                SR18_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ18_QCD_muRmuF)
                SR18_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ18_QCD_muRmuF)

		SR18_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ18_PDF)
		SR18_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ18_PDF)
		SR18_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ18_PDF)
		SR18_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ18_PDF)

		SR18_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR18_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR18_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR18_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)	

		SR18_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR18_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ18)
		SR18_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR19_WZ= bkt.addChannel("cuts",["SR19_WZ"],srNBins,srBinLow,srBinHigh)
	#SR19_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR19_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muR)
                SR19_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muR)
                SR19_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muR)
                SR19_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ19_QCD_muR)

                SR19_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muF)
                SR19_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muF)
                SR19_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muF)
                SR19_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ19_QCD_muF)

                SR19_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muRmuF)
                SR19_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muRmuF)
                SR19_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ19_QCD_muRmuF)
                SR19_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ19_QCD_muRmuF)

		SR19_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ19_PDF)
		SR19_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ19_PDF)
		SR19_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ19_PDF)
		SR19_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ19_PDF)
		
		SR19_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR19_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR19_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR19_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR19_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR19_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ19)
		SR19_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR20_WZ= bkt.addChannel("cuts",["SR20_WZ"],srNBins,srBinLow,srBinHigh)
	#SR19_WZ.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR20_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muR)
                SR20_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muR)
                SR20_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muR)
                SR20_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ20_QCD_muR)

                SR20_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muF)
                SR20_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muF)
                SR20_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muF)
                SR20_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ20_QCD_muF)

                SR20_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muRmuF)
                SR20_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muRmuF)
                SR20_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ20_QCD_muRmuF)
                SR20_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ20_QCD_muRmuF)

		SR20_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWZ20_PDF)
		SR20_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWZ20_PDF)
		SR20_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWZ20_PDF)
		SR20_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWZ20_PDF)

		SR20_WZ.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR20_WZ.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR20_WZ.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
		SR20_WZ.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

		SR20_WZ.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

		SR20_WZ.getSample("Fakes").addSystematic(syst_FFstat_SRWZ20)
		SR20_WZ.getSample("Fakes").addSystematic(syst_FakeClosure_SR)	


	bkt.setSignalChannels([
                                SR1_WZ,
                                SR2_WZ,
                                SR3_WZ,
                                SR4_WZ,
                                SR5_WZ,
                                SR6_WZ,
                                SR7_WZ,
                                SR8_WZ,
                                SR9_WZ,
                                SR10_WZ,
                                SR11_WZ,
                                SR12_WZ,
                                SR13_WZ,
                                SR14_WZ,
                                SR15_WZ,
                                SR16_WZ,
                                SR17_WZ,
                                SR18_WZ,
                                SR19_WZ,
                                SR20_WZ,                              
                                ])

if "offshell" in whichSR:	

	SR1_Wh= bkt.addChannel("cuts",["SR1_Wh"],srNBins,srBinLow,srBinHigh)
	#SR1_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR1_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh1_QCD_muR)
		SR1_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh1_QCD_muR)
		SR1_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh1_QCD_muR)
		SR1_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh1_QCD_muR)

		SR1_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh1_QCD_muF)
                SR1_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh1_QCD_muF)
                SR1_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh1_QCD_muF)
                SR1_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh1_QCD_muF)

		SR1_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh1_QCD_muRmuF)
                SR1_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh1_QCD_muRmuF)
                SR1_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh1_QCD_muRmuF)
                SR1_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh1_QCD_muRmuF)

		SR1_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh1_PDF)
		SR1_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh1_PDF)
		SR1_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh1_PDF)
		SR1_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh1_PDF)
		
		SR1_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR1_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR1_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR1_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR1_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
		SR1_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh1_PDF)

		SR1_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh1)
		SR1_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR2_Wh= bkt.addChannel("cuts",["SR2_Wh"],srNBins,srBinLow,srBinHigh)
	#SR2_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR2_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh2_QCD_muR)
		SR2_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh2_QCD_muR)
		SR2_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh2_QCD_muR)
		SR2_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh2_QCD_muR)

		SR2_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh2_QCD_muF)
                SR2_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh2_QCD_muF)
                SR2_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh2_QCD_muF)
                SR2_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh2_QCD_muF)

		SR2_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh2_QCD_muRmuF)
                SR2_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh2_QCD_muRmuF)
                SR2_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh2_QCD_muRmuF)
                SR2_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh2_QCD_muRmuF)

		SR2_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh2_PDF)
		SR2_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh2_PDF)
		SR2_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh2_PDF)
		SR2_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh2_PDF)
		
		SR2_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR2_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR2_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR2_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR2_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR2_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh2_PDF)

		SR2_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh2)
		SR2_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR3_Wh= bkt.addChannel("cuts",["SR3_Wh"],srNBins,srBinLow,srBinHigh)
	#SR3_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR3_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh3_QCD_muR)
		SR3_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh3_QCD_muR)
		SR3_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh3_QCD_muR)
		SR3_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh3_QCD_muR)

		SR3_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh3_QCD_muF)
                SR3_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh3_QCD_muF)
                SR3_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh3_QCD_muF)
                SR3_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh3_QCD_muF)

		SR3_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh3_QCD_muRmuF)
                SR3_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh3_QCD_muRmuF)
                SR3_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh3_QCD_muRmuF)
                SR3_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh3_QCD_muRmuF)

		SR3_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh3_PDF)
		SR3_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh3_PDF)
		SR3_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh3_PDF)
		SR3_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh3_PDF)
		
		SR3_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR3_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR3_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR3_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR3_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR3_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh3_PDF)

		SR3_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh3)
		SR3_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)
		
	SR4_Wh= bkt.addChannel("cuts",["SR4_Wh"],srNBins,srBinLow,srBinHigh)
	#SR4_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR4_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh4_QCD_muR)
		SR4_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh4_QCD_muR)
		SR4_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh4_QCD_muR)
		SR4_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh4_QCD_muR)

		SR4_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh4_QCD_muF)
                SR4_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh4_QCD_muF)
                SR4_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh4_QCD_muF)
                SR4_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh4_QCD_muF)

		SR4_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh4_QCD_muRmuF)
                SR4_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh4_QCD_muRmuF)
                SR4_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh4_QCD_muRmuF)
                SR4_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh4_QCD_muRmuF)

		SR4_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh4_PDF)
		SR4_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh4_PDF)
		SR4_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh4_PDF)
		SR4_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh4_PDF)
		
		SR4_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR4_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR4_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR4_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR4_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR4_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh4_PDF)

		SR4_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh4)
		SR4_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR5_Wh= bkt.addChannel("cuts",["SR5_Wh"],srNBins,srBinLow,srBinHigh)
	#SR5_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR5_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh5_QCD_muR)
		SR5_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh5_QCD_muR)
		SR5_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh5_QCD_muR)
		SR5_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh5_QCD_muR)

		SR5_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh5_QCD_muF)
                SR5_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh5_QCD_muF)
                SR5_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh5_QCD_muF)
                SR5_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh5_QCD_muF)

		SR5_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh5_QCD_muRmuF)
                SR5_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh5_QCD_muRmuF)
                SR5_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh5_QCD_muRmuF)
                SR5_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh5_QCD_muRmuF)

		SR5_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh5_PDF)
		SR5_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh5_PDF)
		SR5_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh5_PDF)
		SR5_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh5_PDF)

		SR5_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR5_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR5_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR5_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR5_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR5_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh5_PDF)

		SR5_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh5)
		SR5_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR6_Wh= bkt.addChannel("cuts",["SR6_Wh"],srNBins,srBinLow,srBinHigh)
	#SR6_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR6_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh6_QCD_muR)
		SR6_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh6_QCD_muR)
		SR6_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh6_QCD_muR)
		SR6_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh6_QCD_muR)

		SR6_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh6_QCD_muF)
                SR6_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh6_QCD_muF)
                SR6_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh6_QCD_muF)
                SR6_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh6_QCD_muF)

		SR6_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh6_QCD_muRmuF)
                SR6_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh6_QCD_muRmuF)
                SR6_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh6_QCD_muRmuF)
                SR6_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh6_QCD_muRmuF)

		SR6_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh6_PDF)
		SR6_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh6_PDF)
		SR6_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh6_PDF)
		SR6_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh6_PDF)
		
		SR6_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR6_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR6_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR6_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR6_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR6_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh6_PDF)

		SR6_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh6)
		SR6_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR7_Wh= bkt.addChannel("cuts",["SR7_Wh"],srNBins,srBinLow,srBinHigh)
	#SR7_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR7_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh7_QCD_muR)
		SR7_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh7_QCD_muR)
		SR7_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh7_QCD_muR)
		SR7_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh7_QCD_muR)

		SR7_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh7_QCD_muF)
                SR7_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh7_QCD_muF)
                SR7_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh7_QCD_muF)
                SR7_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh7_QCD_muF)

		SR7_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh7_QCD_muRmuF)
                SR7_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh7_QCD_muRmuF)
                SR7_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh7_QCD_muRmuF)
                SR7_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh7_QCD_muRmuF)

		SR7_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh7_PDF)
		SR7_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh7_PDF)
		SR7_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh7_PDF)
		SR7_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh7_PDF)
		
		SR7_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR7_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR7_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
		SR7_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

		SR7_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR7_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh7_PDF)

		SR7_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh7)
		SR7_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR8_Wh= bkt.addChannel("cuts",["SR8_Wh"],srNBins,srBinLow,srBinHigh)
	#SR8_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR8_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh8_QCD_muR)
		SR8_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh8_QCD_muR)
		SR8_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh8_QCD_muR)
		SR8_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh8_QCD_muR)

		SR8_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh8_QCD_muF)
                SR8_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh8_QCD_muF)
                SR8_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh8_QCD_muF)
                SR8_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh8_QCD_muF)

		SR8_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh8_QCD_muRmuF)
                SR8_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh8_QCD_muRmuF)
                SR8_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh8_QCD_muRmuF)
                SR8_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh8_QCD_muRmuF)

		SR8_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh8_PDF)
		SR8_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh8_PDF)
		SR8_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh8_PDF)
		SR8_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh8_PDF)
		
		SR8_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR8_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR8_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR8_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR8_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh8_ME)
		SR8_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh8_PS)
		SR8_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh8_ISR)
		SR8_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh8_FSR)
                SR8_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh8_PDF)

		SR8_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh8)
		SR8_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR9_Wh= bkt.addChannel("cuts",["SR9_Wh"],srNBins,srBinLow,srBinHigh)
	#SR9_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR9_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh9_QCD_muR)
		SR9_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh9_QCD_muR)
		SR9_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh9_QCD_muR)
		SR9_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh9_QCD_muR)

		SR9_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh9_QCD_muF)
                SR9_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh9_QCD_muF)
                SR9_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh9_QCD_muF)
                SR9_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh9_QCD_muF)

		SR9_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh9_QCD_muRmuF)
                SR9_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh9_QCD_muRmuF)
                SR9_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh9_QCD_muRmuF)
                SR9_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh9_QCD_muRmuF)

		SR9_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh9_PDF)
		SR9_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh9_PDF)
		SR9_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh9_PDF)
		SR9_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh9_PDF)
		
		SR9_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR9_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR9_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR9_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR9_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh9_ME)
                SR9_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh9_PS)
                SR9_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh9_ISR)
                SR9_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh9_FSR)
                SR9_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh9_PDF)

		SR9_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh9)
		SR9_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR10_Wh= bkt.addChannel("cuts",["SR10_Wh"],srNBins,srBinLow,srBinHigh)
	#SR10_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR10_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh10_QCD_muR)
		SR10_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh10_QCD_muR)
		SR10_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh10_QCD_muR)
		SR10_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh10_QCD_muR)

		SR10_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh10_QCD_muF)
                SR10_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh10_QCD_muF)
                SR10_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh10_QCD_muF)
                SR10_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh10_QCD_muF)

		SR10_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh10_QCD_muRmuF)
                SR10_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh10_QCD_muRmuF)
                SR10_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh10_QCD_muRmuF)
                SR10_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh10_QCD_muRmuF)

		SR10_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh10_PDF)
		SR10_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh10_PDF)
		SR10_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh10_PDF)
		SR10_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh10_PDF)
		
		SR10_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR10_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR10_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR10_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR10_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh10_ME)
                SR10_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh10_PS)
                SR10_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh10_ISR)
                SR10_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh10_FSR)
                SR10_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh10_PDF)

		SR10_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh10)
		SR10_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR11_Wh= bkt.addChannel("cuts",["SR11_Wh"],srNBins,srBinLow,srBinHigh)
	#SR11_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR11_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh11_QCD_muR)
		SR11_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh11_QCD_muR)
		SR11_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh11_QCD_muR)
		SR11_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh11_QCD_muR)

		SR11_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh11_QCD_muF)
                SR11_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh11_QCD_muF)
                SR11_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh11_QCD_muF)
                SR11_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh11_QCD_muF)

		SR11_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh11_QCD_muRmuF)
                SR11_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh11_QCD_muRmuF)
                SR11_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh11_QCD_muRmuF)
                SR11_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh11_QCD_muRmuF)

		SR11_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh11_PDF)
		SR11_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh11_PDF)
		SR11_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh11_PDF)
		SR11_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh11_PDF)
		
		SR11_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR11_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR11_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR11_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR11_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh11_ME)
                SR11_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh11_PS)
                SR11_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh11_ISR)
                SR11_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh11_FSR)
                SR11_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh11_PDF)

		SR11_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh11)
		SR11_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR12_Wh= bkt.addChannel("cuts",["SR12_Wh"],srNBins,srBinLow,srBinHigh)
	#SR12_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR12_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh12_QCD_muR)
		SR12_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh12_QCD_muR)
		SR12_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh12_QCD_muR)
		SR12_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh12_QCD_muR)

		SR12_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh12_QCD_muF)
                SR12_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh12_QCD_muF)
                SR12_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh12_QCD_muF)
                SR12_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh12_QCD_muF)

		SR12_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh12_QCD_muRmuF)
                SR12_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh12_QCD_muRmuF)
                SR12_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh12_QCD_muRmuF)
                SR12_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh12_QCD_muRmuF)

		SR12_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh12_PDF)
		SR12_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh12_PDF)
		SR12_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh12_PDF)
		SR12_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh12_PDF)
		
		SR12_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR12_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR12_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR12_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR12_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh12_ME)
                SR12_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh12_PS)
                SR12_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh12_ISR)
                SR12_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh12_FSR)
                SR12_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh12_PDF)

		SR12_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh12)
		SR12_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR13_Wh= bkt.addChannel("cuts",["SR13_Wh"],srNBins,srBinLow,srBinHigh)
	#SR13_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR13_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh13_QCD_muR)
		SR13_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh13_QCD_muR)
		SR13_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh13_QCD_muR)
		SR13_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh13_QCD_muR)

		SR13_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh13_QCD_muF)
                SR13_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh13_QCD_muF)
                SR13_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh13_QCD_muF)
                SR13_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh13_QCD_muF)

		SR13_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh13_QCD_muRmuF)
                SR13_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh13_QCD_muRmuF)
                SR13_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh13_QCD_muRmuF)
                SR13_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh13_QCD_muRmuF)

		SR13_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh13_PDF)
		SR13_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh13_PDF)
		SR13_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh13_PDF)
		SR13_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh13_PDF)
		
		SR13_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR13_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR13_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR13_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR13_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh13_ME)
                SR13_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh13_PS)
                SR13_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh13_ISR)
                SR13_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh13_FSR)
                SR13_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh13_PDF)

		SR13_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh13)
		SR13_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR14_Wh= bkt.addChannel("cuts",["SR14_Wh"],srNBins,srBinLow,srBinHigh)
	#SR14_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR14_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh14_QCD_muR)
		SR14_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh14_QCD_muR)
		SR14_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh14_QCD_muR)
		SR14_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh14_QCD_muR)

		SR14_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh14_QCD_muF)
                SR14_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh14_QCD_muF)
                SR14_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh14_QCD_muF)
                SR14_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh14_QCD_muF)

		SR14_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh14_QCD_muRmuF)
                SR14_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh14_QCD_muRmuF)
                SR14_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh14_QCD_muRmuF)
                SR14_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh14_QCD_muRmuF)

		SR14_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh14_PDF)
		SR14_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh14_PDF)
		SR14_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh14_PDF)
		SR14_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh14_PDF)
		
		SR14_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR14_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR14_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR14_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR14_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh14_ME)
                SR14_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh14_PS)
                SR14_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh14_ISR)
                SR14_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh14_FSR)
                SR14_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh14_PDF)

		SR14_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh14)
		SR14_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR15_Wh= bkt.addChannel("cuts",["SR15_Wh"],srNBins,srBinLow,srBinHigh)
	#SR15_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR15_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh15_QCD_muR)
		SR15_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh15_QCD_muR)
		SR15_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh15_QCD_muR)
		SR15_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh15_QCD_muR)

		SR15_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh15_QCD_muF)
                SR15_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh15_QCD_muF)
                SR15_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh15_QCD_muF)
                SR15_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh15_QCD_muF)

		SR15_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh15_QCD_muRmuF)
                SR15_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh15_QCD_muRmuF)
                SR15_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh15_QCD_muRmuF)
                SR15_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh15_QCD_muRmuF)

		SR15_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh15_PDF)
		SR15_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh15_PDF)
		SR15_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh15_PDF)
		SR15_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh15_PDF)
		
		SR15_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR15_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR15_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR15_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR15_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh15_ME)
                SR15_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh15_PS)
                SR15_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh15_ISR)
                SR15_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh15_FSR)
                SR15_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh15_PDF)

		SR15_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh15)
		SR15_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR16_Wh= bkt.addChannel("cuts",["SR16_Wh"],srNBins,srBinLow,srBinHigh)
	#SR16_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR16_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh16_QCD_muR)
		SR16_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh16_QCD_muR)
		SR16_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh16_QCD_muR)
		SR16_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh16_QCD_muR)

		SR16_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh16_QCD_muF)
                SR16_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh16_QCD_muF)
                SR16_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh16_QCD_muF)
                SR16_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh16_QCD_muF)

		SR16_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh16_QCD_muRmuF)
                SR16_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh16_QCD_muRmuF)
                SR16_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh16_QCD_muRmuF)
                SR16_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh16_QCD_muRmuF)

		SR16_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh16_PDF)
		SR16_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh16_PDF)
		SR16_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh16_PDF)
		SR16_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh16_PDF)
		
		SR16_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR16_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR16_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
		SR16_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

		SR16_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh16_ME)
                SR16_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh16_PS)
                SR16_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh16_ISR)
                SR16_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh16_FSR)
                SR16_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh16_PDF)

		SR16_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh16)
		SR16_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR17_Wh= bkt.addChannel("cuts",["SR17_Wh"],srNBins,srBinLow,srBinHigh)
	#SR17_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR17_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh17_QCD_muR)
		SR17_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh17_QCD_muR)
		SR17_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh17_QCD_muR)
		SR17_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh17_QCD_muR)

		SR17_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh17_QCD_muF)
                SR17_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh17_QCD_muF)
                SR17_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh17_QCD_muF)
                SR17_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh17_QCD_muF)

		SR17_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh17_QCD_muRmuF)
                SR17_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh17_QCD_muRmuF)
                SR17_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh17_QCD_muRmuF)
                SR17_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh17_QCD_muRmuF)

		SR17_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh17_PDF)
		SR17_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh17_PDF)
		SR17_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh17_PDF)
		SR17_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh17_PDF)

		SR17_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR17_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR17_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR17_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRHigh0jets_AltSample)

                SR17_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR17_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh17_PDF)

		SR17_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh17)
		SR17_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR18_Wh= bkt.addChannel("cuts",["SR18_Wh"],srNBins,srBinLow,srBinHigh)
	#SR18_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR18_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh18_QCD_muR)
		SR18_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh18_QCD_muR)
		SR18_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh18_QCD_muR)
		SR18_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh18_QCD_muR)

		SR18_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh18_QCD_muF)
                SR18_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh18_QCD_muF)
                SR18_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh18_QCD_muF)
                SR18_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh18_QCD_muF)

		SR18_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh18_QCD_muRmuF)
                SR18_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh18_QCD_muRmuF)
                SR18_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh18_QCD_muRmuF)
                SR18_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh18_QCD_muRmuF)

		SR18_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh18_PDF)
		SR18_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh18_PDF)
		SR18_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh18_PDF)
		SR18_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh18_PDF)
		
		SR18_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR18_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR18_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR18_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRHigh0jets_AltSample)
		
		SR18_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR18_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh18_PDF)

		SR18_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh18)
		SR18_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	SR19_Wh= bkt.addChannel("cuts",["SR19_Wh"],srNBins,srBinLow,srBinHigh)
	#SR19_Wh.blind = True #REALLY REALLY REALLY IMPORTANT
	if not noSysts and doThUncert:
		SR19_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh19_QCD_muR)
		SR19_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh19_QCD_muR)
		SR19_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh19_QCD_muR)
		SR19_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh19_QCD_muR)

		SR19_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh19_QCD_muF)
                SR19_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh19_QCD_muF)
                SR19_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh19_QCD_muF)
                SR19_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh19_QCD_muF)

		SR19_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh19_QCD_muRmuF)
                SR19_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh19_QCD_muRmuF)
                SR19_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh19_QCD_muRmuF)
                SR19_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh19_QCD_muRmuF)

		SR19_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRWh19_PDF)
		SR19_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRWh19_PDF)
		SR19_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRWh19_PDF)
		SR19_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRWh19_PDF)
		
		SR19_Wh.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR19_Wh.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR19_Wh.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRHigh0jets_AltSample)
		SR19_Wh.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRHigh0jets_AltSample)

		SR19_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
                SR19_Wh.getSample("ttbar").addSystematic(syst_Theory_ttbar_SRWh19_PDF)

		SR19_Wh.getSample("Fakes").addSystematic(syst_FFstat_SRWh19)
		SR19_Wh.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

	bkt.setSignalChannels([
                                SR1_Wh,
                                SR2_Wh,
                                SR3_Wh,
                                SR4_Wh,
                                SR5_Wh,
                                SR6_Wh,
                                SR7_Wh,
                                SR8_Wh,
                                SR9_Wh,
                                SR10_Wh,
                                SR11_Wh,
                                SR12_Wh,
                                SR13_Wh,
                                SR14_Wh,
                                SR15_Wh,
                                SR16_Wh,
                                SR17_Wh,
                                SR18_Wh,
                                SR19_Wh
                                ])

bkt.setBkgConstrainChannels([CR_WZ_0jets,CR_WZ_LowHT,CR_WZ_HighHT])

sigSamples=[]

if not fullGrid:
	sigSamples=[whichPoint]
	print' not full grid, selecting one sample'

else:
	sigSamples=[
		'SMAwz13TeV_100_0',
		'SMAwz13TeV_100_10',
		'SMAwz13TeV_100_20',
		'SMAwz13TeV_100_30',
		'SMAwz13TeV_100_40',
		'SMAwz13TeV_100_60',
		'SMAwz13TeV_100_80',
		'SMAwz13TeV_100_90',
		'SMAwz13TeV_110_20',
		'SMAwz13TeV_110_30',
		'SMAwz13TeV_110_40',
		'SMAwz13TeV_110_50',
		'SMAwz13TeV_110_60',
		'SMAwz13TeV_110_70',
		'SMAwz13TeV_110_85',
		'SMAwz13TeV_110_95',
		'SMAwz13TeV_125_100',
		'SMAwz13TeV_125_110',
		'SMAwz13TeV_125_115',
		'SMAwz13TeV_125_35',
		'SMAwz13TeV_125_45',
		'SMAwz13TeV_125_55',
		'SMAwz13TeV_125_65',
		'SMAwz13TeV_125_85',
		'SMAwz13TeV_140_100',
		'SMAwz13TeV_140_115',
		'SMAwz13TeV_140_125',
		'SMAwz13TeV_140_130',
		'SMAwz13TeV_140_50',
		'SMAwz13TeV_140_60',
		'SMAwz13TeV_140_70',
		'SMAwz13TeV_140_80',
		'SMAwz13TeV_150_0',
		'SMAwz13TeV_150_110',
		'SMAwz13TeV_150_130',
		'SMAwz13TeV_150_140',
		'SMAwz13TeV_150_1',
		'SMAwz13TeV_150_50',
		'SMAwz13TeV_150_60',
		'SMAwz13TeV_150_70',
		'SMAwz13TeV_150_80',
		'SMAwz13TeV_150_90',
		'SMAwz13TeV_152_22',
		'SMAwz13TeV_162_12',
		'SMAwz13TeV_165_35',
		'SMAwz13TeV_175_0',
		'SMAwz13TeV_175_25',
		'SMAwz13TeV_177_47',
		'SMAwz13TeV_187_12',
		'SMAwz13TeV_187_37',
		'SMAwz13TeV_190_60',
		'SMAwz13TeV_200_0',
		'SMAwz13TeV_200_100',
		'SMAwz13TeV_200_110',
		'SMAwz13TeV_200_120',
		'SMAwz13TeV_200_130',
		'SMAwz13TeV_200_140',
		'SMAwz13TeV_200_160',
		'SMAwz13TeV_200_180',
		'SMAwz13TeV_200_190',
		'SMAwz13TeV_200_1',
		'SMAwz13TeV_200_25',
		'SMAwz13TeV_200_50',
		'SMAwz13TeV_202_72',
		'SMAwz13TeV_212_37',
		'SMAwz13TeV_212_62',
		'SMAwz13TeV_225_0',
		'SMAwz13TeV_225_25',
		'SMAwz13TeV_225_50',
		'SMAwz13TeV_225_75',
		'SMAwz13TeV_237_62',
		'SMAwz13TeV_250_0',
		'SMAwz13TeV_250_100',
		'SMAwz13TeV_250_150',
		'SMAwz13TeV_250_170',
		'SMAwz13TeV_250_190',
		'SMAwz13TeV_250_1',
		'SMAwz13TeV_250_210',
		'SMAwz13TeV_250_230',
		'SMAwz13TeV_250_240',
		'SMAwz13TeV_250_25',
		'SMAwz13TeV_250_50',
		'SMAwz13TeV_250_75',
		'SMAwz13TeV_275_0',
		'SMAwz13TeV_275_25',
		'SMAwz13TeV_275_50',
		'SMAwz13TeV_275_75',
		'SMAwz13TeV_300_0',
		'SMAwz13TeV_300_100',
		'SMAwz13TeV_300_150',
		'SMAwz13TeV_300_200',
		'SMAwz13TeV_300_220',
		'SMAwz13TeV_300_240',
		'SMAwz13TeV_300_25',
		'SMAwz13TeV_300_260',
		'SMAwz13TeV_300_280',
		'SMAwz13TeV_300_290',
		'SMAwz13TeV_300_50',
		'SMAwz13TeV_300_75',
		'SMAwz13TeV_325_0',
		'SMAwz13TeV_325_50',
		'SMAwz13TeV_350_0',
		'SMAwz13TeV_350_100',
		'SMAwz13TeV_350_150',
		'SMAwz13TeV_350_200',
		'SMAwz13TeV_350_250',
		'SMAwz13TeV_350_25',
		'SMAwz13TeV_350_270',
		'SMAwz13TeV_350_290',
		'SMAwz13TeV_350_310',
		'SMAwz13TeV_350_330',
		'SMAwz13TeV_350_340',
		'SMAwz13TeV_350_50',
		'SMAwz13TeV_350_75',
		'SMAwz13TeV_375_0',
		'SMAwz13TeV_375_50',
		'SMAwz13TeV_400_0',
		'SMAwz13TeV_400_100',
		'SMAwz13TeV_400_200',
		'SMAwz13TeV_400_250',
		'SMAwz13TeV_400_25',
		'SMAwz13TeV_400_300',
		'SMAwz13TeV_400_320',
		'SMAwz13TeV_400_340',
		'SMAwz13TeV_400_360',
		'SMAwz13TeV_400_380',
		'SMAwz13TeV_400_390',
		'SMAwz13TeV_425_0',
		'SMAwz13TeV_450_150',
		'SMAwz13TeV_450_200',
		'SMAwz13TeV_450_250',
		'SMAwz13TeV_450_300',
		'SMAwz13TeV_450_350',
		'SMAwz13TeV_450_50',
		'SMAwz13TeV_500_0',
		'SMAwz13TeV_500_100',
		'SMAwz13TeV_500_150',
		'SMAwz13TeV_500_200',
		'SMAwz13TeV_500_250',
		'SMAwz13TeV_500_300',
		'SMAwz13TeV_500_350',
		'SMAwz13TeV_500_400',
		'SMAwz13TeV_500_50',
		'SMAwz13TeV_550_0',
		'SMAwz13TeV_550_100',
		'SMAwz13TeV_550_150',
		'SMAwz13TeV_550_200',
		'SMAwz13TeV_550_250',
		'SMAwz13TeV_550_300',
		'SMAwz13TeV_550_350',
		'SMAwz13TeV_550_400',
		'SMAwz13TeV_550_50',
		'SMAwz13TeV_600_0',
		'SMAwz13TeV_600_100',
		'SMAwz13TeV_600_150',
		'SMAwz13TeV_600_200',
		'SMAwz13TeV_600_250',
		'SMAwz13TeV_600_300',
		'SMAwz13TeV_600_350',
		'SMAwz13TeV_600_400',
		'SMAwz13TeV_600_50',
		'SMAwz13TeV_650_0',
		'SMAwz13TeV_650_100',
		'SMAwz13TeV_650_150',
		'SMAwz13TeV_650_200',
		'SMAwz13TeV_650_250',
		'SMAwz13TeV_650_300',
		'SMAwz13TeV_650_350',
		'SMAwz13TeV_650_400',
		'SMAwz13TeV_650_50',
		'SMAwz13TeV_700_0',
		'SMAwz13TeV_700_100',
		'SMAwz13TeV_700_150',
		'SMAwz13TeV_700_200',
		'SMAwz13TeV_700_250',
		'SMAwz13TeV_700_300',
		'SMAwz13TeV_700_350',
		'SMAwz13TeV_700_400',
		'SMAwz13TeV_700_50',
		'SMAwz13TeV_750_0',
		'SMAwz13TeV_750_100',
		'SMAwz13TeV_750_150',
		'SMAwz13TeV_750_200',
		'SMAwz13TeV_750_250',
		'SMAwz13TeV_750_350',
		'SMAwz13TeV_750_400',
		'SMAwz13TeV_750_50',
		'SMAwz13TeV_800_0',
		'SMAwz13TeV_800_100',
		'SMAwz13TeV_800_150',
		'SMAwz13TeV_800_200',
		'SMAwz13TeV_800_250',
		'SMAwz13TeV_800_300',
		'SMAwz13TeV_800_350',
		'SMAwz13TeV_800_400',
		'SMAwz13TeV_800_50',
		]

print 'testing signal samples'
print sigSamples

for sig in sigSamples:
	myTopLvl = configMgr.addFitConfigClone(bkt,"%s"%sig)
	sigSample = Sample(sig,kPink)
	sigSample.addInputs(sigFiles_a,sig+"_a")
	sigSample.addInputs(sigFiles_d,sig+"_d")
	sigSample.addInputs(sigFiles_e,sig+"_e")
	sigSample.setNormByTheory()
        sigSample.setNormFactor('mu_SIG',1.,0.,5.)
        sigSample.setStatConfig(useStat)

	if not noSysts:
		sigSample.addSystematic(syst_XSec_Signal_flat)
                sigSample.addSystematic(syst_Theory_Signal_flat)
        	for syst in sig_systList:
                	sigSample.addSystematic(syst)
	
	myTopLvl.addSamples(sigSample)
	myTopLvl.setSignalSample(sigSample)

	if "AllSRs" in whichSR:
        	myTopLvl.setSignalChannels([
					SR1_WZ,
                                        SR2_WZ,
                                        SR3_WZ,
                                        SR4_WZ,
                                        SR5_WZ,
                                        SR6_WZ,
                                        SR7_WZ,
                                        SR8_WZ,
                                        SR9_WZ,
                                        SR10_WZ,
                                        SR11_WZ,
                                        SR12_WZ,
                                        SR13_WZ,
                                        SR14_WZ,
                                        SR15_WZ,
                                        SR16_WZ,
                                        SR17_WZ,
                                        SR18_WZ,
                                        SR19_WZ,
                                        SR20_WZ,
                                        SR1_Wh,
                                        SR2_Wh,
                                        SR3_Wh,
                                        SR4_Wh,
                                        SR5_Wh,
                                        SR6_Wh,
                                        SR7_Wh,
                                        SR8_Wh,
                                        SR9_Wh,
                                        SR10_Wh,
                                        SR11_Wh,
                                        SR12_Wh,
                                        SR13_Wh,
                                        SR14_Wh,
                                        SR15_Wh,
                                        SR16_Wh,
                                        SR17_Wh,
                                        SR18_Wh,
                                        SR19_Wh 
                                	])

	if "onshell" in whichSR:
        	myTopLvl.setSignalChannels([
					SR1_WZ,
                                        SR2_WZ,
                                        SR3_WZ,
                                        SR4_WZ,
                                        SR5_WZ,
                                        SR6_WZ,
                                        SR7_WZ,
                                        SR8_WZ,
                                        SR9_WZ,
                                        SR10_WZ,
                                        SR11_WZ,
                                        SR12_WZ,
                                        SR13_WZ,
                                        SR14_WZ,
                                        SR15_WZ,
                                        SR16_WZ,
                                        SR17_WZ,
                                        SR18_WZ,
                                        SR19_WZ,
                                        SR20_WZ,
                                	])

	if "offshell" in whichSR:
        	myTopLvl.setSignalChannels([
                                        SR1_Wh,
                                        SR2_Wh,
                                        SR3_Wh,
                                        SR4_Wh,
                                        SR5_Wh,
                                        SR6_Wh,
                                        SR7_Wh,
                                        SR8_Wh,
                                        SR9_Wh,
                                        SR10_Wh,
                                        SR11_Wh,
                                        SR12_Wh,
                                        SR13_Wh,
                                        SR14_Wh,
                                        SR15_Wh,
                                        SR16_Wh,
                                        SR17_Wh,
                                        SR18_Wh,
                                        SR19_Wh 
                                	])


# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory

if configMgr.executeHistFactory:
	if os.path.isfile("data/%s.root"%configMgr.analysisName):
		os.remove("data/%s.root"%configMgr.analysisName)

