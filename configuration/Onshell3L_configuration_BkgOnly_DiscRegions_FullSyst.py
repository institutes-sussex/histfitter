from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,TCanvas,TLegend,TLegendEntry
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from optparse import OptionParser
from copy import deepcopy

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
import ROOT
GeV = 1000.;

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    newList.append(newWeight)
    return newList

#-------------------------------
# Parse command line
#-------------------------------
myUserArgs= configMgr.userArg.split(' ')
myInputParser = OptionParser()
myInputParser.add_option('', '--point', dest = 'point', default = '')
myInputParser.add_option('', '--signalUncert', dest = 'signalUncert', default = 'Nom')
myInputParser.add_option('', '--SR', dest = 'SR', default = 'AllSR')
myInputParser.add_option('', '--doFullGrid', dest = 'doFullGrid', default = 'True')
myInputParser.add_option('', '--useSysts', dest = 'useSysts', default = 'False')
myInputParser.add_option('', '--addFakes', dest = 'addFakes', default = 'True')
myInputParser.add_option('', '--doThUncert', dest = 'doThUncert', default = 'True')
myInputParser.add_option('', '--doFakeUncert', dest = 'doFakeUncert', default = 'True')
myInputParser.add_option('', '--ShapeFit', dest = 'ShapeFit', default = 'False')
myInputParser.add_option('', '--makePlots', dest = 'makePlots', default = 'False')
myInputParser.add_option('', '--runToys', dest = 'runToys', default = 'False')
myInputParser.add_option('', '--toyIndex', dest = 'toyIndex', default = '3')
myInputParser.add_option('', '--Name',dest = 'Name',default='EWK_Wh3L')

(options, args) = myInputParser.parse_args(myUserArgs)
whichPoint = options.point
whichSR = options.SR
signalXSec=options.signalUncert
name = options.Name
toyindex= options.toyIndex

#---------------------------------------
# Flags to control options
#---------------------------------------

fullGrid=False
if options.doFullGrid== "True" or options.doFullGrid == "true":
	fullGrid=True
doToys=False
if options.runToys== "True" or options.runToys == "true":
	doToys=True
useFakes=False
if options.addFakes== "True" or options.addFakes == "true":
	useFakes=True
doShapeFit = False
if options.ShapeFit == "True" or options.ShapeFit == "true":
	doShapeFit = True
doThUncert=False
if options.doThUncert == "True" or options.doThUncert == "true":
	doThUncert=True
doFakeUncert=False
if options.doFakeUncert == "True" or options.doFakeUncert == "true":
        doFakeUncert=True
noSysts=True
if options.useSysts== "True" or options.useSysts == "true":
        print "disabling systematics"
        noSysts=False

useStat=True
testSignalYield=False

print "########## Running Bkg Only fit #############"


#-------------------------------
# Parameters for hypothesis test
#-------------------------------

# have been running with this commented out and set to false
#configMgr.doHypoTest=True
configMgr.nTOYs=2000
if not doToys:
	configMgr.calculatorType=2 # 0=toys, 2=asymptotic calc.
else:
	configMgr.calculatorType=0 # 0=toys, 2=asymptotic calc.

configMgr.testStatType=3
configMgr.nPoints=20
#configMgr.scanRange = (1., 4.)
#configMgr.drawBeforeAfterFit = False

configMgr.blindSR = True #CAUTION: SRs in background only fit are seen as VRs so for BkgOnly they are unblinded! FIXME need to unblind them ONE BY ONE
configMgr.blindCR = False
configMgr.blindVR = False 

#--------------------------------
# Now we start to build the model
#--------------------------------

# First define HistFactory attributes
if not noSysts:
	name = name + "_syst"

if not doToys:
	configMgr.analysisName = name
else:
	configMgr.analysisName = name + "_"+ whichPoint+"_withToys_"+toyindex

configMgr.inputLumi = 1.	# Luminosity of input TTree
configMgr.outputLumi = 1.  # Luminosity required for output histograms

configMgr.setLumiUnits("fb-1")
configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"
configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

configMgr.ReduceCorrMatrix=True #CorrMatrix to be given in a reduced version
# Set the files to read from
bgdFiles_a = []
bgdFiles_d = []
bgdFiles_e = []
dataFiles = []
fakeFiles = []
sigFiles = []

ntupDir = ""

#Systematics pruning
configMgr.prun = True
configMgr.prunThreshold = 0.01 #Set at 1%

# Recycle histograms --------------------------- FIXME for first iteration we are gonna reproduce all histograms from scratch but will keep the lines here for later

#configMgr.useCacheToTreeFallback = True # enable the fallback to trees
#configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
#configMgr.histBackupCacheFile = "/lustre/scratch/epp/atlas/ft81/workdir/Limits/HF61/data/backup.root"

# Input files ----------------------------------------------------------------------------------------------------

if configMgr.readFromTree or configMgr.useCacheToTreeFallback:
	bgdFiles_a.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_mcaJES/L3_BG13TeV_a.root")
	bgdFiles_d.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_mcdJES/L3_BG13TeV_d.root")
	bgdFiles_e.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_mceJES/L3_BG13TeV_e.root")
	dataFiles.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_datav91/L_DATA13TeV.root")
	fakeFiles.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_fakes/L3_FakesFF13TeV.root")
			
# Regions ----------------------------------------------------------------------------------------------------

print "Defying 3L regions"

# common preselection for ALL the regions
configMgr.cutsDict["baseThreeLep"]= "cleaning && n_comblep==3 && LepPt1>20000. && LepPt0>25000. && pass3l && matchtrigger" #trig matching

#####################
# Discovery Regions #
#####################

configMgr.cutsDict["SR1_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>100000 && Mt<160000 && MET>100000 && MET<200000)"
configMgr.cutsDict["SR2_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>100000 && Mt<160000 && MET>200000)"
configMgr.cutsDict["SR3_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>75000. && mSFOS<105000. && njets==0 && Mt>160000 && MET>200000)"
configMgr.cutsDict["SR4_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>75000. && mSFOS<105000. && njets>0 && Mt>100000 && Mt<160000 && MET>150000 && MET<250000)"
configMgr.cutsDict["SR5_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>75000. && mSFOS<105000. && njets>0 && Mt>100000 && Mt<160000 && MET>250000)"
configMgr.cutsDict["SR6_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>75000. && mSFOS<105000. && njets>0 && Mt>160000 && MET>200000)"
configMgr.cutsDict["SR7_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt<100000 && MET>50000)"
configMgr.cutsDict["SR8_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt>100000 && Mt<160000 && MET>50000)"
configMgr.cutsDict["SR9_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000. && mSFOS>12000 && mSFOS<75000. && njets==0 && Mt>160000 && MET>50000)"
configMgr.cutsDict["SR10_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>100000 && Mt<160000 && MET>75000)"
configMgr.cutsDict["SR11_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000. && fabs(Mlll-91200)>15000 && mSFOS>12000 && mSFOS<75000. && njets>0 && HT<200000 && Mt>160000 && MET>75000)"
configMgr.cutsDict["SR12_Disc"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passDF && nbjets==0 && MET>50000. && METSig>8 && LepPt2>20000 && deltaRmin<1.2 && njets<3)"

configMgr.cutsDict["WZ_CR_0jets"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt<100000 && Mt>20000 && MET<100000 && njets==0)"
configMgr.cutsDict["WZ_CR_LowHT"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt<100000 && Mt>20000 && MET<100000 && njets>0 && HT<200000)"
configMgr.cutsDict["WZ_CR_HighHT"]= "("+str( configMgr.cutsDict["baseThreeLep"])+"&& passSFOS && nbjets==0 && MET>50000 && fabs(Mlll-91200)>15000 && mSFOS>75000 && mSFOS<105000 && Mt<100000 && Mt>20000 && MET<100000 && njets>0 && HT>200000)"

# Weights ----------------------------------------------------------------------------------------------------
if 'Nom' in signalXSec:
	weights = ["bTagWeight","pileupWeight","JVTSF", "ElecSF","MuonSF","EvtWeight","NormWeight"]
	configMgr.weights = weights		
elif 'up' in signalXSec:
	configMgr.weights =["eventweightUp"]
elif 'down' in signalXSec:
	configMgr.weights =["eventweightDown"]
configMgr.nomName = "_CENTRAL"

# Systematics ----------------------------------------------------------------------------------------------------
#FIXME no systematics yet for now. We will add them later on, leaving a couple of examples below for future references

####===> TREE BASED
#jets
#syst_jes_1_MC = Systematic("syst_jes_1","_CENTRAL","_JET_GroupedNP_1_UP","_JET_GroupedNP_1_DN","tree","histoSys")
#syst_jes_2_MC = Systematic("syst_jes_2","_CENTRAL","_JET_GroupedNP_2_UP","_JET_GroupedNP_2_DN","tree","histoSys")
#syst_jes_3_MC = Systematic("syst_jes_3","_CENTRAL","_JET_GroupedNP_3_UP","_JET_GroupedNP_3_DN","tree","histoSys")

#syst_jes_1_norm = Systematic("syst_jes_1","_CENTRAL","_JET_GroupedNP_1_UP","_JET_GroupedNP_1_DN","tree","normHistoSys")
#syst_jes_2_norm = Systematic("syst_jes_2","_CENTRAL","_JET_GroupedNP_2_UP","_JET_GroupedNP_2_DN","tree","normHistoSys")
#syst_jes_3_norm = Systematic("syst_jes_3","_CENTRAL","_JET_GroupedNP_3_UP","_JET_GroupedNP_3_DN","tree","normHistoSys")


syst_jes_1_MC = Systematic("syst_Bjes_resp","_CENTRAL","_JET_BJES_Response_UP","_JET_BJES_Response_DN","tree","histoSys")
syst_jes_2_MC = Systematic("syst_jes_det1","_CENTRAL","_JET_EffectiveNP_Detector1_UP","_JET_EffectiveNP_Detector1_DN","tree","histoSys")
syst_jes_3_MC = Systematic("syst_jes_det2","_CENTRAL","_JET_EffectiveNP_Detector2_UP","_JET_EffectiveNP_Detector2_DN","tree","histoSys")
syst_jes_4_MC = Systematic("syst_jes_mix1","_CENTRAL","_JET_EffectiveNP_Mixed1_UP","_JET_EffectiveNP_Mixed1_DN","tree","histoSys")
syst_jes_5_MC = Systematic("syst_jes_mix2","_CENTRAL","_JET_EffectiveNP_Mixed2_UP","_JET_EffectiveNP_Mixed2_DN","tree","histoSys")
syst_jes_6_MC = Systematic("syst_jes_mix3","_CENTRAL","_JET_EffectiveNP_Mixed3_UP","_JET_EffectiveNP_Mixed3_DN","tree","histoSys")
syst_jes_7_MC = Systematic("syst_jes_mod1","_CENTRAL","_JET_EffectiveNP_Modelling1_UP","_JET_EffectiveNP_Modelling1_DN","tree","histoSys")
syst_jes_8_MC = Systematic("syst_jes_mod2","_CENTRAL","_JET_EffectiveNP_Modelling2_UP","_JET_EffectiveNP_Modelling2_DN","tree","histoSys")
syst_jes_9_MC = Systematic("syst_jes_mod3","_CENTRAL","_JET_EffectiveNP_Modelling3_UP","_JET_EffectiveNP_Modelling3_DN","tree","histoSys")
syst_jes_10_MC = Systematic("syst_jes_mod4","_CENTRAL","_JET_EffectiveNP_Modelling4_UP","_JET_EffectiveNP_Modelling4_DN","tree","histoSys")
syst_jes_11_MC = Systematic("syst_jes_stat1","_CENTRAL","_JET_EffectiveNP_Statistical1_UP","_JET_EffectiveNP_Statistical1_DN","tree","histoSys")
syst_jes_12_MC = Systematic("syst_jes_stat2","_CENTRAL","_JET_EffectiveNP_Statistical2_UP","_JET_EffectiveNP_Statistical2_DN","tree","histoSys")
syst_jes_13_MC = Systematic("syst_jes_stat3","_CENTRAL","_JET_EffectiveNP_Statistical3_UP","_JET_EffectiveNP_Statistical3_DN","tree","histoSys")
syst_jes_14_MC = Systematic("syst_jes_stat4","_CENTRAL","_JET_EffectiveNP_Statistical4_UP","_JET_EffectiveNP_Statistical4_DN","tree","histoSys")
syst_jes_15_MC = Systematic("syst_etaInter_mod","_CENTRAL","_JET_EtaIntercalibration_Modelling_UP","_JET_EtaIntercalibration_Modelling_DN","tree","histoSys")
syst_jes_16_MC = Systematic("syst_etaInter_NC2018","_CENTRAL","_JET_EtaIntercalibration_NonClosure_2018data_UP","_JET_EtaIntercalibration_NonClosure_2018data_DN","tree","histoSys")
syst_jes_17_MC = Systematic("syst_etaInter_NChighE","_CENTRAL","_JET_EtaIntercalibration_NonClosure_highE_UP","_JET_EtaIntercalibration_NonClosure_highE_DN","tree","histoSys")
syst_jes_18_MC = Systematic("syst_etaInter_negEta","_CENTRAL","_JET_EtaIntercalibration_NonClosure_negEta_UP","_JET_EtaIntercalibration_NonClosure_negEta_DN","tree","histoSys")
syst_jes_19_MC = Systematic("syst_etaInter_posEta","_CENTRAL","_JET_EtaIntercalibration_NonClosure_posEta_UP","_JET_EtaIntercalibration_NonClosure_posEta_DN","tree","histoSys")
syst_jes_20_MC = Systematic("syst_etaInter_stat","_CENTRAL","_JET_EtaIntercalibration_TotalStat_UP","_JET_EtaIntercalibration_TotalStat_DN","tree","histoSys")
syst_jes_21_MC = Systematic("syst_flav_comp","_CENTRAL","_JET_Flavor_Composition_UP","_JET_Flavor_Composition_DN","tree","histoSys")
syst_jes_22_MC = Systematic("syst_flav_resp","_CENTRAL","_JET_Flavor_Response_UP","_JET_Flavor_Response_DN","tree","histoSys")
syst_jes_23_MC = Systematic("syst_PU_offsetMu","_CENTRAL","_JET_Pileup_OffsetMu_UP","_JET_Pileup_OffsetMu_DN","tree","histoSys")
syst_jes_24_MC = Systematic("syst_PU_offsetNPV","_CENTRAL","_JET_Pileup_OffsetNPV_UP","_JET_Pileup_OffsetNPV_DN","tree","histoSys")
syst_jes_25_MC = Systematic("syst_PU_pt","_CENTRAL","_JET_Pileup_PtTerm_UP","_JET_Pileup_PtTerm_DN","tree","histoSys")
syst_jes_26_MC = Systematic("syst_PU_rho","_CENTRAL","_JET_Pileup_RhoTopology_UP","_JET_Pileup_RhoTopology_DN","tree","histoSys")
syst_jes_27_MC = Systematic("syst_singleP_pt","_CENTRAL","_JET_SingleParticle_HighPt_UP","_JET_SingleParticle_HighPt_DN","tree","histoSys")

syst_jes_1_MC_norm = Systematic("syst_Bjes_resp","_CENTRAL","_JET_BJES_Response_UP","_JET_BJES_Response_DN","tree","normHistoSys")
syst_jes_2_MC_norm = Systematic("syst_jes_det1","_CENTRAL","_JET_EffectiveNP_Detector1_UP","_JET_EffectiveNP_Detector1_DN","tree","normHistoSys")
syst_jes_3_MC_norm = Systematic("syst_jes_det2","_CENTRAL","_JET_EffectiveNP_Detector2_UP","_JET_EffectiveNP_Detector2_DN","tree","normHistoSys")
syst_jes_4_MC_norm = Systematic("syst_jes_mix1","_CENTRAL","_JET_EffectiveNP_Mixed1_UP","_JET_EffectiveNP_Mixed1_DN","tree","normHistoSys")
syst_jes_5_MC_norm = Systematic("syst_jes_mix2","_CENTRAL","_JET_EffectiveNP_Mixed2_UP","_JET_EffectiveNP_Mixed2_DN","tree","normHistoSys")
syst_jes_6_MC_norm = Systematic("syst_jes_mix3","_CENTRAL","_JET_EffectiveNP_Mixed3_UP","_JET_EffectiveNP_Mixed3_DN","tree","normHistoSys")
syst_jes_7_MC_norm = Systematic("syst_jes_mod1","_CENTRAL","_JET_EffectiveNP_Modelling1_UP","_JET_EffectiveNP_Modelling1_DN","tree","normHistoSys")
syst_jes_8_MC_norm = Systematic("syst_jes_mod2","_CENTRAL","_JET_EffectiveNP_Modelling2_UP","_JET_EffectiveNP_Modelling2_DN","tree","normHistoSys")
syst_jes_9_MC_norm = Systematic("syst_jes_mod3","_CENTRAL","_JET_EffectiveNP_Modelling3_UP","_JET_EffectiveNP_Modelling3_DN","tree","normHistoSys")
syst_jes_10_MC_norm = Systematic("syst_jes_mod4","_CENTRAL","_JET_EffectiveNP_Modelling4_UP","_JET_EffectiveNP_Modelling4_DN","tree","normHistoSys")
syst_jes_11_MC_norm = Systematic("syst_jes_stat1","_CENTRAL","_JET_EffectiveNP_Statistical1_UP","_JET_EffectiveNP_Statistical1_DN","tree","normHistoSys")
syst_jes_12_MC_norm = Systematic("syst_jes_stat2","_CENTRAL","_JET_EffectiveNP_Statistical2_UP","_JET_EffectiveNP_Statistical2_DN","tree","normHistoSys")
syst_jes_13_MC_norm = Systematic("syst_jes_stat3","_CENTRAL","_JET_EffectiveNP_Statistical3_UP","_JET_EffectiveNP_Statistical3_DN","tree","normHistoSys")
syst_jes_14_MC_norm = Systematic("syst_jes_stat4","_CENTRAL","_JET_EffectiveNP_Statistical4_UP","_JET_EffectiveNP_Statistical4_DN","tree","normHistoSys")
syst_jes_15_MC_norm = Systematic("syst_etaInter_mod","_CENTRAL","_JET_EtaIntercalibration_Modelling_UP","_JET_EtaIntercalibration_Modelling_DN","tree","normHistoSys")
syst_jes_16_MC_norm = Systematic("syst_etaInter_NC2018","_CENTRAL","_JET_EtaIntercalibration_NonClosure_2018data_UP","_JET_EtaIntercalibration_NonClosure_2018data_DN","tree","normHistoSys")
syst_jes_17_MC_norm = Systematic("syst_etaInter_NChighE","_CENTRAL","_JET_EtaIntercalibration_NonClosure_highE_UP","_JET_EtaIntercalibration_NonClosure_highE_DN","tree","normHistoSys")
syst_jes_18_MC_norm = Systematic("syst_etaInter_negEta","_CENTRAL","_JET_EtaIntercalibration_NonClosure_negEta_UP","_JET_EtaIntercalibration_NonClosure_negEta_DN","tree","normHistoSys")
syst_jes_19_MC_norm = Systematic("syst_etaInter_posEta","_CENTRAL","_JET_EtaIntercalibration_NonClosure_posEta_UP","_JET_EtaIntercalibration_NonClosure_posEta_DN","tree","normHistoSys")
syst_jes_20_MC_norm = Systematic("syst_etaInter_stat","_CENTRAL","_JET_EtaIntercalibration_TotalStat_UP","_JET_EtaIntercalibration_TotalStat_DN","tree","normHistoSys")
syst_jes_21_MC_norm = Systematic("syst_flav_comp","_CENTRAL","_JET_Flavor_Composition_UP","_JET_Flavor_Composition_DN","tree","normHistoSys")
syst_jes_22_MC_norm = Systematic("syst_flav_resp","_CENTRAL","_JET_Flavor_Response_UP","_JET_Flavor_Response_DN","tree","normHistoSys")
syst_jes_23_MC_norm = Systematic("syst_PU_offsetMu","_CENTRAL","_JET_Pileup_OffsetMu_UP","_JET_Pileup_OffsetMu_DN","tree","normHistoSys")
syst_jes_24_MC_norm = Systematic("syst_PU_offsetNPV","_CENTRAL","_JET_Pileup_OffsetNPV_UP","_JET_Pileup_OffsetNPV_DN","tree","normHistoSys")
syst_jes_25_MC_norm = Systematic("syst_PU_pt","_CENTRAL","_JET_Pileup_PtTerm_UP","_JET_Pileup_PtTerm_DN","tree","normHistoSys")
syst_jes_26_MC_norm = Systematic("syst_PU_rho","_CENTRAL","_JET_Pileup_RhoTopology_UP","_JET_Pileup_RhoTopology_DN","tree","normHistoSys")
syst_jes_27_MC_norm = Systematic("syst_singleP_pt","_CENTRAL","_JET_SingleParticle_HighPt_UP","_JET_SingleParticle_HighPt_DN","tree","normHistoSys")


syst_jer_dataMC_MC = Systematic("syst_jer_dataMC","_CENTRAL","_JET_JER_DataVsMC_MC16_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_1_MC = Systematic("syst_jer_1","_CENTRAL","_JET_JER_EffectiveNP_1_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_2_MC = Systematic("syst_jer_2","_CENTRAL","_JET_JER_EffectiveNP_2_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_3_MC = Systematic("syst_jer_3","_CENTRAL","_JET_JER_EffectiveNP_3_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_4_MC = Systematic("syst_jer_4","_CENTRAL","_JET_JER_EffectiveNP_4_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_5_MC = Systematic("syst_jer_5","_CENTRAL","_JET_JER_EffectiveNP_5_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_6_MC = Systematic("syst_jer_6","_CENTRAL","_JET_JER_EffectiveNP_6_UP","_CENTRAL","tree","histoSysOneSideSym")
syst_jer_7_MC = Systematic("syst_jer_7","_CENTRAL","_JET_JER_EffectiveNP_7restTerm_UP","_CENTRAL","tree","histoSysOneSideSym")

syst_jer_dataMC_norm = Systematic("syst_jer_dataMC","_CENTRAL","_JET_JER_DataVsMC_MC16_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_1_norm = Systematic("syst_jer_1","_CENTRAL","_JET_JER_EffectiveNP_1_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_2_norm = Systematic("syst_jer_2","_CENTRAL","_JET_JER_EffectiveNP_2_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_3_norm = Systematic("syst_jer_3","_CENTRAL","_JET_JER_EffectiveNP_3_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_4_norm = Systematic("syst_jer_4","_CENTRAL","_JET_JER_EffectiveNP_4_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_5_norm = Systematic("syst_jer_5","_CENTRAL","_JET_JER_EffectiveNP_5_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_6_norm = Systematic("syst_jer_6","_CENTRAL","_JET_JER_EffectiveNP_6_UP","_CENTRAL","tree","normHistoSysOneSideSym")
syst_jer_7_norm = Systematic("syst_jer_7","_CENTRAL","_JET_JER_EffectiveNP_7restTerm_UP","_CENTRAL","tree","normHistoSysOneSideSym")

#MET
syst_MET_SoftTrk_ResoPara_MC = Systematic("MET_SoftTrk_ResoPara","_CENTRAL","_MET_SoftTrk_ResoPara","_CENTRAL","tree","histoSysOneSideSym")
syst_MET_SoftTrk_ResoPerp_MC = Systematic("MET_SoftTrk_ResoPerp","_CENTRAL","_MET_SoftTrk_ResoPerp","_CENTRAL","tree","histoSysOneSideSym")
syst_MET_SoftTrk_Scale_MC = Systematic("MET_SoftTrk_Scale","_CENTRAL","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","histoSys")

syst_MET_SoftTrk_ResoPara_norm = Systematic("MET_SoftTrk_ResoPara","_CENTRAL","_MET_SoftTrk_ResoPara","_CENTRAL","tree","normHistoSysOneSideSym")
syst_MET_SoftTrk_ResoPerp_norm = Systematic("MET_SoftTrk_ResoPerp","_CENTRAL","_MET_SoftTrk_ResoPerp","_CENTRAL","tree","normHistoSysOneSideSym")
syst_MET_SoftTrk_Scale_norm = Systematic("MET_SoftTrk_Scale","_CENTRAL","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","normHistoSys")

#Egamma
syst_EG_Scale_MC = Systematic("EG_Scale","_CENTRAL","_EG_SCALE_ALL_UP","_EG_SCALE_ALL_DN","tree","histoSys")

syst_EG_Scale_norm = Systematic("EG_Scale","_CENTRAL","_EG_SCALE_ALL_UP","_EG_SCALE_ALL_DN","tree","normHistoSys")

#Muon
syst_MuID_MC = Systematic("Muon_ID","_CENTRAL","_MUON_ID_UP","_MUON_ID_DN","tree","histoSys")
syst_MuMS_MC = Systematic("Muon_MS","_CENTRAL","_MUON_MS_UP","_MUON_MS_DN","tree","histoSys")
syst_MuScale_MC = Systematic("Muon_Scale","_CENTRAL","_MUON_SCALE_UP","_MUON_SCALE_DN","tree","histoSys")
syst_MuSagResBias_MC = Systematic("Muon_Sag_Resbias","_CENTRAL","_MUON_SAGITTA_RESBIAS_UP","_MUON_SAGITTA_RESBIAS_DN","tree","histoSys")
syst_MuSagRho_MC = Systematic("Muon_SagRho","_CENTRAL","_MUON_SAGITTA_RHO_UP","_MUON_SAGITTA_RHO_DN","tree","histoSys")

syst_MuID_norm = Systematic("Muon_ID","_CENTRAL","_MUON_ID_UP","_MUON_ID_DN","tree","normHistoSys")
syst_MuMS_norm = Systematic("Muon_MS","_CENTRAL","_MUON_MS_UP","_MUON_MS_DN","tree","normHistoSys")
syst_MuScale_norm = Systematic("Muon_Scale","_CENTRAL","_MUON_SCALE_UP","_MUON_SCALE_DN","tree","normHistoSys")
syst_MuSagResBias_norm = Systematic("Muon_Sag_Resbias","_CENTRAL","_MUON_SAGITTA_RESBIAS_UP","_MUON_SAGITTA_RESBIAS_DN","tree","normHistoSys")
syst_MuSagRho_norm = Systematic("Muon_SagRho","_CENTRAL","_MUON_SAGITTA_RHO_UP","_MUON_SAGITTA_RHO_DN","tree","normHistoSys")

####===> WEIGHTS

#electrons
syst_elecSF_EFF_Iso_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up")
syst_elecSF_EFF_Iso_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down")
syst_elecSF_EFF_Iso = Systematic("syst_elecSF_Iso", configMgr.weights, syst_elecSF_EFF_Iso_UP, syst_elecSF_EFF_Iso_DOWN, 'weight', 'overallHistoSys')

syst_elecSF_EFF_Iso_norm = Systematic("syst_elecSF_Iso", configMgr.weights, syst_elecSF_EFF_Iso_UP, syst_elecSF_EFF_Iso_DOWN, 'weight', 'overallNormHistoSys')


syst_elecSF_EFF_ID_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up")
syst_elecSF_EFF_ID_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down")
syst_elecSF_EFF_ID = Systematic("syst_elecSF_ID", configMgr.weights, syst_elecSF_EFF_ID_UP, syst_elecSF_EFF_ID_DOWN, 'weight', 'overallHistoSys')

syst_elecSF_EFF_ID_norm = Systematic("syst_elecSF_ID", configMgr.weights, syst_elecSF_EFF_ID_UP, syst_elecSF_EFF_ID_DOWN, 'weight', 'overallNormHistoSys')

syst_elecSF_EFF_Reco_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up")
syst_elecSF_EFF_Reco_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down")
syst_elecSF_EFF_Reco = Systematic("syst_elecSF_Reco", configMgr.weights, syst_elecSF_EFF_Reco_UP, syst_elecSF_EFF_Reco_DOWN, 'weight', 'overallHistoSys')

syst_elecSF_EFF_Reco_norm = Systematic("syst_elecSF_Reco", configMgr.weights, syst_elecSF_EFF_Reco_UP, syst_elecSF_EFF_Reco_DOWN, 'weight', 'overallNormHistoSys')

syst_elecSF_EFF_TriggerEff_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_TriggerEff_up")
syst_elecSF_EFF_TriggerEff_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_TriggerEff_down")
syst_elecSF_EFF_TriggerEff = Systematic("syst_elecSF_TrigEff", configMgr.weights, syst_elecSF_EFF_TriggerEff_UP, syst_elecSF_EFF_TriggerEff_DOWN, 'weight', 'overallHistoSys')

syst_elecSF_EFF_TriggerEff_norm = Systematic("syst_elecSF_TrigEff", configMgr.weights, syst_elecSF_EFF_TriggerEff_UP, syst_elecSF_EFF_TriggerEff_DOWN, 'weight', 'overallNormHistoSys')

#muons
syst_muonSF_EFF_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_up")
syst_muonSF_EFF_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_down")
syst_muonSF_EFF_Stat = Systematic("syst_muonSF_Eff_Stat", configMgr.weights, syst_muonSF_EFF_Stat_UP, syst_muonSF_EFF_Stat_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_EFF_Stat_norm = Systematic("syst_muonSF_Eff_Stat", configMgr.weights, syst_muonSF_EFF_Stat_UP, syst_muonSF_EFF_Stat_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_EFF_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_up")
syst_muonSF_EFF_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_down")
syst_muonSF_EFF_Sys = Systematic("syst_muonSF_Eff_Sys", configMgr.weights, syst_muonSF_EFF_Sys_UP, syst_muonSF_EFF_Sys_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_EFF_Sys_norm = Systematic("syst_muonSF_Eff_Sys", configMgr.weights, syst_muonSF_EFF_Sys_UP, syst_muonSF_EFF_Sys_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_EFF_Stat_lowPt_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_LOWPT_up")
syst_muonSF_EFF_Stat_lowPt_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_LOWPT_down")
syst_muonSF_EFF_Stat_lowPt = Systematic("syst_muonSF_Eff_Stat_lowPt", configMgr.weights, syst_muonSF_EFF_Stat_lowPt_UP, syst_muonSF_EFF_Stat_lowPt_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_EFF_Stat_lowPt_norm = Systematic("syst_muonSF_Eff_Stat_lowPt", configMgr.weights, syst_muonSF_EFF_Stat_lowPt_UP, syst_muonSF_EFF_Stat_lowPt_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_EFF_Sys_lowPt_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_up")
syst_muonSF_EFF_Sys_lowPt_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_down")
syst_muonSF_EFF_Sys_lowPt = Systematic("syst_muonSF_Eff_Sys_lowPt", configMgr.weights, syst_muonSF_EFF_Sys_lowPt_UP, syst_muonSF_EFF_Sys_lowPt_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_EFF_Sys_lowPt_norm = Systematic("syst_muonSF_Eff_Sys_lowPt", configMgr.weights, syst_muonSF_EFF_Sys_lowPt_UP, syst_muonSF_EFF_Sys_lowPt_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_ISO_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_STAT_up")
syst_muonSF_ISO_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_STAT_down")
syst_muonSF_ISO_Stat = Systematic("syst_muonSF_Iso_Stat", configMgr.weights, syst_muonSF_ISO_Stat_UP, syst_muonSF_ISO_Stat_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_ISO_Stat_norm = Systematic("syst_muonSF_Iso_Stat", configMgr.weights, syst_muonSF_ISO_Stat_UP, syst_muonSF_ISO_Stat_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_ISO_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_SYS_up")
syst_muonSF_ISO_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_SYS_down")
syst_muonSF_ISO_Sys = Systematic("syst_muonSF_Iso_Sys", configMgr.weights, syst_muonSF_ISO_Sys_UP, syst_muonSF_ISO_Sys_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_ISO_Sys_norm = Systematic("syst_muonSF_Iso_Sys", configMgr.weights, syst_muonSF_ISO_Sys_UP, syst_muonSF_ISO_Sys_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_TTVA_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_STAT_up")
syst_muonSF_TTVA_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_STAT_down")
syst_muonSF_TTVA_Stat = Systematic("syst_muonSF_TTVA_Stat", configMgr.weights, syst_muonSF_TTVA_Stat_UP, syst_muonSF_TTVA_Stat_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_TTVA_Stat_norm = Systematic("syst_muonSF_TTVA_Stat", configMgr.weights, syst_muonSF_TTVA_Stat_UP, syst_muonSF_TTVA_Stat_DOWN, 'weight', 'overallNormHistoSys')

syst_muonSF_TTVA_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_SYS_up")
syst_muonSF_TTVA_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_SYS_down")
syst_muonSF_TTVA_Sys = Systematic("syst_muonSF_TTVA_Sys", configMgr.weights, syst_muonSF_TTVA_Sys_UP, syst_muonSF_TTVA_Sys_DOWN, 'weight', 'overallHistoSys')

syst_muonSF_TTVA_Sys_norm = Systematic("syst_muonSF_TTVA_Sys", configMgr.weights, syst_muonSF_TTVA_Sys_UP, syst_muonSF_TTVA_Sys_DOWN, 'weight', 'overallNormHistoSys')

#Flavour tagging
syst_FT_EFF_B_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_B_up")
syst_FT_EFF_B_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_B_down")
syst_FT_EFF_B = Systematic("syst_FT_Eff_B", configMgr.weights, syst_FT_EFF_B_UP, syst_FT_EFF_B_DOWN, 'weight', 'overallHistoSys')

syst_FT_EFF_B_norm = Systematic("syst_FT_Eff_B", configMgr.weights, syst_FT_EFF_B_UP, syst_FT_EFF_B_DOWN, 'weight', 'overallNormHistoSys')

syst_FT_EFF_C_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_C_up")
syst_FT_EFF_C_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_C_down")
syst_FT_EFF_C = Systematic("syst_FT_Eff_C", configMgr.weights, syst_FT_EFF_C_UP, syst_FT_EFF_C_DOWN, 'weight', 'overallHistoSys') 

syst_FT_EFF_C_norm = Systematic("syst_FT_Eff_C", configMgr.weights, syst_FT_EFF_C_UP, syst_FT_EFF_C_DOWN, 'weight', 'overallNormHistoSys')

syst_FT_EFF_L_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_Light_up")
syst_FT_EFF_L_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_Light_down")
syst_FT_EFF_L = Systematic("syst_FT_Eff_L", configMgr.weights, syst_FT_EFF_L_UP, syst_FT_EFF_L_DOWN, 'weight', 'overallHistoSys')

syst_FT_EFF_L_norm = Systematic("syst_FT_Eff_L", configMgr.weights, syst_FT_EFF_L_UP, syst_FT_EFF_L_DOWN, 'weight', 'overallNormHistoSys')

syst_FT_EFF_extrCharm_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_extrapolationFromCharm_up")
syst_FT_EFF_extrCharm_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_extrapolationFromCharm_down")
syst_FT_EFF_extrCharm = Systematic("syst_FT_Eff_extrCharm", configMgr.weights, syst_FT_EFF_extrCharm_UP, syst_FT_EFF_extrCharm_DOWN, 'weight', 'overallHistoSys')

syst_FT_EFF_extrCharm_norm = Systematic("syst_FT_Eff_extrCharm", configMgr.weights, syst_FT_EFF_extrCharm_UP, syst_FT_EFF_extrCharm_DOWN, 'weight', 'overallNormHistoSys')

#jets
syst_jvtSF_UP = replaceWeight(configMgr.weights, "JVTSF","syst_jvtSF_up")
syst_jvtSF_DOWN = replaceWeight(configMgr.weights, "JVTSF","syst_jvtSF_down")
syst_jvtSF = Systematic("syst_jvtSF", configMgr.weights, syst_jvtSF_UP, syst_jvtSF_DOWN, 'weight', 'overallHistoSys')

syst_jvtSF_norm = Systematic("syst_jvtSF", configMgr.weights, syst_jvtSF_UP, syst_jvtSF_DOWN, 'weight', 'overallNormHistoSys')

#cross-section syst
syst_xSec_ZZ_flat = Systematic("syst_xSec_ZZ_flat", 1.,1.06,0.94,"user","userOverallSys")

syst_xSec_VVV_flat = Systematic("syst_xSec_VVV_flat", 1.,1.2,0.8,"user","userOverallSys")

syst_xSec_ttW_UP = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.+(runNumber==410155)*0.12)")
syst_xSec_ttW_DN = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.-(runNumber==410155)*0.12)")
syst_xSec_ttW_flat = Systematic("syst_xSec_ttW_flat", 1., syst_xSec_ttW_UP, syst_xSec_ttW_DN, "weight", "overallSys")

syst_xSec_ttZ_UP = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.+(runNumber==410156 || runNumber==410157 || runNumber==410218 || runNumber==410219 || runNumber==410220 || runNumber==410276 || runNumber==410277 || runNumber==410278)*0.13)")
syst_xSec_ttZ_DN = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.-(runNumber==410156 || runNumber==410157 || runNumber==410218 || runNumber==410219 || runNumber==410220 || runNumber==410276 || runNumber==410277 || runNumber==410278)*0.13)")
syst_xSec_ttZ_flat = Systematic("syst_xSec_ttZ_flat", 1., syst_xSec_ttZ_UP, syst_xSec_ttZ_DN, "weight", "overallSys")

syst_xSec_ttH_flat = Systematic("syst_xSec_ttH_flat", 1.,1.1,0.9,"user","userOverallSys")

syst_xSec_other_UP = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.+(runNumber==304014 || runNumber==410080 || runNumber==410081 || runNumber==410560 || runNumber==410408)*0.5)")
syst_xSec_other_DN = replaceWeight(configMgr.weights,"pileupWeight","pileupWeight*(1.-(runNumber==304014 || runNumber==410080 || runNumber==410081 || runNumber==410560 || runNumber==410408)*0.5)")
syst_xSec_other_flat = Systematic("syst_xSec_other_flat", 1., syst_xSec_other_UP, syst_xSec_other_DN, "weight", "overallSys")


# theory syst 3L - WZ
syst_Theory_WZ_SR1disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0055,0.9845,"user","userOverallSys")
syst_Theory_WZ_SR2disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0706,0.8528,"user","userOverallSys")
syst_Theory_WZ_SR3disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0518,0.9717,"user","userOverallSys")
syst_Theory_WZ_SR4disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0483,0.9604,"user","userOverallSys")
syst_Theory_WZ_SR5disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0548,0.9501,"user","userOverallSys")
syst_Theory_WZ_SR6disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 0.9985,0.9909,"user","userOverallSys")
syst_Theory_WZ_SR7disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0227,0.9862,"user","userOverallSys")
syst_Theory_WZ_SR8disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0317,0.9823,"user","userOverallSys")
syst_Theory_WZ_SR9disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0056,0.9887,"user","userOverallSys")
syst_Theory_WZ_SR10disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0226,0.9841,"user","userOverallSys")
syst_Theory_WZ_SR11disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.0120,0.9770,"user","userOverallSys")
syst_Theory_WZ_SR12disc_QCD_muR = Systematic("syst_Theory_WZ_QCD_muR", 1., 1.2544,1.2544,"user","userOverallSys")

syst_Theory_WZ_SR1disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0047,0.9957,"user","userOverallSys")
syst_Theory_WZ_SR2disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0513,0.9487,"user","userOverallSys")
syst_Theory_WZ_SR3disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0180,0.9856,"user","userOverallSys")
syst_Theory_WZ_SR4disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0108,0.9917,"user","userOverallSys")
syst_Theory_WZ_SR5disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0047,0.9858,"user","userOverallSys")
syst_Theory_WZ_SR6disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0065,0.9973,"user","userOverallSys")
syst_Theory_WZ_SR7disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0091,0.9872,"user","userOverallSys")
syst_Theory_WZ_SR8disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0069,0.9920,"user","userOverallSys")
syst_Theory_WZ_SR9disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0023,0.9991,"user","userOverallSys")
syst_Theory_WZ_SR10disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 0.9996,0.9976,"user","userOverallSys")
syst_Theory_WZ_SR11disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0055,0.9960,"user","userOverallSys")
syst_Theory_WZ_SR12disc_QCD_muF = Systematic("syst_Theory_WZ_QCD_muF", 1., 1.0000,0.9948,"user","userOverallSys")

syst_Theory_WZ_SR1disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0074,0.9771,"user","userOverallSys")
syst_Theory_WZ_SR2disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0953,1.0953,"user","userOverallSys")
syst_Theory_WZ_SR3disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0156,0.9760,"user","userOverallSys")
syst_Theory_WZ_SR4disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0527,0.9484,"user","userOverallSys")
syst_Theory_WZ_SR5disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0499,0.9305,"user","userOverallSys")
syst_Theory_WZ_SR6disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 0.9869,0.9865,"user","userOverallSys")
syst_Theory_WZ_SR7disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0053,0.9942,"user","userOverallSys")
syst_Theory_WZ_SR8disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0194,0.9871,"user","userOverallSys")
syst_Theory_WZ_SR9disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0018,0.9795,"user","userOverallSys")
syst_Theory_WZ_SR10disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0219,0.9840,"user","userOverallSys")
syst_Theory_WZ_SR11disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.0127,0.9670,"user","userOverallSys")
syst_Theory_WZ_SR12disc_QCD_muRmuF = Systematic("syst_Theory_WZ_QCD_muRmuF", 1., 1.2555,1.2555,"user","userOverallSys")

syst_Theory_WZ_SR1disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0042,0.9958,"user","userOverallSys")
syst_Theory_WZ_SR2disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0117,0.9883,"user","userOverallSys")
syst_Theory_WZ_SR3disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0122,0.9878,"user","userOverallSys")
syst_Theory_WZ_SR4disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0065,0.9935,"user","userOverallSys")
syst_Theory_WZ_SR5disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0154,0.9846,"user","userOverallSys")
syst_Theory_WZ_SR6disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0166,0.9834,"user","userOverallSys")
syst_Theory_WZ_SR7disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0052,0.9948,"user","userOverallSys")
syst_Theory_WZ_SR8disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0026,0.9974,"user","userOverallSys")
syst_Theory_WZ_SR9disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0295,0.9705,"user","userOverallSys")
syst_Theory_WZ_SR10disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0019,0.9981,"user","userOverallSys")
syst_Theory_WZ_SR11disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0047,0.9953,"user","userOverallSys")
syst_Theory_WZ_SR12disc_PDF = Systematic("syst_Theory_WZ_PDF", 1., 1.0068,0.9932,"user","userOverallSys")

syst_Theory_WZ_SROn0jets_AltSample = Systematic("syst_Theory_WZ_SROn0jets_AltSample", 1., 1.0409681,0.9590319,"user","userOverallSys")
syst_Theory_WZ_SROnJets_AltSample = Systematic("syst_Theory_WZ_SROnJets_AltSample", 1., 1.0697464,0.9302536,"user","userOverallSys")
syst_Theory_WZ_SRLow0jets_AltSample = Systematic("syst_Theory_WZ_SRLow0jets_AltSample", 1., 1.110437,0.889563,"user","userOverallSys")
syst_Theory_WZ_SRLowJets_AltSample = Systematic("syst_Theory_WZ_SRLowJets_AltSample", 1., 1.0978209,0.9021791,"user","userOverallSys")
syst_Theory_WZ_SRHigh0jets_AltSample = Systematic("syst_Theory_WZ_SRHigh0jets_AltSample", 1., 1.113117,0.886883,"user","userOverallSys")
syst_Theory_WZ_SRDFOS_AltSample = Systematic("syst_Theory_WZ_SRDFOS_AltSample", 1.,1.104972,0.895028,"user","userOverallSys")

# theory syst 3L - ZZ

syst_Theory_ZZ_SR1disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0206,0.9485,"user","userOverallSys")
syst_Theory_ZZ_SR2disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0000,0.7500,"user","userOverallSys")
syst_Theory_ZZ_SR3disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1429,1.1429,"user","userOverallSys")
syst_Theory_ZZ_SR4disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1961,0.8627,"user","userOverallSys")
syst_Theory_ZZ_SR5disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.1250,0.8750,"user","userOverallSys")
syst_Theory_ZZ_SR6disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0889,0.8889,"user","userOverallSys")
syst_Theory_ZZ_SR7disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0246,0.9730,"user","userOverallSys")
syst_Theory_ZZ_SR8disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0652,0.9457,"user","userOverallSys")
syst_Theory_ZZ_SR9disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0119,0.9881,"user","userOverallSys")
syst_Theory_ZZ_SR10disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.2091,0.8545,"user","userOverallSys")
syst_Theory_ZZ_SR11disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.0615,0.9308,"user","userOverallSys")
syst_Theory_ZZ_SR12disc_QCD_muR = Systematic("syst_Theory_ZZ_QCD_muR", 1., 1.2353,0.7647,"user","userOverallSys")

syst_Theory_ZZ_SR1disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SR2disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SR3disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SR4disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SR5disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SR6disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 0.9556,0.9556,"user","userOverallSys")
syst_Theory_ZZ_SR7disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0176,0.9789,"user","userOverallSys")
syst_Theory_ZZ_SR8disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0109,0.9837,"user","userOverallSys")
syst_Theory_ZZ_SR9disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0119,1.0000,"user","userOverallSys")
syst_Theory_ZZ_SR10disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0091,0.9909,"user","userOverallSys")
syst_Theory_ZZ_SR11disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0154,0.9769,"user","userOverallSys")
syst_Theory_ZZ_SR12disc_QCD_muF = Systematic("syst_Theory_ZZ_QCD_muF", 1., 1.0000,0.9882,"user","userOverallSys")

syst_Theory_ZZ_SR1disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0206,0.9588,"user","userOverallSys")
syst_Theory_ZZ_SR2disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0000,0.7500,"user","userOverallSys")
syst_Theory_ZZ_SR3disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1429,1.1429,"user","userOverallSys")
syst_Theory_ZZ_SR4disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1961,0.8627,"user","userOverallSys")
syst_Theory_ZZ_SR5disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1250,0.8750,"user","userOverallSys")
syst_Theory_ZZ_SR6disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0444,0.8667,"user","userOverallSys")
syst_Theory_ZZ_SR7disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0082,0.9930,"user","userOverallSys")
syst_Theory_ZZ_SR8disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0543,0.9565,"user","userOverallSys")
syst_Theory_ZZ_SR9disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0119,0.9881,"user","userOverallSys")
syst_Theory_ZZ_SR10disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.2091,0.8636,"user","userOverallSys")
syst_Theory_ZZ_SR11disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.0385,0.9462,"user","userOverallSys")
syst_Theory_ZZ_SR12disc_QCD_muRmuF = Systematic("syst_Theory_ZZ_QCD_muRmuF", 1., 1.1529,0.7647,"user","userOverallSys")

syst_Theory_ZZ_SR1disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0881,0.9119,"user","userOverallSys")
syst_Theory_ZZ_SR2disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0249,0.9751,"user","userOverallSys")
syst_Theory_ZZ_SR3disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.1059,0.8941,"user","userOverallSys")
syst_Theory_ZZ_SR4disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0131,0.9869,"user","userOverallSys")
syst_Theory_ZZ_SR5disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.1410,0.8590,"user","userOverallSys")
syst_Theory_ZZ_SR6disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0298,0.9702,"user","userOverallSys")
syst_Theory_ZZ_SR7disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0133,0.9867,"user","userOverallSys")
syst_Theory_ZZ_SR8disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0124,0.9876,"user","userOverallSys")
syst_Theory_ZZ_SR9disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0152,0.9848,"user","userOverallSys")
syst_Theory_ZZ_SR10disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.4119,0.5881,"user","userOverallSys")
syst_Theory_ZZ_SR11disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0115,0.9885,"user","userOverallSys")
syst_Theory_ZZ_SR12disc_PDF = Systematic("syst_Theory_ZZ_PDF", 1., 1.0138,0.9862,"user","userOverallSys")

syst_Theory_ZZ_SROn0jets_AltSample = Systematic("syst_Theory_ZZ_SROn0jets_AltSample", 1., 1.165436,0.834564,"user","userOverallSys")
syst_Theory_ZZ_SROnJets_AltSample = Systematic("syst_Theory_ZZ_SROnJets_AltSample", 1., 1.391625,0.608375,"user","userOverallSys")
syst_Theory_ZZ_SRLow0jets_AltSample = Systematic("syst_Theory_ZZ_SRLow0jets_AltSample", 1., 1.168288,0.831712,"user","userOverallSys")
syst_Theory_ZZ_SRLowJets_AltSample = Systematic("syst_Theory_ZZ_SRLowJets_AltSample", 1., 1.0671805,0.9328195,"user","userOverallSys")
syst_Theory_ZZ_SRHigh0jets_AltSample = Systematic("syst_Theory_ZZ_SRHigh0jets_AltSample", 1., 1.33822,0.66178,"user","userOverallSys")
syst_Theory_ZZ_SRDFOS_AltSample = Systematic("syst_Theory_ZZ_SRDFOS_AltSample", 1.,1.10888,0.89112,"user","userOverallSys")

# theory syst 3L - VVV
syst_Theory_VVV_SR12disc_QCD_muR = Systematic("syst_Theory_VVV_QCD_muR", 1., 1.1027,0.9193,"user","userOverallSys")
syst_Theory_VVV_SR12disc_QCD_muF = Systematic("syst_Theory_VVV_QCD_muF", 1., 1.0073,0.9902,"user","userOverallSys")
syst_Theory_VVV_SR12disc_QCD_muRmuF = Systematic("syst_Theory_VVV_QCD_muRmuF", 1., 1.1076,0.9120,"user","userOverallSys")
syst_Theory_VVV_SR12disc_PDF = Systematic("syst_Theory_VVV_PDF", 1., 1.0090,0.9910,"user","userOverallSys")

syst_Theory_VVV_flat = Systematic("syst_Theory_VVV_flat", 1., 1.2,0.8,"user","userOverallSys")

# theory syst 3L - ttV
syst_Theory_ttV_flat = Systematic("syst_Theory_ttV_flat", 1., 1.2,0.8,"user","userOverallSys")

# Theory syst - 3L ttbar
syst_Theory_ttbar_flat = Systematic("syst_Theory_ttbar_flat", 1., 1.2000,0.8000,"user","userOverallSys")

syst_Theory_ttbar_SR7disc_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.0803,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SR8disc_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.0803,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SR9disc_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.0000,0.8063,"user","userOverallSys")
syst_Theory_ttbar_SR10disc_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.0457,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SR11disc_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.1823,1.0000,"user","userOverallSys")
syst_Theory_ttbar_SR12disc_ME = Systematic("syst_Theory_ttbar_ME", 1., 1.0000,0.9761,"user","userOverallSys")

syst_Theory_ttbar_SR7disc_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7924,"user","userOverallSys")
syst_Theory_ttbar_SR8disc_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7924,"user","userOverallSys")
syst_Theory_ttbar_SR9disc_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.6245,"user","userOverallSys")
syst_Theory_ttbar_SR10disc_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7704,"user","userOverallSys")
syst_Theory_ttbar_SR11disc_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.7706,"user","userOverallSys")
syst_Theory_ttbar_SR12disc_PS = Systematic("syst_Theory_ttbar_PS", 1., 1.0000,0.6314,"user","userOverallSys")

syst_Theory_ttbar_SR7disc_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9804,"user","userOverallSys")
syst_Theory_ttbar_SR8disc_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9804,"user","userOverallSys")
syst_Theory_ttbar_SR9disc_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0408,0.8867,"user","userOverallSys")
syst_Theory_ttbar_SR10disc_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9525,"user","userOverallSys")
syst_Theory_ttbar_SR11disc_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.0000,0.9870,"user","userOverallSys")
syst_Theory_ttbar_SR12disc_ISR = Systematic("syst_Theory_ttbar_ISR", 1., 1.1115,0.8567,"user","userOverallSys")

syst_Theory_ttbar_SR7disc_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0451,0.8688,"user","userOverallSys")
syst_Theory_ttbar_SR8disc_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0451,0.8688,"user","userOverallSys")
syst_Theory_ttbar_SR9disc_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0000,0.8696,"user","userOverallSys")
syst_Theory_ttbar_SR10disc_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0172,0.8603,"user","userOverallSys")
syst_Theory_ttbar_SR11disc_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.0238,0.9870,"user","userOverallSys")
syst_Theory_ttbar_SR12disc_FSR = Systematic("syst_Theory_ttbar_FSR", 1., 1.1718,0.9329,"user","userOverallSys")

syst_Theory_ttbar_SR7disc_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0143,0.9857,"user","userOverallSys")
syst_Theory_ttbar_SR8disc_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0124,0.9876,"user","userOverallSys")
syst_Theory_ttbar_SR9disc_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0129,0.9871,"user","userOverallSys")
syst_Theory_ttbar_SR10disc_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0132,0.9868,"user","userOverallSys")
syst_Theory_ttbar_SR11disc_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0125,0.9875,"user","userOverallSys")
syst_Theory_ttbar_SR12disc_PDF = Systematic("syst_Theory_ttbar_PDF", 1., 1.0117,0.9883,"user","userOverallSys")

# Fake syst        
syst_FFstat_CRWZ_0j = Systematic("syst_FFstat_CRWZ_0j", 1., 0.9927 , 0.9747, "user","userOverallSys")
syst_FFstat_CRWZ_lowht = Systematic("syst_FFstat_CRWZ_lowht", 1., 1.1454 , 0.869, "user","userOverallSys")
syst_FFstat_CRWZ_highht = Systematic("syst_FFstat_CRWZ_highht", 1., 1.5184 , 0.6795, "user","userOverallSys")

syst_FFstat_SR1disc = Systematic("syst_FFstat_SR1", 1., -0.1045 , 2.1388, "user","userOverallSys")
syst_FFstat_SR2disc = Systematic("syst_FFstat_SR2", 1., 1.6755 , 0.3501, "user","userOverallSys")
syst_FFstat_SR3disc = Systematic("syst_FFstat_SR3", 1., 1.8852 , 0.1078, "user","userOverallSys")
syst_FFstat_SR4disc = Systematic("syst_FFstat_SR4", 1., 1.7067 , 0.4046, "user","userOverallSys")
syst_FFstat_SR5disc = Systematic("syst_FFstat_SR5", 1., 0.1596 , 2.7886, "user","userOverallSys")
syst_FFstat_SR6disc = Systematic("syst_FFstat_SR6", 1., 0.6784 , 1.0324, "user","userOverallSys")
syst_FFstat_SR7disc = Systematic("syst_FFstat_SR7", 1., 0.1807 , 1.0673, "user","userOverallSys")
syst_FFstat_SR8disc = Systematic("syst_FFstat_SR8", 1., 1.1262 , 0.8621, "user","userOverallSys")
syst_FFstat_SR9disc = Systematic("syst_FFstat_SR9", 1., 0.4812 , 1.7865, "user","userOverallSys")
syst_FFstat_SR10disc = Systematic("syst_FFstat_SR10", 1., 2.3956 , -0.1709, "user","userOverallSys")
syst_FFstat_SR11disc = Systematic("syst_FFstat_SR11", 1., -2.0834 , 4.2228, "user","userOverallSys")
syst_FFstat_SR12disc = Systematic("syst_FFstat_SR12", 1., 3.1399 , 0.3485, "user","userOverallSys")

syst_FakeClosure_CR = Systematic("syst_FakeClosure_CR", 1., 1.30, 0.7, "user","userOverallSys")
syst_FakeClosure_SR = Systematic("syst_FakeClosure_SR_SFOS", 1., 1.5, 0.5, "user","userOverallSys")
syst_FakeClosure_SRDF = Systematic("syst_FakeClosure_SR_DFOS", 1., 1.2, 0.8, "user","userOverallSys")

#Theory x-sec uncertainties
#syst_ttZxsec_UP = replaceWeight(configMgr.weights,"36.1","36.1*(1.+(runNumber>=410111 || runNumber<=410116)*0.12)")
#syst_ttZxsec_DN = replaceWeight(configMgr.weights,"36.1","36.1*(1.-(runNumber>=410111 || runNumber<=410116)*0.12)")
#syst_ttZxsec = Systematic("syst_ttZxsec", 1., syst_ttZxsec_UP, syst_ttZxsec_DN, "weight", "overallSys")

#FIXME no syst now

#systList = [
#	syst_jes_1_MC, 
#	syst_jes_2_MC,
#	syst_jes_3_MC,
#	syst_jes_eta_MC,
#       syst_EG_res_MC,  
#	syst_EG_scale_MC,	
#	syst_MET_SoftTrk_ResoPara_MC,  
# 	syst_MET_SoftTrk_ResoPerp_MC,    
#	syst_MET_SoftTrk_Scale_MC,
#      	syst_JER_MC,  
#	syst_jvt_MC,
#	syst_Muon_ID_MC,  
#	syst_Muon_MS_MC, 
#	syst_Muon_Scale_MC,
#	syst_btagB_MC,   
#	syst_btagC_MC,  
#	syst_btagL_MC,
#	syst_ftagExtrapo_MC, 
#	syst_ftagExtrapoCharm_MC,
#       syst_ElecSF_ID_MC, 
#	syst_ElecIsoSF_MC, 
#	syst_ElecSF_Reco_MC,
#       syst_ElecTrig_MC, 
#	syst_ElecTrigEff_MC,
#       syst_MuIsoStat_MC, 
#	syst_MuIsoSys_MC,
#        syst_MuTTVAStat_MC, 
#	syst_MuTTVASys_MC,
#        syst_MuEffStat_MC, 
#	syst_MuEffSys_MC, 
#        syst_MuEffTrigStat_MC,
#        syst_MuEffStatLow_MC, 
#	syst_MuEffSysLow_MC,
#        syst_PRW
#	]
	
systList = [
	syst_jes_1_MC,
	syst_jes_2_MC,
	syst_jes_3_MC,
        syst_jes_4_MC,
        syst_jes_5_MC,
        syst_jes_6_MC,
        syst_jes_7_MC,
        syst_jes_8_MC,
        syst_jes_9_MC,
        syst_jes_10_MC,
        syst_jes_11_MC,
        syst_jes_12_MC,
        syst_jes_13_MC,
        syst_jes_14_MC,
        syst_jes_15_MC,
        syst_jes_16_MC,
        syst_jes_17_MC,
        syst_jes_18_MC,
        syst_jes_19_MC,
        syst_jes_20_MC,
        syst_jes_21_MC,
        syst_jes_22_MC,
        syst_jes_23_MC,
        syst_jes_24_MC,
        syst_jes_25_MC,
        syst_jes_26_MC,
        syst_jes_27_MC,
	syst_jer_dataMC_MC,
	syst_jer_1_MC,
	syst_jer_2_MC,
	syst_jer_3_MC,
        syst_jer_4_MC,
	syst_jer_5_MC,
        syst_jer_6_MC,
	syst_jer_7_MC,
        syst_MET_SoftTrk_ResoPara_MC,  
        syst_MET_SoftTrk_ResoPerp_MC,    
        syst_MET_SoftTrk_Scale_MC,
	syst_EG_Scale_MC,
	syst_MuID_MC,
	syst_MuMS_MC,
	syst_MuScale_MC,
	syst_MuSagResBias_MC,
	syst_MuSagRho_MC,
	syst_elecSF_EFF_Iso,
	syst_elecSF_EFF_ID,
	syst_elecSF_EFF_Reco,
	syst_elecSF_EFF_TriggerEff,
	syst_muonSF_EFF_Stat,
	syst_muonSF_EFF_Sys,
	syst_muonSF_EFF_Stat_lowPt,
	syst_muonSF_EFF_Sys_lowPt,
	syst_muonSF_ISO_Stat,
	syst_muonSF_ISO_Sys,
	syst_muonSF_TTVA_Stat,
	syst_muonSF_TTVA_Sys,
	syst_FT_EFF_B,
	syst_FT_EFF_C,
	syst_FT_EFF_L,
	syst_FT_EFF_extrCharm,
	syst_jvtSF
	]	

normSystList = [
        syst_jes_1_MC_norm,
        syst_jes_2_MC_norm,
        syst_jes_3_MC_norm,
        syst_jes_4_MC_norm,
        syst_jes_5_MC_norm,
        syst_jes_6_MC_norm,
        syst_jes_7_MC_norm,
        syst_jes_8_MC_norm,
        syst_jes_9_MC_norm,
        syst_jes_10_MC_norm,
        syst_jes_11_MC_norm,
        syst_jes_12_MC_norm,
        syst_jes_13_MC_norm,
        syst_jes_14_MC_norm,
        syst_jes_15_MC_norm,
        syst_jes_16_MC_norm,
        syst_jes_17_MC_norm,
        syst_jes_18_MC_norm,
        syst_jes_19_MC_norm,
        syst_jes_20_MC_norm,
        syst_jes_21_MC_norm,
        syst_jes_22_MC_norm,
        syst_jes_23_MC_norm,
        syst_jes_24_MC_norm,
        syst_jes_25_MC_norm,
        syst_jes_26_MC_norm,
        syst_jes_27_MC_norm,
        syst_jer_dataMC_norm,
        syst_jer_1_norm,
        syst_jer_2_norm,
        syst_jer_3_norm,
        syst_jer_4_norm,
        syst_jer_5_norm,
        syst_jer_6_norm,
        syst_jer_7_norm,
        syst_MET_SoftTrk_ResoPara_norm,
        syst_MET_SoftTrk_ResoPerp_norm,
        syst_MET_SoftTrk_Scale_norm,
        syst_EG_Scale_norm,
        syst_MuID_norm,
        syst_MuMS_norm,
        syst_MuScale_norm,
        syst_MuSagResBias_norm,
        syst_MuSagRho_norm,
        syst_elecSF_EFF_Iso_norm,
        syst_elecSF_EFF_ID_norm,
        syst_elecSF_EFF_Reco_norm,
        syst_elecSF_EFF_TriggerEff_norm,
        syst_muonSF_EFF_Stat_norm,
        syst_muonSF_EFF_Sys_norm,
        syst_muonSF_EFF_Stat_lowPt_norm,
        syst_muonSF_EFF_Sys_lowPt_norm,
        syst_muonSF_ISO_Stat_norm,
        syst_muonSF_ISO_Sys_norm,
        syst_muonSF_TTVA_Stat_norm,
        syst_muonSF_TTVA_Sys_norm,
        syst_FT_EFF_B_norm,
        syst_FT_EFF_C_norm,
        syst_FT_EFF_L_norm,
        syst_FT_EFF_extrCharm_norm,
        syst_jvtSF_norm
        ]

# 3L configuration ----------------------------------------------------------------------------------------------------
#FIXME check naming convention

Diboson3L0jSample=Sample("Dibosons_3L_0j", kPink)
Diboson3L0jSample.setStatConfig(useStat)
Diboson3L0jSample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
Diboson3L0jSample.setNormRegions([("WZ_CR_0jets","cuts")])
Diboson3L0jSample.addInputs(bgdFiles_a,"Dibosons_3L_nJ0_a")
Diboson3L0jSample.addInputs(bgdFiles_d,"Dibosons_3L_nJ0_d")
Diboson3L0jSample.addInputs(bgdFiles_e,"Dibosons_3L_nJ0_e")

Diboson3LLowHTSample=Sample("Dibosons_3L_lowHT", kPink)
Diboson3LLowHTSample.setStatConfig(useStat)
Diboson3LLowHTSample.setNormFactor("mu_WZSF_LowHT",1.,0.,5.)
Diboson3LLowHTSample.setNormRegions([("WZ_CR_LowHT","cuts")])
Diboson3LLowHTSample.addInputs(bgdFiles_a,"Dibosons_3L_LowHT_a")
Diboson3LLowHTSample.addInputs(bgdFiles_d,"Dibosons_3L_LowHT_d")
Diboson3LLowHTSample.addInputs(bgdFiles_e,"Dibosons_3L_LowHT_e")

Diboson3LHighHTSample=Sample("Dibosons_3L_highHT", kPink)
Diboson3LHighHTSample.setStatConfig(useStat)
Diboson3LHighHTSample.setNormFactor("mu_WZSF_HighHT",1.,0.,5.)
Diboson3LHighHTSample.setNormRegions([("WZ_CR_HighHT","cuts")])
Diboson3LHighHTSample.addInputs(bgdFiles_a,"Dibosons_3L_HighHT_a")
Diboson3LHighHTSample.addInputs(bgdFiles_d,"Dibosons_3L_HighHT_d")
Diboson3LHighHTSample.addInputs(bgdFiles_e,"Dibosons_3L_HighHT_e")

Diboson4LSample=Sample("Dibosons_4L", kGreen)
Diboson4LSample.setStatConfig(useStat)
Diboson4LSample.setNormByTheory()
Diboson4LSample.addInputs(bgdFiles_a,"Dibosons_4L_a")
Diboson4LSample.addInputs(bgdFiles_d,"Dibosons_4L_d")
Diboson4LSample.addInputs(bgdFiles_e,"Dibosons_4L_e")
if not noSysts and doThUncert: Diboson4LSample.addSystematic(syst_xSec_ZZ_flat)

Diboson2LSample=Sample("Dibosons_2L", kGreen)
Diboson2LSample.setStatConfig(useStat)
Diboson2LSample.setNormByTheory()
Diboson2LSample.addInputs(bgdFiles_a,"Dibosons_2L_a")
Diboson2LSample.addInputs(bgdFiles_d,"Dibosons_2L_d")
Diboson2LSample.addInputs(bgdFiles_e,"Dibosons_2L_e")
if not noSysts and doThUncert: Diboson2LSample.addSystematic(syst_xSec_ZZ_flat)

higgsSample=Sample("Higgs_VH", kPink)
higgsSample.setStatConfig(useStat)
higgsSample.setNormByTheory()
higgsSample.addInputs(bgdFiles_a,"Higgs_a")
higgsSample.addInputs(bgdFiles_d,"Higgs_d")
higgsSample.addInputs(bgdFiles_e,"Higgs_e")
higgsSample.addSampleSpecificWeight("!(runNumber==346343 || runNumber==346344 || runNumber==346345)")

ttHSample=Sample("Higgs_ttH", kPink)
ttHSample.setStatConfig(useStat)
ttHSample.setNormByTheory()
ttHSample.addInputs(bgdFiles_a,"Higgs_a")
ttHSample.addInputs(bgdFiles_d,"Higgs_d")
ttHSample.addInputs(bgdFiles_e,"Higgs_e")
ttHSample.addSampleSpecificWeight("(runNumber==346343 || runNumber==346344 || runNumber==346345)")
if not noSysts and doThUncert: ttHSample.addSystematic(syst_xSec_ttH_flat)

ttVSample=Sample("ttV", kOrange)
ttVSample.setStatConfig(useStat)
ttVSample.setNormByTheory()
ttVSample.addInputs(bgdFiles_a,"ttV_a")
ttVSample.addInputs(bgdFiles_d,"ttV_d")
ttVSample.addInputs(bgdFiles_e,"ttV_e")
if not noSysts and doThUncert: ttVSample.addSystematic(syst_xSec_ttW_flat)
if not noSysts and doThUncert: ttVSample.addSystematic(syst_xSec_ttZ_flat)
if not noSysts and doThUncert: ttVSample.addSystematic(syst_xSec_other_flat)
if not noSysts and doThUncert: ttVSample.addSystematic(syst_Theory_ttV_flat)

tribosonSample=Sample("VVV", kCyan+2)
tribosonSample.setStatConfig(useStat)
tribosonSample.setNormByTheory()
tribosonSample.addInputs(bgdFiles_a,"VVV_a")
tribosonSample.addInputs(bgdFiles_d,"VVV_d")
tribosonSample.addInputs(bgdFiles_e,"VVV_e")
if not noSysts and doThUncert: tribosonSample.addSystematic(syst_xSec_VVV_flat)

VgammaSample=Sample("MCFakes_Vg", kCyan+2)
VgammaSample.setStatConfig(useStat)
VgammaSample.setNormByTheory()
VgammaSample.addInputs(bgdFiles_a,"Vgamma_a")
VgammaSample.addInputs(bgdFiles_d,"Vgamma_d")
VgammaSample.addInputs(bgdFiles_e,"Vgamma_e")

ttbarSample=Sample("ttbar", kCyan-9)
ttbarSample.setStatConfig(useStat)
ttbarSample.setNormByTheory()
ttbarSample.addInputs(bgdFiles_a,"ttbar_a")
ttbarSample.addInputs(bgdFiles_d,"ttbar_d")
ttbarSample.addInputs(bgdFiles_e,"ttbar_e")
#if not noSysts and doThUncert: ttbarSample.addSystematic(syst_Theory_ttbar_flat)
#multitopSample.buildHisto([0.01], "SR3LSFOS0ja", "cuts")

singleTSample=Sample("SingleT", kCyan+4)
singleTSample.setStatConfig(useStat)
singleTSample.setNormByTheory()
singleTSample.addInputs(bgdFiles_a,"SingleT_a")
singleTSample.addInputs(bgdFiles_d,"SingleT_d")
singleTSample.addInputs(bgdFiles_e,"SingleT_e")

if not useFakes:
	       
	allmcSamples=['Dibosons_3L_0j','Dibosons_3L_lowHT','Dibosons_3L_highHT','Dibosons_4L','Dibosons_2L','MCFakes','MCFakes_Vg','VVV','Higgs','ttV', 'ttbar','SingleT']
	mconlySamples=['Dibosons_4L','Dibosons_2L','ZZ','VVV','Higgs','ttV', 'ttbar','SingleT']

	mcfakesSample=Sample("MCFakes", kMagenta+6)
        mcfakesSample.setStatConfig(useStat)
        mcfakesSample.setNormByTheory()
	mcfakesSample.addInputs(bgdFiles_a,"Zjets_a")
	mcfakesSample.addInputs(bgdFiles_d,"ZZ_d")
	mcfakesSample.addInputs(bgdFiles_e,"Zjets_e")

if useFakes:
	
	allmcSamples=['Dibosons_3L_0j','Dibosons_3L_lowHT','Dibosons_3L_highHT','Dibosons_4L','Dibosons_2L','VVV','Higgs','ttV', 'ttbar','SingleT']
	mconlySamples=['Dibosons_4L','Dibosons_2L','VVV','Higgs_VH','Higgs_ttH','ttV', 'ttbar','SingleT']
	normSamples=['Dibosons_3L_0j','Dibosons_3L_lowHT','Dibosons_3L_highHT']	

	fakeSample=Sample("Fakes",kMagenta+6)
	#fakeSample.setNormByTheory()
	fakeSample.setStatConfig(False)
	#fakeSample.addSampleSpecificWeight(")
	fakeSample.addInputs(fakeFiles)

# Data setup ----------------------------------------------------------------------------------------------------

dataSample = Sample("Data_CENTRAL",kBlack)
dataSample.setData()
#dataSample.setFileList(dataFiles)
dataSample.addInputs(dataFiles)

#dataSample.buildHisto([0.01], "SR3LDFOS0j", "cuts")
#dataSample.buildHisto([0.01], "SR3LSFOS0ja", "cuts")

# ----------------------------------------------------------------------------------------------------
#Here we are basically saying which backgrounds we want to include in the analysis, adding the input from the files

#if not useFakes:
#	
#	for sam in [mcfakesSample,Diboson3L0jSample,Diboson4LSample,Diboson2LSample,ttVSample,higgsSample,ttbarSample]:
#		#sam.setFileList(bgdFiles)
#		sam.setSuffixTreeName("_a_CENTRAL")
#		sam.addInputs(bgdFiles_a)
#		sam.setSuffixTreeName("_d_CENTRAL")
#		sam.addInputs(bgdFiles_d)
#		sam.setSuffixTreeName("_e_CENTRAL")
#		sam.addInputs(bgdFiles_e)
#else:
#	for sam in [wzdibosonSample,zzdibosonSample,tribosonSample,ttVSample,higgsSample,multitopSample]:
#		#sam.setFileList(bgdFiles)
#		sam.addInputs(bgdFiles)
#	#fakeSample.setFileList(fakeFiles)
#	fakeSample.addInputs(fakeFiles)
        

# Fits ----------------------------------------------------------------------------------------------------
#Binnings
crNBins	     = 1
crBinLow     = 0.5
crBinHigh    = 1.5

srNBins	  = 1
srBinLow  = 0.5
srBinHigh = 1.5

#---------------------------------------------------background only ------------------------------------------------
#
bkt = configMgr.addFitConfig("BkgOnly")
#Not sure what the following lines are doing, but nothing major, will keep them commented

#if useStat:
#	bkt.statErrThreshold = None #0.01
#else:
#	bkt.statErrThreshold=None

#*****************************************************************************************************************
#Adding systematics to the samples
if useFakes:			
	bkt.addSamples([tribosonSample,higgsSample,ttHSample,ttVSample,Diboson3L0jSample,Diboson3LLowHTSample,Diboson3LHighHTSample,Diboson4LSample,Diboson2LSample,singleTSample,ttbarSample,fakeSample,dataSample])
	
else:
	bkt.addSamples([tribosonSample,higgsSample,ttHSample,ttVSample,Diboson3L0jSample,Diboson3LLowHTSample,Diboson3LHighHTSample,Diboson4LSample,Diboson2LSample,mcfakesSample,singleTSample,ttbarSample,VgammaSample,dataSample])

if not noSysts:
	for sample in mconlySamples:	
		for syst in systList:
			bkt.getSample(sample).addSystematic(syst)

	for sample in normSamples:
                for syst in normSystList:
                        bkt.getSample(sample).addSystematic(syst)
		#for sample in mconlySamples:
                #        for syst in systList:
                #                bkt.getSample(sample).addSystematic(syst)

		#for sample in normSamples:
                #        for syst in normSystList:
                #                bkt.getSample(sample).addSystematic(syst)

meas=bkt.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.017) #Lumi uncertainties is here!
meas.addPOI("mu_SIG")
meas.addParamSetting("mu_BG",True,1)
        
#---------------------------------------------------------------------------------------------------- 3L CA fits
#LET'S START WITH THE FIT

CR_WZ_0jets = bkt.addChannel("cuts",["WZ_CR_0jets"],srNBins,srBinLow,srBinHigh)
CR_WZ_LowHT = bkt.addChannel("cuts",["WZ_CR_LowHT"],srNBins,srBinLow,srBinHigh)
CR_WZ_HighHT = bkt.addChannel("cuts",["WZ_CR_HighHT"],srNBins,srBinLow,srBinHigh)

if doFakeUncert :
	if not noSysts: 
		CR_WZ_0jets.getSample("Fakes").addSystematic(syst_FFstat_CRWZ_0j)
        	CR_WZ_0jets.getSample("Fakes").addSystematic(syst_FakeClosure_CR)

		CR_WZ_LowHT.getSample("Fakes").addSystematic(syst_FFstat_CRWZ_lowht)
                CR_WZ_LowHT.getSample("Fakes").addSystematic(syst_FakeClosure_CR)

		CR_WZ_HighHT.getSample("Fakes").addSystematic(syst_FFstat_CRWZ_highht)
                CR_WZ_HighHT.getSample("Fakes").addSystematic(syst_FakeClosure_CR)



SR1_Disc = bkt.addChannel("cuts",["SR1_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR1_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR1disc_QCD_muR)
	SR1_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR1disc_QCD_muR)
	SR1_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR1disc_QCD_muR)
	SR1_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR1disc_QCD_muR)

	SR1_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR1disc_QCD_muF)
        SR1_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR1disc_QCD_muF)
        SR1_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR1disc_QCD_muF)
        SR1_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR1disc_QCD_muF)

	SR1_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR1disc_QCD_muRmuF)
        SR1_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR1disc_QCD_muRmuF)
        SR1_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR1disc_QCD_muRmuF)
        SR1_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR1disc_QCD_muRmuF)

	SR1_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR1disc_PDF)
	SR1_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR1disc_PDF)
	SR1_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR1disc_PDF)
	SR1_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR1disc_PDF)
	
	SR1_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
 
	SR1_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)
	
	SR1_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR1_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR1_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR1_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

	SR1_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR1disc)
	SR1_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR2_Disc = bkt.addChannel("cuts",["SR2_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR2_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR2disc_QCD_muR)
        SR2_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR2disc_QCD_muR)
        SR2_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR2disc_QCD_muR)
        SR2_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR2disc_QCD_muR)

        SR2_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR2disc_QCD_muF)
        SR2_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR2disc_QCD_muF)
        SR2_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR2disc_QCD_muF)
        SR2_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR2disc_QCD_muF)

        SR2_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR2disc_QCD_muRmuF)
        SR2_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR2disc_QCD_muRmuF)
        SR2_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR2disc_QCD_muRmuF)
        SR2_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR2disc_QCD_muRmuF)

	SR2_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR2disc_PDF)
	SR2_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR2disc_PDF)
	SR2_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR2disc_PDF)
	SR2_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR2disc_PDF)

	SR2_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)
	
	SR2_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR2_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR2_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR2_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR2_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

	SR2_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR2disc)
	SR2_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR3_Disc = bkt.addChannel("cuts",["SR3_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR3_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR3disc_QCD_muR)
        SR3_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR3disc_QCD_muR)
        SR3_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR3disc_QCD_muR)
        SR3_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR3disc_QCD_muR)

        SR3_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR3disc_QCD_muF)
        SR3_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR3disc_QCD_muF)
        SR3_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR3disc_QCD_muF)
        SR3_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR3disc_QCD_muF)

        SR3_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR3disc_QCD_muRmuF)
        SR3_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR3disc_QCD_muRmuF)
        SR3_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR3disc_QCD_muRmuF)
        SR3_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR3disc_QCD_muRmuF)

	SR3_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

	SR3_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR3disc_PDF)
	SR3_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR3disc_PDF)
	SR3_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR3disc_PDF)
	SR3_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR3disc_PDF)

	SR3_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR3_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR3_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR3_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROn0jets_AltSample)
	SR3_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROn0jets_AltSample)

	SR3_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR3disc)
	SR3_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR4_Disc = bkt.addChannel("cuts",["SR4_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR4_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR4disc_QCD_muR)
        SR4_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR4disc_QCD_muR)
        SR4_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR4disc_QCD_muR)
        SR4_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR4disc_QCD_muR)

        SR4_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR4disc_QCD_muF)
        SR4_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR4disc_QCD_muF)
        SR4_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR4disc_QCD_muF)
        SR4_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR4disc_QCD_muF)

        SR4_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR4disc_QCD_muRmuF)
        SR4_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR4disc_QCD_muRmuF)
        SR4_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR4disc_QCD_muRmuF)
        SR4_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR4disc_QCD_muRmuF)

        SR4_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

	SR4_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR4disc_PDF)
	SR4_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR4disc_PDF)
	SR4_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR4disc_PDF)
	SR4_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR4disc_PDF)

	SR4_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR4_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR4_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR4_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR4_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

	SR4_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR4disc)
	SR4_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR5_Disc = bkt.addChannel("cuts",["SR5_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR5_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR5disc_QCD_muR)
        SR5_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR5disc_QCD_muR)
        SR5_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR5disc_QCD_muR)
        SR5_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR5disc_QCD_muR)

        SR5_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR5disc_QCD_muF)
        SR5_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR5disc_QCD_muF)
        SR5_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR5disc_QCD_muF)
        SR5_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR5disc_QCD_muF)

        SR5_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR5disc_QCD_muRmuF)
        SR5_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR5disc_QCD_muRmuF)
        SR5_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR5disc_QCD_muRmuF)
        SR5_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR5disc_QCD_muRmuF)

        SR5_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

	SR5_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR5disc_PDF)
	SR5_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR5disc_PDF)
	SR5_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR5disc_PDF)
	SR5_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR5disc_PDF)

	SR5_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR5_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR5_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR5_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR5_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

	SR5_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR5disc)
	SR5_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR6_Disc = bkt.addChannel("cuts",["SR6_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR6_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR6disc_QCD_muR)
        SR6_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR6disc_QCD_muR)
        SR6_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR6disc_QCD_muR)
        SR6_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR6disc_QCD_muR)

        SR6_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR6disc_QCD_muF)
        SR6_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR6disc_QCD_muF)
        SR6_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR6disc_QCD_muF)
        SR6_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR6disc_QCD_muF)

        SR6_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR6disc_QCD_muRmuF)
        SR6_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR6disc_QCD_muRmuF)
        SR6_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR6disc_QCD_muRmuF)
        SR6_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR6disc_QCD_muRmuF)

        SR6_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_flat)

	SR6_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR6disc_PDF)
	SR6_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR6disc_PDF)
	SR6_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR6disc_PDF)
	SR6_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR6disc_PDF)

	SR6_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR6_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR6_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR6_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SROnJets_AltSample)
	SR6_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SROnJets_AltSample)

	SR6_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR6disc)
	SR6_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR7_Disc = bkt.addChannel("cuts",["SR7_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR7_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR7disc_QCD_muR)
        SR7_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR7disc_QCD_muR)
        SR7_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR7disc_QCD_muR)
        SR7_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR7disc_QCD_muR)

        SR7_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR7disc_QCD_muF)
        SR7_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR7disc_QCD_muF)
        SR7_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR7disc_QCD_muF)
        SR7_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR7disc_QCD_muF)

        SR7_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR7disc_QCD_muRmuF)
        SR7_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR7disc_QCD_muRmuF)
        SR7_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR7disc_QCD_muRmuF)
        SR7_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR7disc_QCD_muRmuF)

	SR7_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR7disc_ME)
        SR7_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR7disc_PS)
        SR7_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR7disc_ISR)
        SR7_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR7disc_FSR)
        SR7_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR7disc_PDF)

	SR7_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR7disc_PDF)
	SR7_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR7disc_PDF)
	SR7_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR7disc_PDF)
	SR7_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR7disc_PDF)

	SR7_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR7_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR7_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR7_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR7_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

	SR7_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR7disc)
	SR7_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR8_Disc = bkt.addChannel("cuts",["SR8_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR8_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR8disc_QCD_muR)
        SR8_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR8disc_QCD_muR)
        SR8_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR8disc_QCD_muR)
        SR8_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR8disc_QCD_muR)

        SR8_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR8disc_QCD_muF)
        SR8_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR8disc_QCD_muF)
        SR8_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR8disc_QCD_muF)
        SR8_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR8disc_QCD_muF)

        SR8_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR8disc_QCD_muRmuF)
        SR8_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR8disc_QCD_muRmuF)
        SR8_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR8disc_QCD_muRmuF)
        SR8_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR8disc_QCD_muRmuF)

        SR8_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR8disc_ME)
        SR8_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR8disc_PS)
        SR8_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR8disc_ISR)
        SR8_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR8disc_FSR)
        SR8_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR8disc_PDF)

	SR8_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR8disc_PDF)
	SR8_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR8disc_PDF)
	SR8_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR8disc_PDF)
	SR8_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR8disc_PDF)

	SR8_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR8_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR8_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR8_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR8_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

	SR8_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR8disc)
	SR8_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)

SR9_Disc = bkt.addChannel("cuts",["SR9_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR9_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR9disc_QCD_muR)
        SR9_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR9disc_QCD_muR)
        SR9_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR9disc_QCD_muR)
        SR9_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR9disc_QCD_muR)

        SR9_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR9disc_QCD_muF)
        SR9_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR9disc_QCD_muF)
        SR9_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR9disc_QCD_muF)
        SR9_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR9disc_QCD_muF)

        SR9_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR9disc_QCD_muRmuF)
        SR9_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR9disc_QCD_muRmuF)
        SR9_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR9disc_QCD_muRmuF)
        SR9_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR9disc_QCD_muRmuF)

        SR9_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR9disc_ME)
        SR9_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR9disc_PS)
        SR9_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR9disc_ISR)
        SR9_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR9disc_FSR)
        SR9_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR9disc_PDF)

	SR9_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR9disc_PDF)
	SR9_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR9disc_PDF)
	SR9_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR9disc_PDF)
	SR9_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR9disc_PDF)

	SR9_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR9_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR9_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR9_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLow0jets_AltSample)
	SR9_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLow0jets_AltSample)

	SR9_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR9disc)
	SR9_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR10_Disc = bkt.addChannel("cuts",["SR10_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR10_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR10disc_QCD_muR)
        SR10_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR10disc_QCD_muR)
        SR10_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR10disc_QCD_muR)
        SR10_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR10disc_QCD_muR)

        SR10_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR10disc_QCD_muF)
        SR10_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR10disc_QCD_muF)
        SR10_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR10disc_QCD_muF)
        SR10_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR10disc_QCD_muF)

        SR10_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR10disc_QCD_muRmuF)
        SR10_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR10disc_QCD_muRmuF)
        SR10_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR10disc_QCD_muRmuF)
        SR10_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR10disc_QCD_muRmuF)

        SR10_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR10disc_ME)
        SR10_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR10disc_PS)
        SR10_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR10disc_ISR)
        SR10_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR10disc_FSR)
        SR10_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR10disc_PDF)

	SR10_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR10disc_PDF)
	SR10_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR10disc_PDF)
	SR10_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR10disc_PDF)
	SR10_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR10disc_PDF)

	SR10_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR10_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
	SR10_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
	SR10_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
	SR10_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

	SR10_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR10disc)
	SR10_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR11_Disc = bkt.addChannel("cuts",["SR11_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR11_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR11disc_QCD_muR)
        SR11_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR11disc_QCD_muR)
        SR11_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR11disc_QCD_muR)
        SR11_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR11disc_QCD_muR)

        SR11_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR11disc_QCD_muF)
        SR11_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR11disc_QCD_muF)
        SR11_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR11disc_QCD_muF)
        SR11_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR11disc_QCD_muF)

        SR11_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR11disc_QCD_muRmuF)
        SR11_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR11disc_QCD_muRmuF)
        SR11_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR11disc_QCD_muRmuF)
        SR11_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR11disc_QCD_muRmuF)

        SR11_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR11disc_ME)
        SR11_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR11disc_PS)
        SR11_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR11disc_ISR)
        SR11_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR11disc_FSR)
        SR11_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR11disc_PDF)

	SR11_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR11disc_PDF)
	SR11_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR11disc_PDF)
	SR11_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR11disc_PDF)
	SR11_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR11disc_PDF)

	SR11_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_flat)

	SR11_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
	SR11_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
	SR11_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRLowJets_AltSample)
	SR11_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRLowJets_AltSample)

	SR11_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR11disc)
	SR11_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SR)


SR12_Disc = bkt.addChannel("cuts",["SR12_Disc"],srNBins,srBinLow,srBinHigh)
if not noSysts and doThUncert:
	SR12_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR12disc_QCD_muR)
        SR12_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR12disc_QCD_muR)
        SR12_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR12disc_QCD_muR)
        SR12_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR12disc_QCD_muR)

        SR12_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR12disc_QCD_muF)
        SR12_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR12disc_QCD_muF)
        SR12_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR12disc_QCD_muF)
        SR12_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR12disc_QCD_muF)

        SR12_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR12disc_QCD_muRmuF)
        SR12_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR12disc_QCD_muRmuF)
        SR12_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR12disc_QCD_muRmuF)
        SR12_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR12disc_QCD_muRmuF)

        SR12_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR12disc_ME)
        SR12_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR12disc_PS)
        SR12_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR12disc_ISR)
        SR12_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR12disc_FSR)
        SR12_Disc.getSample("ttbar").addSystematic(syst_Theory_ttbar_SR12disc_PDF)

	SR12_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SR12disc_PDF)
	SR12_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SR12disc_PDF)
	SR12_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SR12disc_PDF)
	SR12_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SR12disc_PDF)

	SR12_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_SR12disc_QCD_muR)
	SR12_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_SR12disc_QCD_muF)
	SR12_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_SR12disc_QCD_muRmuF)
	SR12_Disc.getSample("VVV").addSystematic(syst_Theory_VVV_SR12disc_PDF)

	SR12_Disc.getSample("Dibosons_3L_0j").addSystematic(syst_Theory_WZ_SRDFOS_AltSample)
	SR12_Disc.getSample("Dibosons_3L_lowHT").addSystematic(syst_Theory_WZ_SRDFOS_AltSample)
	SR12_Disc.getSample("Dibosons_3L_highHT").addSystematic(syst_Theory_WZ_SRDFOS_AltSample)
	SR12_Disc.getSample("Dibosons_4L").addSystematic(syst_Theory_ZZ_SRDFOS_AltSample)

	SR12_Disc.getSample("Fakes").addSystematic(syst_FFstat_SR12disc)
	SR12_Disc.getSample("Fakes").addSystematic(syst_FakeClosure_SRDF)

bkt.setValidationChannels([
			SR1_Disc,
                        SR2_Disc,
			SR3_Disc,
                        SR4_Disc,
                        SR5_Disc,
                        SR6_Disc,
                        SR7_Disc,
                        SR8_Disc,
                        SR9_Disc,
                        SR10_Disc,
                        SR11_Disc,
                        SR12_Disc,
			])

#bkt.setValidationChannels([ttbar_VR,fakes_VR,WZ_VR_HighHT,WZ_VR_LowHT,WZ_VR_nJ0])
bkt.setBkgConstrainChannels([CR_WZ_0jets,CR_WZ_LowHT,CR_WZ_HighHT])


# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory

if configMgr.executeHistFactory:
	if os.path.isfile("data/%s.root"%configMgr.analysisName):
		os.remove("data/%s.root"%configMgr.analysisName)

