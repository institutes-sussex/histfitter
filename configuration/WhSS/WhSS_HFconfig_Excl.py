from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,TCanvas,TLegend,TLegendEntry
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from optparse import OptionParser
from copy import deepcopy

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
import ROOT
GeV = 1000.;

def removeWeight(oldList,oldWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    return newList

def replaceWeight(oldList,oldWeight,newWeight):
    newList = deepcopy(oldList)
    newList.remove(oldWeight)
    newList.append(newWeight)
    return newList

#-------------------------------
# Parse command line
#-------------------------------
myUserArgs= configMgr.userArg.split(' ')
myInputParser = OptionParser()
myInputParser.add_option('', '--point', dest = 'point', default = '')
myInputParser.add_option('', '--signalUncert', dest = 'signalUncert', default = 'Nom')
myInputParser.add_option('', '--SR', dest = 'SR', default = 'WZ')
myInputParser.add_option('', '--doFullGrid', dest = 'doFullGrid', default = 'True')
myInputParser.add_option('', '--useSysts', dest = 'useSysts', default = 'False')
myInputParser.add_option('', '--addFakes', dest = 'addFakes', default = 'True')
myInputParser.add_option('', '--doThUncert', dest = 'doThUncert', default = 'True')
myInputParser.add_option('', '--doFakeUncert', dest = 'doFakeUncert', default = 'True')
myInputParser.add_option('', '--ShapeFit', dest = 'ShapeFit', default = 'False')
myInputParser.add_option('', '--makePlots', dest = 'makePlots', default = 'False')
myInputParser.add_option('', '--runToys', dest = 'runToys', default = 'False')
myInputParser.add_option('', '--toyIndex', dest = 'toyIndex', default = '3')
myInputParser.add_option('', '--Name', dest = 'Name', default='EWK_Wh3L')

(options, args) = myInputParser.parse_args(myUserArgs)
whichPoint = options.point
whichSR = options.SR
signalXSec = options.signalUncert
name = options.Name
toyindex = options.toyIndex

#---------------------------------------
# Flags to control options
#---------------------------------------

fullGrid = False
if options.doFullGrid == "True" or options.doFullGrid == "true":
    fullGrid=True

doToys = False
if options.runToys == "True" or options.runToys == "true":
    doToys = True

useFakes = False
if options.addFakes == "True" or options.addFakes == "true":
    useFakes = True

doShapeFit = False
if options.ShapeFit == "True" or options.ShapeFit == "true":
    doShapeFit = True

doThUncert = False
if options.doThUncert == "True" or options.doThUncert == "true":
    doThUncert = True

doFakeUncert = False
if options.doFakeUncert == "True" or options.doFakeUncert == "true":
    doFakeUncert = True

noSysts = True
if options.useSysts == "True" or options.useSysts == "true":
    print "disabling systematics"
    noSysts = False

useStat = True
testSignalYield = False

print "########## Running Exclusion fit #############"


#-------------------------------
# Parameters for hypothesis test
#-------------------------------

# have been running with this commented out and set to false
# configMgr.doHypoTest = True
configMgr.nTOYs = 2000
if not doToys:
  configMgr.calculatorType = 2 # 0=toys, 2=asymptotic calc.
else:
  configMgr.calculatorType = 0 # 0=toys, 2=asymptotic calc.

configMgr.testStatType = 3
configMgr.nPoints = 20
# configMgr.scanRange = (1., 4.)
# configMgr.drawBeforeAfterFit = False
configMgr.doExclusion = True


configMgr.blindSR = True #CAUTION: SRs in background only fit are seen as VRs so for BkgOnly they are unblinded! FIXME need to unblind them ONE BY ONE
configMgr.blindCR = False
configMgr.blindVR = False 

#--------------------------------
# Now we start to build the model
#--------------------------------

# First define HistFactory attributes
if not noSysts:
  name = name + "_syst"

if not doToys:
  configMgr.analysisName = name
else:
  configMgr.analysisName = name + "_" + whichPoint + "_withToys_" + toyindex

configMgr.inputLumi = 1.  # Luminosity of input TTree
configMgr.outputLumi = 1.  # Luminosity required for output histograms

configMgr.setLumiUnits("fb-1")
configMgr.histCacheFile = "data/" + configMgr.analysisName + ".root"
configMgr.outputFileName = "results/" + configMgr.analysisName + "_Output.root"

configMgr.ReduceCorrMatrix = True #CorrMatrix to be given in a reduced version

# Set the files to read from
##bgdFiles_a = []
##bgdFiles_d = []
##bgdFiles_e = []
##dataFiles = []
##fakeFiles = []
##sigFiles_a = []
##sigFiles_d = []
##sigFiles_e = []

##ntupDir = ""

sigDir_a = "/mnt/lustre/scratch/epp_test/ma2008/SS3L/ntuples/SusySusx/v120_v2/mc16a_SigNewGrid_merged_withxsec/";
sigDir_d = "/mnt/lustre/scratch/epp_test/ma2008/SS3L/ntuples/SusySusx/v120_v2/mc16d_SigNewGrid_merged_withxsec/";
sigDir_e = "/mnt/lustre/scratch/epp_test/ma2008/SS3L/ntuples/SusySusx/v120_v2/mc16e_SigNewGrid_merged_withxsec/";

bkgDir_a = "/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16a_merged_withxsec_withvars_skimmed/";
bkgDir_d = "/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16d_merged_withxsec_withvars_skimmed/";
bkgDir_e = "/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16e_merged_withxsec_withvars_skimmed/";

dataDir16 = "/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data16_merged_withvars_skimmed/";
dataDir17 = "/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data17_merged_withvars_skimmed/";
dataDir18 = "/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data18_merged_withvars_skimmed/";

""""
bgd_SingleTop_File_a = ""
bgd_VVV_File_a = ""
bgd_ttV_File_a = ""
bgd_ttbar_File_a = ""
bgd_WJets_File_a = ""
bgd_VV_1L_File_a = ""
bgd_VV_2L_File_a = ""
bgd_VV_3L_File_a = ""
bgd_VV_4L_File_a = ""
bgd_ZJets_ee_File_a = ""
bgd_ZJets_mm_File_a = ""
bgd_ZJets_tt_File_a = ""

bgd_SingleTop_File_d = ""
bgd_VVV_File_d = ""
bgd_ttV_File_d = ""
bgd_ttbar_File_d = ""
bgd_WJets_File_d = ""
bgd_VV_1L_File_d = ""
bgd_VV_2L_File_d = ""
bgd_VV_3L_File_d = ""
bgd_VV_4L_File_d = ""
bgd_ZJets_ee_File_d = ""
bgd_ZJets_mm_File_d = ""
bgd_ZJets_tt_File_d = ""

bgd_SingleTop_File_e = ""
bgd_VVV_File_e = ""
bgd_ttV_File_e = ""
bgd_ttbar_File_e = ""
bgd_WJets_File_e = ""
bgd_VV_1L_File_e = ""
bgd_VV_2L_File_e = ""
bgd_VV_3L_File_e = ""
bgd_VV_4L_File_e = ""
bgd_ZJets_ee_File_e = ""
bgd_ZJets_mm_File_e = ""
bgd_ZJets_tt_File_e = ""

data_File_a = ""
data_File_d = ""
data_File_e = ""
"""

# Recycle histograms --------------------------- FIXME for first iteration we are gonna reproduce all histograms from scratch but will keep the lines here for later

#configMgr.useCacheToTreeFallback = True # enable the fallback to trees
#configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
#configMgr.histBackupCacheFile = "/lustre/scratch/epp/atlas/ft81/workdir/Limits/HF61/data/backup.root"

# Input files ----------------------------------------------------------------------------------------------------

##if configMgr.readFromTree or configMgr.useCacheToTreeFallback:
  ##bgdFiles_a.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_mca/L3_BG13TeV_a.root")
  ##bgdFiles_d.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_mcd/L3_BG13TeV_d.root")
  ##bgdFiles_e.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_mce/L3_BG13TeV_e.root")
  ##sigFiles_a.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_mca/L3_SMWZ13TeV_a.root")
  ##sigFiles_d.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_mcd/L3_SMWZ13TeV_d.root") 
  ##sigFiles_e.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_mce/L3_SMWZ13TeV_e.root") 
  ##dataFiles.append("/lustre/scratch/epp/atlas/bs336/WorkDir/HISTFitter/MakeHFT/Trees_data/L_DATA13TeV.root")
  ##fakeFiles.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_mca/L3_FakesFF13TeV.root")

bgd_SingleTop_File_a =  bkgDir_a + "SingleTop.root"
bgd_SingleTop_File_d = bkgDir_d + "SingleTop.root"
bgd_SingleTop_File_e = bkgDir_e + "SingleTop.root"
bgd_VVV_File_a = bkgDir_a + "VVV.root"
bgd_VVV_File_d = bkgDir_d + "VVV.root"
bgd_VVV_File_e = bkgDir_e + "VVV.root"
bgd_ttV_File_a = bkgDir_a + "ttV.root"
bgd_ttV_File_d = bkgDir_d + "ttV.root"
bgd_ttV_File_e = bkgDir_e + "ttV.root"
bgd_ttbar_File_a = bkgDir_a + "ttbar.root"
bgd_ttbar_File_d = bkgDir_d + "ttbar.root"
bgd_ttbar_File_e = bkgDir_e + "ttbar.root"
bgd_WJets_File_a = bkgDir_a + "WJets.root"
bgd_WJets_File_d = bkgDir_d + "WJets.root"
bgd_WJets_File_e = bkgDir_e + "WJets.root"
bgd_VV_1L_File_a = bkgDir_a + "VV_1L.root"
bgd_VV_1L_File_d = bkgDir_d + "VV_1L.root"
bgd_VV_1L_File_e = bkgDir_e + "VV_1L.root"
#bgd_VV_2L_File_a = bkgDir_a + "VV_2L.root"
#bgd_VV_2L_File_d = bkgDir_d + "VV_2L.root"
#bgd_VV_2L_File_e = bkgDir_e + "VV_2L.root"
bgd_VV_2L_OS_File_a = bkgDir_a + "VV_2L_OS.root"
bgd_VV_2L_OS_File_d = bkgDir_d + "VV_2L_OS.root"
bgd_VV_2L_OS_File_e = bkgDir_e + "VV_2L_OS.root"
bgd_VV_2L_SS_File_a = bkgDir_a + "VV_2L_SS.root"
bgd_VV_2L_SS_File_d = bkgDir_d + "VV_2L_SS.root"
bgd_VV_2L_SS_File_e = bkgDir_e + "VV_2L_SS.root"
bgd_VV_3L_File_a = bkgDir_a + "VV_3L.root"
bgd_VV_3L_File_d = bkgDir_d + "VV_3L.root"
bgd_VV_3L_File_e = bkgDir_e + "VV_3L.root"
bgd_VV_4L_File_a = bkgDir_a + "VV_4L.root"
bgd_VV_4L_File_d = bkgDir_d + "VV_4L.root"
bgd_VV_4L_File_e = bkgDir_e + "VV_4L.root"
bgd_ZJets_ee_File_a = bkgDir_a + "ZJets_ee.root"
bgd_ZJets_ee_File_d = bkgDir_d + "ZJets_ee.root"
bgd_ZJets_ee_File_e = bkgDir_e + "ZJets_ee.root"
bgd_ZJets_mm_File_a = bkgDir_a + "ZJets_mm.root"
bgd_ZJets_mm_File_d = bkgDir_d + "ZJets_mm.root"
bgd_ZJets_mm_File_e = bkgDir_e + "ZJets_mm.root"
bgd_ZJets_tt_File_a = bkgDir_a + "ZJets_tt.root"
bgd_ZJets_tt_File_d = bkgDir_d + "ZJets_tt.root"
bgd_ZJets_tt_File_e = bkgDir_e + "ZJets_tt.root"

data_File_a =  dataDir16 + "data16.root"
data_File_d =  dataDir17 + "data17.root"
data_File_e =  dataDir18 + "data18.root"

# Regions ----------------------------------------------------------------------------------------------------

print "Defining regions"

# common preselection for ALL the regions
configMgr.cutsDict["preselWhSS"]= "passSS3LTrig==1 && pass2lcut==1 && nCombLep==2 && nJets>=1 && nBjet_WP70==0 && isSS==1 && eT_miss>=50 && LepPt[0]>=25 && LepPt[1]>=25"
#configMgr.cutsDict["preselWhSS"]= "passSS3LTrig_noMET==1 && pass2lcut==1 && nCombLep==2 && nJets>=1 && nBjet_WP70==0 && isSS==1 && eT_miss>=50 && LepPt[0]>=25 && LepPt[1]>=25"

##########################
# SRs for Wh-SS analysis #
##########################

SR1a_cuts = "mt2>=80 && met_Sig>=7 && eT_miss>=75 && eT_miss<125 && mjj<350"
SR1b_cuts = "mt2>=80 && met_Sig>=7 && eT_miss>=125 && eT_miss<200 && mjj<350"
SR1c_cuts = "mt2>=80 && met_Sig>=7 && eT_miss>=200 && mjj<350"
SR2_cuts = "mt2<80 && min(mTl1,mTl2)>=100 && met_Sig>=6 && mjj<350"
SR3_cuts = "mt2<80 && min(mTl1,mTl2)<100 && met_Sig>=6 && mjj<350"

configMgr.cutsDict["SR1a_EE"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEE==1 && " + SR1a_cuts
configMgr.cutsDict["SR1a_EM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEM==1 && " + SR1a_cuts
configMgr.cutsDict["SR1a_MM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isMM==1 && " + SR1a_cuts
configMgr.cutsDict["SR1b_EE"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEE==1 && " + SR1b_cuts
configMgr.cutsDict["SR1b_EM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEM==1 && " + SR1b_cuts
configMgr.cutsDict["SR1b_MM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isMM==1 && " + SR1b_cuts
configMgr.cutsDict["SR1c_EE"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEE==1 && " + SR1c_cuts
configMgr.cutsDict["SR1c_EM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEM==1 && " + SR1c_cuts
configMgr.cutsDict["SR1c_MM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isMM==1 && " + SR1c_cuts
configMgr.cutsDict["SR2_EE"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEE==1 && " + SR2_cuts
configMgr.cutsDict["SR2_EM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEM==1 && " + SR2_cuts
configMgr.cutsDict["SR2_MM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isMM==1 && " + SR2_cuts
configMgr.cutsDict["SR3_EE"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEE==1 && " + SR3_cuts
configMgr.cutsDict["SR3_EM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isEM==1 && " + SR3_cuts
configMgr.cutsDict["SR3_MM"] = str(configMgr.cutsDict["preselWhSS"]) + " && isMM==1 && " + SR3_cuts

# Weights ----------------------------------------------------------------------------------------------------
if 'Nom' in signalXSec:
  weights = [ "EventWeight", "XSecWeight", "elecSF", "muonSF", "pileupweight", "jvtSF", "chargeFlipSF", "SS3LTriggerSF_noMET", "bjetSF_WP70" ]
  configMgr.weights = weights
elif 'up' in signalXSec:
  configMgr.weights = [ "eventweightUp" ]
elif 'down' in signalXSec:
  configMgr.weights = [ "eventweightDown" ]
configMgr.nomName = "NONE"

# Systematics ----------------------------------------------------------------------------------------------------
#FIXME no systematics yet for now. We will add them later on, leaving a couple of examples below for future references

####===> TREE BASED
#jets
##syst_jes_1_MC = Systematic("syst_jes_1","_CENTRAL","_JET_GroupedNP_1_UP","_JET_GroupedNP_1_DN","tree","histoSys")
##syst_jes_2_MC = Systematic("syst_jes_2","_CENTRAL","_JET_GroupedNP_2_UP","_JET_GroupedNP_2_DN","tree","histoSys")
##syst_jes_3_MC = Systematic("syst_jes_3","_CENTRAL","_JET_GroupedNP_3_UP","_JET_GroupedNP_3_DN","tree","histoSys")

#syst_jer_dataMC_MC = Systematic("syst_jer_dataMC","_CENTRAL","_JER_DataVsMC_MC16_UP","_JER_DataVsMC_MC16_DN","tree","histoSys")
#syst_jer_1_MC = Systematic("syst_jer_1","_CENTRAL","_JER_EffectiveNP_1_UP","_JER_EffectiveNP_1_DN","tree","histoSys")
#syst_jer_2_MC = Systematic("syst_jer_2","_CENTRAL","_JER_EffectiveNP_2_UP","_JER_EffectiveNP_2_DN","tree","histoSys")
#syst_jer_3_MC = Systematic("syst_jer_3","_CENTRAL","_JER_EffectiveNP_3_UP","_JER_EffectiveNP_3_DN","tree","histoSys")
#syst_jer_4_MC = Systematic("syst_jer_4","_CENTRAL","_JER_EffectiveNP_4_UP","_JER_EffectiveNP_4_DN","tree","histoSys")
#syst_jer_5_MC = Systematic("syst_jer_5","_CENTRAL","_JER_EffectiveNP_5_UP","_JER_EffectiveNP_5_DN","tree","histoSys")
#syst_jer_6_MC = Systematic("syst_jer_6","_CENTRAL","_JER_EffectiveNP_6_UP","_JER_EffectiveNP_6_DN","tree","histoSys")
#syst_jer_7_MC = Systematic("syst_jer_7","_CENTRAL","_JER_EffectiveNP_7restTerm_UP","_JER_EffectiveNP_7restTerm_DN","tree","histoSys")

##syst_jer_dataMC_MC = Systematic("syst_jer_dataMC","_CENTRAL","_JER_DataVsMC_MC16_UP","_CENTRAL","tree","histoSysOneSideSym")
##syst_jer_1_MC = Systematic("syst_jer_1","_CENTRAL","_JER_EffectiveNP_1_UP","_CENTRAL","tree","histoSysOneSideSym")
##syst_jer_2_MC = Systematic("syst_jer_2","_CENTRAL","_JER_EffectiveNP_2_UP","_CENTRAL","tree","histoSysOneSideSym")
##syst_jer_3_MC = Systematic("syst_jer_3","_CENTRAL","_JER_EffectiveNP_3_UP","_CENTRAL","tree","histoSysOneSideSym")
##syst_jer_4_MC = Systematic("syst_jer_4","_CENTRAL","_JER_EffectiveNP_4_UP","_CENTRAL","tree","histoSysOneSideSym")
##syst_jer_5_MC = Systematic("syst_jer_5","_CENTRAL","_JER_EffectiveNP_5_UP","_CENTRAL","tree","histoSysOneSideSym")
##syst_jer_6_MC = Systematic("syst_jer_6","_CENTRAL","_JER_EffectiveNP_6_UP","_CENTRAL","tree","histoSysOneSideSym")
##syst_jer_7_MC = Systematic("syst_jer_7","_CENTRAL","_JER_EffectiveNP_7restTerm_UP","_CENTRAL","tree","histoSysOneSideSym")

#MET
##syst_MET_SoftTrk_ResoPara_MC = Systematic("MET_SoftTrk_ResoPara","_CENTRAL","_MET_SoftTrk_ResoPara","_CENTRAL","tree","histoSysOneSideSym")
##syst_MET_SoftTrk_ResoPerp_MC = Systematic("MET_SoftTrk_ResoPerp","_CENTRAL","_MET_SoftTrk_ResoPerp","_CENTRAL","tree","histoSysOneSideSym")
##syst_MET_SoftTrk_Scale_MC = Systematic("MET_SoftTrk_Scale","_CENTRAL","_MET_SoftTrk_ScaleUp","_MET_SoftTrk_ScaleDown","tree","histoSys")

#Egamma
##syst_EG_Scale_MC = Systematic("EG_Scale","_CENTRAL","_EG_SCALE_ALL_UP","_EG_SCALE_ALL_DN","tree","histoSys")

#Muon
##syst_MuID_MC = Systematic("Muon_ID","_CENTRAL","_MUON_ID_UP","_MUON_ID_DN","tree","histoSys")
##syst_MuMS_MC = Systematic("Muon_MS","_CENTRAL","_MUON_MS_UP","_MUON_MS_DN","tree","histoSys")
##syst_MuScale_MC = Systematic("Muon_Scale","_CENTRAL","_MUON_SCALE_UP","_MUON_SCALE_DN","tree","histoSys")
##syst_MuSagResBias_MC = Systematic("Muon_Sag_Resbias","_CENTRAL","_MUON_SAGITTA_RESBIAS_UP","_MUON_SAGITTA_RESBIAS_DN","tree","histoSys")
##syst_MuSagRho_MC = Systematic("Muon_SagRho","_CENTRAL","_MUON_SAGITTA_RHO_UP","_MUON_SAGITTA_RHO_DN","tree","histoSys")

####===> WEIGHTS

#electrons
##syst_elecSF_EFF_Iso_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_up")
##syst_elecSF_EFF_Iso_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Iso_TOTAL_UncorrUncertainty_down")
##syst_elecSF_EFF_Iso = Systematic("syst_elecSF_Iso", configMgr.weights, syst_elecSF_EFF_Iso_UP, syst_elecSF_EFF_Iso_DOWN, 'weight', 'overallNormHistoSys')

##syst_elecSF_EFF_ID_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_ID_TOTAL_UncorrUncertainty_up")
##syst_elecSF_EFF_ID_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_ID_TOTAL_UncorrUncertainty_down")
##syst_elecSF_EFF_ID = Systematic("syst_elecSF_ID", configMgr.weights, syst_elecSF_EFF_ID_UP, syst_elecSF_EFF_ID_DOWN, 'weight', 'overallNormHistoSys')

##syst_elecSF_EFF_Reco_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_up")
##syst_elecSF_EFF_Reco_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_Reco_TOTAL_UncorrUncertainty_down")
##syst_elecSF_EFF_Reco = Systematic("syst_elecSF_Reco", configMgr.weights, syst_elecSF_EFF_Reco_UP, syst_elecSF_EFF_Reco_DOWN, 'weight', 'overallNormHistoSys')

##syst_elecSF_EFF_TriggerEff_UP = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_TriggerEff_up")
##syst_elecSF_EFF_TriggerEff_DOWN = replaceWeight(configMgr.weights, "ElecSF","syst_EL_EFF_TriggerEff_down")
##syst_elecSF_EFF_TriggerEff = Systematic("syst_elecSF_TrigEff", configMgr.weights, syst_elecSF_EFF_TriggerEff_UP, syst_elecSF_EFF_TriggerEff_DOWN, 'weight', 'overallNormHistoSys')

#muons
##syst_muonSF_EFF_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_up")
##syst_muonSF_EFF_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_down")
##syst_muonSF_EFF_Stat = Systematic("syst_muonSF_Eff_Stat", configMgr.weights, syst_muonSF_EFF_Stat_UP, syst_muonSF_EFF_Stat_DOWN, 'weight', 'overallNormHistoSys')

##syst_muonSF_EFF_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_up")
##syst_muonSF_EFF_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_down")
##syst_muonSF_EFF_Sys = Systematic("syst_muonSF_Eff_Sys", configMgr.weights, syst_muonSF_EFF_Sys_UP, syst_muonSF_EFF_Sys_DOWN, 'weight', 'overallNormHistoSys')

##syst_muonSF_EFF_Stat_lowPt_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_LOWPT_up")
##syst_muonSF_EFF_Stat_lowPt_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_STAT_LOWPT_down")
##syst_muonSF_EFF_Stat_lowPt = Systematic("syst_muonSF_Eff_Stat_lowPt", configMgr.weights, syst_muonSF_EFF_Stat_lowPt_UP, syst_muonSF_EFF_Stat_lowPt_DOWN, 'weight', 'overallNormHistoSys')

##syst_muonSF_EFF_Sys_lowPt_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_up")
##syst_muonSF_EFF_Sys_lowPt_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_EFF_SYS_down")
##syst_muonSF_EFF_Sys_lowPt = Systematic("syst_muonSF_Eff_Sys_lowPt", configMgr.weights, syst_muonSF_EFF_Sys_lowPt_UP, syst_muonSF_EFF_Sys_lowPt_DOWN, 'weight', 'overallNormHistoSys')

##syst_muonSF_ISO_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_STAT_up")
##syst_muonSF_ISO_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_STAT_down")
##syst_muonSF_ISO_Stat = Systematic("syst_muonSF_Iso_Stat", configMgr.weights, syst_muonSF_ISO_Stat_UP, syst_muonSF_ISO_Stat_DOWN, 'weight', 'overallNormHistoSys')

##syst_muonSF_ISO_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_SYS_up")
##syst_muonSF_ISO_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_ISO_SYS_down")
##syst_muonSF_ISO_Sys = Systematic("syst_muonSF_Iso_Sys", configMgr.weights, syst_muonSF_ISO_Sys_UP, syst_muonSF_ISO_Sys_DOWN, 'weight', 'overallNormHistoSys')

##syst_muonSF_TTVA_Stat_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_STAT_up")
##syst_muonSF_TTVA_Stat_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_STAT_down")
##syst_muonSF_TTVA_Stat = Systematic("syst_muonSF_TTVA_Stat", configMgr.weights, syst_muonSF_TTVA_Stat_UP, syst_muonSF_TTVA_Stat_DOWN, 'weight', 'overallNormHistoSys')

##syst_muonSF_TTVA_Sys_UP = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_SYS_up")
##syst_muonSF_TTVA_Sys_DOWN = replaceWeight(configMgr.weights, "MuonSF","syst_MUON_TTVA_SYS_down")
##syst_muonSF_TTVA_Sys = Systematic("syst_muonSF_TTVA_Sys", configMgr.weights, syst_muonSF_TTVA_Sys_UP, syst_muonSF_TTVA_Sys_DOWN, 'weight', 'overallNormHistoSys')

#Flavour tagging
##syst_FT_EFF_B_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_B_up")
##syst_FT_EFF_B_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_B_down")
##syst_FT_EFF_B = Systematic("syst_FT_Eff_B", configMgr.weights, syst_FT_EFF_B_UP, syst_FT_EFF_B_DOWN, 'weight', 'overallNormHistoSys')

##syst_FT_EFF_C_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_C_up")
##syst_FT_EFF_C_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_C_down")
##syst_FT_EFF_C = Systematic("syst_FT_Eff_C", configMgr.weights, syst_FT_EFF_C_UP, syst_FT_EFF_C_DOWN, 'weight', 'overallNormHistoSys')

##syst_FT_EFF_L_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_Light_up")
##syst_FT_EFF_L_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_Light_down")
##syst_FT_EFF_L = Systematic("syst_FT_Eff_L", configMgr.weights, syst_FT_EFF_L_UP, syst_FT_EFF_L_DOWN, 'weight', 'overallNormHistoSys')

##syst_FT_EFF_extrCharm_UP = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_extrapolationFromCharm_up")
##syst_FT_EFF_extrCharm_DOWN = replaceWeight(configMgr.weights, "bTagWeight","syst_FT_EFF_extrapolationFromCharm_down")
##syst_FT_EFF_extrCharm = Systematic("syst_FT_Eff_extrCharm", configMgr.weights, syst_FT_EFF_extrCharm_UP, syst_FT_EFF_extrCharm_DOWN, 'weight', 'overallNormHistoSys')

#jets
##syst_jvtSF_UP = replaceWeight(configMgr.weights, "JVTSF","syst_jvtSF_up")
##syst_jvtSF_DOWN = replaceWeight(configMgr.weights, "JVTSF","syst_jvtSF_down")
##syst_jvtSF = Systematic("syst_jvtSF", configMgr.weights, syst_jvtSF_UP, syst_jvtSF_DOWN, 'weight', 'overallNormHistoSys')


    
# theory syst 3L - WZ Uncorr
##syst_WZQCD_SR3LDFOS0j = Systematic("syst_WZQCD", 1., 1.0476, 0.9524, "user","userOverallSys")

# theory syst 3L - VVV
##syst_VVVQCD_SR3LDFOS0j = Systematic("syst_VVVQCD", 1., 1.0182, 0.9818, "user","userOverallSys")

# Fake syst        
##syst_FakeStat_SR3LDFOS0j = Systematic("syst_FakeStat_SR3LDFOS0j", 1., 1.4316, 0.5684, "user","userOverallSys")

##syst_FakeUncorr_SR3LDFOS0j = Systematic("syst_FakeUncorr_SR3LDFOS0j", 1., 1.7579, 0.2421, "user","userOverallSys")

##syst_FakeCorr_SR3LDFOS0j = Systematic("syst_FakeCorr", 1., 1.2, 0.8, "user","userOverallSys")

#Theory x-sec uncertainties
#syst_ttZxsec_UP = replaceWeight(configMgr.weights,"36.1","36.1*(1.+(runNumber>=410111 || runNumber<=410116)*0.12)")
#syst_ttZxsec_DN = replaceWeight(configMgr.weights,"36.1","36.1*(1.-(runNumber>=410111 || runNumber<=410116)*0.12)")
#syst_ttZxsec = Systematic("syst_ttZxsec", 1., syst_ttZxsec_UP, syst_ttZxsec_DN, "weight", "overallSys")

# WhSS overall 30% syst - same name to correlate them all for all backgrounds
syst_overall = Systematic("syst_30", 1., 1.15, 0.85, "user","userOverallSys")
#syst_overall = Systematic("syst_30", 1., 1.3, 0.7, "user","userOverallSys")

systList = [ syst_overall ]


#FIXME no syst now

#systList = [
# syst_jes_1_MC, 
# syst_jes_2_MC,
# syst_jes_3_MC,
# syst_jes_eta_MC,
#       syst_EG_res_MC,  
# syst_EG_scale_MC, 
# syst_MET_SoftTrk_ResoPara_MC,  
#   syst_MET_SoftTrk_ResoPerp_MC,    
# syst_MET_SoftTrk_Scale_MC,
#       syst_JER_MC,  
# syst_jvt_MC,
# syst_Muon_ID_MC,  
# syst_Muon_MS_MC, 
# syst_Muon_Scale_MC,
# syst_btagB_MC,   
# syst_btagC_MC,  
# syst_btagL_MC,
# syst_ftagExtrapo_MC, 
# syst_ftagExtrapoCharm_MC,
#       syst_ElecSF_ID_MC, 
# syst_ElecIsoSF_MC, 
# syst_ElecSF_Reco_MC,
#       syst_ElecTrig_MC, 
# syst_ElecTrigEff_MC,
#       syst_MuIsoStat_MC, 
# syst_MuIsoSys_MC,
#        syst_MuTTVAStat_MC, 
# syst_MuTTVASys_MC,
#        syst_MuEffStat_MC, 
# syst_MuEffSys_MC, 
#        syst_MuEffTrigStat_MC,
#        syst_MuEffStatLow_MC, 
# syst_MuEffSysLow_MC,
#        syst_PRW
# ]

##systList = [
##        syst_jes_1_MC,
##        syst_jes_2_MC,
##        syst_jes_3_MC,
##        syst_jer_dataMC_MC,
##        syst_jer_1_MC,
##        syst_jer_2_MC,
##        syst_jer_3_MC,
##        syst_jer_4_MC,
##        syst_jer_5_MC,
##        syst_jer_6_MC,
##        syst_jer_7_MC,
##        syst_MET_SoftTrk_ResoPara_MC,
##        syst_MET_SoftTrk_ResoPerp_MC,
##       syst_MET_SoftTrk_Scale_MC,
##        syst_EG_Scale_MC,
##        syst_MuID_MC,
##        syst_MuMS_MC,
##        syst_MuScale_MC,
##        syst_MuSagResBias_MC,
##        syst_MuSagRho_MC,
##        syst_elecSF_EFF_Iso,
##        syst_elecSF_EFF_ID,
##        syst_elecSF_EFF_Reco,
##        syst_elecSF_EFF_TriggerEff,
##        syst_muonSF_EFF_Stat,
##        syst_muonSF_EFF_Sys,
##        syst_muonSF_EFF_Stat_lowPt,
##        syst_muonSF_EFF_Sys_lowPt,
##        syst_muonSF_ISO_Stat,
##        syst_muonSF_ISO_Sys,
##        syst_muonSF_TTVA_Stat,
##        syst_muonSF_TTVA_Sys,
##        syst_FT_EFF_B,
##        syst_FT_EFF_C,
##        syst_FT_EFF_L,
##        syst_FT_EFF_extrCharm,
##        syst_jvtSF
##  ]
  
##sig_systList = [
##  syst_jes_1_MC,
##  syst_jes_2_MC,
##  syst_jes_3_MC,
##  #syst_jer_dataMC_MC,
##  syst_jer_1_MC,
##  syst_jer_2_MC,
##  syst_jer_3_MC,
##        syst_jer_4_MC,
##  syst_jer_5_MC,
##        syst_jer_6_MC,
##  syst_jer_7_MC,
##        syst_MET_SoftTrk_ResoPara_MC,  
##        syst_MET_SoftTrk_ResoPerp_MC,    
##        syst_MET_SoftTrk_Scale_MC,
##  syst_EG_Scale_MC,
##  syst_MuID_MC,
##  syst_MuMS_MC,
##  syst_MuScale_MC,
##  syst_MuSagResBias_MC,
##  syst_MuSagRho_MC,
##  syst_elecSF_EFF_Iso,
##        syst_elecSF_EFF_ID,
##        syst_elecSF_EFF_Reco,
##        syst_elecSF_EFF_TriggerEff,
##        syst_muonSF_EFF_Stat,
##        syst_muonSF_EFF_Sys,
##        syst_muonSF_EFF_Stat_lowPt,
##        syst_muonSF_EFF_Sys_lowPt,
##        syst_muonSF_ISO_Stat,
##        syst_muonSF_ISO_Sys,
##        syst_muonSF_TTVA_Stat,
##        syst_muonSF_TTVA_Sys,
##        syst_FT_EFF_B,
##        syst_FT_EFF_C,
##        syst_FT_EFF_L,
##        syst_FT_EFF_extrCharm,
##        syst_jvtSF
##  ] 


# WhSS configuration ----------------------------------------------------------------------------------------------------
#FIXME check naming convention

SingleTop_Sample = Sample( "SingleTop", kGreen+9 )
SingleTop_Sample.setStatConfig( useStat )
#SingleTop_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#SingleTop_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
SingleTop_Sample.setNormByTheory()
SingleTop_Sample.addInput( bgd_SingleTop_File_a, "HFntuple" )
SingleTop_Sample.addInput( bgd_SingleTop_File_d, "HFntuple" )
SingleTop_Sample.addInput( bgd_SingleTop_File_e, "HFntuple" )

VVV_Sample = Sample( "VVV", kCyan-5 )
VVV_Sample.setStatConfig( useStat )
#VVV_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#VVV_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
VVV_Sample.setNormByTheory()
VVV_Sample.addInput( bgd_VVV_File_a, "HFntuple" )
VVV_Sample.addInput( bgd_VVV_File_d, "HFntuple" )
VVV_Sample.addInput( bgd_VVV_File_e, "HFntuple" )

ttV_Sample = Sample( "ttV", kCyan-1 )
ttV_Sample.setStatConfig( useStat )
#ttV_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#ttV_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
ttV_Sample.setNormByTheory()
ttV_Sample.addInput( bgd_ttV_File_a, "HFntuple" )
ttV_Sample.addInput( bgd_ttV_File_d, "HFntuple" )
ttV_Sample.addInput( bgd_ttV_File_e, "HFntuple" )

ttbar_Sample = Sample( "ttbar", kMagenta-5 )
ttbar_Sample.setStatConfig( useStat )
#ttbar_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#ttbar_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
ttbar_Sample.setNormByTheory()
ttbar_Sample.addInput( bgd_ttbar_File_a, "HFntuple" )
ttbar_Sample.addInput( bgd_ttbar_File_d, "HFntuple" )
ttbar_Sample.addInput( bgd_ttbar_File_e, "HFntuple" )

WJets_Sample = Sample( "WJets", kGray )
WJets_Sample.setStatConfig( useStat )
#WJets_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#WJets_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
WJets_Sample.setNormByTheory()
WJets_Sample.addInput( bgd_WJets_File_a, "HFntuple" )
WJets_Sample.addInput( bgd_WJets_File_d, "HFntuple" )
WJets_Sample.addInput( bgd_WJets_File_e, "HFntuple" )

VV_1L_Sample = Sample( "VV_1L", kCyan-6 )
VV_1L_Sample.setStatConfig( useStat )
#VV_1L_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#VV_1L_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
VV_1L_Sample.setNormByTheory()
VV_1L_Sample.addInput( bgd_VV_1L_File_a, "HFntuple" )
VV_1L_Sample.addInput( bgd_VV_1L_File_d, "HFntuple" )
VV_1L_Sample.addInput( bgd_VV_1L_File_e, "HFntuple" )

#VV_2L_Sample = Sample( "VV_2L", kAzure+4 )
#VV_2L_Sample.setStatConfig( useStat )
##VV_2L_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
##VV_2L_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
#VV_2L_Sample.setNormByTheory()
#VV_2L_Sample.addInput( bgd_VV_2L_File_a, "HFntuple" )
#VV_2L_Sample.addInput( bgd_VV_2L_File_d, "HFntuple" )
#VV_2L_Sample.addInput( bgd_VV_2L_File_e, "HFntuple" )

VV_2L_OS_Sample = Sample( "VV_2L_OS", kAzure+4 )
VV_2L_OS_Sample.setStatConfig( useStat )
#VV_2L_OS_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#VV_2L_OS_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
VV_2L_OS_Sample.setNormByTheory()
VV_2L_OS_Sample.addInput( bgd_VV_2L_OS_File_a, "HFntuple" )
VV_2L_OS_Sample.addInput( bgd_VV_2L_OS_File_d, "HFntuple" )
VV_2L_OS_Sample.addInput( bgd_VV_2L_OS_File_e, "HFntuple" )

VV_2L_SS_Sample = Sample( "VV_2L_SS", kAzure+4 )
VV_2L_SS_Sample.setStatConfig( useStat )
VV_2L_SS_Sample.setNormFactor("mu_WWSS",1.5,1.4,1.6,True)
#VV_2L_SS_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
VV_2L_SS_Sample.setNormByTheory()
VV_2L_SS_Sample.addInput( bgd_VV_2L_SS_File_a, "HFntuple" )
VV_2L_SS_Sample.addInput( bgd_VV_2L_SS_File_d, "HFntuple" )
VV_2L_SS_Sample.addInput( bgd_VV_2L_SS_File_e, "HFntuple" )

VV_3L_Sample = Sample( "VV_3L", kOrange-3 )
VV_3L_Sample.setStatConfig( useStat )
#VV_3L_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#VV_3L_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
VV_3L_Sample.setNormByTheory()
VV_3L_Sample.addInput( bgd_VV_3L_File_a, "HFntuple" )
VV_3L_Sample.addInput( bgd_VV_3L_File_d, "HFntuple" )
VV_3L_Sample.addInput( bgd_VV_3L_File_e, "HFntuple" )

VV_4L_Sample = Sample( "VV_4L", kYellow-3 )
VV_4L_Sample.setStatConfig( useStat )
#VV_4L_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#VV_4L_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
VV_4L_Sample.setNormByTheory()
VV_4L_Sample.addInput( bgd_VV_4L_File_a, "HFntuple" )
VV_4L_Sample.addInput( bgd_VV_4L_File_d, "HFntuple" )
VV_4L_Sample.addInput( bgd_VV_4L_File_e, "HFntuple" )

ZJets_ee_Sample = Sample( "ZJets_ee", kRed-7 )
ZJets_ee_Sample.setStatConfig( useStat )
#ZJets_ee_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#ZJets_ee_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
ZJets_ee_Sample.setNormByTheory()
ZJets_ee_Sample.addInput( bgd_ZJets_ee_File_a, "HFntuple" )
ZJets_ee_Sample.addInput( bgd_ZJets_ee_File_d, "HFntuple" )
ZJets_ee_Sample.addInput( bgd_ZJets_ee_File_e, "HFntuple" )

ZJets_mm_Sample = Sample( "ZJets_mm", kRed-7 )
ZJets_mm_Sample.setStatConfig( useStat )
#ZJets_mm_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#ZJets_mm_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
ZJets_mm_Sample.setNormByTheory()
ZJets_mm_Sample.addInput( bgd_ZJets_mm_File_a, "HFntuple" )
ZJets_mm_Sample.addInput( bgd_ZJets_mm_File_d, "HFntuple" )
ZJets_mm_Sample.addInput( bgd_ZJets_mm_File_e, "HFntuple" )

ZJets_tt_Sample = Sample( "ZJets_tt", kRed-7 )
ZJets_tt_Sample.setStatConfig( useStat )
#ZJets_tt_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#ZJets_tt_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
ZJets_tt_Sample.setNormByTheory()
ZJets_tt_Sample.addInput( bgd_ZJets_tt_File_a, "HFntuple" )
ZJets_tt_Sample.addInput( bgd_ZJets_tt_File_d, "HFntuple" )
ZJets_tt_Sample.addInput( bgd_ZJets_tt_File_e, "HFntuple" )

# ----------------------------------------
# TotSM sample
TotSM_Sample = Sample( "TotSM", kGreen+9 )
TotSM_Sample.setStatConfig( useStat )
#TotSM_Sample.setNormFactor("mu_WZSF_0j",1.,0.,5.)
#TotSM_Sample.setNormRegions([("WZ_CR_0jets","cuts")])
TotSM_Sample.setNormByTheory()
TotSM_Sample.addInput( bgd_SingleTop_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_SingleTop_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_SingleTop_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_VVV_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_VVV_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_VVV_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_ttV_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_ttV_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_ttV_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_ttbar_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_ttbar_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_ttbar_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_WJets_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_WJets_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_WJets_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_1L_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_1L_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_1L_File_e, "HFntuple" )
#TotSM_Sample.addInput( bgd_VV_2L_File_a, "HFntuple" )
#TotSM_Sample.addInput( bgd_VV_2L_File_d, "HFntuple" )
#TotSM_Sample.addInput( bgd_VV_2L_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_2L_OS_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_2L_OS_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_2L_OS_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_2L_SS_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_2L_SS_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_2L_SS_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_3L_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_3L_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_3L_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_4L_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_4L_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_VV_4L_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_ee_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_ee_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_ee_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_mm_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_mm_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_mm_File_e, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_tt_File_a, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_tt_File_d, "HFntuple" )
TotSM_Sample.addInput( bgd_ZJets_tt_File_e, "HFntuple" )


##if not useFakes:
         
##  allmcSamples=['Dibosons_3L_0j','Dibosons_3L_lowHT','Dibosons_3L_highHT','Dibosons_4L','Dibosons_2L','MCFakes','VVV','Higgs','ttV', 'ttbar','SingleT']
##  mconlySamples=['Dibosons_4L','Dibosons_2L','ZZ','VVV','Higgs','ttV', 'ttbar','SingleT']

##  mcfakesSample=Sample("MCFakes", kMagenta+6)
##        mcfakesSample.setStatConfig(useStat)
##        mcfakesSample.setNormByTheory()
##  mcfakesSample.addInputs(bgdFiles_a,"Zjets_a")
##  mcfakesSample.addInputs(bgdFiles_d,"ZZ_d")
##  mcfakesSample.addInputs(bgdFiles_e,"Zjets_e")

##if useFakes:
##  
##  allmcSamples=['Dibosons_3L_0j','Dibosons_3L_lowHT','Dibosons_3L_highHT','Dibosons_4L','Dibosons_2L','VVV','Higgs','ttV', 'ttbar','SingleT']
##        mconlySamples=['Dibosons_4L','Dibosons_2L','ZZ','VVV','Higgs','ttV', 'ttbar','SingleT']
##        normSamples=['WZ']
##
##        fakeSample=Sample("Fakes",kMagenta+6)
##        fakeSample.setNormByTheory()
##        fakeSample.setStatConfig(False)
##        #fakeSample.addSampleSpecificWeight(")
##        fakeSample.addInputs(fakeFiles)

# Data setup ----------------------------------------------------------------------------------------------------

data_Sample = Sample( "Data", kBlack)
data_Sample.setData()
###dataSample.setFileList(dataFiles)
##dataSample.addInputs(dataFiles)
data_Sample.addInput( data_File_a, "HFntuple" )
data_Sample.addInput( data_File_d, "HFntuple" )
data_Sample.addInput( data_File_e, "HFntuple" )

#dataSample.buildHisto([0.01], "SR3LDFOS0j", "cuts")
#dataSample.buildHisto([0.01], "SR3LSFOS0ja", "cuts")

# ----------------------------------------------------------------------------------------------------

all_Samples = [
  SingleTop_Sample,
  VVV_Sample,
  ttV_Sample,
  ttbar_Sample,
  WJets_Sample,
  VV_1L_Sample,
  VV_2L_OS_Sample,
  VV_2L_SS_Sample,
  VV_3L_Sample,
  VV_4L_Sample,
  ZJets_ee_Sample,
  ZJets_mm_Sample,
  ZJets_tt_Sample,
  data_Sample
]
#all_Samples = [ TotSM_Sample, data_Sample ]

all_mcSamples = [
  SingleTop_Sample,
  VVV_Sample,
  ttV_Sample,
  ttbar_Sample,
  WJets_Sample,
  VV_1L_Sample,
  VV_2L_OS_Sample,
  VV_2L_SS_Sample,
  VV_3L_Sample,
  VV_4L_Sample,
  ZJets_ee_Sample,
  ZJets_mm_Sample,
  ZJets_tt_Sample
]
#all_mcSamples = [ TotSM_Sample ]

# ----------------------------------------------------------------------------------------------------
#Here we are basically saying which backgrounds we want to include in the analysis, adding the input from the files

#if not useFakes:
# 
# for sam in [mcfakesSample,Diboson3L0jSample,Diboson4LSample,Diboson2LSample,ttVSample,higgsSample,ttbarSample]:
#   #sam.setFileList(bgdFiles)
#   sam.setSuffixTreeName("_a_CENTRAL")
#   sam.addInputs(bgdFiles_a)
#   sam.setSuffixTreeName("_d_CENTRAL")
#   sam.addInputs(bgdFiles_d)
#   sam.setSuffixTreeName("_e_CENTRAL")
#   sam.addInputs(bgdFiles_e)
#else:
# for sam in [wzdibosonSample,zzdibosonSample,tribosonSample,ttVSample,higgsSample,multitopSample]:
#   #sam.setFileList(bgdFiles)
#   sam.addInputs(bgdFiles)
# #fakeSample.setFileList(fakeFiles)
# fakeSample.addInputs(fakeFiles)
        

# Fits ----------------------------------------------------------------------------------------------------
#Binnings
crNBins      = 1
crBinLow     = 0.5
crBinHigh    = 1.5

srNBins   = 1
srBinLow  = 0.5
srBinHigh = 1.5

#---------------------------------------------------background only ------------------------------------------------
#
bkt = configMgr.addFitConfig("BkgOnly")
#Not sure what the following lines are doing, but nothing major, will keep them commented

#if useStat:
# bkt.statErrThreshold = None #0.01
#else:
# bkt.statErrThreshold=None

#*****************************************************************************************************************
#Adding systematics to the samples
##if useFakes:      
##  bkt.addSamples([tribosonSample,higgsSample,ttVSample,Diboson3L0jSample,Diboson3LLowHTSample,Diboson3LHighHTSample,Diboson4LSample,Diboson2LSample,fakeSample,singleTSample,ttbarSample,dataSample]) 
  
##else:
##  bkt.addSamples([tribosonSample,higgsSample,ttVSample,Diboson3L0jSample,Diboson3LLowHTSample,Diboson3LHighHTSample,Diboson4LSample,Diboson2LSample,mcfakesSample,singleTSample,ttbarSample,dataSample])

bkt.addSamples( all_Samples )

if not noSysts:
  for sample in all_mcSamples:  
    for syst in systList:
      bkt.getSample(sample.name).addSystematic(syst)

    #for sample in mconlySamples:
                #        for syst in systList:
                #                bkt.getSample(sample).addSystematic(syst)

    #for sample in normSamples:
                #        for syst in normSystList:
                #                bkt.getSample(sample).addSystematic(syst)

meas = bkt.addMeasurement( name="NormalMeasurement", lumi=1.0, lumiErr=0.017) #Lumi uncertainties is here!
meas.addPOI("mu_SIG")
meas.addParamSetting("mu_BG",True,1)
        
#---------------------------------------------------------------------------------------------------- 3L CA fits
#LET'S START WITH THE FIT

##CR_WZ_0jets = bkt.addChannel("cuts",["WZ_CR_0jets"],srNBins,srBinLow,srBinHigh)
##CR_WZ_LowHT = bkt.addChannel("cuts",["WZ_CR_LowHT"],srNBins,srBinLow,srBinHigh)
##CR_WZ_HighHT = bkt.addChannel("cuts",["WZ_CR_HighHT"],srNBins,srBinLow,srBinHigh)
#if doFakeUncert :
  #if not noSysts:  CR3LonZ.getSample("Fakes").addSystematic(syst_FakeUncorr_CRonZhigh)

######################
# Validation Regions #
######################

#WZ_VR_HighHT = bkt.addChannel("cuts",["WZ_VR_HighHT"],srNBins,srBinLow,srBinHigh)
#WZ_VR_LowHT = bkt.addChannel("cuts",["WZ_VR_LowHT"],srNBins,srBinLow,srBinHigh)
#WZ_VR_nJ0 = bkt.addChannel("cuts",["WZ_VR_nJ0"],srNBins,srBinLow,srBinHigh)
#ttbar_VR = bkt.addChannel("cuts",["top_VR"],srNBins,srBinLow,srBinHigh)
#ttbar_VRAl = bkt.addChannel("cuts",["top_VRAl"],srNBins,srBinLow,srBinHigh)
#fakes_VR = bkt.addChannel("cuts",["fakes_VR"],srNBins,srBinLow,srBinHigh)

##################
# Signal Regions #
##################

## adding WhSS SR channels

SR1a_EE = bkt.addChannel( "cuts", ["SR1a_EE"], srNBins, srBinLow, srBinHigh )
SR1a_EE.blind = True #REALLY REALLY REALLY IMPORTANT

SR1a_EM = bkt.addChannel( "cuts", ["SR1a_EM"], srNBins, srBinLow, srBinHigh )
SR1a_EM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1a_MM = bkt.addChannel( "cuts", ["SR1a_MM"], srNBins, srBinLow, srBinHigh )
SR1a_MM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1b_EE = bkt.addChannel( "cuts", ["SR1b_EE"], srNBins, srBinLow, srBinHigh )
SR1b_EE.blind = True #REALLY REALLY REALLY IMPORTANT

SR1b_EM = bkt.addChannel( "cuts", ["SR1b_EM"], srNBins, srBinLow, srBinHigh )
SR1b_EM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1b_MM = bkt.addChannel( "cuts", ["SR1b_MM"], srNBins, srBinLow, srBinHigh )
SR1b_MM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1c_EE = bkt.addChannel( "cuts", ["SR1c_EE"], srNBins, srBinLow, srBinHigh )
SR1c_EE.blind = True #REALLY REALLY REALLY IMPORTANT

SR1c_EM = bkt.addChannel( "cuts", ["SR1c_EM"], srNBins, srBinLow, srBinHigh )
SR1c_EM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1c_MM = bkt.addChannel( "cuts", ["SR1c_MM"], srNBins, srBinLow, srBinHigh )
SR1c_MM.blind = True #REALLY REALLY REALLY IMPORTANT

SR2_EE = bkt.addChannel( "cuts", ["SR2_EE"], srNBins, srBinLow, srBinHigh )
SR2_EE.blind = True #REALLY REALLY REALLY IMPORTANT

SR2_EM = bkt.addChannel( "cuts", ["SR2_EM"], srNBins, srBinLow, srBinHigh )
SR2_EM.blind = True #REALLY REALLY REALLY IMPORTANT

SR2_MM = bkt.addChannel( "cuts", ["SR2_MM"], srNBins, srBinLow, srBinHigh )
SR2_MM.blind = True #REALLY REALLY REALLY IMPORTANT

#SR3_EE = bkt.addChannel( "cuts", ["SR3_EE"], srNBins, srBinLow, srBinHigh )
#SR3_EE.blind = True #REALLY REALLY REALLY IMPORTANT

#SR3_EM = bkt.addChannel( "cuts", ["SR3_EM"], srNBins, srBinLow, srBinHigh )
#SR3_EM.blind = True #REALLY REALLY REALLY IMPORTANT

#SR3_MM = bkt.addChannel( "cuts", ["SR3_MM"], srNBins, srBinLow, srBinHigh )
#SR3_MM.blind = True #REALLY REALLY REALLY IMPORTANT

all_SRs = [
  SR1a_EE,
  SR1a_EM,
  SR1a_MM,
  SR1b_EE,
  SR1b_EM,
  SR1b_MM,
  SR1c_EE,
  SR1c_EM,
  SR1c_MM,
  SR2_EE,
  SR2_EM,
  SR2_MM#,
  #SR3_EE,
  #SR3_EM,
  #SR3_MM
]

bkt.setSignalChannels( all_SRs )

##bkt.setBkgConstrainChannels([CR_WZ_0jets,CR_WZ_LowHT,CR_WZ_HighHT])

sig_Samples = []

if not fullGrid:
  sig_Samples = [ whichPoint ]
  print' not full grid, selecting one sample'

else:
  sig_Samples = [
    'C1N2_Wh_hall_150p0_0p0_2L7',
    'C1N2_Wh_hall_152p5_22p5_2L7',
    'C1N2_Wh_hall_162p5_12p5_2L7',
    'C1N2_Wh_hall_165p0_35p0_2L7',
    'C1N2_Wh_hall_175p0_0p0_2L7',
    'C1N2_Wh_hall_175p0_25p0_2L7',
    'C1N2_Wh_hall_177p5_47p5_2L7',
    'C1N2_Wh_hall_187p5_12p5_2L7',
    'C1N2_Wh_hall_187p5_37p5_2L7',
    'C1N2_Wh_hall_190p0_60p0_2L7',
    'C1N2_Wh_hall_200p0_0p0_2L7',
    'C1N2_Wh_hall_200p0_25p0_2L7',
    'C1N2_Wh_hall_200p0_50p0_2L7',
    'C1N2_Wh_hall_202p5_72p5_2L7',
    'C1N2_Wh_hall_212p5_37p5_2L7',
    'C1N2_Wh_hall_212p5_62p5_2L7',
    'C1N2_Wh_hall_225p0_0p0_2L7',
    'C1N2_Wh_hall_225p0_25p0_2L7',
    'C1N2_Wh_hall_225p0_50p0_2L7',
    'C1N2_Wh_hall_225p0_75p0_2L7',
    'C1N2_Wh_hall_250p0_0p0_2L7',
    'C1N2_Wh_hall_250p0_100p0_2L7',
    'C1N2_Wh_hall_250p0_25p0_2L7',
    'C1N2_Wh_hall_250p0_50p0_2L7',
    'C1N2_Wh_hall_250p0_75p0_2L7',
    'C1N2_Wh_hall_275p0_0p0_2L7',
    'C1N2_Wh_hall_275p0_25p0_2L7',
    'C1N2_Wh_hall_275p0_50p0_2L7',
    'C1N2_Wh_hall_275p0_75p0_2L7',
    'C1N2_Wh_hall_300p0_0p0_2L7',
    'C1N2_Wh_hall_300p0_100p0_2L7',
    'C1N2_Wh_hall_300p0_25p0_2L7',
    'C1N2_Wh_hall_300p0_50p0_2L7',
    'C1N2_Wh_hall_300p0_75p0_2L7',
    'C1N2_Wh_hall_325p0_0p0_2L7',
    'C1N2_Wh_hall_325p0_50p0_2L7',
    'C1N2_Wh_hall_350p0_0p0_2L7',
    'C1N2_Wh_hall_350p0_100p0_2L7',
    'C1N2_Wh_hall_350p0_25p0_2L7',
    'C1N2_Wh_hall_350p0_50p0_2L7',
    'C1N2_Wh_hall_350p0_75p0_2L7',
    'C1N2_Wh_hall_375p0_0p0_2L7',
    'C1N2_Wh_hall_375p0_50p0_2L7',
    'C1N2_Wh_hall_400p0_0p0_2L7',
    'C1N2_Wh_hall_400p0_25p0_2L7',
    'C1N2_Wh_hall_425p0_0p0_2L7'
  ]

print 'testing signal samples'
print sig_Samples

for sig in sig_Samples:
  myTopLvl = configMgr.addFitConfigClone( bkt, "%s"%sig )
  sig_Sample = Sample( sig, kBlue )
  sig_Sample.addInput( sigDir_a + sig + ".root", "HFntuple" )
  sig_Sample.addInput( sigDir_d + sig + ".root", "HFntuple" )
  sig_Sample.addInput( sigDir_e + sig + ".root", "HFntuple" )
  sig_Sample.setNormByTheory()
  sig_Sample.setNormFactor('mu_SIG',1.,0.,5.)
  sig_Sample.setStatConfig(useStat)

##  if not noSysts:
##          for syst in sig_systList:
##                  sigSample.addSystematic(syst)
  
  myTopLvl.addSamples( sig_Sample )
  myTopLvl.setSignalSample( sig_Sample )

  myTopLvl.setSignalChannels( all_SRs )

# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory

if configMgr.executeHistFactory:
  if os.path.isfile("data/%s.root"%configMgr.analysisName):
    os.remove("data/%s.root"%configMgr.analysisName)
