#!/usr/bin/env python

import pickle, os

import ROOT
from ROOT import *

def get_integral(syst, overflow=False, underflow=False) :

    hist = h[syst]
    nbins = hist.GetNbinsX()
    fbin = 1
    if underflow:
        fbin = 0
    if overflow:
        nbins += 1
    return hist.Integral(fbin, nbins)

ROOT.gROOT.LoadMacro("~/atlasstyle/AtlasStyle.C")
ROOT.SetAtlasStyle()
ROOT.gROOT.LoadMacro("~/atlasstyle/AtlasLabels.C")

f = open("Systtest_WZ.pickle","rb")
t = pickle.load(f)

print t

c = TCanvas("systematics","systematics",800,600)


n_channels = t.keys()

n_channels = sorted(n_channels)

h_totsyst = TH1F("h_totsyst","h_totsyst", len(n_channels), 0, len(n_channels))

list_of_syst = ['MC Stats','Experimental','Modelling','Fakes','Normalisation']

h={}
for syst in list_of_syst:
    h[syst] = TH1F(syst,syst, len(n_channels), 0, len(n_channels))

i=1
for key in n_channels:    
    h_totsyst.SetBinContent(i,t[key]['totsyserr']/t[key]['nfitted'])
    for syst in list_of_syst:
        h[syst].SetBinContent(i,t[key]['syserr_'+syst]/t[key]['nfitted'])   
    h_totsyst.GetXaxis().SetBinLabel(i, key.replace("_cuts",""))
    i=i+1
    
    
h_totsyst.SetFillColor(kCyan-10)
h_totsyst.SetLineColor(kBlack)
h_totsyst.SetLineWidth(3)
h_totsyst.LabelsOption("v","x")

h_totsyst.SetAxisRange(0.,0.9,"Y")

h_totsyst.GetYaxis().SetTitle('Relative uncertainty')

h_totsyst.Draw()

colors = [kOrange,kRed,kGreen,kBlue,kMagenta]
styles = [1,3,5,2,7]
j=0
for syst in list_of_syst:
    h[syst].SetLineColor(colors[j])
    h[syst].SetLineWidth(3)
    h[syst].SetLineStyle(styles[j])
    h[syst].Draw("same")
    j = j+1
    
    
l = TLegend(0.55,0.75,0.9,0.9)
l.SetTextFont(42)
l.SetNColumns(2)
l.SetBorderSize(0)
#l.AddEntry(h_totsyst,"Total", "l")

description = {
		"MC Stats" : "MC stats",
		"Experimental" : "Experimental",
		"Modelling" : "Modelling",
		"Fakes" : "Fake",
		"Normalisation" : "Normalisation"}

k=0
for syst in reversed(sorted(list_of_syst, key=get_integral)):
    l.AddEntry(h[syst],description[syst],'l')
    k = k+1
    
l.AddEntry(h_totsyst,"Total", "l")
l.Draw("same")    


#ATLASLabel(0.2,0.85,"Preliminary")
   
l2 = ROOT.TLatex()
l2.SetNDC()
l2.SetTextFont(42)
    
#l2.DrawLatex(0.2,0.73,"Wh")
l2.DrawLatex(0.2,0.79,"#sqrt{s} = 13 TeV, 139 fb^{-1}")


c.Print("systematics_summary_WZ_thesis.png")
c.Print("systematics_summary_WZ_thesis.eps")
c.Print("systematics_summary_WZ_thesis.pdf")
c.Print("systematics_summary_WZ_thesis.C")
