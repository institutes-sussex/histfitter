"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#ROOT.gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils
from pullPlotUtils import *

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["SS_metmeff2Jet"] = "SR exclusion"
    myRegionDict["SLVR2_nJet"] = "VR2"
    myRegionDict["SR1_Disc"] = "SR1"    
    myRegionDict["SR2_Disc"] = "SR2"
    myRegionDict["SR3_Disc"] = "SR3"
    myRegionDict["SR4_Disc"] = "SR4"
    myRegionDict["SR5_Disc"] = "SR5"
    myRegionDict["SR6_Disc"] = "SR6"
    myRegionDict["SR7_Disc"] = "SR7"
    myRegionDict["SR8_Disc"] = "SR8"
    myRegionDict["SR9_Disc"] = "SR9"
    myRegionDict["SR10_Disc"] = "SR10"
    myRegionDict["SR11_Disc"] = "SR11"
    myRegionDict["SR12_Disc"] = "SR12"
    myRegionDict["SR1_Wh"] = "SRWh-SFOS-1"
    myRegionDict["SR2_Wh"] = "SRWh-SFOS-2"
    myRegionDict["SR3_Wh"] = "SRWh-SFOS-3"
    myRegionDict["SR4_Wh"] = "SRWh-SFOS-4"
    myRegionDict["SR5_Wh"] = "SRWh-SFOS-5"
    myRegionDict["SR6_Wh"] = "SRWh-SFOS-6"
    myRegionDict["SR7_Wh"] = "SRWh-SFOS-7"
    myRegionDict["SR8_Wh"] = "SRWh-SFOS-8"
    myRegionDict["SR9_Wh"] = "SRWh-SFOS-9"
    myRegionDict["SR10_Wh"] = "SRWh-SFOS-10"
    myRegionDict["SR11_Wh"] = "SRWh-SFOS-11"
    myRegionDict["SR12_Wh"] = "SRWh-SFOS-12"
    myRegionDict["SR13_Wh"] = "SRWh-SFOS-13"
    myRegionDict["SR14_Wh"] = "SRWh-SFOS-14"
    myRegionDict["SR15_Wh"] = "SRWh-SFOS-15"
    myRegionDict["SR16_Wh"] = "SRWh-SFOS-16"
    myRegionDict["SR17_Wh"] = "SRWh-SFOS-17"
    myRegionDict["SR18_Wh"] = "SRWh-SFOS-18"
    myRegionDict["SR19_Wh"] = "SRWh-SFOS-19"
    myRegionDict["SR_DFOS_0j"] = "SRWh-DFOS-1"
    myRegionDict["SR_DFOS_1j"] = "SRWh-DFOS-2"

    myRegionDict["SR1_WZ"] = "SRWZ-1"
    myRegionDict["SR2_WZ"] = "SRWZ-2"
    myRegionDict["SR3_WZ"] = "SRWZ-3"
    myRegionDict["SR4_WZ"] = "SRWZ-4"
    myRegionDict["SR5_WZ"] = "SRWZ-5"
    myRegionDict["SR6_WZ"] = "SRWZ-6"
    myRegionDict["SR7_WZ"] = "SRWZ-7"
    myRegionDict["SR8_WZ"] = "SRWZ-8"
    myRegionDict["SR9_WZ"] = "SRWZ-9"
    myRegionDict["SR10_WZ"] = "SRWZ-10"
    myRegionDict["SR11_WZ"] = "SRWZ-11"
    myRegionDict["SR12_WZ"] = "SRWZ-12"
    myRegionDict["SR13_WZ"] = "SRWZ-13"
    myRegionDict["SR14_WZ"] = "SRWZ-14"
    myRegionDict["SR15_WZ"] = "SRWZ-15"
    myRegionDict["SR16_WZ"] = "SRWZ-16"
    myRegionDict["SR17_WZ"] = "SRWZ-17"
    myRegionDict["SR18_WZ"] = "SRWZ-18"
    myRegionDict["SR19_WZ"] = "SRWZ-19"
    myRegionDict["SR20_WZ"] = "SRWZ-20"

    return myRegionDict

def RenameSamples():

    LegendSamples = {}

    LegendSamples["Dibosons_3L"] = "W+Z"
    LegendSamples["Dibosons_4L"] = "Z+Z"
    LegendSamples["Dibosons_2L"] = "W+W"
    LegendSamples["ttV"] = "t#bar{t}/V"
    LegendSamples["ttbar"] = "t#bar{t}"
    LegendSamples["VVV"] = "VVV"
    LegendSamples["Higgs"] = "Higgs"
    LegendSamples["Fakes"] = "FNP"
    LegendSamples["SingleT"] = "Single top"

    LegendSamples["SMAwh13TeV_200_25"] = "Wh(200,25)"
    LegendSamples["SMAwh13TeV_190_60"] = "Wh(190,60)"
    LegendSamples["SMAwh13TeV_175_25"] = "Wh(175,25)"

    LegendSamples["SMAwz13TeV_600_100"] = "Wh(600,100)"
    LegendSamples["SMAwz13TeV_300_200"] = "Wh(300,200)"
    return LegendSamples

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["SR1_Wh","SR2_Wh","SR3_Wh","SR4_Wh","SR5_Wh","SR6_Wh","SR7_Wh","SR8_Wh","SR9_Wh","SR10_Wh","SR11_Wh","SR12_Wh","SR13_Wh","SR14_Wh","SR15_Wh","SR16_Wh","SR17_Wh","SR18_Wh","SR19_Wh","SR_DFOS_0j","SR_DFOS_1j"]
    #regionList += ["SR1_WZ","SR2_WZ","SR3_WZ","SR4_WZ","SR5_WZ","SR6_WZ","SR7_WZ","SR8_WZ","SR9_WZ","SR10_WZ","SR11_WZ","SR12_WZ","SR13_WZ","SR14_WZ","SR15_WZ","SR16_WZ","SR17_WZ","SR18_WZ","SR19_WZ","SR20_WZ"]
    #regionList += ["WZ_CR_0jets","WZ_CR_LowHT","WZ_CR_HighHT"]
    #regionList += ["WZ_CR_0jets","WZ_CR_LowHT","WZ_CR_HighHT","WZ_VR_nJ0","WZ_VR_LowHT","WZ_VR_HighHT","fakes_VR","top_VR","top_VRAl"]
    #regionList += ["SR1_Disc","SR2_Disc","SR3_Disc","SR4_Disc","SR5_Disc","SR6_Disc","SR7_Disc","SR8_Disc","SR9_Disc","SR10_Disc","SR11_Disc","SR12_Disc"]

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("SR") != -1: return kRed
    if name.find("CR") != -1: return kBlue
    if name.find("VR") != -1: return kGreen
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    #colour scheme for CONF note
   
    if sample == "ttbar":         return kOrange+1
    if sample == "ttV":       return kOrange-4
    if sample == "SingleT":     return kPink+1
    if sample == "Higgs":     return kPink + 1
    if sample == "Fakes":     return kGray
    if sample == "VVV":     return kPink+1
    if sample == "Dibosons_2L":     return kPink+1
    if sample == "Dibosons_3L":     return kAzure+1
    if sample == "Dibosons_4L":     return kAzure-2
    if sample == "SMAwh13TeV_200_25": return kGreen
    if sample == "SMAwh13TeV_175_25": return kMagenta
    #if sample == "SMAwz13TeV_300_200": return kGreen
    #if sample == "SMAwz13TeV_600_100": return kMagenta

    #colour scheme for INT note
    #if sample == "ttbar":         return kMagenta-5
    #if sample == "ttV":       return kCyan-1
    #if sample == "SingleT":     return kGreen+9
    #if sample == "Higgs":     return kPink+1
    #if sample == "Fakes":     return kOrange+9
    #if sample == "VVV":     return kCyan-5
    #if sample == "Dibosons_2L":     return kAzure+4
    #if sample == "Dibosons_3L":     return kOrange-3
    #if sample == "Dibosons_4L":     return kYellow-3


    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils.getRegionColor = getRegionColor
    pullPlotUtils.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/results/MyConfigExample/BkgOnly_combined_NormalMeasurement_model_afterFit.root" # 

    # Where's the pickle file?
    pickleFilename = os.getenv("HISTFITTER")+"/tablesForCONF/BkgOnly_Wh_AllSRs.pickle"
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "Wh_CONF"

    # Samples to stack on top of eachother in each region
    samples = "Dibosons_3L,Dibosons_4L,ttbar,VVV,Higgs,Fakes,Dibosons_2L,SingleT,ttV"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    legendSamples = RenameSamples()

    if not os.path.exists(pickleFilename):
        print "pickle filename %s does not exist" % pickleFilename
        print "will proceed to run yieldstable again"
        
        # Run YieldsTable.py with all regions and samples requested
        cmd = "YieldsTable.py -c %s -s %s -w %s -o MyYieldsTable.tex" % (",".join(regionList), samples, wsfilename)
        print cmd
        subprocess.call(cmd, shell=True)

    if not os.path.exists(pickleFilename):
        print "pickle filename %s still does not exist" % pickleFilename
        return
    
    # Open the pickle and make the pull plot
    #MakeHistPullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind)
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, legendSamples, region, doBlind,outDir="pullsForCONF",plotSignificance="atlas",doSignal="True")

if __name__ == "__main__":
    main()
