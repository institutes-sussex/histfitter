#!/usr/bin/env python
import sys

inf_nm=sys.argv[1]
sys.argv=[sys.argv[0], '-b']

from ROOT import *

f=TFile.Open(inf_nm)

w = f.Get("combined")
if not w: w = f.Get("combWS")

m = w.obj("ModelConfig")
var = m.GetNuisanceParameters()
print var

if (var.getSize()>0):
   var.sort()
   print "variables"
   print "---------"
   #print var.getSize()
   iter_var = var.createIterator()
   end = True
   while(end):
       par = iter_var.Next()
       if par:
	   print par.getTitle()
       else: end = False
       
poi = m.GetParametersOfInterest()
if (poi.getSize()>0):
    print "\n"
    print "POIS:"
    print "-----"
    iter_poi = poi.createIterator()
    end = True
    while(end):
        thispoi = iter_poi.Next()
        if thispoi:
	    print thispoi.getTitle()
        else: end = False    

#print "all fine!"



