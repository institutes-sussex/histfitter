./RemoveNomHistos EWK_WhSS_BkgOnly.root EWK_WhSS_BkgOnly_syst_group1.root
./RemoveNomHistos EWK_WhSS_BkgOnly.root EWK_WhSS_BkgOnly_syst_group2.root
./RemoveNomHistos EWK_WhSS_BkgOnly.root EWK_WhSS_BkgOnly_syst_group3.root
./RemoveNomHistos EWK_WhSS_BkgOnly.root EWK_WhSS_BkgOnly_syst_group4.root

hadd -f data/EWK_WhSS_BkgOnly_syst_cache.root\
	data/EWK_WhSS_BkgOnly.root\
	data/EWK_WhSS_BkgOnly_syst_group1_noNom.root\
	data/EWK_WhSS_BkgOnly_syst_group2_noNom.root\
	data/EWK_WhSS_BkgOnly_syst_group3_noNom.root\
	data/EWK_WhSS_BkgOnly_syst_group4_noNom.root
