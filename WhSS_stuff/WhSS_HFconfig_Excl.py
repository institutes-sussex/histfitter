from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,TCanvas,TLegend,TLegendEntry
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from optparse import OptionParser
from copy import deepcopy

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
import ROOT
GeV = 1000.;

def removeWeight( oldList, oldWeight ):
    newList = deepcopy( oldList )
    newList.remove( oldWeight )
    return newList

def replaceWeight( oldList, oldWeight, newWeight ):
    newList = deepcopy( oldList )
    newList.remove( oldWeight )
    newList.append( newWeight )
    return newList

#-------------------------------
# Parse command line
#-------------------------------
myUserArgs= configMgr.userArg.split(' ')
myInputParser = OptionParser()
myInputParser.add_option('', '--point', dest = 'point', default = '')
myInputParser.add_option('', '--signalUncert', dest = 'signalUncert', default = 'Nom')
myInputParser.add_option('', '--SR', dest = 'SR', default = 'WZ')
myInputParser.add_option('', '--doFullGrid', dest = 'doFullGrid', default = 'False')
myInputParser.add_option('', '--useSysts', dest = 'useSysts', default = 'False')
myInputParser.add_option('', '--addFakes', dest = 'addFakes', default = 'True')
myInputParser.add_option('', '--doThUncert', dest = 'doThUncert', default = 'True')
myInputParser.add_option('', '--doFakeUncert', dest = 'doFakeUncert', default = 'True')
myInputParser.add_option('', '--ShapeFit', dest = 'ShapeFit', default = 'False')
myInputParser.add_option('', '--makePlots', dest = 'makePlots', default = 'False')
myInputParser.add_option('', '--runToys', dest = 'runToys', default = 'False')
myInputParser.add_option('', '--toyIndex', dest = 'toyIndex', default = '3')
myInputParser.add_option('', '--Name', dest = 'Name', default='EWK_WhSS')
myInputParser.add_option('', '--Cache', dest = 'Cache', default='')
myInputParser.add_option('', '--SysGroup', dest = 'SysGroup', default='-1')

(options, args) = myInputParser.parse_args( myUserArgs )
whichPoint = options.point
whichSR = options.SR
signalXSec = options.signalUncert
name = options.Name
toyindex = options.toyIndex
sysGroup = options.SysGroup

#---------------------------------------
# Flags to control options
#---------------------------------------

fullGrid = False
if options.doFullGrid == "True" or options.doFullGrid == "true":
    fullGrid=True

doToys = False
if options.runToys == "True" or options.runToys == "true":
    doToys = True

useFakes = False
if options.addFakes == "True" or options.addFakes == "true":
    useFakes = True

doShapeFit = False
if options.ShapeFit == "True" or options.ShapeFit == "true":
    doShapeFit = True

doThUncert = False
if options.doThUncert == "True" or options.doThUncert == "true":
    doThUncert = True

doFakeUncert = False
if options.doFakeUncert == "True" or options.doFakeUncert == "true":
    doFakeUncert = True

noSysts = True
if options.useSysts == "True" or options.useSysts == "true":
    print "disabling systematics"
    noSysts = False

useStat = True
testSignalYield = False

print "########## Running Exclusion fit #############"


#-------------------------------
# Parameters for hypothesis test
#-------------------------------

# have been running with this commented out and set to false
# configMgr.doHypoTest = True
configMgr.nTOYs = 2000
if not doToys:
  configMgr.calculatorType = 2 # 0=toys, 2=asymptotic calc.
else:
  configMgr.calculatorType = 0 # 0=toys, 2=asymptotic calc.

configMgr.testStatType = 3
configMgr.nPoints = 20
# configMgr.scanRange = (1., 4.)
# configMgr.drawBeforeAfterFit = False
# configMgr.doExclusion = True


configMgr.blindSR = True #CAUTION: SRs in background only fit are seen as VRs so for BkgOnly they are unblinded! FIXME need to unblind them ONE BY ONE
configMgr.blindCR = False
configMgr.blindVR = False 

#--------------------------------
# Now we start to build the model
#--------------------------------

# First define HistFactory attributes
if not noSysts :
    name = name + "_syst"
    if sysGroup != "-1" :
        name = name + "_group" + sysGroup

if not doToys :
    configMgr.analysisName = name
else :
    configMgr.analysisName = name + "_" + whichPoint + "_withToys_" + toyindex

configMgr.inputLumi = 1.  # Luminosity of input TTree
configMgr.outputLumi = 1.  # Luminosity required for output histograms

configMgr.setLumiUnits( "fb-1" )
configMgr.histCacheFile = "data/" + configMgr.analysisName + ".root"
configMgr.outputFileName = "results/" + configMgr.analysisName + "_Output.root"

configMgr.ReduceCorrMatrix = True # CorrMatrix to be given in a reduced version

# Set the files to read from
basedir_a = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_a/'
basedir_d = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_d/'
basedir_e = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_e/'

sigdir_a = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_skimmed/'
sigdir_d = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_skimmed/'
sigdir_e = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_skimmed/'

# Cache configuration ---------------------------------------------------------------------------------------------------

cache_file = "/its/home/ma2008/Desktop/Analysis2018/SS3L/Framework/HistFitter_v0.64.0/data/EWK_WhSS_BkgOnly_syst.root" # default value

# set cache_file = user defined value
if options.Cache != "" :
    cache_file = options.Cache

if configMgr.useHistBackupCacheFile :
    configMgr.histBackupCacheFile = cache_file

# Recycle histograms ----------------------------------------------------------------------------------------------------

configMgr.useCacheToTreeFallback = True # enable the fallback to trees

# config for histo fallback - not implemented yet FIXME

##if configMgr.readFromTree or configMgr.useCacheToTreeFallback:
  ##bgdFiles_a.append("/lustre/scratch/epp/atlas/ft81/workdir/SUSYLimits/MakeHFT/Trees_mca/L3_BG13TeV_a.root")

# Input files ----------------------------------------------------------------------------------------------------

bkg_SingleTop_File_a = basedir_a + "SingleTop.root"
bkg_SingleTop_File_d = basedir_d + "SingleTop.root"
bkg_SingleTop_File_e = basedir_e + "SingleTop.root"
bkg_VVV_File_a = basedir_a + "VVV_221.root"
bkg_VVV_File_d = basedir_d + "VVV_221.root"
bkg_VVV_File_e = basedir_e + "VVV_221.root"
bkg_ttV_File_a = basedir_a + "ttV.root"
bkg_ttV_File_d = basedir_d + "ttV.root"
bkg_ttV_File_e = basedir_e + "ttV.root"
bkg_ttbar_File_a = basedir_a + "ttbar.root"
bkg_ttbar_File_d = basedir_d + "ttbar.root"
bkg_ttbar_File_e = basedir_e + "ttbar.root"
bkg_WJets_File_a = basedir_a + "WJets.root"
bkg_WJets_File_d = basedir_d + "WJets.root"
bkg_WJets_File_e = basedir_e + "WJets.root"
bkg_VV_1L_File_a = basedir_a + "VV_1L.root"
bkg_VV_1L_File_d = basedir_d + "VV_1L.root"
bkg_VV_1L_File_e = basedir_e + "VV_1L.root"
bkg_VV_2L_OS_File_a = basedir_a + "VV_2L_OS.root"
bkg_VV_2L_OS_File_d = basedir_d + "VV_2L_OS.root"
bkg_VV_2L_OS_File_e = basedir_e + "VV_2L_OS.root"
bkg_VV_2L_SS_File_a = basedir_a + "VV_2L_SS.root"
bkg_VV_2L_SS_File_d = basedir_d + "VV_2L_SS.root"
bkg_VV_2L_SS_File_e = basedir_e + "VV_2L_SS.root"
bkg_VV_3L_File_a = basedir_a + "VV_3L.root"
bkg_VV_3L_File_d = basedir_d + "VV_3L.root"
bkg_VV_3L_File_e = basedir_e + "VV_3L.root"
bkg_VV_4L_File_a = basedir_a + "VV_4L.root"
bkg_VV_4L_File_d = basedir_d + "VV_4L.root"
bkg_VV_4L_File_e = basedir_e + "VV_4L.root"
bkg_ZJets_ee_File_a = basedir_a + "ZJets_ee.root"
bkg_ZJets_ee_File_d = basedir_d + "ZJets_ee.root"
bkg_ZJets_ee_File_e = basedir_e + "ZJets_ee.root"
bkg_ZJets_mm_File_a = basedir_a + "ZJets_mm.root"
bkg_ZJets_mm_File_d = basedir_d + "ZJets_mm.root"
bkg_ZJets_mm_File_e = basedir_e + "ZJets_mm.root"
bkg_ZJets_tt_File_a = basedir_a + "ZJets_tt.root"
bkg_ZJets_tt_File_d = basedir_d + "ZJets_tt.root"
bkg_ZJets_tt_File_e = basedir_e + "ZJets_tt.root"

bkg_Fake_File_a = basedir_a + "Fake.root"
bkg_Fake_File_d = basedir_d + "Fake.root"
bkg_Fake_File_e = basedir_e + "Fake.root"
bkg_CF_File_a = basedir_a + "ChargeFlip.root"
bkg_CF_File_d = basedir_d + "ChargeFlip.root"
bkg_CF_File_e = basedir_e + "ChargeFlip.root"

data_File_a =  basedir_a + "data16.root"
data_File_d =  basedir_d + "data17.root"
data_File_e =  basedir_e + "data18.root"

# Regions ----------------------------------------------------------------------------------------------------

print "Defining regions"

# common preselection for ALL the regions
#configMgr.cutsDict["preselWhSS"]= "passSS3LTrig==1 && passSS3LTrigMatch==1 && pass2lcut==1 && nCombLep==2 && nJets>=1 && num_bjets==0 && isSS==1 && eT_miss>=50 && LepPt[0]>=25 && LepPt[1]>=25"
#configMgr.cutsDict["preselWhSS"]= "passSS3LTrig==1 && pass2lcut==1 && nCombLep==2 && nJets>=1 && num_bjets==0 && isSS==1 && eT_miss>=50 && LepPt[0]>=25 && LepPt[1]>=25"
configMgr.cutsDict["preselWhSS"]= "passSS3LTrig==1 && pass2lcut==1 && nCombLep==2 && nJets>=1 && num_bjets==0 && isSS==1 && eT_miss>=50 && LepPt[0]>=25 && LepPt[1]>=25 && mll>=20"

##########################
# SRs for Wh-SS analysis #
##########################

SR1a_cuts = "mt2>=80 && met_Sig>=7 && eT_miss>=75 && eT_miss<125 && mjj<350"
SR1b_cuts = "mt2>=80 && met_Sig>=7 && eT_miss>=125 && eT_miss<200 && mjj<350"
SR1c_cuts = "mt2>=80 && met_Sig>=7 && eT_miss>=200 && mjj<350"
SR2_cuts  = "mt2<80 && min(mTl1,mTl2)>=100 && met_Sig>=6 && mjj<350"
SR_isEE = " && isEE==1 && "
SR_isEM = " && isEM==1 && "
SR_isMM = " && isMM==1 && "

configMgr.cutsDict["SR1a_EE"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isEE + SR1a_cuts
configMgr.cutsDict["SR1a_EM"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isEM + SR1a_cuts
configMgr.cutsDict["SR1a_MM"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isMM + SR1a_cuts
configMgr.cutsDict["SR1b_EE"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isEE + SR1b_cuts
configMgr.cutsDict["SR1b_EM"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isEM + SR1b_cuts
configMgr.cutsDict["SR1b_MM"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isMM + SR1b_cuts
configMgr.cutsDict["SR1c_EE"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isEE + SR1c_cuts
configMgr.cutsDict["SR1c_EM"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isEM + SR1c_cuts
configMgr.cutsDict["SR1c_MM"] = str(configMgr.cutsDict["preselWhSS"]) + SR_isMM + SR1c_cuts
configMgr.cutsDict["SR2_EE"]  = str(configMgr.cutsDict["preselWhSS"]) + SR_isEE + SR2_cuts
configMgr.cutsDict["SR2_EM"]  = str(configMgr.cutsDict["preselWhSS"]) + SR_isEM + SR2_cuts
configMgr.cutsDict["SR2_MM"]  = str(configMgr.cutsDict["preselWhSS"]) + SR_isMM + SR2_cuts

##########################
# CRs for Wh-SS analysis #
##########################

CR_WZ_cuts = "passSS3LTrig==1 && nEl+nMu==2 && isSS==1 && nCombLep==3 && nBaseLep==3 && BL_pass2lossf==1 && num_bjets==0 && nJets>=1 && eT_miss>=50 && fabs(BL_mlll-91.2)>=10 && fabs(BL_mll3lZ-91.2)<15 && LepPt[0]>=25 && LepPt[1]>=25 && met_Sig<6"
CRVR_WZ_isEE = " && isEE==1 && Alt$(BL_ECIDS_L_pass[0],1)==1 && Alt$(BL_ECIDS_L_pass[1],1)==1 && Alt$(BL_ECIDS_L_pass[2],1)==1 && fabs(BL_LepEta[0])<2 && fabs(BL_LepEta[1])<2 && fabs(BL_LepEta[2])<2"
CRVR_WZ_isEM = " && isEM==1 && Alt$(BL_ECIDS_L_pass[0],1)==1 && Alt$(BL_ECIDS_L_pass[1],1)==1 && ((BL_LepPdgId[0]==11)*fabs(BL_LepEta[0]))<2 && ((BL_LepPdgId[1]==11)*fabs(BL_LepEta[1]))<2"
CRVR_WZ_isMM = " && isMM==1"

configMgr.cutsDict["CRWZ_EE"] = CR_WZ_cuts + CRVR_WZ_isEE
configMgr.cutsDict["CRWZ_EM"] = CR_WZ_cuts + CRVR_WZ_isEM
configMgr.cutsDict["CRWZ_MM"] = CR_WZ_cuts + CRVR_WZ_isMM

#CR_WWSS_cuts = "nJets>=2 && mjj>=350 && JetPt[1]>=75 && met_Sig<6"
#CRVR_WWSS_isEE = SR_isEE + "fabs(mll-91.18)>=15 && met_Sig>=4 && "
#CR_WWSS_cuts = "nJets>=2 && mjj>=350 && JetPt[1]>=75 && met_Sig>=2 && met_Sig<6"
#CRVR_WWSS_isEE = SR_isEE + "fabs(mll-91.18)>=15 && "
CR_WWSS_cuts = "nJets>=2 && mjj>=350 && JetPt[1]>=75 && met_Sig<6"
CRVR_WWSS_isEE = SR_isEE + "fabs(mll-91.18)>=15 && "
CRVR_WWSS_isEM = SR_isEM
CRVR_WWSS_isMM = SR_isMM

configMgr.cutsDict["CRWWSS_EE"] = str(configMgr.cutsDict["preselWhSS"]) + CRVR_WWSS_isEE + CR_WWSS_cuts
configMgr.cutsDict["CRWWSS_EM"] = str(configMgr.cutsDict["preselWhSS"]) + CRVR_WWSS_isEM + CR_WWSS_cuts
configMgr.cutsDict["CRWWSS_MM"] = str(configMgr.cutsDict["preselWhSS"]) + CRVR_WWSS_isMM + CR_WWSS_cuts

##########################
# VRs for Wh-SS analysis #
##########################

VR_WZ_cuts = "passSS3LTrig==1 && nEl+nMu==2 && isSS==1 && nCombLep==3 && nBaseLep==3 && BL_pass2lossf==1 && num_bjets==0 && nJets>=1 && eT_miss>=50 && fabs(BL_mlll-91.2)>=10 && fabs(BL_mll3lZ-91.2)<15 && LepPt[0]>=25 && LepPt[1]>=25 && met_Sig>=6"

configMgr.cutsDict["VRWZ_EE"] = VR_WZ_cuts + CRVR_WZ_isEE
configMgr.cutsDict["VRWZ_EM"] = VR_WZ_cuts + CRVR_WZ_isEM
configMgr.cutsDict["VRWZ_MM"] = VR_WZ_cuts + CRVR_WZ_isMM

VR_WWSS_cuts = "nJets>=2 && mjj>=350 && JetPt[1]>=75 && met_Sig>=6"

configMgr.cutsDict["VRWWSS_EE"] = str(configMgr.cutsDict["preselWhSS"]) + CRVR_WWSS_isEE + VR_WWSS_cuts
configMgr.cutsDict["VRWWSS_EM"] = str(configMgr.cutsDict["preselWhSS"]) + CRVR_WWSS_isEM + VR_WWSS_cuts
configMgr.cutsDict["VRWWSS_MM"] = str(configMgr.cutsDict["preselWhSS"]) + CRVR_WWSS_isMM + VR_WWSS_cuts

VR_Fake_cuts = "mll>=20 && mt2<80 && min(mTl1,mTl2)<100 && met_Sig<5 && mjj<350"
VR_Fake_isEE = SR_isEE + "fabs(mll-91.18)>=15 && "
VR_Fake_isEM = SR_isEM
VR_Fake_isMM = SR_isMM

configMgr.cutsDict["VRFake_EE"] = str(configMgr.cutsDict["preselWhSS"]) + VR_Fake_isEE + VR_Fake_cuts
configMgr.cutsDict["VRFake_EM"] = str(configMgr.cutsDict["preselWhSS"]) + VR_Fake_isEM + VR_Fake_cuts
configMgr.cutsDict["VRFake_MM"] = str(configMgr.cutsDict["preselWhSS"]) + VR_Fake_isMM + VR_Fake_cuts

VR_CF_EE_cuts = " && mll>=20 && mt2<80 && min(mTl1,mTl2)<100 && met_Sig<5 && mjj<350 && isEE==1 && fabs(mll-91.18)<15"

configMgr.cutsDict["VRCF_EE"] = str(configMgr.cutsDict["preselWhSS"]) + VR_CF_EE_cuts


# Weights ----------------------------------------------------------------------------------------------------

if 'Nom' in signalXSec:
  weights = [ "EventWeight", "XSecWeight", "elecSF", "muonSF", "pileupweight", "jvtSF", "elecChargeSF", "SS3LTriggerSF", "btagSFCentral" ]
  configMgr.weights = weights
elif 'up' in signalXSec:
  configMgr.weights = [ "eventweightUp" ]
elif 'down' in signalXSec:
  configMgr.weights = [ "eventweightDown" ]

CFweights = [ "CFWeight" ]
FFweights = [ "FFWeight" ]

configMgr.nomName = "NONE"


# Experimental Systematics ----------------------------------------------------------------------------------------------------

#### ===> TREE-BASED
tree_sys_names = []

# Egamma
tree_sys_names.extend([
    "EG_RESOLUTION_ALL",
    "EG_SCALE_AF2",
    "EG_SCALE_ALL"
])

# JET
tree_sys_names.extend([
    "JET_BJES_Response",
    "JET_EffectiveNP_Detector1",
    "JET_EffectiveNP_Detector2",
    "JET_EffectiveNP_Mixed1",
    "JET_EffectiveNP_Mixed2",
    "JET_EffectiveNP_Mixed3",
    "JET_EffectiveNP_Modelling1",
    "JET_EffectiveNP_Modelling2",
    "JET_EffectiveNP_Modelling3",
    "JET_EffectiveNP_Modelling4",
    "JET_EffectiveNP_Statistical1",
    "JET_EffectiveNP_Statistical2",
    "JET_EffectiveNP_Statistical3",
    "JET_EffectiveNP_Statistical4",
    "JET_EffectiveNP_Statistical5",
    "JET_EffectiveNP_Statistical6",
    "JET_EtaIntercalibration_Modelling",
    "JET_EtaIntercalibration_NonClosure_2018data",
    "JET_EtaIntercalibration_NonClosure_highE",
    "JET_EtaIntercalibration_NonClosure_negEta",
    "JET_EtaIntercalibration_NonClosure_posEta",
    "JET_EtaIntercalibration_TotalStat",
    "JET_Flavor_Composition",
    "JET_Flavor_Response",
    "JET_JER_DataVsMC_MC16",
    "JET_JER_DataVsMC_AFII",
    "JET_JER_EffectiveNP_1",
    "JET_JER_EffectiveNP_2",
    "JET_JER_EffectiveNP_3",
    "JET_JER_EffectiveNP_4",
    "JET_JER_EffectiveNP_5",
    "JET_JER_EffectiveNP_6",
    "JET_JER_EffectiveNP_7restTerm",
    "JET_Pileup_OffsetMu",
    "JET_Pileup_OffsetNPV",
    "JET_Pileup_PtTerm",
    "JET_Pileup_RhoTopology",
    "JET_PunchThrough_MC16",
    "JET_PunchThrough_AFII",
    "JET_RelativeNonClosure_AFII",
    "JET_SingleParticle_HighPt"
])

# MET
tree_sys_names.extend([
    "MET_SoftTrk_ResoPara",
    "MET_SoftTrk_ResoPerp",
    "MET_SoftTrk_Scale"
])

# MUON
tree_sys_names.extend([
    "MUON_ID",
    "MUON_MS",
    "MUON_SAGITTA_RESBIAS",
    "MUON_SAGITTA_RHO",
    "MUON_SCALE"
])

# creating list of non-norm tree-based syst
tree_sys = []
for sys_name in tree_sys_names :
    if 'MET_SoftTrk_Reso' in sys_name :
        tree_sys.append( Systematic( "sys_"+sys_name, configMgr.nomName, sys_name, configMgr.nomName, "tree", "histoSysOneSide" ) )
        #tree_sys.append( Systematic( "sys_"+sys_name, configMgr.nomName, sys_name, configMgr.nomName, "tree", "histoSysOneSideSym" ) )
    else :
        tree_sys.append( Systematic( "sys_"+sys_name, configMgr.nomName, sys_name+"__1up", sys_name+"__1down", "tree", "histoSys" ) )

# creating list of norm tree-based syst
tree_sys_norm = []
for sys_name in tree_sys_names :
    if 'MET_SoftTrk_Reso' in sys_name :
        tree_sys_norm.append( Systematic( "sys_"+sys_name, configMgr.nomName, sys_name, configMgr.nomName, "tree", "normHistoSysOneSide" ) )
        #tree_sys_norm.append( Systematic( "sys_"+sys_name, configMgr.nomName, sys_name, configMgr.nomName, "tree", "normHistoSysOneSideSym" ) )
    else :
        tree_sys_norm.append( Systematic( "sys_"+sys_name, configMgr.nomName, sys_name+"__1up", sys_name+"__1down", "tree", "normHistoSys" ) )


#### ===> WEIGHT-BASED

weight_sys_Dict = {}  # key : ( sys_name, weight_nom, weight_up, weight_down )

# Electrons
weight_sys_Dict.update({
    "elChIDStat" : ( "elChIDStat", "elecChargeSF", "elecChargeSF_CHARGEID_STAT_Up", "elecChargeSF_CHARGEID_STAT_Down" ),
    "elChIDSys" : ( "elChIDSys", "elecChargeSF", "elecChargeSF_CHARGEID_SYStotal_Up", "elecChargeSF_CHARGEID_SYStotal_Down" ),
    "ECIDS" : ( "ECIDS", "elecChargeSF", "elecChargeSF_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR_Up", "elecChargeSF_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR_Down" ),
    "elID" : ( "elID", "elecSF", "elecSF_EFF_ID_Up", "elecSF_EFF_ID_Down" ),
    "elIso" : ( "elIso", "elecSF", "elecSF_EFF_Iso_Up", "elecSF_EFF_Iso_Down" ),
    "elReco" : ( "elReco", "elecSF", "elecSF_EFF_Reco_Up", "elecSF_EFF_Reco_Down" )
})

# Flavour Tagging
weight_sys_Dict.update({
    "FTb" : ( "FTb", "btagSFCentral", "bEffUpWeight", "bEffDownWeight" ),
    "FTc" : ( "FTc", "btagSFCentral", "cEffUpWeight", "cEffDownWeight" ),
    "FTl" : ( "FTl", "btagSFCentral", "lEffUpWeight", "lEffDownWeight" ),
    "FText" : ( "FText", "btagSFCentral", "extrapolationUpWeight", "extrapolationDownWeight" ),
    "FTextC" : ( "FTextC", "btagSFCentral", "FT_EFF_extrapolationFromCharm_UP", "FT_EFF_extrapolationFromCharm_DOWN" )
})

# JVT
weight_sys_Dict.update({
    "JVT" : ( "JVT", "jvtSF", "jvtSFup", "jvtSFdown" )
})

# Muons
weight_sys_Dict.update({
    "muBadSys" : ( "muBadSys", "muonSF", "muonSF_EFF_BADMUON_SYS_Up", "muonSF_EFF_BADMUON_SYS_Down" ),
    "muIsoStat" : ( "muIsoStat", "muonSF", "muonSF_ISO_STAT_Up", "muonSF_ISO_STAT_Down" ),
    "muIsoSys" : ( "muIsoSys", "muonSF", "muonSF_ISO_SYS_Up", "muonSF_ISO_SYS_Down" ),
    "muRecoStat" : ( "muRecoStat", "muonSF", "muonSF_EFF_RECO_STAT_Up", "muonSF_EFF_RECO_STAT_Down" ),
    "muRecoStatLowPt" : ( "muRecoStatLowPt", "muonSF", "muonSF_EFF_RECO_STAT_LOWPT_Up", "muonSF_EFF_RECO_STAT_LOWPT_Down" ),
    "muRecoSys" : ( "muRecoSys", "muonSF", "muonSF_EFF_RECO_SYS_Up", "muonSF_EFF_RECO_SYS_Down" ),
    "muRecoSysLowPt" : ( "muRecoSysLowPt", "muonSF", "muonSF_EFF_RECO_SYS_LOWPT_Up", "muonSF_EFF_RECO_SYS_LOWPT_Down" ),
    "muTTVAStat" : ( "muTTVAStat", "muonSF", "muonSF_TTVA_STAT_Up", "muonSF_TTVA_STAT_Down" ),
    "muTTVASys" : ( "muTTVASys", "muonSF", "muonSF_TTVA_SYS_Up", "muonSF_TTVA_SYS_Down" )
})

# Pile-up
weight_sys_Dict.update({
    "PU" : ( "PU", "pileupweight", "pileupweightUP", "pileupweightDOWN" )
})

# Trigger
weight_sys_Dict.update({
    "elTrigEff" : ( "elTrigEff", "SS3LTriggerSF", "SS3LTriggerSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR_Up", "SS3LTriggerSF_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR_Down" ),
    "elTrig" : ( "elTrig", "SS3LTriggerSF", "SS3LTriggerSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_Up", "SS3LTriggerSF_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_Down" ),
    "muTrigStat" : ( "muTrigStat", "SS3LTriggerSF", "SS3LTriggerSF_MUON_EFF_TrigStatUncertainty_Up", "SS3LTriggerSF_MUON_EFF_TrigStatUncertainty_Down" ),
    "muTrigSys" : ( "muTrigSys", "SS3LTriggerSF", "SS3LTriggerSF_MUON_EFF_TrigSystUncertainty_Up", "SS3LTriggerSF_MUON_EFF_TrigSystUncertainty_Down" )
})

# creating list of non-norm weight-based syst
weight_sys = []
for sys_name, sys_conf in weight_sys_Dict.iteritems() :
    weight_sys.append( Systematic( "sys_"+sys_conf[0], configMgr.weights, 
                       replaceWeight( configMgr.weights, sys_conf[1], sys_conf[2] ),
                       replaceWeight( configMgr.weights, sys_conf[1], sys_conf[3] ),
                       "weight", "overallHistoSys" )
                     )

# creating list of norm weight-based syst
weight_sys_norm = []
for sys_name, sys_conf in weight_sys_Dict.iteritems() :
    weight_sys_norm.append( Systematic( "sys_"+sys_conf[0], configMgr.weights, 
                            replaceWeight( configMgr.weights, sys_conf[1], sys_conf[2] ),
                            replaceWeight( configMgr.weights, sys_conf[1], sys_conf[3] ),
                            "weight", "overallNormHistoSys" )
                          )

# combined lists of tree- and weight-based experimental systematics
exper_sys = tree_sys + weight_sys
exper_sys_norm = tree_sys_norm + weight_sys_norm


# Data-driven background Systematics ----------------------------------------------------------------------------------------------------

####===> Fake and ChargeFlip WEIGHT-BASED Systematics

fake_sys_Dict = {}  # key : ( sys_name, weight_nom, weight_up, weight_down )
fake_sys_Dict.update({
    "Stat" : ( "FF_Stat", "FFWeight", "FFWeight__STAT_up", "FFWeight__STAT_dn" ),
    "CRSRextr" : ( "FF_CRSRextr", "FFWeight", "FFWeight__CRSRextr_up", "FFWeight__CRSRextr_dn" ),
#    "FakeTag" : ( "FF_FakeTag", "FFWeight", "FFWeight__FakeTag_up", "FFWeight__FakeTag_dn" ),
    "Psub" : ( "FF_Psub", "FFWeight", "FFWeight__Psub_up", "FFWeight__Psub_dn" ),
    "CFsub" : ( "FF_CFsub", "FFWeight", "FFWeight__CFsub_up", "FFWeight__CFsub_dn" )
})

# creating list of fake non-norm weight-based syst
fake_sys = []
for sys_name, sys_conf in fake_sys_Dict.iteritems() :
    fake_sys.append( Systematic( "sys_"+sys_conf[0], FFweights, 
                     replaceWeight( FFweights, sys_conf[1], sys_conf[2] ),
                     replaceWeight( FFweights, sys_conf[1], sys_conf[3] ),
                     "weight", "overallHistoSys" )
                   )


cf_sys_Dict = {}  # key : ( sys_name, weight_nom, weight_up, weight_down )
cf_sys_Dict.update({
    "MCstat" : ( "CF_MCstat", "CFWeight", "CFWeight__MCstat_up", "CFWeight__MCstat_dn" ),
    "Stat" : ( "CF_Stat", "CFWeight", "CFWeight__STAT_up", "CFWeight__STAT_dn" ),
    "Sys" : ( "CF_Sys", "CFWeight", "CFWeight__SYST_up", "CFWeight__SYST_dn" )
})

# creating list of charge-flip non-norm weight-based syst
cf_sys = []
for sys_name, sys_conf in cf_sys_Dict.iteritems() :
    cf_sys.append( Systematic( "sys_"+sys_conf[0], CFweights, 
                   replaceWeight( CFweights, sys_conf[1], sys_conf[2] ),
                   replaceWeight( CFweights, sys_conf[1], sys_conf[3] ),
                   "weight", "overallHistoSys" )
                 )


# Systematics on the xsec k factor needed for Sheerpa WWSS samples
WWSS_sys = []
WWSS_sys.append( Systematic( "kWWSS_stat", 1., 1.1793, 0.8345, "user", "userOverallSys" ) )
WWSS_sys.append( Systematic( "kWWSS_sys",  1., 1.1931, 0.8483, "user", "userOverallSys" ) )


# theory syst 3L - WZ Uncorr
##syst_WZQCD_SR3LDFOS0j = Systematic("syst_WZQCD", 1., 1.0476, 0.9524, "user","userOverallSys")

# theory syst 3L - VVV
##syst_VVVQCD_SR3LDFOS0j = Systematic("syst_VVVQCD", 1., 1.0182, 0.9818, "user","userOverallSys")


#Theory x-sec uncertainties
#syst_ttZxsec_UP = replaceWeight(configMgr.weights,"36.1","36.1*(1.+(runNumber>=410111 || runNumber<=410116)*0.12)")
#syst_ttZxsec_DN = replaceWeight(configMgr.weights,"36.1","36.1*(1.-(runNumber>=410111 || runNumber<=410116)*0.12)")
#syst_ttZxsec = Systematic("syst_ttZxsec", 1., syst_ttZxsec_UP, syst_ttZxsec_DN, "weight", "overallSys")

# WhSS overall 30% syst - same name to correlate them all for all backgrounds
#syst_overall = Systematic("syst_30", 1., 1.15, 0.85, "user","userOverallSys")
#syst_overall = Systematic("syst_30", 1., 1.3, 0.7, "user","userOverallSys")

#systList = [ syst_overall ]


# Defining samples ----------------------------------------------------------------------------------------------------

# WZ
#WZ_Sample = Sample( "WZ", kOrange-3 )
#WZ_Sample.setStatConfig( useStat )
#WZ_Sample.setNormFactor( "mu_WZ", 1., 0., 5. )
#WZ_Sample.setNormRegions( [("CRWZ_EE", "cuts"), ("CRWZ_EM", "cuts"), ("CRWZ_MM", "cuts")] )
#WZ_Sample.setNormByTheory()
#WZ_Sample.addInput( bkg_VV_3L_File_a, "HFntuple" )
#WZ_Sample.addInput( bkg_VV_3L_File_d, "HFntuple" )
#WZ_Sample.addInput( bkg_VV_3L_File_e, "HFntuple" )

# WZ - EE channel
WZ_EE_Sample = Sample( "WZ_EE", kOrange-3 )
WZ_EE_Sample.setStatConfig( useStat )
WZ_EE_Sample.setNormFactor( "mu_WZ_EE", 1., 0., 5. )
WZ_EE_Sample.addSampleSpecificWeight( "(isEE==1)" )
WZ_EE_Sample.setNormRegions( [("CRWZ_EE", "cuts")] )
WZ_EE_Sample.setNormByTheory()
WZ_EE_Sample.addInput( bkg_VV_3L_File_a, "HFntuple" )
WZ_EE_Sample.addInput( bkg_VV_3L_File_d, "HFntuple" )
WZ_EE_Sample.addInput( bkg_VV_3L_File_e, "HFntuple" )

# WZ - EM channel
WZ_EM_Sample = Sample( "WZ_EM", kOrange-3 )
WZ_EM_Sample.setStatConfig( useStat )
WZ_EM_Sample.setNormFactor( "mu_WZ_EM", 1., 0., 5. )
WZ_EM_Sample.addSampleSpecificWeight( "(isEM==1)" )
WZ_EM_Sample.setNormRegions( [("CRWZ_EM", "cuts")] )
WZ_EM_Sample.setNormByTheory()
WZ_EM_Sample.addInput( bkg_VV_3L_File_a, "HFntuple" )
WZ_EM_Sample.addInput( bkg_VV_3L_File_d, "HFntuple" )
WZ_EM_Sample.addInput( bkg_VV_3L_File_e, "HFntuple" )

# WZ - MM channel
WZ_MM_Sample = Sample( "WZ_MM", kOrange-3 )
WZ_MM_Sample.setStatConfig( useStat )
WZ_MM_Sample.setNormFactor( "mu_WZ_MM", 1., 0., 5. )
WZ_MM_Sample.addSampleSpecificWeight( "(isMM==1)" )
WZ_MM_Sample.setNormRegions( [("CRWZ_MM", "cuts")] )
WZ_MM_Sample.setNormByTheory()
WZ_MM_Sample.addInput( bkg_VV_3L_File_a, "HFntuple" )
WZ_MM_Sample.addInput( bkg_VV_3L_File_d, "HFntuple" )
WZ_MM_Sample.addInput( bkg_VV_3L_File_e, "HFntuple" )

# WWSS
#WWSS_Sample = Sample( "WWSS", kAzure+4 )
#WWSS_Sample.setStatConfig( useStat )
#WWSS_Sample.setNormFactor( "mu_WWSS", 1., 0., 5. )
#WWSS_Sample.setNormRegions( [("CRWWSS_EE", "cuts"), ("CRWWSS_EM", "cuts"), ("CRWWSS_MM", "cuts")] )
#WWSS_Sample.setNormByTheory()
#WWSS_Sample.addInput( bkg_VV_2L_SS_File_a, "HFntuple" )
#WWSS_Sample.addInput( bkg_VV_2L_SS_File_d, "HFntuple" )
#WWSS_Sample.addInput( bkg_VV_2L_SS_File_e, "HFntuple" )

# WWSS - EE channel
WWSS_EE_Sample = Sample( "WWSS_EE", kAzure+4 )
WWSS_EE_Sample.setStatConfig( useStat )
WWSS_EE_Sample.setNormFactor( "mu_WWSS_EE", 1., 0., 5. )
WWSS_EE_Sample.addSampleSpecificWeight( "(isEE==1)" )
WWSS_EE_Sample.addSampleSpecificWeight( "1.44" ) # kWWSS for Sherpa samples
WWSS_EE_Sample.setNormRegions( [("CRWWSS_EE", "cuts")] )
WWSS_EE_Sample.setNormByTheory()
WWSS_EE_Sample.addInput( bkg_VV_2L_SS_File_a, "HFntuple" )
WWSS_EE_Sample.addInput( bkg_VV_2L_SS_File_d, "HFntuple" )
WWSS_EE_Sample.addInput( bkg_VV_2L_SS_File_e, "HFntuple" )

# WWSS - EM channel
WWSS_EM_Sample = Sample( "WWSS_EM", kAzure+4 )
WWSS_EM_Sample.setStatConfig( useStat )
WWSS_EM_Sample.setNormFactor( "mu_WWSS_EM", 1., 0., 5. )
WWSS_EM_Sample.addSampleSpecificWeight( "(isEM==1)" )
WWSS_EM_Sample.addSampleSpecificWeight( "1.44" ) # kWWSS for Sherpa samples
WWSS_EM_Sample.setNormRegions( [("CRWWSS_EM", "cuts")] )
WWSS_EM_Sample.setNormByTheory()
WWSS_EM_Sample.addInput( bkg_VV_2L_SS_File_a, "HFntuple" )
WWSS_EM_Sample.addInput( bkg_VV_2L_SS_File_d, "HFntuple" )
WWSS_EM_Sample.addInput( bkg_VV_2L_SS_File_e, "HFntuple" )

# WWSS - MM channel
WWSS_MM_Sample = Sample( "WWSS_MM", kAzure+4 )
WWSS_MM_Sample.setStatConfig( useStat )
WWSS_MM_Sample.setNormFactor( "mu_WWSS_MM", 1., 0., 5. )
WWSS_MM_Sample.addSampleSpecificWeight( "(isMM==1)" )
WWSS_MM_Sample.addSampleSpecificWeight( "1.44" ) # kWWSS for Sherpa samples
WWSS_MM_Sample.setNormRegions( [("CRWWSS_MM", "cuts")] )
WWSS_MM_Sample.setNormByTheory()
WWSS_MM_Sample.addInput( bkg_VV_2L_SS_File_a, "HFntuple" )
WWSS_MM_Sample.addInput( bkg_VV_2L_SS_File_d, "HFntuple" )
WWSS_MM_Sample.addInput( bkg_VV_2L_SS_File_e, "HFntuple" )

# ttV
ttV_Sample = Sample( "ttV", kCyan-1 )
ttV_Sample.setStatConfig( useStat )
ttV_Sample.setNormByTheory()
ttV_Sample.addInput( bkg_ttV_File_a, "HFntuple" )
ttV_Sample.addInput( bkg_ttV_File_d, "HFntuple" )
ttV_Sample.addInput( bkg_ttV_File_e, "HFntuple" )
# if DD samples are included, perform a prompt- and photon-conv.-matching for events of this sample
if useFakes:
    ttV_Sample.addSampleSpecificWeight( "(EventTruthType==1 || EventTruthType==3)" )

# Other
Other_Sample = Sample( "Other", kYellow-3 )
Other_Sample.setStatConfig( useStat )
Other_Sample.setNormByTheory()
Other_Sample.addInput( bkg_SingleTop_File_a, "HFntuple" )
Other_Sample.addInput( bkg_SingleTop_File_d, "HFntuple" )
Other_Sample.addInput( bkg_SingleTop_File_e, "HFntuple" )
Other_Sample.addInput( bkg_VVV_File_a, "HFntuple" )
Other_Sample.addInput( bkg_VVV_File_d, "HFntuple" )
Other_Sample.addInput( bkg_VVV_File_e, "HFntuple" )
Other_Sample.addInput( bkg_ttbar_File_a, "HFntuple" )
Other_Sample.addInput( bkg_ttbar_File_d, "HFntuple" )
Other_Sample.addInput( bkg_ttbar_File_e, "HFntuple" )
Other_Sample.addInput( bkg_WJets_File_a, "HFntuple" )
Other_Sample.addInput( bkg_WJets_File_d, "HFntuple" )
Other_Sample.addInput( bkg_WJets_File_e, "HFntuple" )
Other_Sample.addInput( bkg_VV_1L_File_a, "HFntuple" )
Other_Sample.addInput( bkg_VV_1L_File_d, "HFntuple" )
Other_Sample.addInput( bkg_VV_1L_File_e, "HFntuple" )
Other_Sample.addInput( bkg_VV_2L_OS_File_a, "HFntuple" )
Other_Sample.addInput( bkg_VV_2L_OS_File_d, "HFntuple" )
Other_Sample.addInput( bkg_VV_2L_OS_File_e, "HFntuple" )
Other_Sample.addInput( bkg_VV_4L_File_a, "HFntuple" )
Other_Sample.addInput( bkg_VV_4L_File_d, "HFntuple" )
Other_Sample.addInput( bkg_VV_4L_File_e, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_ee_File_a, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_ee_File_d, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_ee_File_e, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_mm_File_a, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_mm_File_d, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_mm_File_e, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_tt_File_a, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_tt_File_d, "HFntuple" )
Other_Sample.addInput( bkg_ZJets_tt_File_e, "HFntuple" )
Other_Sample.addSampleSpecificWeight( "(EventNumber!=22049940)" ) # remove huge weight event
# if DD samples are included, perform a prompt- and photon-conv.-matching for events of this sample
if useFakes:
    Other_Sample.addSampleSpecificWeight( "(EventTruthType==1 || EventTruthType==3)" )


#AllSamples = [ WZ_Sample, WWSS_Sample, ttV_Sample, Other_Sample ] 
AllSamples = [ WZ_EE_Sample, WZ_EM_Sample, WZ_MM_Sample, 
               WWSS_EE_Sample, WWSS_EM_Sample, WWSS_MM_Sample,
               ttV_Sample, Other_Sample ] 
MCSamples = [ ttV_Sample, Other_Sample ] 
#NormSamples = [ WZ_Sample, WWSS_Sample ] 
NormSamples = [ WZ_EE_Sample, WZ_EM_Sample, WZ_MM_Sample,
                WWSS_EE_Sample, WWSS_EM_Sample, WWSS_MM_Sample ]
DDSamples = []

if useFakes :
    # Data-driven Fake/non-prompt
    Fake_Sample = Sample( "Fake", kMagenta-9 )
    Fake_Sample.setStatConfig( False ) # do not use MC-like statistical uncertainties
    #Fake_Sample.setNormByTheory() # do not consider lumi-related uncertainties
    Fake_Sample.addInput( bkg_Fake_File_a, "HFntuple" )
    Fake_Sample.addInput( bkg_Fake_File_d, "HFntuple" )
    Fake_Sample.addInput( bkg_Fake_File_e, "HFntuple" )
    Fake_Sample.setWeights( FFweights ) # ovewrite MC weight list with Fake-Factor weight
    
    # Data-driven Charge-flip
    CF_Sample = Sample( "ChargeFlip", kYellow-7 )
    CF_Sample.setStatConfig( False ) # do not use MC-like statistical uncertainties
    #CF_Sample.setNormByTheory() # do not consider lumi-related uncertainties
    CF_Sample.addInput( bkg_CF_File_a, "HFntuple" )
    CF_Sample.addInput( bkg_CF_File_d, "HFntuple" )
    CF_Sample.addInput( bkg_CF_File_e, "HFntuple" )
    CF_Sample.setWeights( CFweights ) # ovewrite MC weight list with Charge-flip weight

    AllSamples.extend([ Fake_Sample, CF_Sample ])
    DDSamples.extend([ Fake_Sample, CF_Sample ])


# Data setup ----------------------------------------------------------------------------------------------------

Data_Sample = Sample( "Data", kBlack )
Data_Sample.setData()
Data_Sample.addInput( data_File_a, "HFntupleNONE" )
Data_Sample.addInput( data_File_d, "HFntupleNONE" )
Data_Sample.addInput( data_File_e, "HFntupleNONE" )

AllSamples.extend([ Data_Sample ])

# Fits ----------------------------------------------------------------------------------------------------

#Binnings
NBins   = 1
BinLow  = 0.5
BinHigh = 1.5

#----------------------------------------------------------------------------------------------------------

# Defining fit configuaration ( name = BkgOnly or Excl_sigPoint )

FitConfigName = "BkgOnly"
if configMgr.myFitType == configMgr.FitType.Exclusion :
    MySigName = "Excl_" + whichPoint
    FitConfigName = MySigName.replace( "p0", "" ).replace( "p5", "" )
    
#bkt = configMgr.addFitConfig( "BkgOnly" )
bkt = configMgr.addFitConfig( FitConfigName )

#Not sure what the following lines are doing, but nothing major, will keep them commented

#if useStat:
# bkt.statErrThreshold = None #0.01
#else:
# bkt.statErrThreshold=None

# Adding all samples to bkg-only fit config
bkt.addSamples( AllSamples )


#*****************************************************************************************************************
#****************************************  Adding systematics to the samples  ************************************
#*****************************************************************************************************************

if not noSysts:

    if sysGroup == "-1" or sysGroup == "1" :
        # Experimental syst for non-normalised MC samples
        for sample in MCSamples :  
            for syst in exper_sys :
                bkt.getSample( sample.name ).addSystematic( syst )

    if sysGroup == "-1" or sysGroup == "2" :
        # Experimental syst for normalised MC samples
        for sample in NormSamples :  
            for syst in exper_sys_norm :
                bkt.getSample( sample.name ).addSystematic( syst )

    if sysGroup == "-1" or sysGroup == "3" :
        # Data-driven backgrounds systematics
        if useFakes and doFakeUncert :
            # Fake/non-prompt
            for syst in fake_sys :
                bkt.getSample( Fake_Sample.name ).addSystematic( syst )
            # Charge-flip
            for syst in cf_sys :
                bkt.getSample( CF_Sample.name ).addSystematic( syst )

    if sysGroup == "-1" or sysGroup == "4" :
        # kWWSS systematics
        for syst in WWSS_sys :
            bkt.getSample( "WWSS_EE" ).addSystematic( syst )
            bkt.getSample( "WWSS_EM" ).addSystematic( syst )
            bkt.getSample( "WWSS_MM" ).addSystematic( syst )

#---------------------------------------------------------------------------------------------------------------

# Adding luminosity measuremnet and uncertainty
meas = bkt.addMeasurement( name="NormalMeasurement", lumi=1.0, lumiErr=0.017 )

# setting signal strength and Parameter of Interest (also needed in bkg-only fit)
meas.addPOI( "mu_SIG" )
meas.addParamSetting( "mu_BG", True, 1 ) # FIXME don't know what this does...
        
#---------------------------------------------------------------------------------------------------------------
#LET'S START WITH THE FIT

###################
# Control Regions #
###################

CRWZ_EE = bkt.addChannel( "cuts", ["CRWZ_EE"], NBins, BinLow, BinHigh )
CRWZ_EM = bkt.addChannel( "cuts", ["CRWZ_EM"], NBins, BinLow, BinHigh )
CRWZ_MM = bkt.addChannel( "cuts", ["CRWZ_MM"], NBins, BinLow, BinHigh )

CRWWSS_EE = bkt.addChannel( "cuts", ["CRWWSS_EE"], NBins, BinLow, BinHigh )
CRWWSS_EM = bkt.addChannel( "cuts", ["CRWWSS_EM"], NBins, BinLow, BinHigh )
CRWWSS_MM = bkt.addChannel( "cuts", ["CRWWSS_MM"], NBins, BinLow, BinHigh )

AllCRs = [ CRWZ_EE, CRWZ_EM, CRWZ_MM,
           CRWWSS_EE, CRWWSS_EM, CRWWSS_MM
         ]

######################
# Validation Regions #
######################

VR_WZ_EE = bkt.addChannel( "cuts", ["VRWZ_EE"], NBins, BinLow, BinHigh )
VR_WZ_EM = bkt.addChannel( "cuts", ["VRWZ_EM"], NBins, BinLow, BinHigh )
VR_WZ_MM = bkt.addChannel( "cuts", ["VRWZ_MM"], NBins, BinLow, BinHigh )

VR_WWSS_EE = bkt.addChannel( "cuts", ["VRWWSS_EE"], NBins, BinLow, BinHigh )
VR_WWSS_EM = bkt.addChannel( "cuts", ["VRWWSS_EM"], NBins, BinLow, BinHigh )
VR_WWSS_MM = bkt.addChannel( "cuts", ["VRWWSS_MM"], NBins, BinLow, BinHigh )

VR_Fake_EE = bkt.addChannel( "cuts", ["VRFake_EE"], NBins, BinLow, BinHigh )
VR_Fake_EM = bkt.addChannel( "cuts", ["VRFake_EM"], NBins, BinLow, BinHigh )
VR_Fake_MM = bkt.addChannel( "cuts", ["VRFake_MM"], NBins, BinLow, BinHigh )

VR_CF_EE = bkt.addChannel( "cuts", ["VRCF_EE"], NBins, BinLow, BinHigh )

AllVRs = [ VR_WZ_EE, VR_WZ_EM, VR_WZ_MM,
           VR_WWSS_EE, VR_WWSS_EM, VR_WWSS_MM,
           VR_Fake_EE, VR_Fake_EM, VR_Fake_MM,
           VR_CF_EE
         ]

##################
# Signal Regions #
##################

SR1a_EE = bkt.addChannel( "cuts", ["SR1a_EE"], NBins, BinLow, BinHigh )
SR1a_EE.blind = True #REALLY REALLY REALLY IMPORTANT

SR1a_EM = bkt.addChannel( "cuts", ["SR1a_EM"], NBins, BinLow, BinHigh )
SR1a_EM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1a_MM = bkt.addChannel( "cuts", ["SR1a_MM"], NBins, BinLow, BinHigh )
SR1a_MM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1b_EE = bkt.addChannel( "cuts", ["SR1b_EE"], NBins, BinLow, BinHigh )
SR1b_EE.blind = True #REALLY REALLY REALLY IMPORTANT

SR1b_EM = bkt.addChannel( "cuts", ["SR1b_EM"], NBins, BinLow, BinHigh )
SR1b_EM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1b_MM = bkt.addChannel( "cuts", ["SR1b_MM"], NBins, BinLow, BinHigh )
SR1b_MM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1c_EE = bkt.addChannel( "cuts", ["SR1c_EE"], NBins, BinLow, BinHigh )
SR1c_EE.blind = True #REALLY REALLY REALLY IMPORTANT

SR1c_EM = bkt.addChannel( "cuts", ["SR1c_EM"], NBins, BinLow, BinHigh )
SR1c_EM.blind = True #REALLY REALLY REALLY IMPORTANT

SR1c_MM = bkt.addChannel( "cuts", ["SR1c_MM"], NBins, BinLow, BinHigh )
SR1c_MM.blind = True #REALLY REALLY REALLY IMPORTANT

SR2_EE = bkt.addChannel( "cuts", ["SR2_EE"], NBins, BinLow, BinHigh )
SR2_EE.blind = True #REALLY REALLY REALLY IMPORTANT

SR2_EM = bkt.addChannel( "cuts", ["SR2_EM"], NBins, BinLow, BinHigh )
SR2_EM.blind = True #REALLY REALLY REALLY IMPORTANT

SR2_MM = bkt.addChannel( "cuts", ["SR2_MM"], NBins, BinLow, BinHigh )
SR2_MM.blind = True #REALLY REALLY REALLY IMPORTANT

AllSRs = [ SR1a_EE, SR1a_EM, SR1a_MM,
           SR1b_EE, SR1b_EM, SR1b_MM,
           SR1c_EE, SR1c_EM, SR1c_MM,
           SR2_EE,  SR2_EM,  SR2_MM
         ]

#---------------------------------------------------------------------------------------------------------------

# Setting SRs as Signal Channels in bkg-only fit
bkt.addSignalChannels( AllSRs )

# Setting CRs as bkg constrain Channels in bkg-only fit
bkt.addBkgConstrainChannels( AllCRs )

# Setting VRs as validation Channels, not included in bkg-only fit
bkt.addValidationChannels( AllVRs )


# Defining Signal samples --------------------------------------------------------------------------------------

SigSamples = []

if not fullGrid:
  SigSamples = [ whichPoint ]
  print ' not full grid, selecting one sample'

else:
  SigSamples = [
    'C1N2_Wh_hall_150p0_0p0_2L7',
    'C1N2_Wh_hall_152p5_22p5_2L7',
    'C1N2_Wh_hall_162p5_12p5_2L7',
    'C1N2_Wh_hall_165p0_35p0_2L7',
    'C1N2_Wh_hall_175p0_0p0_2L7',
    'C1N2_Wh_hall_175p0_25p0_2L7',
    'C1N2_Wh_hall_177p5_47p5_2L7',
    'C1N2_Wh_hall_187p5_12p5_2L7',
    'C1N2_Wh_hall_187p5_37p5_2L7',
    'C1N2_Wh_hall_190p0_60p0_2L7',
    'C1N2_Wh_hall_200p0_0p0_2L7',
    'C1N2_Wh_hall_200p0_25p0_2L7',
    'C1N2_Wh_hall_200p0_50p0_2L7',
    'C1N2_Wh_hall_202p5_72p5_2L7',
    'C1N2_Wh_hall_212p5_37p5_2L7',
    'C1N2_Wh_hall_212p5_62p5_2L7',
    'C1N2_Wh_hall_225p0_0p0_2L7',
    'C1N2_Wh_hall_225p0_25p0_2L7',
    'C1N2_Wh_hall_225p0_50p0_2L7',
    'C1N2_Wh_hall_225p0_75p0_2L7',
    'C1N2_Wh_hall_250p0_0p0_2L7',
    'C1N2_Wh_hall_250p0_100p0_2L7',
    'C1N2_Wh_hall_250p0_25p0_2L7',
    'C1N2_Wh_hall_250p0_50p0_2L7',
    'C1N2_Wh_hall_250p0_75p0_2L7',
    'C1N2_Wh_hall_275p0_0p0_2L7',
    'C1N2_Wh_hall_275p0_25p0_2L7',
    'C1N2_Wh_hall_275p0_50p0_2L7',
    'C1N2_Wh_hall_275p0_75p0_2L7',
    'C1N2_Wh_hall_300p0_0p0_2L7',
    'C1N2_Wh_hall_300p0_100p0_2L7',
    'C1N2_Wh_hall_300p0_25p0_2L7',
    'C1N2_Wh_hall_300p0_50p0_2L7',
    'C1N2_Wh_hall_300p0_75p0_2L7',
    'C1N2_Wh_hall_325p0_0p0_2L7',
    'C1N2_Wh_hall_325p0_50p0_2L7',
    'C1N2_Wh_hall_350p0_0p0_2L7',
    'C1N2_Wh_hall_350p0_100p0_2L7',
    'C1N2_Wh_hall_350p0_25p0_2L7',
    'C1N2_Wh_hall_350p0_50p0_2L7',
    'C1N2_Wh_hall_350p0_75p0_2L7',
    'C1N2_Wh_hall_375p0_0p0_2L7',
    'C1N2_Wh_hall_375p0_50p0_2L7',
    'C1N2_Wh_hall_400p0_0p0_2L7',
    'C1N2_Wh_hall_400p0_25p0_2L7',
    'C1N2_Wh_hall_425p0_0p0_2L7'
  ]

print 'testing signal samples'
print SigSamples

# -------------------------------------------------------------------------------------------------------------------
# ------ Defining a model-dependent/exclusion fit for each signal point from a copy of the bkg-only fit config ------
# -------------------------------------------------------------------------------------------------------------------

# do the following only for Exclusion fits
if configMgr.myFitType == configMgr.FitType.Exclusion :

    for sig in SigSamples :

        # Defining exclusion fit by cloning bkt
        #myTopLvl = configMgr.addFitConfigClone( bkt, "%s"%MySigName )

        # Turning off VRs for exclusion fit
        for channel in AllVRs :
            name = channel.name.replace( "cuts_", "" ) + "_cuts"
            #iPop = myTopLvl.validationChannels.index( name )
            #myTopLvl.validationChannels.pop( iPop )
            iPop = bkt.validationChannels.index( name )
            bkt.validationChannels.pop( iPop )

        # new signal name without p0 or p5
        MySigName = sig.replace( "p0", "" ).replace( "p5", "" )

        # Defining signal sample
        SigSample = Sample( MySigName, kBlue )
        SigSample.addInput( sigdir_a + sig + ".root", "HFntuple" )
        SigSample.addInput( sigdir_d + sig + ".root", "HFntuple" )
        SigSample.addInput( sigdir_e + sig + ".root", "HFntuple" )
        SigSample.setNormFactor( 'mu_SIG', 1., 0., 5. ) # setting signal strength normalisation
        SigSample.setNormByTheory()
        SigSample.setStatConfig( useStat )

        # Adding experimental systematics to signal
        if not noSysts:
            for syst in exper_sys :

                # removing non-AF2 systematics for signal
                if 'EG_SCALE_ALL' in syst.name : continue
                if 'MC16' in syst.name : continue

                # pruning unphysically large signal systematics
                if 'PU' in syst.name : continue
                if 'JET_Flavor_Composition' in syst.name : continue
                if 'JET_Flavor_Response' in syst.name : continue
                if 'JET_Pileup_RhoTopology' in syst.name : continue
                if 'JET_EffectiveNP_Modelling1' in syst.name : continue
                if 'JET_EtaIntercalibration_Modelling' in syst.name : continue
                if 'JET_Pileup_OffsetNPV' in syst.name : continue

                # adding remaining systematics
                SigSample.addSystematic( syst )
  
        # Adding signal sample to exclusion fit config
        #myTopLvl.addSamples( SigSample )
        #myTopLvl.setSignalSample( SigSample )
        bkt.addSamples( SigSample )
        bkt.setSignalSample( SigSample )

        # set (again?) SRs as signal channels after adding the signal sample
        bkt.addSignalChannels( AllSRs )

# -------------------------------------------------------------------------------------------------------------------
# These lines are needed for the user analysis to run
# Make sure file is re-made when executing HistFactory

if configMgr.executeHistFactory :
  if os.path.isfile( "data/%s.root"%configMgr.analysisName ) :
    os.remove( "data/%s.root"%configMgr.analysisName )
