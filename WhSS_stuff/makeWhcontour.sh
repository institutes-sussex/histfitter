hadd -f results/EWK_WhSS_Excl_C1N2_Wh_hall_2L7_syst_Output_hypotest.root results/EWK_WhSS_Excl_C1N2_Wh_hall_*_2L7_syst_Output_hypotest.root 
GenerateJSONOutput.py -i results/EWK_WhSS_Excl_C1N2_Wh_hall_2L7_syst_Output_hypotest.root -f "hypo_C1N2_Wh_hall_%f_%f_2L7" -p "NLSP:LSP"
harvestToContours.py -i EWK_WhSS_Excl_C1N2_Wh_hall_2L7_syst_Output_hypotest__1_harvest_list.json -x NLSP -y LSP -d -l "x-125." -o Wh_graphs.root -r -s k5a
python WhSS_plotter.py
