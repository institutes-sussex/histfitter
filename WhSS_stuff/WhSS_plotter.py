#!/usr/bin/env python

import ROOT

import contourPlotter

#ROOT.gROOT.LoadMacro("~/atlasstyle/AtlasStyle.C")
#ROOT.SetAtlasStyle()
#ROOT.gROOT.LoadMacro("~/atlasstyle/AtlasLabels.C")


#drawTheorySysts = True
drawTheorySysts = False

#plot = contourPlotter.contourPlotter("Wh3L_exclusion_FullSyst_2sigma",800,600)
#plot = contourPlotter.contourPlotter("WhSS_exclusion",800,600)
plot = contourPlotter.contourPlotter("WhSS_exclusion",1600,1200)
plot.processLabel = "#tilde{#chi}^{0}_{2}#tilde{#chi}^{#pm}_{1}#rightarrowWh#tilde{#chi}^{0}_{1}#tilde{#chi}^{0}_{1}"
plot.lumiLabel = "#sqrt{s}=13 TeV, 139 fb^{-1}"#, All limits at 95% CL"


## Just open up a root file with TGraphs in it so you can hand them to the functions below!

f = ROOT.TFile("Wh_graphs.root")
fold = ROOT.TFile("MyPlots/Wh_graphs_36fb.root")
f.ls()

## Axes

print "Here1"

plot.drawAxes( [150,0,520,300] )

## Other limits to draw

#plot.drawShadedRegion( externalGraphs.curve, title="ATLAS 8 TeV, 20.3 fb^{-1} (observed)" )

## Main Result

print "Here2"

#plot.drawTextFromTGraph2D(f.Get("CLs_gr"), angle=30, title ="Grey Numbers Represent Observed CLs Value")
plot.drawTextFromTGraph2D(f.Get("CLsexp_gr"), angle=30, title ="Grey Numbers Represent Expected CLs Value")

print "Here3"

#drawing of yellow band
yellow_band = f.Get("SubGraphs/clsd1s_Contour_0")
#yellow_band = f.Get("SubGraphs/clsd1s_Contour_0")
yellow_band.SetPoint(yellow_band.GetN()+1,0,0)
#yellow_band.SetPoint(i,0,0)
#yellow_band1 = f.Get("SubGraphs/clsd1s_Contour_2")
#yellow_band1.SetPoint(yellow_band1.GetN()+1,0,0)

#yellow_band2 = f.Get("SubGraphs/clsd1s_Contour_1")
#yellow_band2.SetPoint(yellow_band2.GetN()+1,0,0)

#plot.drawOneSigmaBand(yellow_band)

#plot.drawOneSigmaBand(yellow_band1)

#plot.drawOneSigmaBand(yellow_band2, legendOrder="")

#plot.drawOneSigmaBand(  f.Get("SubGraphs/clsd1s_Contour_0_Down")   )
#plot.drawOneSigmaBand(      f.Get("Band_2s_0"), color=ROOT.kGreen, alpha=0.4, legendOrder=1       )
#plot.drawOneSigmaBand(      f.Get("Band_1s_0"), alpha=1)
plot.drawOneSigmaBand(      f.Get("Band_1s_0")       )
#plot.drawOneSigmaBand(      f.Get("Band_2s_0"), color=ROOT.kGreen       )
#plot.drawExpected(      f.Get("Exp_0"), alpha=1       )
plot.drawExpected(      fold.Get("Exp_0"),       title="36.1 fb^{-1} Expected Limit", color=ROOT.kBlue, alpha=0.7, legendOrder=2)
#plot.drawExpected(      f5.Get("Exp_0"),       title="5#sigma discovery", color=ROOT.kBlue, alpha=0.7, legendOrder=4)
plot.drawExpected(      f.Get("Exp_0")       )
#plot.drawExpected(      f.Get("Exp_2")       )
plot.drawObserved(      fold.Get("Obs_0"),       title="36.1 fb^{-1} Observed Limit", color=ROOT.kBlue, alpha=0.7, legendOrder=3)
#plot.drawObserved(      f.Get("Obs_0"), title="Observed Limit (#pm1 #sigma_{theory})" if drawTheorySysts else "Observed Limit")
#plot.drawObserved(      f.Get("Obs_1"), title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})", legendOrder=None )
#plot.drawObserved(      f.Get("Obs_1"), title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})", color=ROOT.TColor.GetColor("#800000"), alpha=0.7, legendOrder=None )
#plot.drawObserved(      f.Get("Obs_2"), title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})", color=ROOT.TColor.GetColor("#800000"), alpha=0.7, legendOrder=None )
## Draw Lines

#plot.drawLine(  coordinates = [150,125,350,125], color=ROOT.kBlack, style = 1)

plot.drawLine(  coordinates = [150,25,300,175], color=ROOT.kGray+1, label = "#it{m}(#tilde{#chi}^{0}_{2}) = #it{m}(#tilde{#chi}^{0}_{1}) + #it{m}_{h}", style = 7, labelLocation=[175,60], angle = 47) #angle=41 for 1sigmaonly
#plot.drawLine(  coordinates = [150,20,300,170], label = "m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}) < m(#tilde{#chi}^{0}_{1}) + 130 GeV", style = 7, angle = 40 )

## Axis Labels

plot.setXAxisLabel( "m(#tilde{#chi}^{#pm}_{1}/#tilde{#chi}^{0}_{2}) [GeV]" )
plot.setYAxisLabel( "m(#tilde{#chi}^{0}_{1}) [GeV]"  )
lx1=0.23; ly1=0.65; lx2=0.37; ly2=0.76 #1sigma band
#lx1=0.2; ly1=0.65; lx2=0.37; ly2=0.77

legend1=plot.createLegend(shape=(lx1,ly1,lx2,ly2))
legend1.SetTextSize(.034)
legend1.Draw("same")

if drawTheorySysts:
    #plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up"), alpha=1 )
    plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
    #plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down"), alpha=1 )
    plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
    # coordinate in NDC
    ancho = lx2-lx1
    alto = ly2-ly1
    if drawTheorySysts: plot.drawTheoryLegendLines( xyCoord=(lx1+(0.032*ancho),ly1+(alto*0.09)), length=ancho*.2 )
    #if drawTheorySysts: plot.drawTheoryLegendLines( xyCoord=(lx1+(0.032*ancho),ly1+(alto*0.07)), length=ancho*.2 )

#ROOT.ATLASLabel(0.24,0.85,"internal")	

plot.decorateCanvas( )

#l = ROOT.TLatex()
#l.SetNDC()
#l.SetTextColor( 1 )
#l.SetTextFont(42)
#l.SetTextSize(0.05)
#l.DrawLatex(0.25,0.6, "3l")


plot.writePlot( )
plot.writePlot("png")

##Save output
#f_out = ROOT.TFile("WhSS_plot.root","RECREATE")
#f_out.cd()

#obs_graph = f.Get("Obs_0")
#obs_graph.SetName("Obs_contour")

#yellow_band.SetName("Yellow_band")

#obs_graph.Write()
#yellow_band.Write()
#f_out.Write()
#f_out.Close()
