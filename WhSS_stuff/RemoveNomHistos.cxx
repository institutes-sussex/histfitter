#include <TFile.h>
#include <TH1F.h>
#include <string>
#include <TString.h>
#include <sstream>
#include <fstream>
#include <vector>
#include <TCanvas.h>
#include <TROOT.h>
#include <TLegend.h>
#include <iostream>
#include <iomanip>
#include <TSystem.h>
#include <TKey.h>
#include <algorithm>

using namespace std;


void RemoveNomHistos( string fnom_name, string f_name ) {

	/// getting the nominal list of histos
	TFile* f_nom = TFile::Open( ("data/"+fnom_name).c_str() );
	TIter nextkey_nom( f_nom->GetListOfKeys() );
	vector<string> nom_keys;
  TKey *key;

	while ( key = (TKey*)nextkey_nom() ) {
		string key_name = (string)key->GetName();
		//cout << key_name << endl;
		nom_keys.push_back( key_name );
	}
	delete( f_nom );

	/// creating a copy of the input file
	TString sf_name( f_name );
	sf_name.ReplaceAll( ".root", "_noNom.root" );
	string f_name_out( sf_name );
	gSystem->CopyFile( ("data/"+f_name).c_str(), ("data/"+f_name_out).c_str(), kTRUE );

	/// opening output file and deleting nominal histos
	TFile* f_out = TFile::Open( ("data/"+f_name_out).c_str(), "UPDATE" );
	for ( int i=0 ; i<nom_keys.size() ; i++ ) {
		string objn = nom_keys.at(i);
		objn += ";*";
		f_out->Delete( objn.c_str() );
	}

}


int main( int argc, char *argv[] ) {

  string fnom_name = string( argv[1] );
  string f_name    = string( argv[2] );

	RemoveNomHistos( fnom_name, f_name );
  return 0;
}
